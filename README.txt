# Getting Setup for Development

1. Install Adobe Creative Cloud and Flash Builder Premium (assuming you'll use
   Flash Builder Premium for development)

  - go here: http://www.adobe.com/creativecloud.html
  - sign in (create an Adobe account if you don't already have one)
  - request a free trial or purchase Flash Builder Premium

2. Download Apache Flex 4.13.0

  - http://flex.apache.org/
  - NOTE: The "Apache Flex SDK Installer" installs Flex/AIR/Flash Player
  - Use the default path for installing the Apache Flex Installer
  - I put the Apache Flex Library under /Applications/Adobe Flash Builder 4.7/sdks/<version>
  - note: it can be helpful to list the flex sdk version + flash and air versions in the
    folder names, e.g., "Apache Flex 4.13.0 FP 15 AIR 15 en_US"
  - NOTE: There may be a bug with the installer or Adobe when trying to install the option components "Adobe Embedded Font Libraries and Utilities" and "Remoting Support". It brings up a dialog asking for a username and password. Just hitting Cancel gets you through it (but without the optional components).

3. Make sure git or subversion is installed

  - I recommend homebrew (http://brew.sh/) to install for Mac
  - For linux, use your operating system's package manager
  - For Windows, install your version control manually or as part of a client

4. Start Adobe Flash Builder 4.7 and add license key (or via the free trial)

5. Add the new Flex SDK

  - go to Flash Builder > Preferences
  - go to Flash Builder > Installed Flex SDKs
  - click "Add"
  - Browse to the place you put the Flex SDK in step 4

6. Quit Adobe Flash Builder 4.7

7. Check out the repository.

  - The URL of the Subversion repository is: svn.bigladder.com/elements/application/trunk
  - The Git repository is: https://bitbucket.org/bigladdersoftware/elements.git

8. Copy the certificate file to the main directory

  - the file "elements-certificate.p12" comes from outside of the code repository
  - For Big Ladder personnel, it is currently housed in the "Elements Builds" folder on Google Drive
  - the password is in a document on Google Drive called "Elements Security Certificate Password"
  - for non-Big Ladder personnel, read up on how to create your own security
    certificate here: http://help.adobe.com/en_US/air/build/WS144092a96ffef7cc4c0afd1212601c9a36f-8000.html
  - Note: you only need a certification if you want to create a signed
    executable application

9. Start Adobe Flash Builder 4.7 and choose File > Import Flash Builder Project

  - select "Project folder" and browse to the folder where you checked out the
    source code in step 7

10. Set the CERTIFICATE linked resources variable.

  Create a variable called CERTIFICATE that points to elements-certificate.p12

  Do this by:

  ## Windows

  1. Go to Windows>Preferences>General>Workspace>Linked Resources
  2. Add a new variable called "CERTIFICATE" that points to elements-certificate.p12

  ## Mac OS X

  1. Go to Flash Builder > Preferences > General > Workspace > Linked Resources
  2. Add a new variable called "CERTIFICATE" that points to elements-certificate.p12

11. To build a release version, choose Project > Export Release Build... Choose "Signed application with captive runtime". You should be prompted for the Certificate password on the next page. If not (and on Windows), try resizing the dialog box to be a larger and the password field should appear.

  Be sure to update the version number in the Elements-app.xml prior to any new version release!

  ## Mac OS X

  The target (i.e., path for "Export to folder") for Mac OS X should be "meta/dmg-installer/source". Base filename should be "Elements". Export as: "Signed application with captive runtime".

  Go to "meta/dmg-installer" with a command shell and run the `make_dmg.sh` file after updating any variables inside the file for the version numbering etc. you want:

      ./make_dmg.sh

  This should create a dmg file in meta/dmg-installer, something like `elements-<version number>.dmg`

  ## Windows

  See the README in meta/win-installer for further information.
