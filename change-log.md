# Elements 1.0.6 -- Released November 29, 2016

* Fixed bug where EPW files were not saving and restoring changes to Cloud
  Cover. Cloud Cover is now aliased with Total Sky Cover in EPW files.
* Elements now remembers the last directory you opened or saved from and
  defaults to that directory when you next open or save.
* Add sum of current solar insolation to the normalization box for solar
* Add new Global Solar scaling method: Beam Fraction of Total. This method
  holds the relative fractions of beam and diffuse solar constant while
  scaling.
* Fix issue with incredibly poor performance on applying selection tools
  (Scale, Offset, Normalize) to solar (especially Global Solar)
* Fix issue where after a tool was applied to a selection (offset, scale,
  normalize, normalize by month), the selection would remain but the tool-bar
  buttons would grey out. Now, after applying a tool, both the buttons and the
  selection stay active.
* Fix issue where Elements would crash if data parsable as a number was
  entered into a header field
* Fix bug where Elements would freeze if negative solar data was entered when
  cos(Z) is < 0.0. Elements no longer depends on the beam normal component of
  solar under those circumstances.
* Fix issue with the internal time series metadata for solar elevation angle
  getting confused with solar zenith angle. This would cause Elements to
  crash if a user attempted to click on values from this column
* Fix issue with Elements hanging upon edit of Wind Direction column
* Fix issue with diffuse horizontal irradiance calculator at sunrise/sunset
  where the calculated irradiance could be much larger than it is supposed
  to be when a strong beam normal component is present and the average cozine
  of the zenith angle over the hour is negative. This functionality is only
  used on import of DOE2 style weather files.

# Elements 1.0.5 -- Released October 8, 2015

* Fix bug in average cosine of zenith angle function which caused strange
  behavior during the last 10 hours of the year
* The above bug fix may have caused issues with users who were experiencing
  cut/paste issues to/from a spreadsheet program when attempting to paste
  for an entire year
* Add defensive code to protect Element's internal data from bad code on import
  of solar variables: negative values of solar insolation or solar insolation
  values that are too high
* Fix chart bug where chart times were being reported in UTC (Greenwich Mean
  Time) instead of the local time zone

# May 12, 2015

* Add solar elevation angle (a.k.a., solar altitude angle) as a viewable metric
* Fix bug related to correctly calculating the cosine of the zenith angle for
  East longitudes
* Update README with newer build instructions for both Mac and Windows
* Update comments in Solar file to make it easier to find referenced equations
* Correct incorrect calculations of zenith angle and solar hour angle
* Correct solar azimuthal angle from south to be consistent with Duffie and
  Beckman 2013. Solar Engineering of Thermal Processes.
* Simplify calculation of the average cosine of the zenith angle
* Add functionality for Elements to derive illumination data from solar data on
  export to EPW format

# January 7, 2015

* Upgrade from Apache Flex SDK 4.11.0 to 4.13.0
* Upgrade to Flash Player 15 and AIR 15
* Change copyright notice years to be correct for 2015
* Fix error in copy/paste between Elements documents
* Fix error in copy/paste from Elements to Excel
