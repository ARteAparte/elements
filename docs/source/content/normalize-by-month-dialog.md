% Normalize By Month Dialog - User Guide | Elements

# Normalize By Month Dialog

The Normalize By Month Dialog is opened by clicking the `Normalize By Month` button in the [Document Window](document-window.html).

![Normalize By Month Dialog for Averages](images/normalize-by-month-dialog-average.png)

The Normalize By Month Dialog works slightly different depending on the variable involved but in all cases, the goal is to change the monthly average of a variable to match some external data. When the Normalize By Month Dialog opens, you will be presented with all of the data from your weather file summarized by month. To use the Normalize By Month Dialog to scale the averages for a given month, first select the target variable that you want to scale. For certain variables, the `Variables to Hold Constant` drop-down list will be enabled.

Next, choose which scaling method you would like to use: "Scale by Average", "Scale by Minimum/Maximum", or "Scale by Sum". Not all methods are available for all variables.

"Scale by Average" allows you to set new average monthly values for the variable.

"Scale by Minimum/Maximum" allows you to set new minimum and maximum monthly values for the variable.

"Scale by Sum" is available for solar variables which allows you to set the summed monthly value of solar irradiation.

The table at the bottom of the Normalize By Month Dialog shows the current averages, minimums/maximums, or sums (depending on the variable selected). Double click on any cell in the New columns and type in the new value.

When you have set your desired monthly averages, minimums/maximums, or sums, click `OK` to apply the scaling factors to the data selection. Click `Cancel` to close the dialog without scaling.
