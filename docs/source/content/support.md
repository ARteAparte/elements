% Support - User Guide | Elements

# Support

You can get support for *Elements* by sending an email to <elements@bigladdersoftware.com>. Bug reports, feature suggestions, complaints, and compliments are all welcome!
