% Acknowledgments - User Guide | Elements

# Acknowledgments

*Elements* was developed by <a href="http://bigladdersoftware.com" target="_blank">Big Ladder Software</a> in collaboration with <a href="http://www.rmi.org/">Rocky Mountain Institute</a>. This software was partially funded by <a href="http://www.rmi.org/">Rocky Mountain Institute</a>, a Colorado nonprofit 501(c)(3) corporation, as part of their <a href="http://www.rmi.org/ModelingTools" target="_blank">library of free and open-source tools</a> that enable cost effective, low energy buildings.

<a href="http://bigladdersoftware.com/" target="_blank"><img alt="Big Ladder Logo" border="0" src="media/bigladderlogo.png" height="70"></a> &nbsp; &nbsp; &nbsp; <a href="http://www.rmi.org/" target="_blank"><img alt="RMI Logo" border="0" src="media/rmilogo.png" height="70"></a>
