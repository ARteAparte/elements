% Elements File Format - User Guide | Elements

# Elements File Format

The native *Elements* file format (\*.elements) uses the <a href="http://en.wikipedia.org/wiki/Zip_%28file_format%29" target="_blank">Zip file format</a> to store two compressed files: "header.csv" and "timeseries.csv". The \*.csv extension indicates these files are in comma-separated value (CSV) format. The CSV format was chosen for the data container to allow interoperability between *Elements* and spreadsheet programs such as *Excel* which can easily read and write CSV files.

If you would like to work with an *Elements* \*.elements file directly, we recommend the following workflow:

1. Copy your *.elements file.
2. Rename the extension of the copied file to .zip. Note that you may need to configure your operating system to show extensions.
3. Double click the file to decompress or "unzip" it. Depending on your system, either two files or a folder will appear.
4. Open the "header.csv" and/or "timeseries.csv" using your spreadsheet program and edit as desired.

If you would like to turn existing CSV data into an \*.elements file format, we recommend the following workflow:

1. Create two new CSV documents: one called "header.csv" and the other called "timeseries.csv".
2. Edit the CSV documents with your spreadsheet program.
3. Close and save the documents.
4. Compress the two files to a Zip archive. For example, on Mac select the two files and select `File/Compress 2 Items` from the Finder menu. This will create a *.zip file.
5. Rename the Zip file to have an ".elements" extension.
