% User Guide | Elements

# Introduction

*Elements* is a free, open-source, cross-platform software tool for creating and editing custom weather files for building energy modeling. The goal of the project is to develop a comprehensive, integrated application suitable for handling all of the common tasks associated with weather files.

*Elements* was developed by <a href="http://bigladdersoftware.com" target="_blank">Big Ladder Software</a> with the generous funding and collaboration of <a href="http://www.rmi.org" target="_blank">Rocky Mountain Institute</a>.

<a href="http://bigladdersoftware.com/" target="_blank"><img alt="Big Ladder Logo" border="0" src="media/bigladderlogo.png" height="70"></a> &nbsp; &nbsp; &nbsp; <a href="http://www.rmi.org/" target="_blank"><img alt="RMI Logo" border="0" src="media/rmilogo.png" height="70"></a>

## Features

Major features include capabilities to:

- Read/write common weather file formats
    - *DOE-2* (.bin, .fmt)
    - *EnergyPlus* (.epw)
- Browse/edit weather data
    - Smart spreadsheet editor
    - Automatic preservation of psychrometric and solar relationships
    - Tools for bulk data transformation: offset, scale, normalize
    - Copy and paste to/from *Excel*
    - File header editor
    - IP or SI units
- Visualize weather data
    - Dynamic, multi-variable line chart (experimental)
