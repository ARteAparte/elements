% Edit Header Data - User Guide | Elements

# Edit Header Data

To edit header data, use the following procedure:

1. Start *Elements*.
2. Click the `New Weather File` button, or open an existing file with `Open Weather File...`.
3. Note the current Latitude and Longitude.
4. Click the `Header` button.
5. Scroll to the Latitude row and click in the value column. Change the value to "39.7" degrees and hit `Enter`.
6. Scroll to the Longitude row and click in the value column. Change the value to "105.0" degrees and hit `Enter`.
7. Scroll to the City (Site Location) row and click in the value column. Change the value to "Denver, CO" and hit `Enter`.
8. Click the `Close` button.
9. Note that the Site Name, Latitude, and Longitude values have been updated back in the [Document Window](document-window.html).
