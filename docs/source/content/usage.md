% Usage - User Guide | Elements

# Usage

When you launch *Elements*, you are presented with a simple Welcome Window with two buttons: `Open Weather File...` and `New Weather File`.

![Welcome Window](images/welcome-window.png)

Most of the time you will click `Open Weather File...` and then select an existing weather file from your hard drive. The selected weather file will open in a new [Document Window](document-window.html) for viewing and editing. The supported formats are:

- DOE-2 (.bin): Binary format for the *DOE-2* engine
- DOE-2 (.fmt): Text format for the *DOE-2* engine
- EnergyPlus (.epw): Text format for the *EnergyPlus* engine
- Elements (.elements): Native [Elements file format](elements-file-format.html).

On occasion, you may want to [create a weather file from scratch](create-from-scratch.html). Click `New Weather File` to open a new [Document Window](document-window.html) populated with some default data.

> At any time you can access the *User Guide* documentation (what you are reading now) by clicking `Help/User Guide`.

> At any time you can exit the program by clicking `File/Quit` on Windows or `Elements/Quit Elements` on Mac.
