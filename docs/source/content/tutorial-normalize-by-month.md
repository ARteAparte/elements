% Normalize By Month - User Guide | Elements

# Normalize By Month

The `Normalize By Month` tool is useful when you have an existing weather file that you would like to calibrate to match some known monthly aggregate values, e.g., average monthly dry-bulb temperature.

We will use `Normalize By Month` on a new default weather file to get a sense for how it works.

1. Start *Elements*.
2. Click on `New Weather File`.
3. Select IP Units.
4. Click anywhere under the Dry Bulb Temperature column.
5. Click the `Normalize By Month` button and the [Normalize by Month Dialog](normalize-by-month-dialog.html) will appear.
6. Check that `Variables to Hold Constant` is set to "Relative Humidity, Atmospheric Pressure".
7. Select "Scale by Average".
8. Under New Average, type "65" for January and hit `Enter`.
9. Click the `OK` button.
10. You should now see all of the January dry-bulb temperatures are equal to 65 F.
11. Set the temperature value of the first dry bulb temperature (2013/01/01 @ 00:00:00) to 50 F.
12. Set the temperature value of the second dry bulb temperature (2013/01/01 @ 01:00:00) to 70 F.
13. Select a cell under Dry Bulb Temperature.
14. Click the `Normalize By Month` button.
15. For Scaling Method, choose "Scale by Minimum/Maximum".
16. Set New Max to 60 F for January.
17. Click the `OK` button.
18. Note that the values for all temperatures that were previously 65 F (3/4 of the range between 50 and 70) have been rescaled to 57.5 F (which is again 3/4 of the range between 50 and 60).
19. Click anywhere in the Diffuse Solar column. Note that during daylight hours, diffuse solar has a value of 89.53 BTU/ft2.
20. Click the `Normalize By Month` button.
21. Check that `Variables to Hold Constant` is set to "Normal Solar".
22. Change the New Sum value for January to roughly half the current value (13,850 BTU/ft2).
23. Click the `OK` button.
24. Note that the value of Diffuse Solar is now 37.29 BTU/ft2 or about half of its previous value.
