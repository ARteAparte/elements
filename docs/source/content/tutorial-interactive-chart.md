% Interactive Chart - User Guide | Elements

# Interactive Chart

In this tutorial, you will plot a few variables and zoom in to a specific month using the [Chart Window](chart-window.html).

1. Start *Elements*.
2. Click the `New Weather File` button, or open an existing file with `Open Weather File...`.
3. Select SI Units.
4. Click the `Chart` button.
5. The chart defaults to show the Dry Bulb Temperature data line over the year.
6. Click `Add` or `Remove` to add or remove variables to/from the chart.
7. Drag the two dark gray rectangles on the thumbnail chart to bracket the desired date range.
8. Click in between the two rectangles and, with the mouse button held down, begin sliding from left to right. Return the dragging box to be positioned roughly around the desired date range and let the mouse button go.
9. Note that the main chart window resizes to zoom in on the desired date range.
10. Hover over the data line for one of the variables. Note the values for the variable and the corresponding date/time.
