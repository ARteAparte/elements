% License - User Guide | Elements

# License

*Elements* is open-source software distributed under the <a href="http://www.gnu.org/copyleft/gpl.html" target="_blank">GNU General Public License Version 3</a>. You can modify and redistribute the software under the terms of the license.

The source code can be downloaded from the *Elements* project page at <a href="http://bigladdersoftware.com/projects/elements/" target="_blank">http://bigladdersoftware.com/projects/elements/</a>
