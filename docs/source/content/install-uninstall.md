% Install/Uninstall - User Guide | Elements

# Install/Uninstall

## Installing the Program

If you do not already have an installer program for *Elements*, you can download one for your platform from the *Elements* project page at <a href="http://bigladdersoftware.com/projects/elements/" target="_blank">http://bigladdersoftware.com/projects/elements/</a>

### Windows

Double click the *Elements* installer for Windows (elements-#.#.#.exe) and follow the interactive prompts to install the program.

After *Elements* is successfully installed you may delete the installer.

### Mac

Double click the *Elements* installer for Mac (elements-#.#.#.dmg) to mount the disk image. A new window should open with a prompt to drag Elements.app to the Applications directory. (If a window does not open, you should open the mounted disk image and manually copy Elements.app to your Applications directory.)

If a previous version of Elements.app is already installed, click on `Replace` when prompted to replace the previous version with the new one.

After *Elements* is successfully installed you may unmount the disk image and delete the installer.

## Uninstalling the Program

### Windows

You can uninstall *Elements* in the normal way using the Control Panel.

### Mac

You can uninstall *Elements* by opening your Applications directory and deleting Elements.app.
