% Background - User Guide | Elements

# Background

Building energy simulation requires site-specific weather data in order to perform accurate energy-consumption assessments. Various weather data sources exist; however, it is often the case that building energy modelers must confirm the integrity of that data and adjust the weather file for various purposes such using a nearby site's weather data for a site with no weather data. Each energy modeler handles this task differently, resulting in a re-implementation of various techniques in multiple *Excel* spreadsheets, script files, etc.

*Elements* aims to consolidate the tasks required to browse and process weather data files into one convenient tool. *Elements* helps you to visualize and manipulate weather data loaded from various file formats including the ability to transform weather data from one format to another.

Annual weather data is a critical input for building energy modeling; all building models require weather as a fundamental driving factor. Usually before the energy modeler begins to construct the building model she will seek out an appropriate weather file for the proposed building location. The task of obtaining the correct weather file is often easy. There are thousands of weather data files currently available covering many world cities. Many of these files can be downloaded for free from the internet. There are now also online services that will sell you a weather file for a reasonable fee.

Yet there still remain cases for which the available weather file sources are not sufficient.

- The proposed location might not have a corresponding weather file (at least for free)
- The weather file is not available in the right format for the simulation engine in use
- The weather file must be adjusted for local microclimate variations at the site
- A custom weather file must be created based on measured meteorological data.

In these cases the task of obtaining an appropriate weather file can be difficult and time consuming.

Even when a weather file is readily available for the proposed site, it is important to be able to analyze the weather data in some detail. An energy modeler should give the weather file a sanity check to make sure all required inputs are present and believable. An energy modeler might also be interested in analyzing the weather file to characterize the climate and identify possible design options (e.g., evaporative cooling in dryer climates, etc.).

To address these advanced weather tasks, energy modelers around the world have undoubtedly solved the same problems a countless number of times. Admittedly the tasks are not too technically demanding and can be accomplished with just about any spreadsheet and chart program. But the redundancy of these tasks, the time wasted, and the opportunities for error that they introduce, begs the question: Why isn't there an easy-to-use weather application available to solve all of these problems for the building energy community? While bits and pieces of the solution are available in a variety of programs, there is no comprehensive, integrated software tool suitable for solving all of the common weather tasks associated with building energy modeling.

*Elements* aims to address the majority of common weather tasks faced by building energy modelers in one integrated, cross-platform software application. With our initial release of the program, we are taking a first step toward this goal. With future development we will continue to add more features and capabilities that bring us closer and closer to a comprehensive tool.
