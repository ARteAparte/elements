% Create From Scratch - User Guide | Elements

# Create From Scratch

In this tutorial, you will start from a new default "scratch" weather file document and create a weather file for use with *EnergyPlus*.

1. Obtain a TMY3 Weather file for Chicago O'Hare International Airport (station number 725300) from <a href="http://rredc.nrel.gov/solar/old_data/nsrdb/1991-2005/tmy3/by_state_and_city.html#I" target="_blank">http://rredc.nrel.gov/solar/old_data/nsrdb/1991-2005/tmy3/by_state_and_city.html#I</a>. Open the file (which is a CSV file) with *Excel* or another spreadsheet program.
2. Start *Elements*.
3. Click the `New Weather File` button.
4. Select SI Units.
5. Click the `Header` button and change the following in the [Header Dialog](header-dialog.html):
    * City (Site Location) to "Chicago"
    * Country to "USA"
    * Elevation (m) to "182"
    * Latitude (degrees) to "41.88"
    * Longitude (degrees) to "87.63"
    * Data source to "TMY3 for Chicago O'Hare"
    * State/Province/Region to "Illinois"
    * Time Zone to "-6"
    * World Meteorological Organization station number to "72530".
6. With the TMY3 file in *Excel*, find "Dry-bulb (C)". Select and copy the entire column from January 1 to December 31.
7. In *Elements* place your cursor on the first cell of the Dry Bulb Temperature column. Ensure that `Variables to Hold Constant` is set to "Relative Humidity, Atmospheric Pressure". Paste the values into the column.
8. Return to the TMY3 file and find "RHum (%)" which is the relative humidity. Select and copy the entire column from January 1 to December 31.
9. In *Elements* place your cursor on the first cell of the Relative Humidity column. Ensure that `Variables to Hold Constant` is set to "Dry Bulb Temperature, Atmospheric Pressure". Paste the values into the column.
10. Return to the TMY3 file and find "Pressure (mbar)". Create a new column to the right of the pressure column and title it "Pressure (kPa)". Define each cell in "Pressure (kPa)" to be the corresponding entry under "Pressure (mbar)" times 0.1. Ensure "Pressure (kPa)" is populated from January 1 to December 31. Select and copy the entire column from January 1 to December 31.
11. In *Elements* place your cursor on the first cell of the Atmospheric Pressure column. Paste the values into the column.
12. Return to the TMY3 file and find "DNI (W/m^2)" which stands for "Direct Normal Irradiance". Select and copy the entire column from January 1 to December 31.
13. In *Elements* place your cursor below the Normal Solar column. Note: Don't worry that DNI is in W/m^2 and Elements is expecting Wh/m2 as the integration time is one hour so the values are the same. Ensure that `Variables to Hold Constant` is set to "Diffuse Solar". Paste the values into the column.
14. Similarly, return to the TMY3 file and find "DHI (W/m^2)" which stands for "Diffuse Horizontal Irradiance". Select and copy the entire column from January 1 to December 31.
15. In *Elements* place your cursor on the first cell below the Diffuse Solar column. Ensure that `Variables to Hold Constant` is set to "Normal Solar". Paste the values into the column.
16. Return to the TMY3 file and find "Wspd (m/s)" which is the wind speed. Select and copy the entire column from January 1 to December 31.
17. In *Elements* place your cursor on the first cell below the Wind Speed column. Paste the values into the column.
18. Close the TMY3 file.
19. In *Elements* click on `File/Save As.../EnergyPlus (.epw)` and save the file as "chicago.epw".
