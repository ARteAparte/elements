/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package gui{
import flash.desktop.NativeApplication;
import flash.display.NativeWindow;
import flash.display.Screen;
import flash.events.Event;
import flash.events.IEventDispatcher;
import flash.geom.Rectangle;

import spark.components.Window;

import elements.AppEvents;
import elements.AppHandlers;
import elements.AppState;

import pdt.IPVec;

public class WindowManager{
  public static function update(topic:String):void{
    switch (topic) {
      case AppEvents.EVENT_ACTIVE_WINDOW_CHANGED:
        activateActiveWindow();
        break;
      case AppEvents.EVENT_OPEN_WINDOWS_CHANGED:
        exitIfAllClosedAndNoPersistentMenu();
      default:
        break;
    }
  }
  // activateActiveWindow :: () -> IO ()
  // Once the active window has changed, it must be activated to ensure that
  // it will get focus. If this is not carried out, the window will not rise
  // to the top when, for example, the menu is changed.
  public static function activateActiveWindow():void{
    //trace('WindowManager.activateActiveWindow');
    var appState:AppState = AppHandlers.readAppState(AppHandlers.instance());
    var w:Window = appState.activeWindow as Window;
    if (w != null) {
      //trace('WindowManager.activateActiveWindow :: activating ', w.title);
      w.activate();
    }
  }
  // exitIfAllClosedAndNoPersistentMenu :: () -> IO ()
  // If last window closed and no persistent menu then exit!
  public static function exitIfAllClosedAndNoPersistentMenu():void{
    //trace('WindowManager.exitIfAllClosedAndNoPersistentMenu');
    if (!NativeApplication.supportsMenu) {
      var appState:AppState = AppHandlers.readAppState(AppHandlers.instance());
      var numWs:int = appState.numberOfOpenWindows;
      if (numWs == 0) {
        NativeApplication.nativeApplication.dispatchEvent(
          new Event(Event.EXITING, false, true));
      }
    }
  }
  public static function handleExiting(event:Event):void {
    //trace('WindowManager.handleExiting');
    var canceled:Boolean = false;
    var appSC:IManagedRef = AppHandlers.instance();
    var appState:AppState = AppHandlers.readAppState(appSC);
    var openWindows:IPVec = appState.openWindows;
    var msg:String =
      "WindowedApplication.handleExiting: default prevented";
    for each (var window:IEventDispatcher in openWindows) {
      var subEvent:Event = new Event(Event.EXITING, false, true);
      window.dispatchEvent(subEvent);
      if (subEvent.isDefaultPrevented()) {
        //trace(msg);
        canceled = true;
        break;
      }
    }
    if (!canceled) {
      //trace("Shutting down the app");
      NativeApplication.nativeApplication.exit();
    }
  }
  public static function centerWindowOnScreen(win:NativeWindow):void {
    var initialBounds:Rectangle = new Rectangle(
      (Screen.mainScreen.bounds.width / 2 - (win.width/2)),
      (Screen.mainScreen.bounds.height / 2 - (win.height/2)),
      win.width, win.height);
    win.bounds = initialBounds;
    win.visible = true;
  }
  public static function getWindowByTitle(title:String):NativeWindow {
    var allWindows:Array = NativeApplication.nativeApplication.openedWindows;
    for each (var win:NativeWindow in allWindows) {
      if (win.title == title) return win;
    }
    return null;
  }
}
}