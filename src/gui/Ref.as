/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package gui {
import observer.SubscriberManager;

public class Ref extends SubscriberManager implements IRef {
    private var _state:*;

    public function Ref(state:*) {
        super();
        _state = state;
    }

    public function deref():* {
        return _state;
    }

    public function read():* {
        // this is for read-only manipulation of state
        // since we don't have persistent data structures, we just hope and
        // pray...
        return _state;
    }

    public function setRef(newState:*):void {
        _state = newState;
    }
}
}