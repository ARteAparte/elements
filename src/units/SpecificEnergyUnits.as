/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package units {
public class SpecificEnergyUnits {
    public static const KILOJOULES_PER_KILOGRAM:String = "kJ/kg";
    public static const KJ__KG:String = KILOJOULES_PER_KILOGRAM;
    public static const BTU_PER_POUND_MASS:String = "BTU/lbm";
    public static const BTU__LBM:String = BTU_PER_POUND_MASS;

    public static const QUANTITY_OF_MEASURE:String = 'specific energy';
    public static const SI:String = KILOJOULES_PER_KILOGRAM;
    public static const IP:String = BTU_PER_POUND_MASS;

    public static const AVAILABLE_UNITS:Array = [KJ__KG, BTU__LBM];
}
}