/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package units {
public class AngleUnits {
    public static const RADIANS:String = 'radians';
    public static const DEGREES:String = 'degrees';
    public static const COMPASS_DIRECTION:String = "compass directions";
    public static const COMPASS_DIRECTION_NORTH:Number = 0;
    public static const COMPASS_DIRECTION_NORTH_NORTH_EAST:Number = 1;
    public static const COMPASS_DIRECTION_NORTH_EAST:Number = 2;
    public static const COMPASS_DIRECTION_EAST_NORTH_EAST:Number = 3;
    public static const COMPASS_DIRECTION_EAST:Number = 4;
    public static const COMPASS_DIRECTION_EAST_SOUTH_EAST:Number = 5;
    public static const COMPASS_DIRECTION_SOUTH_EAST:Number = 6;
    public static const COMPASS_DIRECTION_SOUTH_SOUTH_EAST:Number = 7;
    public static const COMPASS_DIRECTION_SOUTH:Number = 8;
    public static const COMPASS_DIRECTION_SOUTH_SOUTH_WEST:Number = 9;
    public static const COMPASS_DIRECTION_SOUTH_WEST:Number = 10;
    public static const COMPASS_DIRECTION_WEST_SOUTH_WEST:Number = 11;
    public static const COMPASS_DIRECTION_WEST:Number = 12;
    public static const COMPASS_DIRECTION_WEST_NORTH_WEST:Number = 13;
    public static const COMPASS_DIRECTION_NORTH_WEST:Number = 14;
    public static const COMPASS_DIRECTION_NORTH_NORTH_WEST:Number = 15;

    public static const QUANTITY_OF_MEASURE:String = 'angle (from North)';
    public static const SI:String = DEGREES;
    public static const IP:String = DEGREES;

    public static const AVAILABLE_UNITS:Array = [
      DEGREES, COMPASS_DIRECTION];
}
}