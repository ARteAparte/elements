/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package units {
public class SpeedUnits {
    public static const METERS_PER_SECOND:String = "m/s";
    public static const M__S:String = METERS_PER_SECOND;
    public static const MILES_PER_HOUR:String = "mph";
    public static const MPH:String = MILES_PER_HOUR;
    public static const KNOTS:String = "nautical mph";

    public static const QUANTITY_OF_MEASURE:String = 'speed';
    public static const SI:String = M__S;
    public static const IP:String = MPH;

    public static const AVAILABLE_UNITS:Array = [M__S, MPH, KNOTS];
}
}