/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package units {
public class TemperatureUnits {
    public static const CELSIUS:String = "C";
    public static const CENTEGRADE:String = CELSIUS;
    public static const C:String = CELSIUS;
    public static const FAHRENHEIT:String = "F";
    public static const F:String = FAHRENHEIT;
    public static const KELVIN:String = "K";
    public static const K:String = KELVIN;
    public static const RANKINE:String = "R";
    public static const R:String = RANKINE;

    public static const QUANTITY_OF_MEASURE:String = "temperature";
    public static const SI:String = C;
    public static const IP:String = F;

    public static const AVAILABLE_UNITS:Array = [C, F, K, R];
}
}