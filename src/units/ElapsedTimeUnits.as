/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package units {
public class ElapsedTimeUnits {
    public static const WEEKS:String = "Weeks";
    public static const DAYS:String = "Days";
    public static const HOURS:String = "Hours";
    public static const MINUTES:String = "Minutes";
    public static const SECONDS:String = "Seconds";

    public static const SI:String = SECONDS;
    public static const IP:String = HOURS;

    public static const AVAILABLE_UNITS:Array = [
        WEEKS, DAYS, HOURS, MINUTES, SECONDS];
}
}