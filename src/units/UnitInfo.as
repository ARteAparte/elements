/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package units {
import dse.MetaData;

import pdt.IPMap;
import pdt.PersistentArrayMap;

import utils.ArrayUtils;
import utils.MapUtils;

public class UnitInfo {
    public static const KNOWN_UNITS:Array = [
        AngleUnits,
        DensityUnits,
        ElapsedTimeUnits,
        IrradianceUnits,
        IrradiationUnits,
        LengthUnits,
        NoUnits,
        PressureUnits,
        ScaleUnits,
        SpecificEnergyUnits,
        SpecificVolumeUnits,
        SpeedUnits,
        TemperatureUnits];

    public static function
    listPlugins():String {
        var out:Array = [];
        for each (var item:Object in KNOWN_UNITS) {
            out.push(item.QUANTITY_OF_MEASURE);
        }
        return out.join(", ");
    }

    private static var UNIT_SYSTEM:Object = new Object();

    /* Returns a Map of QUANTITY_OF_MEASURE to SI unit */
    public static function
    getUnitSystem(systemName:String):Object {
        if (UNIT_SYSTEM.hasOwnProperty(systemName)) {
            return UNIT_SYSTEM[systemName];
        }
        var system:Object = new Object();
        for each (var item:Object in KNOWN_UNITS) {
            if (item.hasOwnProperty(systemName) &&
                item.hasOwnProperty('QUANTITY_OF_MEASURE')) {
                system = MapUtils.merge(system,
                    MapUtils.makeObject([
                        item['QUANTITY_OF_MEASURE'],
                        item[systemName]]));
            } else {
                // warning?
            }
        }
        return system;
    }

    public static var UNITS_BY_QOM:Object = null;

    public static function getUnitsByQOM():Object {
      if (UNITS_BY_QOM == null) {
        UNITS_BY_QOM = {};
        for each (var unitModule:Object in KNOWN_UNITS) {
          UNITS_BY_QOM[unitModule.QUANTITY_OF_MEASURE] = unitModule;
        }
      }
      return UNITS_BY_QOM;
    }

    public static function unitsBySystemAndQOM(unitSys:String,
                                               qom:String):String {
      var unitsByQOM:Object = getUnitsByQOM();
      if (unitsByQOM.hasOwnProperty(qom)) {
        return unitsByQOM[qom][unitSys];
      }
      return '';
    }

    // TODO: Need to include headerMetaData when developed
    public static function
    makeDefaultDisplayUnits(seriesMetaMap:Object):IPMap {
        var displayUnits:Object = new Object();
        for each (var name:String in MapUtils.keys(seriesMetaMap)) {
            var sm:MetaData = seriesMetaMap[name] as MetaData;
            displayUnits[name] = sm.baseUnits;
        }
        displayUnits[UnitSystems.UNIT_SYSTEM] = UnitSystems.CUSTOM;
        return PersistentArrayMap.createFromObj(displayUnits);
    }

    public static function
    makeDisplayUnitsForUnitSystem(systemName:String, metaMap:Object):IPMap {
        var knownUnitSystem:Boolean = ArrayUtils.contains(
            UnitSystems.UNIT_SYSTEMS, systemName);
        if (!knownUnitSystem) {
            return makeDisplayUnitsForUnitSystem(UnitSystems.SI, metaMap);
        }
        var displayUnits:Object = new Object();
        var sys:Object = getUnitSystem(systemName);
        for each (var name:String in MapUtils.keys(metaMap)) {
            var qtyOfMeasure:String =
                (metaMap[name] as MetaData).quantityOfMeasure;
            displayUnits[name] = sys[qtyOfMeasure];
        }
        displayUnits[UnitSystems.UNIT_SYSTEM] = systemName;
        return PersistentArrayMap.createFromObj(displayUnits);
    }
}
}