/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package units {
public class PressureUnits {
    public static const KILOPASCAL:String = "kPa";
    public static const KPA:String = KILOPASCAL;
    public static const PASCAL:String = "Pa";
    public static const PA:String = PASCAL;
    public static const ATMOSPHERE:String = "atm";
    public static const ATM:String = ATMOSPHERE;
    public static const BAR:String = "bar";
    public static const TORR:String = "torr";
    public static const POUNDS_PER_SQUARE_INCH:String = "psi";
    public static const PSI:String = POUNDS_PER_SQUARE_INCH;
    public static const INCHES_OF_MERCURY:String = "in Hg";
    public static const IN_HG:String = INCHES_OF_MERCURY;
    public static const INCHES_OF_MERCURY_TIMES_100:String = "in Hg x 100";
    public static const IN_HG_x_100:String = INCHES_OF_MERCURY_TIMES_100;

    public static const QUANTITY_OF_MEASURE:String = 'pressure';
    public static const SI:String = KPA;
    public static const IP:String = ATM;

    public static const AVAILABLE_UNITS:Array = [
        KPA, PA, ATM, BAR, TORR, PSI, IN_HG, IN_HG_x_100];
}
}