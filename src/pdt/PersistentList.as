/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package pdt {
import flash.events.Event;

import mx.collections.IList;
import mx.utils.ObjectUtil;

import utils.ArrayUtils;

public class PersistentList implements IPersistentList, ICounted,
                                       IPersistentStack, IList,
                                       ISeq {
    public static const ERROR_MUTATION_ATTEMPTED:String =
        "Attempt to mutate a persistent list";
    public static const EMPTY:IPersistentList = new EmptyList();

    private var _first:Object;
    private var _rest:IPersistentList;
    private var _count:int;

    public static function create(xs:Array):PersistentList {
        if (xs.length == 0) return null;
        var lst:PersistentList = null;
        var ys:Array = ArrayUtils.copyArray(xs);
        ys.reverse();
        for each (var y:Object in ys) {
            if (lst == null) {
                lst = new PersistentList(y, EMPTY);
            } else {
                lst = lst.cons(y) as PersistentList;
            }
        }
        return lst;
    }

    public function PersistentList(first:Object, rest:IPersistentList=null) {
        _first = first;
        if (rest == null) {
            _count = 1;
            _rest = EMPTY;
        } else {
            _count = rest.count + 1;
            _rest = rest;
        }
    }

    public function get first():Object {
        return _first;
    }

    public function get rest():IPersistentList {
        return _rest;
    }

    public function get count():int {
        return _count;
    }

    public function peek():Object {
        return _first;
    }

    public function pop():IPersistentStack {
        return _rest;
    }

    public function get length():int {
        return _count;
    }

    public function
    addEventListener(type:String, listener:Function,
                     useCapture:Boolean = false, priority:int = 0,
                     useWeakReference:Boolean = false):void {
        // do nothing
    }

    public function addItem(item:Object):void {
        throw new Error(ERROR_MUTATION_ATTEMPTED);
    }

    public function addItemAt(item:Object, index:int):void {
        throw new Error(ERROR_MUTATION_ATTEMPTED);
    }

    public function dispatchEvent(event:Event):Boolean {
        return false;
    }

    public function getItemAt(index:int, prefetch:int = 0):Object {
        if (index == 0) return _first as Object;
        if (index >= _count) return null;
        if (index < 0) return null;
        var idx:int = index;
        var lst:IPersistentList = this;
        while (idx > 0) {
            lst = lst.rest;
            idx--;
        }
        return lst.first as Object;
    }

    public function getItemIndex(item:Object):int {
        var lst:IPersistentList = this;
        for (var idx:int = 0; idx < _count; idx++) {
            if (ObjectUtil.compare(item, lst.first) == 0) {
                return idx;
            }
            lst = lst.rest;
        }
        return -1;
    }

    public function hasEventListener(type:String):Boolean {
        return false;
    }

    public function itemUpdated(item:Object, property:Object = null,
                                oldValue:Object = null,
                                newValue:Object = null):void {
        // do nothing
    }

    public function removeAll():void {
        throw new Error(ERROR_MUTATION_ATTEMPTED);
    }

    public function removeEventListener(type:String, listener:Function,
                                        useCapture:Boolean = false):void {
        // do nothing
    }

    public function removeItemAt(index:int):Object {
        throw new Error(ERROR_MUTATION_ATTEMPTED);
    }

    public function setItemAt(item:Object, index:int):Object {
        throw new Error(ERROR_MUTATION_ATTEMPTED);
    }

    public function toArray():Array {
        var xs:Array = new Array(_count);
        var lst:IPersistentList = this;
        for (var i:int = 0; i < _count; i++) {
            xs.push(lst.first);
            lst = lst.rest;
        }
        return xs;
    }

    public function willTrigger(type:String):Boolean {
        return false;
    }

    public function empty():IPersistentCollection {
        return EMPTY as IPersistentCollection;
    }

    public function seq():ISeq {
        return this as ISeq;
    }

    public function cons(x:Object):ISeq {
        var lst:ISeq =
            (new PersistentList(x, this as IPersistentList)) as ISeq;
        return lst;
    }

    public function equiv(other:Object):Boolean {
        return false;
    }

    public function next():ISeq {
        if (_count == 1) return null;
        return _rest.seq();
    }
}
}