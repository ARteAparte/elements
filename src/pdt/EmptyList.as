/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package pdt {
import flash.events.Event;
import flash.utils.getQualifiedClassName;

import mx.collections.IList;

public class EmptyList implements IPersistentList, IList, ISeq, ICounted {
    public function EmptyList() {
    }

    public function peek():Object {
        return null;
    }

    public function pop():IPersistentStack {
        return null;
    }

    public function cons(x:Object):ISeq {
        return new PersistentList(x, this);
    }

    public function empty():IPersistentCollection {
        return this;
    }

    public function equiv(other:Object):Boolean {
        var cls:Function = flash.utils.getQualifiedClassName;
        if (cls(other) == cls(this)) {
            return true;
        }
        return false;
    }

    public function get count():int {
        return 0;
    }

    public function seq():ISeq {
        return null;
    }

    public function get length():int {
        return 0;
    }

    public function addItem(item:Object):void {
        throw new Error(PersistentList.ERROR_MUTATION_ATTEMPTED);
    }

    public function addItemAt(item:Object, index:int):void {
        throw new Error(PersistentList.ERROR_MUTATION_ATTEMPTED);
    }

    public function getItemAt(index:int, prefetch:int=0):Object {
        return null;
    }

    public function getItemIndex(item:Object):int {
        return -1;
    }

    public function
    itemUpdated(item:Object, property:Object=null, oldValue:Object=null,
                newValue:Object=null):void {
    }

    public function removeAll():void {
        throw new Error(PersistentList.ERROR_MUTATION_ATTEMPTED);
    }

    public function removeItemAt(index:int):Object {
        throw new Error(PersistentList.ERROR_MUTATION_ATTEMPTED);
    }

    public function setItemAt(item:Object, index:int):Object {
        throw new Error(PersistentList.ERROR_MUTATION_ATTEMPTED);
    }

    public function toArray():Array {
        return [];
    }

    public function
    addEventListener(type:String, listener:Function, useCapture:Boolean=false,
                     priority:int=0, useWeakReference:Boolean=false):void {
    }

    public function removeEventListener(type:String, listener:Function,
                                        useCapture:Boolean=false):void {
    }

    public function dispatchEvent(event:Event):Boolean {
        return false;
    }

    public function hasEventListener(type:String):Boolean {
        return false;
    }

    public function willTrigger(type:String):Boolean {
        return false;
    }

    public function get first():Object {
        return null;
    }

    public function get rest():IPersistentList {
        return null;
    }

    public function next():ISeq {
        return null;
    }

    public function more():ISeq {
        return null;
    }
}
}