/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package pdt {
import mx.utils.ObjectUtil;

import utils.ArrayUtils;
import utils.MapUtils;

public class PersistentArrayMap implements IPMap {
    public static const EMPTY:PersistentArrayMap = new PersistentArrayMap();
    public static const ERROR_KEY_ALREADY_PRESENT:String =
        "Key already present.";

    private var _keyVals:Array;

    public static function createFromObj(obj:Object):PersistentArrayMap {
        return new PersistentArrayMap(MapUtils.toKeyValArray(obj));
    }

    public function PersistentArrayMap(keyVals:Array=null) {
        if (keyVals == null) {
            _keyVals = [];
        } else {
            if (!_keysUnique(keyVals)) throw new Error("keys must be unique");
            _keyVals = keyVals;
        }
    }

    public static function _keysUnique(keyVals:Array):Boolean {
        for (var i:int = 0; i < keyVals.length; i += 2) {
            for (var j:int = i+2; j<keyVals.length; j += 2) {
                if (keyVals[i] == keyVals[j]) {
                    return false;
                }
            }
        }
        return true;
    }

    public function get count():int {
        return _keyVals.length / 2;
    }

    public function containsKey(key:Object):Boolean {
        return _indexOf(key) >= 0;
    }

    public function _indexOf(key:Object):int {
        for (var i:int = 0; i < _keyVals.length; i += 2) {
            if (key == _keyVals[i]) {
                return i;
            }
        }
        return -1;
    }

    public function valAt(key:Object, notFound:Object=null):Object {
        var i:int = _indexOf(key as String);
        if (i >= 0) return _keyVals[i + 1];
        return notFound;
    }

    // exclusive assoc
    public function assocEx(key:Object, val:Object):IPMap {
        var i:int = _indexOf(key);
        var newKeyVals:Array;
        if (i >= 0) {
            throw new Error(ERROR_KEY_ALREADY_PRESENT);
        } else {
            newKeyVals = _extendKeyVals(_keyVals, key as String, val);
        }
        return new PersistentArrayMap(newKeyVals);
    }

    // non-exclusive assoc
    public function assoc(key:Object, val:Object):IPMap {
        var i:int = _indexOf(key);
        var newKeyVals:Array;
        // replace the value at the current key
        if (i >= 0) {
            // if the object to replace is the same as what's there, no op
            if (ObjectUtil.compare(_keyVals[i], val) == 0) return this;
            newKeyVals = ArrayUtils.copyArray(_keyVals);
            newKeyVals[i + 1] = val;
        // add the new key / val to existing data-structure
        } else {
            newKeyVals = _extendKeyVals(_keyVals, key as String, val);
        }
        return new PersistentArrayMap(newKeyVals);
    }

    public static function
    _extendKeyVals(keyVals:Array, key:String, val:Object):Array {
        var newKeyVals:Array = new Array(keyVals.length + 2);
        if (keyVals.length > 0) {
            ArrayUtils.copyIntoArray(
                keyVals, 0, newKeyVals, 2, keyVals.length);
        }
        newKeyVals[0] = key;
        newKeyVals[1] = val;
        return newKeyVals;
    }

    public function without(key:Object):IPMap {
        var i:int = _indexOf(key);
        // if we have the key, make a new structure without that entry...
        if (i >= 0) {
            var newLength:int = _keyVals.length - 2;
            if (newLength == 0) return EMPTY as IPMap;
            var newKeyVals:Array = new Array(newLength);
            var srcIdx:int = 0;
            var destIdx:int = 0;
            for (; srcIdx < _keyVals.length; srcIdx += 2) {
                if (_keyVals[srcIdx] != key) {
                    newKeyVals[destIdx] = _keyVals[srcIdx];
                    newKeyVals[destIdx + 1] = _keyVals[srcIdx + 1];
                    destIdx += 2;
                }
            }
            return new PersistentArrayMap(newKeyVals);
        }
        // map is already without this key so just return
        return this;
    }

    // entryAt :: returns an entry object for the given Key.
    // an entry object represents the key/value pair
    public function entryAt(key:Object):IMapEntry {
        var i:int = _indexOf(key);
        if (i >= 0) return new MapEntry(_keyVals[i], _keyVals[i + 1]);
        return null;
    }

    public function toString():String {
        var str:String = MapUtils.stringRepresentation(toObject());
        return "PersistentArrayMap.createFromObj(" + str + ")";
    }

    public function get keys():Array {
        var out:Array = new Array(_keyVals.length / 2);
        var j:int = 0;
        for (var i:int=0; i < _keyVals.length; i+=2, j++) {
            out[j] = _keyVals[i];
        }
        return out;
    }

    public function get vals():Array {
        var out:Array = new Array(_keyVals.length / 2);
        var j:int = 0;
        for (var i:int=1; i < _keyVals.length; i+=2, j++) {
            out[j] = _keyVals[i];
        }
        return out;
    }

    public function toObject():Object {
        return MapUtils.makeObject(_keyVals);
    }

    public function getIn(keys:Array):Object {
        var m:Object = this;
        for each (var key:Object in keys) {
            m = (m as ILookup).valAt(key) as Object;
        }
        return m;
    }

    public function assocIn(tags:Array, val:Object):IPMap {
        var keys_:Array = ArrayUtils.copyArray(tags);
        var parents:Array = new Array();
        var tmp:IAssociative = this;
        if (tags.length == 0) {
            return this;
        } else if (tags.length == 1) {
            return assoc(tags[0], val);
        }
        for each (var key:Object in tags) {
            parents.push(tmp);
            tmp = tmp.valAt(key) as IAssociative;
        }
        var lastParent:IAssociative = parents.pop();
        var lastTag:Object = keys_.pop();
        lastParent = lastParent.assoc_(lastTag, val);
        for each (key in keys_.reverse()) {
            tmp = parents.pop() as IAssociative;
            tmp = tmp.assoc_(key, lastParent);
            lastParent = tmp;
        }
        return lastParent as IPMap;
    }

    public function assoc_(key:Object, val:Object):IAssociative {
        return assoc(key, val) as IAssociative;
    }

    public function empty():IPersistentCollection {
        return PersistentArrayMap.EMPTY;
    }

    public function seq():ISeq {
        var xs:Array = new Array();
        for each (var key:Object in this.keys) {
            xs.push(this.entryAt(key));
        }
        return PersistentVector.create(xs).seq();
    }

    public function equiv(other:Object):Boolean {
        return false;
    }
}
}