/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package pdt {
import flash.events.Event;

import mx.collections.IList;
import mx.utils.ObjectUtil;

import utils.ArrayUtils;

public class PersistentVector implements IPVec {
    public static const EMPTY_NODE:Node = new Node(new Array(32));
    public static const EMPTY:PersistentVector =
        new PersistentVector(0, 5, EMPTY_NODE, []);
    // private by convention
    public var _cnt:int;
    public var _shift:int;
    public var _root:Node;
    public var _tail:Array;

    public static function create(xs:Array):PersistentVector {
        var ret:TransientVector = EMPTY.asTransient().conjAll(xs);
        return ret.persistent() as PersistentVector;
    }

    public function
    PersistentVector(cnt:int, shift:int, root:Node, tail:Array) {
        _cnt = cnt;
        _shift = shift;
        _root = root;
        _tail = tail;
    }

    public function asTransient():TransientVector {
        return TransientVector.fromPersistentVector(this);
    }

    public function get length():int {
        return this.count;
    }

    public function assocN(i:int, val:Object):IPVec {
        if (i >= 0 && i < _cnt) {
            if (i >= _tailoff()) {
                var newTail:Array = new Array(_tail.length);
                ArrayUtils.copyIntoArray(_tail, 0, newTail, 0, _tail.length);
                newTail[i & 0x01f] = val;
                return new PersistentVector(_cnt, _shift, _root, newTail);
            }
            return new PersistentVector(
                _cnt, _shift, _doAssoc(_shift, _root, i, val), _tail);
        }
        if (i == _cnt) return cons(val);
        throw new Error(TransientVector.ERROR_INDEX_OUT_OF_BOUNDS);
    }

    public function _arrayFor(i:int):Array {
        if (i >= 0 && i < _cnt) {
            if (i >= _tailoff()) return _tail;
            var node:Node = _root;
            for (var level:int = _shift; level > 0; level -= 5) {
                node = node.array[(i >>> level) & 0x01f] as Node;
            }
            return node.array;
        }
        throw new Error(TransientVector.ERROR_INDEX_OUT_OF_BOUNDS);
    }

    public static function
    _doAssoc(level:int, node:Node, i:int, val:Object):Node {
        var ret:Node = new Node(ArrayUtils.copyArray(node.array));
        if (level == 0) {
            ret.array[i & 0x01f] = val;
        } else {
            var subidx:int = (i >>> level) & 0x01f;
            ret.array[subidx] = _doAssoc(
                level - 5, node.array[subidx] as Node, i, val);
        }
        return ret;
    }

    public function assoc_(key:Object, val:Object):IAssociative {
        if (key is int)
            return assocN(int(key), val) as IAssociative;
        throw new Error(TransientVector.ERROR_KEY_MUST_BE_AN_INTEGER);
    }

    public function _tailoff():int {
        if (_cnt < 32) return 0;
        return ((_cnt - 1) >>> 5) << 5;
    }
    /**
     * Construct a new persistent vector from the current vector and the new
     * object.
     * 
     * @param val The value to 'add' (or "cons") onto the existing vector
     * 
     * @return A new persistent vector with the new value included.
     */
    public function cons(val:Object):IPVec {
        var i:int = _cnt;
        // room in tail?
        if ((_cnt - _tailoff()) < 32) {
            var newTail:Array = new Array(_tail.length + 1);
            ArrayUtils.copyIntoArray(_tail, 0, newTail, 0, _tail.length);
            newTail[_tail.length] = val;
            return new PersistentVector(_cnt + 1, _shift, _root, newTail);
        }
        var newRoot:Node;
        var tailNode:Node = new Node(_tail);
        var newShift:int = _shift;
        // overflow root?
        if ((_cnt >>> 5) > (1 << _shift)) {
            newRoot = new Node();
            newRoot.array[0] = _root;
            newRoot.array[1] = _newPath(_shift, tailNode);
            newShift += 5;
        } else {
            newRoot = _pushTail(_shift, _root, tailNode);
        }
        return new PersistentVector(_cnt + 1, newShift, newRoot, [val]);
    }

    public function _pushTail(level:int, parent:Node, tailNode:Node):Node {
        /* if parent is leaf, insert node,
        else does it map to an existing child?
            -> noteToInsert = pushNode one more level
        else alloc new path
        return nodeToInsert placed in copy of parent */
        var subidx:int = ((_cnt - 1) >>> level) & 0x01f;
        var ret:Node = new Node(ArrayUtils.copyArray(parent.array));
        var nodeToInsert:Node;
        if (level == 5) {
            nodeToInsert = tailNode;
        } else {
            var child:Node = parent.array[subidx] as Node;
            nodeToInsert = (child != null) ?
                _pushTail(level - 5, child, tailNode):
                _newPath(level - 5, tailNode);
        }
        ret.array[subidx] = nodeToInsert;
        return ret;
    }

    public static function _newPath(level:int, node:Node):Node {
        if (level == 0) return node;
        var ret:Node = new Node();
        ret.array[0] = _newPath(level - 5, node);
        return ret;
    }

    public function peek():Object {
        if (_cnt > 0) return nth(this.count - 1);
        return null;
    }

    public function pop():IPersistentStack {
        if (_cnt == 0)
            throw new Error(TransientVector.ERROR_CANT_POP_EMPTY_VECTOR);
        if (_cnt == 1)
            return EMPTY;
        if (_cnt - _tailoff() > 1) {
            var newTail:Array = new Array(_tail.length - 1);
            ArrayUtils.copyIntoArray(_tail, 0, newTail, 0, newTail.length);
            return new PersistentVector(_cnt - 1, _shift, _root, newTail);
        }
        newTail = _arrayFor(_cnt - 2);
        var newRoot:Node = _popTail(_shift, _root);
        var newShift:int = _shift;
        if (newRoot == null) {
            newRoot = EMPTY_NODE;
        }
        if (_shift > 5 && newRoot.array[1] == null) {
            newRoot = newRoot.array[0] as Node;
            newShift -= 5;
        }
        return new PersistentVector(_cnt - 1, newShift, newRoot, newTail);
    }

    public function _popTail(level:int, node:Node):Node {
        var subidx:int = ((_cnt - 2) >>> level) & 0x01f;
        if (level > 5) {
            var newChild:Node = _popTail(
                level - 5, node.array[subidx] as Node);
            if (newChild == null && subidx == 0)
                return null;
            else {
                var ret:Node = new Node(ArrayUtils.copyArray(node.array));
                ret.array[subidx] = newChild;
                return ret;
            }
        } else if (subidx == 0) {
            return null;
        }
        ret = new Node(ArrayUtils.copyArray(node.array));
        ret.array[subidx] = null;
        return ret;
    }

    public function empty():IPersistentCollection {
        return EMPTY as IPersistentCollection;
    }

    public function equiv(other:Object):Boolean {
        return _doEquiv(this, other);
    }

    public static function _doEquiv(v:IPVec, obj:Object):Boolean {
        if (v == obj) return true;
        if (obj is IList) {
            var lst:IList = obj as IList;
            if (lst.length != v.count) return false;
            for (var idx:int = 0; idx < lst.length; idx++) {
                if (ObjectUtil.compare(lst.getItemAt(idx), v.nth(idx)) != 0)
                    return false;
            }
        } else if (obj is Array) {
            var xs:Array = obj as Array;
            if (xs.length != v.count) return false;
            for (idx = 0; idx < xs.length; idx++) {
                if (ObjectUtil.compare(xs[idx], v.nth(idx)) != 0)
                    return false;
            }
        }
        return true;
    }

    public function get count():int {
        return _cnt;
    }

    public function seq():ISeq {
        return PersistentList.create(toArray()) as ISeq;
    }

    public function rseq():ISeq {
        return null;
    }

    public function nth(i:int, notFound:Object=null):Object {
        if (notFound == null) {
            var node:Array = _arrayFor(i);
            return node[i & 0x01f];
        }
        if (i >= 0 && i < _cnt) return nth(i);
        return notFound;
    }

    public function addItem(item:Object):void {
        throw new Error(PersistentList.ERROR_MUTATION_ATTEMPTED);
    }

    public function addItemAt(item:Object, index:int):void {
        throw new Error(PersistentList.ERROR_MUTATION_ATTEMPTED);
    }

    public function getItemAt(index:int, prefetch:int=0):Object {
        return nth(index);
    }

    public function getItemIndex(item:Object):int {
        for (var i:int=0; i<this.count; i++) {
            if (ObjectUtil.compare(nth(i), item) == 0) {
                return i;
            }
        }
        return -1;
    }

    public function
    itemUpdated(item:Object, property:Object=null, oldValue:Object=null,
                newValue:Object=null):void {
    }

    public function removeAll():void {
        throw new Error(PersistentList.ERROR_MUTATION_ATTEMPTED);
    }

    public function removeItemAt(index:int):Object {
        throw new Error(PersistentList.ERROR_MUTATION_ATTEMPTED);
    }

    public function setItemAt(item:Object, index:int):Object {
        throw new Error(PersistentList.ERROR_MUTATION_ATTEMPTED);
    }

    public function toArray():Array {
        var xs:Array = new Array(_cnt);
        var ys:Array;
        var i:int;
        var numCopied:int = 0;
        for (i=0; i < _cnt; i+=32) {
            ys = _arrayFor(i);
            ArrayUtils.copyIntoArray(ys, 0, xs, i, 32);
            numCopied += 32;
        }
        for (i=numCopied; i < _cnt; i++) {
            xs[i] = nth(i);
        }
        return xs;
    }

    public function
    addEventListener(type:String, listener:Function,
                     useCapture:Boolean=false, priority:int=0,
                     useWeakReference:Boolean=false):void {
        // do nothing
    }

    public function removeEventListener(type:String, listener:Function,
                                        useCapture:Boolean=false):void {
        // do nothing
    }

    public function dispatchEvent(event:Event):Boolean {
        return false;
    }

    public function hasEventListener(type:String):Boolean {
        return false;
    }

    public function willTrigger(type:String):Boolean {
        return false;
    }

    public function valAt(key:Object, notFound:Object=null):Object {
        if (key is int) {
            var idx:int = key as int;
            if ((idx < 0) || (idx >= _cnt)) return notFound;
            return getItemAt(idx);
        }
        throw new Error(TransientVector.ERROR_KEY_MUST_BE_AN_INTEGER);
    }

    public function entryAt(key:Object):IMapEntry {
        if (key is int) {
            var idx:int = key as int;
            return new MapEntry(idx, nth(idx));
        }
        throw new Error(TransientVector.ERROR_KEY_MUST_BE_AN_INTEGER);
    }

    public function containsKey(key:Object):Boolean {
        for (var i:int=0; i < _cnt; i++) {
            if (ObjectUtil.compare(key, nth(i)) == 0) {
                return true;
            }
        }
        return false;
    }

    public function toString():String {
        var xs:Array = toArray();
        return "PersistentVector.create([" + xs.join(", ") + "])";
    }
}
}