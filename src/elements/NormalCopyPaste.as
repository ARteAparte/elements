/*
Copyright (C) 2014-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements
{
import flash.desktop.Clipboard;
import flash.desktop.ClipboardFormats;
import flash.desktop.NativeApplication;

public class NormalCopyPaste implements ICopyPaste
{
  public function NormalCopyPaste()
  {
  }
  public function doCopy():void
  {
    // trace('NormalCopyPaste :: doCopy');
    NativeApplication.nativeApplication.copy();
    var debug:Boolean = false;
    if (debug)
    {
      var cb:Clipboard = Clipboard.generalClipboard;
      var contents:String =
        cb.getData(ClipboardFormats.TEXT_FORMAT) as String;
      trace('NormalCopyPaste.doCopy :: clipboard contents after copy:',
        contents);
    }
  }
  public function doPaste():void
  {
    // trace('NormalCopyPaste :: doPaste');
    NativeApplication.nativeApplication.paste();
  }
}
}