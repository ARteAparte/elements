/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements {
import mx.core.IFactory;

import spark.components.gridClasses.GridColumn;

import dse.DataSet;
import dse.MetaData;

import elements.helpers.IGetPrecision;
import elements.helpers.IGetScaling;
import elements.helpers.IGetUnits;

import gui.IManagedRef;

import pdt.IPMap;
import pdt.PersistentArrayMap;

import plugins.Conditions;
import plugins.Psych;
import plugins.Solar;
import plugins.Wind;

import units.UnitConversions;

import utils.FormatUtils;
import utils.MapUtils;

public class DataDisplayOptions {
  public static const DEFAULT_PRECISION:int = 2;
  public static const DEFAULT_SCALE:Number = 1.0;
  public static const DEFAULT_COLOR:uint = 0x000000;

  public static function
    DEFAULT_NUMERIC_ITEM_EDITOR_MAKER(_docSC:IManagedRef):IFactory {
    return new GridItemEditorWithUnitsFactory(_docSC) as IFactory;
  }

  public static function
    DEFAULT_STRING_ITEM_EDITOR_MAKER(_docSC:IManagedRef):IFactory {
    return new GridStringItemEditorFactory(_docSC) as IFactory;
  }

  public static function
    DEFAULT_ITEM_RENDERER_MAKER(docSC:IManagedRef):IFactory {
    return new SeriesDataRendererFactory(docSC);
  }

  public static function
    MAKE_LABEL_FUNCTION_FOR_NUMBERS(_docSC:IManagedRef):Function {
    return function(dataPoint:Object, column:GridColumn):String {
      var name:String = column.dataField;
      var docState:DocState = DocHandlers.readDocState(_docSC);
      var dataSet:dse.DataSet = docState.dataSet;
      var sm:MetaData = dataSet.metaDataFor(name);
      var hdr:Object = dataSet.header;
      var ddo:DataDisplayOptions = docState.displayOptionsFor(name);
      var valInBaseUnits:Number = sm.getter(hdr, dataPoint) as Number;
      var displayUnitsMap:IPMap = docState.displayUnits;
      var displayUnits:String;
      if (displayUnitsMap.containsKey(name)) {
        displayUnits = displayUnitsMap.valAt(name) as String;
      } else {
        displayUnits = sm.baseUnits;
      }
      var displayVal:Number = UnitConversions.baseToDisplay(
        valInBaseUnits, sm.baseUnits, ddo.scale, displayUnits);
      var precision:int = ddo.precision;
      if(isNaN(displayVal)) {
        return "";
      }
      return FormatUtils.numberToFormattedString(displayVal, precision);
    };
  }

  /*
  data in the item is the ONLY data and it's guaranteed that the
  column dataField is present in item.

  however, we need to use external meta data for the metric corresponding
  to this table to set units, scale, and precision as mentioned below. */
  public static function
    MAKE_LABEL_FUNCTION_FOR_TABLE(metricName:String,
                                  unitsGetter:IGetUnits,
                                  precisionGetter:IGetPrecision,
                                  scalingGetter:IGetScaling):Function {
    return function(item:Object, column:GridColumn):String {
      var ucf:Function =
        unitsGetter.getToDisplayUnitConversionFunctionFor(metricName);
      //MapUtils.traceObject(item, 'item');
      var baseVal:Number = Number(item[column.dataField]);
      var val:Number = ucf(baseVal);
      //trace('column.dataField', column.dataField);
      //trace('baseVal', baseVal);
      //trace('val', val);
      var precision:int = precisionGetter.getPrecisionFor(metricName);
      var scale:Number = scalingGetter.getScalingFor(metricName);
      return FormatUtils.formatFloat(val * scale, precision);
    };
  }

  public static function
    MAKE_LABEL_FUNCTION_FOR_STRING(_docSC:IManagedRef):Function {
    return function(dataPoint:Object, column:GridColumn):String {
      var name:String = column.dataField;
      var docState:DocState = DocHandlers.readDocState(_docSC);
      var dataSet:dse.DataSet = docState.dataSet;
      var sm:MetaData = dataSet.metaDataFor(name);
      var hdr:Object = dataSet.header;
      return sm.getter(hdr, dataPoint) as String;
    };
  }
  [Embed(source="assets/icons/drybulb.png")]
  private static const dryBulbIcon:Class;
  [Embed(source="assets/icons/wetbulb.png")]
  private static const wetBulbIcon:Class;
  [Embed(source="assets/icons/dewpoint.png")]
  private static const dewPointIcon:Class;
  [Embed(source="assets/icons/relhum.png")]
  private static const relHumIcon:Class;
  [Embed(source="assets/icons/humrat.png")]
  private static const humRatIcon:Class;
  [Embed(source="assets/icons/globalsolar.png")]
  private static const globalSolarIcon:Class;
  [Embed(source="assets/icons/normalsolar.png")]
  private static const normalSolarIcon:Class;
  [Embed(source="assets/icons/diffusesolar.png")]
  private static const diffuseSolarIcon:Class;

  // the color to display the variable in. 0x000000 is black
  public var color:uint;
  // precision to display the variable at (if it's a numeric variable)
  public var precision:int;
  // class of the Icon to use
  public var icon:Class;
  /* The following function must have the following signature:
  IManagedRef -> Function
  The function returned must have signature:
  item:Object x column:GridColumn -> String
  Item is the data for the row being rendered, column is the column for the
  cell being edited. */
  public var makeLabelFunction:Function;
  /* The following function must have signature:
  IManagedRef -> IFactory
  For the item and header renderers, the IFactory must return something
  that conforms to the IGridItemRenderer interface. For the itemEditor, it
  must conform to the IGridItemEditor interface. */
  public var itemEditorMaker:Function;
  public var scale:Number;

  public function DataDisplayOptions() {
    color = DEFAULT_COLOR;
    precision = DEFAULT_PRECISION;
    icon = null;
    makeLabelFunction = null;
    itemEditorMaker = null;
    scale = DEFAULT_SCALE;
  }

  public static function
    makeDefaultDisplayOptions(seriesMeta:Object):IPMap {
    var iconClasses:Object = MapUtils.makeObject([
      Psych.DRY_BULB_TEMP_C, dryBulbIcon,
      Psych.WET_BULB_TEMP_C, wetBulbIcon,
      Psych.DEW_POINT_TEMP_C, dewPointIcon,
      Psych.RELATIVE_HUMIDITY, relHumIcon,
      Psych.HUMIDITY_RATIO, humRatIcon,
      Solar.GLOBAL_HORIZ_Wh__m2, globalSolarIcon,
      Solar.BEAM_NORMAL_Wh__m2, normalSolarIcon,
      Solar.DIFFUSE_HORIZONTAL_Wh__m2, diffuseSolarIcon]);
    var varColors:Object = MapUtils.makeObject([
      Psych.DRY_BULB_TEMP_C, 0x669900,
      Psych.WET_BULB_TEMP_C, 0x00CCFF,
      Psych.DEW_POINT_TEMP_C, 0x3366FF,
      Psych.PRESSURE_kPa, 0x993399,
      Psych.DENSITY_kg__m3, 0x996600,
      Psych.RELATIVE_HUMIDITY, 0x3333FF,
      Psych.HUMIDITY_RATIO, 0x9933FF,
      Psych.ENTHALPY_kJ__kg, 0x660099,
      Solar.GLOBAL_HORIZ_Wh__m2, 0xFF0000,
      Solar.BEAM_NORMAL_Wh__m2, 0xFFFF00,
      Solar.DIFFUSE_HORIZONTAL_Wh__m2, 0xFF6600,
      Wind.SPEED_m__s, 0x33FFCC,
      Wind.DIRECTION_deg, 0x33CC99,
      Conditions.OPAQUE_SKY_COVER, 0xCCCCCC]);
    var dispOpts:Object = new Object();
    for each (var name:String in MapUtils.keys(seriesMeta)) {
      var ddo:DataDisplayOptions = new DataDisplayOptions();
      var sm:MetaData = seriesMeta[name] as MetaData;
      ddo.precision = DEFAULT_PRECISION;
      if (iconClasses.hasOwnProperty(name)){
        ddo.icon = iconClasses[name];
      }
      if (varColors.hasOwnProperty(name)){
        ddo.color = varColors[name];
      }
      if (sm.dataType == MetaData.DATA_TYPE__NUMBER) {
        ddo.makeLabelFunction = MAKE_LABEL_FUNCTION_FOR_NUMBERS;
        ddo.itemEditorMaker = DEFAULT_NUMERIC_ITEM_EDITOR_MAKER;
        ddo.scale = sm.defaultScale;
      } else {
        ddo.makeLabelFunction = MAKE_LABEL_FUNCTION_FOR_STRING;
        ddo.itemEditorMaker = DEFAULT_STRING_ITEM_EDITOR_MAKER;
        ddo.scale = DEFAULT_SCALE;
      }
      dispOpts[name] = ddo;
    }
    return PersistentArrayMap.createFromObj(dispOpts);
  }
}
}