/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements {
  import elements.views.BaseWindow;

public class WindowOptions {
  private var _height:int;
  private var _minHeight:int;
  private var _width:int;
  private var _minWidth:int;
  private var _resizable:Boolean;
  private var _minimizable:Boolean;
  private var _maximizable:Boolean;
  private var _title:String;
  private var _titlePrefix:String;
  private var _usePlainTitle:Boolean;
  private var _parentWindow:BaseWindow;
  private var _replace:Object;
  private var _showScrollBars:Boolean;
  private var _onCloseFn:Function;
  private var _copyPaste:ICopyPaste;
  public function WindowOptions(wob:WindowOptionsBuilder) {
    if (wob.height >= -1) {
      this._height = wob.height;
    } else {
      wob.height = -1;
    }
    if (wob.width >= -1) {
      this._width = wob.width;
    } else {
      this._width = -1;
    }
    if (wob.minHeight >= -1) {
      this._minHeight = wob.minHeight;
    } else {
      this._minHeight = -1;
    }
    if (wob.minWidth >= -1) {
      this._minWidth = wob.minWidth;
    } else {
      this._minWidth = -1;
    }
    this._resizable = wob.resizable;
    this._minimizable = wob.minimizable;
    this._maximizable = wob.maximizable;
    if (wob.title == null) {
      this._title = "";
    } else {
      this._title = wob.title;
    }
    if (wob.titlePrefix == null) {
      this._titlePrefix = "";
    } else {
      this._titlePrefix = wob.titlePrefix;
    }
    this._usePlainTitle = wob.usePlainTitle;
    var parentWindow:BaseWindow = wob.parentWindow;
    if (parentWindow != null) {
      // ensure heirarchy only goes one level deep
      var cnt:int = 0;
      while (parentWindow._parentWindow != null) {
        parentWindow = parentWindow._parentWindow;
        cnt = cnt+1;
        if (cnt > 10) {
          throw new Error('WindowOptions: parentWindow chain too deep');
        }
      }
    }
    this._parentWindow = parentWindow;
    this._replace = wob.replace;
    this._showScrollBars = wob.showScrollBars;
    this._onCloseFn = wob.onCloseFn;
    this._copyPaste = wob.copyPaste;
  }
  public function get height():int {
    return _height;
  }
  public function get minHeight():int {
    return _minHeight;
  }
  public function get width():int {
    return _width;
  }
  public function get minWidth():int {
    return _minWidth;
  }
  public function get resizable():Boolean {
    return _resizable;
  }
  public function get minimizable():Boolean {
    return _minimizable;
  }
  public function get maximizable():Boolean {
    return _maximizable;
  }
  public function get title():String {
    return _title;
  }
  public function get titlePrefix():String {
    return _titlePrefix;
  }
  public function get usePlainTitle():Boolean {
    return _usePlainTitle;
  }
  public function get parentWindow():BaseWindow {
    return _parentWindow;
  }
  public function get replace():Object {
    return _replace;
  }
  public function get showScrollBars():Boolean {
    return _showScrollBars;
  }
  public function get onCloseFn():Function {
    return _onCloseFn;
  }
  public function get copyPaste():ICopyPaste {
    return _copyPaste;
  }
}
}