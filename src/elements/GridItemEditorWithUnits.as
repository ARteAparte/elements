/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements {
import spark.components.TextInput;
import spark.components.gridClasses.GridItemEditor;

import dse.MetaData;

import elements.commands.DocUpdateDataPoint;

import gui.IManagedRef;

import pdt.IPVec;

import units.UnitConversions;

import utils.FormatUtils;

public class GridItemEditorWithUnits extends GridItemEditor {
    private var _textInput:TextInput;
    private var _dataPoint:Object;
    private var _docSC:IManagedRef;
    private var _value:Number;
    private var _stringValue:String;

    public function GridItemEditorWithUnits(docSC:IManagedRef) {
      //trace('GridItemEditorWithUnits');
        super();
        _docSC = docSC;
        _textInput = new TextInput();
        _textInput.visible = false;
        _textInput.percentWidth=100.0;
        _textInput.percentHeight=100.0;
        _dataPoint = null;
        _value = NaN;
        _stringValue = "";
        addElement(_textInput);
    }

    override public function get data():Object {
      //trace('GridItemEditorWithUnits.get data()');
        return _dataPoint;
    }

    override public function set data(dataPoint:Object):void {
      //trace('GridItemEditorWithUnits.set data(dataPoint)');
        var docState:DocState = DocHandlers.readDocState(_docSC);
        var dataSeries:String = this.column.dataField;
        var sm:MetaData = docState.dataSet.metaDataFor(dataSeries);
        var hdr:Object = docState.dataSet.header;
        _dataPoint = dataPoint;
        _value = sm.getter(hdr, dataPoint);
        if (!isNaN(_value)) {
          var ucf:Function = getBaseToDisplayConverter();
          var convertedVal:Number = ucf(Number(_value));
          var ddo:DataDisplayOptions = getDataDisplayOptions();
          var precision:int = ddo.precision;
          _stringValue = FormatUtils.numberToFormattedString(
            convertedVal, precision);
        }
        this.value = _value;
    }

    private function getDataDisplayOptions():DataDisplayOptions {
        var ds:DocState = DocHandlers.readDocState(_docSC);
        return ds.displayOptionsFor(this.column.dataField);
    }

    private function getBaseToDisplayConverter():Function {
        var sm:MetaData = getSeriesMeta();
        var displayUnits:String = getDisplayUnits();
        var ddo:DataDisplayOptions = getDataDisplayOptions()
        return function (baseVal:Number):Number {
            return UnitConversions.baseToDisplay(baseVal, sm.baseUnits,
                ddo.scale, displayUnits);
        };
    }

    private function getDisplayToBaseConverter():Function {
        var sm:MetaData = getSeriesMeta();
        var displayUnits:String = getDisplayUnits();
        var ddo:DataDisplayOptions = getDataDisplayOptions()
        return function (displayVal:Number):Number {
            return UnitConversions.displayToBase(displayVal, displayUnits,
                ddo.scale, sm.baseUnits);
        };
    }

    // ref: http://help.adobe.com/en_US/flex/using/WS0ab2a460655f2dc3-427f401412c60d04dca-7ff3.html
    override public function set value(val:Object):void {
      //trace('GridItemEditorWithUnits.set value(val)');
        var ucf:Function = getBaseToDisplayConverter();
        var convertedVal:Number = ucf(Number(val));
        if(isNaN(convertedVal)) {
            _textInput.text = "";
        } else {
          var ddo:DataDisplayOptions = getDataDisplayOptions();
          var precision:int = ddo.precision;
          _textInput.text = FormatUtils.numberToFormattedString(
            convertedVal, precision);
        }
    }

    override public function get value():Object {
      //trace('GridItemEditorWithUnits.get value(val)');
        var ucf:Function = getDisplayToBaseConverter();
        var convertedVal:Number;
        if(_textInput.text == "") {
            convertedVal = NaN;
        } else {
            convertedVal = ucf(Number(_textInput.text));
        }
        return convertedVal;
    }

    override public function save():Boolean {
      //trace('GridItemEditorWithUnits.save()');
        if (!validate()) {
            return false;
        }
        var valToSave:Number = this.value as Number;
        //trace('valToSave:', valToSave);
        if (isNaN(valToSave)) {
            return false;
        }
        var ucf:Function = getBaseToDisplayConverter();
        var convertedVal:Number = ucf(Number(valToSave));
        //trace('convertedVal', convertedVal);
        var ddo:DataDisplayOptions = getDataDisplayOptions();
        var precision:int = ddo.precision;
        var valToSaveRepr:String = FormatUtils.numberToFormattedString(
          convertedVal, precision);
        //trace('valToSaveRepr', valToSaveRepr);
        //trace('_textInput.text', this._textInput.text);
        if (valToSaveRepr != _stringValue) {
          //trace('_stringValue', _stringValue);
          //trace('valToSaveRepr', valToSaveRepr);
          _docSC.handleCommand(
            new DocUpdateDataPoint(
              this.column.dataField, valToSave, this.rowIndex));
          var docState:DocState = DocHandlers.readDocState(_docSC);
          var pts:IPVec = docState.dataSet.points;
          this.data = pts.getItemAt(this.rowIndex);
          dataGrid.dataProvider.setItemAt(this.data, this.rowIndex);
        }
        return true;
    }

    override protected function validate():Boolean {
        var val:Number = Number(this.value);
        if (isNaN(val)) {
            return false;
        }
        /* NOTE: proper validation occurs in the update function
        so we just need to make sure we don't have NaN's or other
        stuff coming in */
        return true;
    }

    private function getSeriesMeta():MetaData {
        var ds:DocState = DocHandlers.readDocState(_docSC);
        return ds.dataSet.metaDataFor(this.column.dataField);
    }

    private function getDisplayUnits():String {
        var ds:DocState = DocHandlers.readDocState(_docSC);
        return ds.displayUnitsFor(this.column.dataField);
    }

    override public function setFocus():void {
        _textInput.visible = true;
        _textInput.setFocus();
        _textInput.validateNow();
    }
}
}