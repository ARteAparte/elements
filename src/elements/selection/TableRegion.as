/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements.selection{
import spark.components.gridClasses.CellPosition;

public class TableRegion{
    public var rowStart:int;
    public var rowEnd:int;
    public var colStart:int;
    public var colEnd:int;

    public function
    TableRegion(rowStart:int, rowEnd:int, colStart:int, colEnd:int){
        if((rowStart > rowEnd) || (colStart > colEnd)){
            throw new Error(
                "Row and columns must be increasing:\nRowStart: " + 
                rowStart.toString() +
                "\nRowEnd: " + rowEnd.toString() + "\nColStart: " + 
                colStart.toString() +
                "\nColEnd: " + colEnd.toString());
        }
        this.rowStart = rowStart;
        this.rowEnd = rowEnd;
        this.colStart = colStart;
        this.colEnd = colEnd;
    }

    public function get numCells():Number{
        return (rowEnd - rowStart + 1) * (colEnd - colStart + 1);
    }

    public function cellsOutsideOfRegion(r:TableRegion):Vector.<CellPosition>{
        var cells:Vector.<CellPosition> = new Vector.<CellPosition>;
        for (var i:int = rowStart; i <= rowEnd; i++){
            for (var j:int = colStart; j <= colEnd; j++){
                if((i >= r.rowStart) && (i <= r.rowEnd) && (j >= r.colStart) &&
                    (j <= r.colEnd)){
                    continue;
                }
                else{
                    cells.push(new CellPosition(i, j));
                }
            }
        }
        return cells;
    }
}
}