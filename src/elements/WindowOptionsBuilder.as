/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements {
import elements.views.BaseWindow;

public class WindowOptionsBuilder {
  internal var width:int;
  internal var minWidth:int;
  internal var height:int;
  internal var minHeight:int;
  internal var resizable:Boolean;
  internal var minimizable:Boolean;
  internal var maximizable:Boolean;
  internal var title:String;
  internal var titlePrefix:String;
  internal var usePlainTitle:Boolean;
  internal var parentWindow:BaseWindow;
  internal var replace:Object;
  internal var showScrollBars:Boolean;
  internal var onCloseFn:Function;
  internal var copyPaste:ICopyPaste;
  public function WindowOptionsBuilder() {
    width = -1;
    minWidth = -1;
    height = -1;
    minHeight = -1;
    resizable = true;
    minimizable = true;
    maximizable = true;
    title = "";
    titlePrefix = "";
    usePlainTitle = false;
    parentWindow = null;
    replace = {};
    showScrollBars = false;
    onCloseFn = null;
    copyPaste = new NormalCopyPaste();
  }
  public function withWidth(w:int):WindowOptionsBuilder {
    this.width = w;
    return this;
  }
  public function withHeight(h:int):WindowOptionsBuilder {
    this.height = h;
    return this;
  }
  public function withMinWidth(w:int):WindowOptionsBuilder {
    this.minWidth = w;
    return this;
  }
  public function withMinHeight(h:int):WindowOptionsBuilder {
    this.minHeight = h;
    return this;
  }
  public function withResizable(flag:Boolean):WindowOptionsBuilder {
    this.resizable = flag;
    return this;
  }
  public function withMinimizable(flag:Boolean):WindowOptionsBuilder {
    this.minimizable = flag;
    return this;
  }
  public function withMaximizable(flag:Boolean):WindowOptionsBuilder {
    this.maximizable = flag;
    return this;
  }
  public function withTitle(t:String):WindowOptionsBuilder {
    this.title = t;
    return this;
  }
  public function withTitlePrefix(tp:String):WindowOptionsBuilder {
    this.titlePrefix = tp;
    return this;
  }
  public function withPlainTitle(flag:Boolean):WindowOptionsBuilder {
    this.usePlainTitle = flag;
    return this;
  }
  public function withParentWindow(pw:BaseWindow):WindowOptionsBuilder {
    this.parentWindow = pw;
    return this;
  }
  public function withReplace(obj:Object):WindowOptionsBuilder {
    this.replace = obj;
    return this;
  }
  public function withScrollBars(flag:Boolean):WindowOptionsBuilder {
    this.showScrollBars = flag;
    return this;
  }
  public function withOnCloseFn(fn:Function):WindowOptionsBuilder {
    this.onCloseFn = fn;
    return this;
  }
  public function withCopyPaste(cp:ICopyPaste):WindowOptionsBuilder {
    this.copyPaste = cp;
    return this;
  }
  public function fromWindowOptions(wo:WindowOptions):WindowOptionsBuilder {
    return (new WindowOptionsBuilder())
      .withHeight(wo.height)
      .withMinHeight(wo.minHeight)
      .withWidth(wo.width)
      .withMinWidth(wo.minWidth)
      .withResizable(wo.resizable)
      .withMinimizable(wo.minimizable)
      .withMaximizable(wo.maximizable)
      .withTitle(wo.title)
      .withTitlePrefix(wo.titlePrefix)
      .withPlainTitle(wo.usePlainTitle)
      .withParentWindow(wo.parentWindow)
      .withReplace(wo.replace)
      .withScrollBars(wo.showScrollBars)
      .withOnCloseFn(wo.onCloseFn)
      .withCopyPaste(wo.copyPaste);
  }
  public function build():WindowOptions {
    return new WindowOptions(this);
  }
}
}