/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements {
import dse.MetaData;

import scaling.ScalingFactors;

import units.NoUnits;

public class DataGridHelpers {
    public static function
    makeHeaderText(docState:DocState, dataField:String):String {
        var text:String;
        var baseHdrTxt:String = makeBaseHeaderText(docState, dataField);
        var unitsTxt:String = makeUnitsString(docState, dataField);
        var scaleTxt:String = makeScaleString(docState, dataField);
        text = baseHdrTxt;
        if (unitsTxt != "") text += " [" + unitsTxt + "]";
        if (scaleTxt != "") text += " " + scaleTxt;
        return text;
    }

    public static function
    makeBaseHeaderText(docState:DocState, dataField:String):String {
        var headerText:String = "";
        var sm:MetaData = docState.dataSet.metaDataFor(dataField);
        return headerText + sm.fullName;
    }

    public static function
    makeUnitsString(docState:DocState, dataField:String):String {
        var opts:DataDisplayOptions = docState.displayOptionsFor(dataField);
        var displayUnits:String = docState.displayUnitsFor(dataField);
        if ((displayUnits == NoUnits.NONE) || (displayUnits == null))
            return "";
        return displayUnits;
    }

    public static function
    makeScaleString(docState:DocState, dataField:String):String {
        var opts:DataDisplayOptions = docState.displayOptionsFor(dataField);
        if (opts.scale != ScalingFactors.x1)
            return ScalingFactors.toString(opts.scale);
        return "";
    }
}
}