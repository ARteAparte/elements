/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements {
public class DocEvents {
  public static const EVENT_HEADER_CHANGED:String =
    'event_headerUpdated';
  public static const EVENT_FILE_STATUS_CHANGED:String =
    'event_fileStatusChanged';
  public static const EVENT_DISPLAY_UNITS_CHANGED:String =
    'event_displayUnitsChanged';
  public static const EVENT_SELECTION_CHANGED:String =
    'event_selectionChanged';
  public static const EVENT_SERIES_DATA_CHANGED:String =
    'event_seriesDataChanged';
  public static const EVENT_DISPLAYED_DATA_COLUMNS_CHANGED:String =
    'event_displayedDataColumnsChanged';
  public static const EVENT_ALL_CHANGED:String =
    'event_allChanged';
  public static const EVENT_ACTIVE_SETTING_SET_CHANGED:String =
    'event_activeSettingSetChanged';
  public static const EVENT_REQUEST_ADD_COLUMN:String =
    'event_request_add_column';
  public static const EVENT_REQUEST_OFFSET:String =
    'event_request_offset';
  public static const EVENT_REQUEST_SCALE:String =
    'event_request_scale';
  public static const EVENT_REQUEST_NORMALIZE:String =
    'event_request_normalize';
  public static const EVENT_REQUEST_NORMALIZE_MONTHLY:String =
    'event_request_normalize_monthly';
  public static const EVENT_REQUEST_HEADER_EDIT:String =
    'event_request_header_edit';
  public static const EVENT_REQUEST_VIEW_CHART:String =
    'event_request_view_chart';
}
}