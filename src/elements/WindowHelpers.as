/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements {
import flash.display.DisplayObject;
import flash.events.Event;
import flash.events.MouseEvent;

import mx.core.IFlexDisplayObject;
import mx.managers.PopUpManager;

import spark.components.Button;

public class WindowHelpers {
    public static function
    modalLaunch(win:IFlexDisplayObject, parent:DisplayObject,
                okButton:Button):void {
        var f:Function = function(e:Event):void {
            PopUpManager.removePopUp(win);
        };
        okButton.addEventListener(MouseEvent.CLICK, f);
        PopUpManager.addPopUp(win, parent, true);
        PopUpManager.centerPopUp(win);
    }
}
}