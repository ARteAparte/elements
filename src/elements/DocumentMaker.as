/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements {
import flash.display.NativeWindow;
import flash.display.Screen;
import flash.events.Event;
import flash.filesystem.File;
import flash.filesystem.FileMode;
import flash.filesystem.FileStream;
import flash.geom.Rectangle;
import flash.html.HTMLLoader;

import mx.core.IVisualElement;
import mx.core.UIComponent;

import spark.components.HGroup;
import spark.components.VScrollBar;

import dse.DataSet;

import elements.commands.AppNewHTMLDocument;
import elements.commands.AppSetActiveWindow;
import elements.commands.DocImportData;
import elements.commands.DocRefreshAll;
import elements.commands.DocSetDisplayUnitSys;
import elements.commands.DocSetFileUnModified;
import elements.helpers.WindowNaming;
import elements.views.AcmeMainView;
import elements.views.BaseWindow;
import elements.views.MenuBar;

import gui.IManagedRef;

import pdt.IPMap;
import pdt.PersistentArrayMap;
import pdt.PersistentVector;

import units.UnitSystems;

import utils.MapUtils;
import utils.NetUtils;

import weatherfile.importers.IImporter;
import weatherfile.importers.ImporterNativeFormat;

public class DocumentMaker{
  public static function newPrePopulatedDoc(opts:WindowOptions=null):Object {
    //trace('DocumentMaker.newPrePopulatedDoc');
    var dataSet:DataSet = DataSetGenerators.gen8760();
    var smd:Object = dataSet.seriesMetaData;
    var units:IPMap = PersistentArrayMap.EMPTY;
    var prefs:Object = loadPreferences();
    if (prefs.hasOwnProperty(UnitSystems.UNIT_SYSTEM)) {
      units = new PersistentArrayMap(
        [UnitSystems.UNIT_SYSTEM, prefs[UnitSystems.UNIT_SYSTEM]]);
    }
    var dispOpts:IPMap = DataDisplayOptions.makeDefaultDisplayOptions(smd);
    var name:String = WindowNaming.nextName();
    var ds:DocState = (new DocStateBuilder())
      .withDataSet(dataSet)
      .withDisplayUnits(units)
      .withSelections(PersistentVector.EMPTY)
      .withDisplayOptionsMap(dispOpts)
      .withShownData(DocHandlers.defaultShownDataColumns(smd))
      .withFileName(name)
      .withTitle(name)
      .withFile(null)
      .withFileModified(true)
      .withActiveSettingSets(PersistentArrayMap.EMPTY)
      .build();
    var docState:IManagedRef = DocHandlers.makeControlledDocState(ds);
    var pl:PreferencesListener = new PreferencesListener(docState);
    var view:AcmeMainView = new AcmeMainView(docState);
    var apSt:AppState = AppHandlers.readAppState(AppHandlers.instance());
    var mb:MenuBar = apSt.menuBar;
    docState.registerSubscriberFunction(mb.update);
    if (opts == null) {
      opts = new WindowOptionsBuilder()
        .withCopyPaste(new DataGridCopyPaste(docState))
        .build();
    } else {
      opts = new WindowOptionsBuilder().fromWindowOptions(opts)
        .withCopyPaste(new DataGridCopyPaste(docState))
        .build();
    }
    return newDocumentFromView(view, docState, true, opts);
  }
  public static function newDocument(opts:WindowOptions=null):Object {
    //trace('DocumentMaker.newDocument');
    var ds:DocState = (new DocStateBuilder()).build();
    var units:IPMap = PersistentArrayMap.EMPTY;
    var prefs:Object = loadPreferences();
    if (prefs.hasOwnProperty(UnitSystems.UNIT_SYSTEM)) {
      units = new PersistentArrayMap(
        [UnitSystems.UNIT_SYSTEM, prefs[UnitSystems.UNIT_SYSTEM]]);
    }
    ds = DocStateBuilder.fromDocState(ds).withDisplayUnits(units).build();
    var docState:IManagedRef = DocHandlers.makeControlledDocState(ds);
    var pl:PreferencesListener = new PreferencesListener(docState);
    var view:AcmeMainView = new AcmeMainView(docState);
    var apSt:AppState = AppHandlers.readAppState(AppHandlers.instance());
    var mb:MenuBar = apSt.menuBar;
    docState.registerSubscriberFunction(mb.update);
    return newDocumentFromView(view, docState, true, opts);
  }
  public static function loadPreferences():Object {
    var prefs:File = File.applicationStorageDirectory.resolvePath(
      PreferencesListener.FILE_NAME);
    var out:Object = {};
    if (prefs.exists) {
      var fs:FileStream = new FileStream();
      fs.open(prefs, FileMode.READ);
      var prefsData:String = fs.readUTFBytes(fs.bytesAvailable);
      var xml:XML = new XML(prefsData);
      out[UnitSystems.UNIT_SYSTEM] = xml.unitSystem;
      if (xml.hasOwnProperty('windowPositions')) {
        out['windowPositions'] = {};
        for each (var win:XML in xml.windowPositions.win) {
          out['windowPositions'][win.title] =
            { x: win.x, y: win.y, width: win.width, height: win.height };
        }
      }
      return out;
    }
    out[UnitSystems.UNIT_SYSTEM] = UnitSystems.SI;
    return out;
  }
  public static function newDocumentFromView(
                             view:IVisualElement,
                             ds:IManagedRef=null,
                             checkClose:Boolean=true,
                             opts:WindowOptions=null,
                             parentWindow:BaseWindow=null):Object{
    //trace('DocumentMaker.newDocumentFromView');
    var prefs:Object = loadPreferences();
    if (ds == null) {
      var b:DocStateBuilder = new DocStateBuilder();
      if ((opts!=null)&&(opts.title != null)) {
        b = b.withTitle(opts.title);
      }
      if ((opts!=null)&&(opts.usePlainTitle)) {
        b = b.withAlwaysPlainTitle(opts.usePlainTitle);
      }
      var docState:DocState = b.build();
      ds = DocHandlers.makeControlledDocState(docState);
      var apSt:AppState = AppHandlers.readAppState(AppHandlers.instance());
      var mb:MenuBar = apSt.menuBar;
      ds.registerSubscriberFunction(mb.update);
    }
    var openWindowActive:Boolean = true;
    var bw:BaseWindow;
    if (opts != null) {
      if ((opts.width > 0) &&
          (opts.height > 0) &&
          (opts.minHeight > 0) &&
          (opts.minWidth > 0)) {
        //trace('DocumentMaker: setting all size options')
        //trace('DocumentMaker:', opts.width);
        //trace('DocumentMaker:', opts.height);
        //trace('DocumentMaker:', opts.minWidth);
        //trace('DocumentMaker:', opts.minHeight);
        bw = new BaseWindow(ds, view, openWindowActive, opts.resizable, opts.minimizable, opts.maximizable,
          opts.width, opts.height, opts.minWidth, opts.minHeight);
      } else if ((opts.width > 0) &&
                 (opts.height > 0) &&
                 (opts.minWidth > 0)) {
        bw = new BaseWindow(ds, view, openWindowActive, opts.resizable, opts.minimizable, opts.maximizable,
          opts.width, opts.height, opts.minWidth);
      } else if ((opts.width > 0) &&
                 (opts.height > 0) &&
                 (opts.minHeight > 0)) {
        bw = new BaseWindow(ds, view, openWindowActive, opts.resizable, opts.minimizable, opts.maximizable,
          opts.width, opts.height, opts.width, opts.minHeight);
      } else if ((opts.width > 0) && (opts.height > 0)) {
        bw = new BaseWindow(ds, view, openWindowActive, opts.resizable, opts.minimizable, opts.maximizable,
          opts.width, opts.height);
      } else if ((opts.width > 0)) {
        bw = new BaseWindow(ds, view, openWindowActive, opts.resizable, opts.minimizable, opts.maximizable, opts.width);
      } else if ((opts.height > 0)) {
        bw = new BaseWindow(ds, view, openWindowActive, opts.resizable, opts.minimizable, opts.maximizable,
          BaseWindow.DEFAULT_WIDTH, opts.height);
      } else {
        bw = new BaseWindow(ds, view, openWindowActive, opts.resizable, opts.minimizable, opts.maximizable);
      }
    } else {
      bw = new BaseWindow(ds, view, openWindowActive, opts.resizable, opts.minimizable, opts.maximizable);
    }
    bw.parentWindow = parentWindow;
    bw.checkClose = checkClose;
    //MapUtils.traceObject(opts, 'opts');
    //bw.width = BaseWindow.DEFAULT_WIDTH;
    //bw.height = BaseWindow.DEFAULT_HEIGHT;
    if (opts != null) {
      applyOpts(bw, opts);
    }
    bw.docStateCtrlr.handleCommand(
      new DocSetDisplayUnitSys(prefs[UnitSystems.UNIT_SYSTEM]));
    bw.docStateCtrlr.handleCommand(new DocRefreshAll());
    bw.validateNow();
    setWindowSizePrefs(bw, prefs);
    var pl:PreferencesListener = new PreferencesListener(ds);
    return bw as Object;
  }
  public static function setWindowSizePrefs(bw:BaseWindow, prefs:Object):void {
    var screen:Rectangle = Screen.mainScreen.bounds;
    var nw:NativeWindow = bw.nativeWindow;
    nw.x = (screen.width - nw.width) / 2;
    nw.y = (screen.height - nw.height) / 2;
    if (prefs.hasOwnProperty('windowPositions') &&
        prefs.windowPositions.hasOwnProperty(bw.tag) &&
        (int(prefs.windowPositions[bw.tag].x) >= 0) &&
        (int(prefs.windowPositions[bw.tag].y) >= 0) &&
        ((int(prefs.windowPositions[bw.tag].width) +
          int(prefs.windowPositions[bw.tag].x)) < screen.width) &&
        ((int(prefs.windowPositions[bw.tag].height) +
          int(prefs.windowPositions[bw.tag].y)) < screen.height)) {
      nw.x = prefs.windowPositions[bw.tag].x;
      nw.y = prefs.windowPositions[bw.tag].y;
      nw.width = prefs.windowPositions[bw.tag].width;
      nw.height = prefs.windowPositions[bw.tag].height;
    }
  }
  public static function loadDocFromFileIntoWin(f:File, win:Object,
                                                opts:WindowOptions=null):void{
    //trace('DocumentMaker.loadDocFromFileIntoWin');
    var imp:IImporter = new ImporterNativeFormat();
    var dw:BaseWindow = win as BaseWindow;
    if (opts != null) {
      applyOpts(dw, opts);
    }
    dw.docStateCtrlr.handleCommand(new DocImportData(imp, f));
    dw.docStateCtrlr.resetUndoRedo();
    dw.docStateCtrlr.handleCommand(new DocSetFileUnModified(f));
    var prefs:Object = loadPreferences();
    dw.docStateCtrlr.handleCommand(
      new DocSetDisplayUnitSys(prefs[UnitSystems.UNIT_SYSTEM] as String));
    dw.docStateCtrlr.handleCommand(new DocRefreshAll());
    dw.validateNow();
    setWindowSizePrefs(dw, prefs);
    var pl:PreferencesListener = new PreferencesListener(dw.docStateCtrlr);
  }
  public static function loadHTMLWindow(file:File, opts:WindowOptions=null):Object {
    //trace('DocumentMaker.loadHTMLWindow');
    //MapUtils.traceObject(opts, 'opts');
    var out:Object = createWindow(opts);
    var win:BaseWindow = out.window as BaseWindow;
    var html:HTMLLoader = out.loader as HTMLLoader;
    html.placeLoadStringContentInApplicationSandbox = true;
    var fs:FileStream = new FileStream();
    fs.addEventListener(Event.COMPLETE, function(e:Event):void {
      var htmlContent:String = fs.readUTFBytes(fs.bytesAvailable);
      //trace('in filestream handler');
      //MapUtils.traceObject(opts, 'opts');
      for each (var key:String in MapUtils.keys(opts.replace)) {
        var val:String = opts.replace[key];
        //trace('replacing ' + key + ' with ' + val);
        htmlContent = htmlContent.replace(key, val);
      }
      html.loadString(htmlContent);
    });
    fs.openAsync(file, FileMode.READ);
    return win as Object;
  }
  private static function createWindow(_opts:WindowOptions):Object {
    var _window:BaseWindow = DocumentMaker.newDocumentFromView(
      null,
      null,
      false,
      _opts,
      null) as BaseWindow;
    if (_opts.copyPaste != null) {
      _window.copyPaste = _opts.copyPaste;
    }
//    window.systemChrome = "none";
//    window.type = "lightweight";
    _window.visible = true;
    _window.checkClose = false;
    var htmlLoader2:HTMLLoader = new HTMLLoader();
    htmlLoader2.width = _window.width;
    htmlLoader2.height = _window.height;
    htmlLoader2.window.openCredits = function():void {
      //trace('Opening HELP Docs from JS');
      var url:String = File.applicationDirectory.resolvePath("htmldocs/user-guide/acknowledgments.html").url;
      NetUtils.openExternalURL(url);
    };
    htmlLoader2.window.openLicense = function():void {
      //trace('Opening License from JS');
      var url:String = File.applicationDirectory.resolvePath("htmldocs/user-guide/license.html").url;
      NetUtils.openExternalURL(url);
    };
    var wrapper:UIComponent = new UIComponent();
    wrapper.addChild(htmlLoader2);
    var sb:VScrollBar = new VScrollBar();
    if (_opts.showScrollBars) {
      var gr:HGroup = new HGroup();
      sb.addEventListener(Event.CHANGE,
        function(e:Event):void {
          //trace('scroll value: ', sb.value);
          var h:Number = htmlLoader2.contentHeight - _window.height;
          htmlLoader2.scrollV = sb.value * h / 100.0;
        });
      sb.enabled = true;
      gr.addElement(wrapper);
      gr.addElement(sb);
      sb.width = 20;
      wrapper.width = _window.width - sb.width;
      htmlLoader2.width = _window.width - sb.width;
      sb.height = _window.height;
      wrapper.height = _window.height;
      htmlLoader2.height = wrapper.height;
      gr.percentHeight=100.0;
      gr.percentWidth=100.0;
      gr.validateNow();
      _window.addElement(gr);
    } else {
      wrapper.percentHeight=100;
      wrapper.percentWidth=100;
      _window.addElement(wrapper);
    }
    _window.addEventListener(Event.RESIZE,
      function(e:Event):void {
        if (_opts.showScrollBars) {
          sb.width = 20;
          wrapper.width = _window.width - sb.width;
          htmlLoader2.width = wrapper.width;
          sb.height = _window.height;
        } else {
          wrapper.width = _window.width;
          htmlLoader2.width = wrapper.width;
        }
        wrapper.height = _window.height;
        htmlLoader2.height = _window.height;
      });
    htmlLoader2.addEventListener(Event.SCROLL,
      function(e:Event):void {
        var h:Number = htmlLoader2.contentHeight - _window.height;
        sb.value = htmlLoader2.scrollV * 100.0 / h;
      });
    return {window:_window, loader:htmlLoader2};
  }
  private static function applyOpts(w:BaseWindow, opts:WindowOptions):void {
    //trace('DocumentMaker.applyOpts');
    if ((opts != null) && (!w.closed)) {
      if (opts.width > 0) {
        w.nativeWindow.width = opts.width;
        if (opts.minWidth > 0) {
          w.minWidth = opts.minWidth;
        } else {
          w.minWidth = opts.width;
        }
      }
      if (opts.height > 0) {
        w.nativeWindow.height = opts.height;
        if (opts.minHeight > 0) {
          w.minHeight = opts.minHeight;
        } else {
          w.minHeight = opts.height;
        }
      }
      if (opts.titlePrefix.length > 0) {
        w._pretitle = opts.titlePrefix;
        w.title = w.title;
      }
      if (opts.title.length > 0) {
        w.title = opts.title;
      }
      if (opts.onCloseFn != null) {
        w.onCloseFn = opts.onCloseFn;
      }
      if (opts.copyPaste != null) {
        w.copyPaste = opts.copyPaste;
      }
    }
  }
}
}