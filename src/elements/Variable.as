/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements {
import dse.MetaData;

public class Variable {
  public var _md:MetaData;
  public var _ddo:DataDisplayOptions;
  
  public function Variable(md:MetaData, ddo:DataDisplayOptions) {
    _md = md;
    _ddo = ddo;
  }
  
  public function get name():String {
    return this._md.name;
  }
  public function get fullName():String {
    return this._md.fullName;
  }
  public function get icon():Class {
    return this._ddo.icon;
  }
  public function get color():uint {
    return this._ddo.color;
  }
  public function get precision():int {
    return this._ddo.precision;
  }
  public function toString():String {
    return "[Variable " + this.name + "]";
  }
}
}