/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements{
import spark.components.TextInput;
import spark.components.gridClasses.GridItemEditor;

import gui.IManagedRef;

public class AcmeGridItemEditor extends GridItemEditor{
  public var _textInput:TextInput;
  public var _rowData:Object;
  public var _sc:IManagedRef;
  public var _value:Number;
  public var _rowDataToNum:IDataGridRowToNum;
  public var _valToStr:IDataGridValToStr;
  public var _valToObj:IDataGridValToObj;
  public var _valSaver:IDataGridValSaver;

  public function AcmeGridItemEditor(sc:IManagedRef,
                                     rowDataToNum:IDataGridRowToNum,
                                     valToStr:IDataGridValToStr,
                                     valToObj:IDataGridValToObj,
                                     saver:IDataGridValSaver){
    super();
    _sc = sc;
    _textInput = new TextInput();
    _textInput.visible = false;
    _rowData = {};
    _value = NaN;
    _rowDataToNum = rowDataToNum;
    _valToStr = valToStr;
    _valToObj = valToObj;
    _valSaver = saver;
    addElement(_textInput);
  }
  override public function get data():Object {
    return this._rowData;
  }
  override public function set data(rowData:Object):void {
    this._rowData = rowData;
    var field:String = column.dataField;
    this.value = _rowDataToNum.dataToNumber(_sc, field, rowData);
  }
  override public function set value(val:Object):void {
    _textInput.text = _valToStr.valueToString(_sc, column.dataField, val);
  }
  override public function get value():Object {
    return _valToObj.valToObj(_sc, this.column.dataField, _textInput.text);
  }
  override public function save():Boolean {
    if (!validate()) {
      return false;
    }
    var valToSave:Number = this.value as Number;
    if (isNaN(valToSave)) {
      return false;
    }
    _valSaver.saveValue(_sc, this, valToSave);
    return true;
  }
  override protected function validate():Boolean {
    var val:Number = Number(this.value);
    if (isNaN(val)) {
      return false;
    }
    return true;
  }
  override public function setFocus():void {
    _textInput.visible = true;
    _textInput.setFocus();
  }
}
}