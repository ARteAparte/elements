/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements {
import mx.core.IFactory;

import gui.IManagedRef;

public class AcmeGridItemEditorFactory implements IFactory {
  public var _sc:IManagedRef;
  public var _rowDataToNum:IDataGridRowToNum;
  public var _valToStr:IDataGridValToStr;
  public var _valToObj:IDataGridValToObj;
  public var _valSaver:IDataGridValSaver;

  public function AcmeGridItemEditorFactory(sc:IManagedRef,
                                            rowDataToNum:IDataGridRowToNum,
                                            valToStr:IDataGridValToStr,
                                            valToObj:IDataGridValToObj,
                                            saver:IDataGridValSaver){
    _sc = sc;
    _rowDataToNum = rowDataToNum;
    _valToStr = valToStr;
    _valToObj = valToObj;
    _valSaver = saver;
  }
  public function newInstance():* {
    return new AcmeGridItemEditor(
      _sc, _rowDataToNum, _valToStr, _valToObj, _valSaver);
  }
}
}