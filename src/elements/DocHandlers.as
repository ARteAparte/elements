/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements {
import flash.filesystem.File;

import mx.collections.ArrayCollection;

import spark.components.gridClasses.CellPosition;

import dse.DataPoint;
import dse.MetaData;
import dse.SeriesSelection;

import gui.AcmeManagedRef;
import gui.IManagedRef;

import pdt.IPVec;
import pdt.PersistentVector;

import plugins.DateAndTime;
import plugins.Psych;
import plugins.Solar;
import plugins.Wind;

import utils.MapUtils;

public class DocHandlers {
  /* makeControlledDocState
  ds -- document state (as created by the "create" functions above)

  returns a StateController which is basically a managed reference
  to mutable state */
  public static function makeControlledDocState(dd:DocState):IManagedRef {
    return new AcmeManagedRef(dd);
  }

  public static function readDocState(sc:IManagedRef):DocState {
    return sc.read() as DocState;
  }

  public static function
    deriveMonthlyAverageDailyIrradiation(ds:DocState):ArrayCollection {
    return Solar.deriveMonthlyAvgGH(ds.dataSet);
  }

  public static function getScaleByMonthPanelData(ds:DocState):NormWizData {
    var map:Object = {};
    var metrics:Array = ds.dataSet.numericMetricNames;
    var metricNames:Array = ds.dataSet.numericMetricFullNames;
    var settingSets:Object = ds.dataSet.settingSetStringMap;
    var selections:Array = getSelectionsByMonth(ds);
    return new NormWizData(metrics, metricNames, settingSets, selections);
  }

  public static function getSelectionsByMonth(ds:DocState):Array {
    var points:IPVec = ds.dataSet.points;
    var hdr:Object = ds.dataSet.header;
    var monthYear:MetaData =
      DateAndTime.SERIES_META_DATA[DateAndTime.MONTH_YEAR] as MetaData;
    var monthYearGetter:Function = monthYear.getter;
    var selections:Array = [];
    var currentMonthYear:String = "";
    var startIdx:int = 0;
    var numVals:int = 0;
    for (var i:int=0; i<points.length; i++) {
      var moYr:String = monthYearGetter(hdr, points.valAt(i));
      if (moYr != currentMonthYear) {
        currentMonthYear = moYr;
        if (i > 0) {
          selections.push(new SeriesSelection(
            points, DateAndTime.MONTH_YEAR, startIdx, numVals));
        }
        startIdx = i;
        numVals = 0;
      }
      numVals++;
    }
    selections.push(new SeriesSelection(
      points, DateAndTime.MONTH_YEAR, startIdx, numVals));
    return selections;
  }

  public static function hasData(ds:DocState):Boolean {
    if (ds.dataSet.points.length > 0) return true;
    return false;
  }

  public static const ARG_DESIRED_Hgh_MJ__m2:String =
    'arg_desiredHgh_MJ__m2';
  public static const ARG_MONTHLY_INSOLATION_OBJ:String =
    'arg_monthlyInsolationObj';

  public static function
  vectorOfCellPositionsToArray(xs:Vector.<CellPosition>):Array {
    var ys:Array = new Array(xs.length);
    var f:Function =
      function(x:CellPosition, idx:int, a:Vector.<CellPosition>):void {
        ys[idx] = x;
      };
    xs.forEach(f);
    return ys;
  }

  public static const DEFAULT_SHOWN_COLUMNS:IPVec = PersistentVector.create([
    DateAndTime.DATE_TIME,
    Psych.DRY_BULB_TEMP_C,
    Psych.WET_BULB_TEMP_C,
    Psych.PRESSURE_kPa,
    Psych.RELATIVE_HUMIDITY,
    Psych.DEW_POINT_TEMP_C,
    Solar.GLOBAL_HORIZ_Wh__m2,
    Solar.BEAM_NORMAL_Wh__m2,
    Solar.DIFFUSE_HORIZONTAL_Wh__m2,
    Wind.SPEED_m__s]);

  /* Select reasonable default headers */
  public static function defaultShownDataColumns(seriesMetas:Object):IPVec {
    //trace('DocState.defaultShownDataColumns');
    var out:Array = new Array();
    var desiredDefaults:Array = DEFAULT_SHOWN_COLUMNS.toArray();
    for each (var deflt:String in desiredDefaults) {
      for each (var name:String in MapUtils.keys(seriesMetas)) {
        if (name == deflt) {
          out.push(name);
        }
      }
    }
    return PersistentVector.create(out);
  }

  public static function
  isProperToolCall(ds:DocState, args:Object, attr:String):Boolean {
    return singleSeriesSelection(ds) &&
      isValidNumericParameter(args, attr);
  }

  public static function validArgsForScaleByMinMax(args:Object):Boolean {
    var oldMin:Number = args.oldMin as Number;
    var oldMax:Number = args.oldMax as Number;
    var newMin:Number = args.newMin as Number;
    var newMax:Number = args.newMax as Number;
    if (oldMin > oldMax) return false;
    if (newMin > newMax) return false;
    return true;
  }

  public static function reduceSelected(ds:DocState, seed:*, f:Function):* {
    var selections:IPVec = ds.selections;
    var smd:Object = ds.dataSet.seriesMetaData;
    var name:String = ds.selectionN(0).selectedSeries;
    var md:MetaData = ds.dataSet.metaDataFor(name);
    var hdr:Object = ds.dataSet.header;
    var acc:* = seed;
    var points:IPVec = ds.dataSet.points;
    for (var i:int=0; i<selections.length; i++) {
      var selection:SeriesSelection =
        selections.getItemAt(i) as SeriesSelection;
      var selectedRows:Array = selection.selectedRows;
      for each (var rowIdx:int in selectedRows) {
        var point:Object = points.getItemAt(rowIdx);
        var value:Number = md.getter(hdr, point);
        acc = f(acc, value);
      }
    }
    return acc;
  }

  public static function singleSeriesSelection(ds:DocState):Boolean {
    // don't proceed if no selections
    var selections:IPVec = ds.selections;
    if (selections.length == 0) {
      return false;
    }
    if (!SeriesSelection.onlyOneSeriesSelected(selections)) {
      // can't apply tool if more than one series selected
      return false;
    }
    return true;
  }

  public static function
  isValidNumericParameter(args:Object, name:String):Boolean {
    if (!args.hasOwnProperty(name)) {
      return false;
    }
    var value:Number = Number(args[name]);
    if (isNaN(value)) {
      return false;
    }
    return true;
  }

  /* map f over selected and update using settingSet.
  f must have type f :: Number -> Number */
  public static function mapSelected(ds:DocState, f:Function):DocState {
    //trace('DocHandlers.mapSelected');
    var selections:IPVec = ds.selections;
    //trace('number of selections', selections.length);
    var smd:Object = ds.dataSet.seriesMetaData;
    var hdr:Object = ds.dataSet.header;
    var name:String = ds.selectionN(0).selectedSeries;
    var settingSetIdx:int = ds.settingSetIndexFor(name);
    var md:MetaData = ds.dataSet.metaDataFor(name);
    //var newPointValuesMap:Object = new Object();
    var rowIdx:int;
    var points:IPVec = ds.dataSet.points;
    var point:Object;
    var value:Number;
    var allSelectedRows:Array = [];
    for (var selIdx:int=0; selIdx < selections.length; selIdx++) {
      var selection:SeriesSelection =
        selections.getItemAt(selIdx) as SeriesSelection;
      var selectedRows:Array = selection.selectedRows;
      allSelectedRows = allSelectedRows.concat(selectedRows);
    }
    var newPointValues:Array = [];
    for each (rowIdx in allSelectedRows) {
      //trace('pushing point:', rowIdx, 'of selection', selIdx);
      point = points.getItemAt(rowIdx);
      value = md.getter(hdr, point);
      newPointValues.push(f(value));
    }
    //trace('DocHandlers.mapSelected -- newPointValues determined');
    return DataPoint.updateMultiplePointsWithSettingsAndDocState(
      name,
      newPointValues,
      ds,
      allSelectedRows,
      settingSetIdx
    );
    //for (var rowIdxAsStr:String in newPointValuesMap) {
    //  rowIdx = int(rowIdxAsStr);
    //  newDS = DataPoint.updateDocStateWithSettingsAndDocState(
    //    name, newPointValuesMap[rowIdxAsStr] as Number,
    //    newDS, rowIdx, settingSetIdx);
    //}
    //var ptIdx:int = 0;
    //var numberOfRows:int = allSelectedRows.length;
    // TODO: call below and comment out for loop
    // updateMultiplePointsWithSettingsAndDocState
    //for each (rowIdx in allSelectedRows) {
      //trace('updating point number', ptIdx + 1, 'of', numberOfRows);
    //  newDS = DataPoint.updateDocStateWithSettingsAndDocState(
    //    name, newPointValues[ptIdx] as Number,
    //    newDS, rowIdx, settingSetIdx);
    //  ptIdx = ptIdx + 1;
    //}
    //return newDS;
  }

  /* OVERVIEW
  ===========

  if UNIT_SYSTEM is SI or IP, then all values will display in SI or IP units
  if UNIT_SYSTEM is custom, then all values will display in DISPLAY_UNITS
  where DISPLAY_UNITS is an Object with keys corresponding to data name and
  values corresponding to the display unit (which should have a conversion
  function in UnitConversions)

  DISPLAY_UNITS should contain a key for UNIT_SYSTEM for helping the GUI
  determine what unit system is in play. UNIT_SYSTEM can be SI, IP, or
  CUSTOM.

  If any IP or SI system units are modified, the UNIT_SYSTEM is set to
  CUSTOM. */
  public static function updateFileTitle(ds:DocState, args:Object):DocState {
    if (ds.fileModified) {
      return ds.settingFileTitleTo(ds.fileName + "*");
    }
    return ds.settingFileTitleTo(ds.fileName);
  }

  public static function fileExists(ds:DocState):Boolean {
    var f:File = ds.file;
    return ((f != null) && (f.exists))
  }
}
}