/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements.views {
import mx.charts.chartClasses.ChartLabel;

public class CustomChartLabel extends ChartLabel {
    private var _color:String;

    public function CustomChartLabel() {
        super();

        //this.setStyle("color", 0xFF0000);
        //this.setStyle("backgroundColor", 0x555555);
        // the only thing that keeps the left gutter the same!
        this.minWidth = 30;
        this.setStyle("textAlign", "right"); // no effect
    }

    public function get color():String {
        return _color;
    }

    public function set color(value:String):void {
        _color = value;
        this.setStyle("color", value);
    }

    // does not work at all
    public function set myWidth(value:Number):void {
        //this.setStyle("width", value);
        this.width = value;
        this.setStyle("width", value);
    }
}
}