/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements.views
{
    import flash.events.Event;
    import flash.events.MouseEvent;

    import mx.controls.ProgressBar;
    import mx.controls.ProgressBarLabelPlacement;
    import mx.controls.ProgressBarMode;
    import mx.managers.PopUpManager;

    import spark.components.Button;
    import spark.components.HGroup;
    import spark.components.Panel;
    import spark.components.Window;

    // Add ModelDialog super class that disables buttons on parent window, keeps window in front, and calls PopUpManager.

    // Generic progress dialog that can be used for file read/write progress as well as time-consuming calculations.
    public class ProgressDialog extends Panel
    {
        private var _progressBar:ProgressBar;

        // Popup constructor
        // This is kind of silly, arguments are almost identical to the normal constructor--a wrapper class might be better
        public static function show(owner:Window, title:String, label:String, cancelable:Boolean = false, indeterminate:Boolean = false):ProgressDialog
        {
            var modal:Boolean = true;
            var progressDialog:ProgressDialog = new ProgressDialog(title, label, cancelable, indeterminate);

            PopUpManager.addPopUp(progressDialog, owner, modal);
            PopUpManager.centerPopUp(progressDialog);
            return(progressDialog);
        }

        public function ProgressDialog(title:String, label:String, cancelable:Boolean = false, indeterminate:Boolean = false)
        {
            super();

            this.title = title;
            this.height = 74;

            var group:HGroup = new HGroup;
            group.percentHeight = 100;
            group.verticalAlign = "middle";
            group.paddingLeft = 10;
            group.paddingRight = 10;
            this.addElement(group);

            this._progressBar = new ProgressBar;
            this._progressBar.label = label;
            this._progressBar.labelPlacement = ProgressBarLabelPlacement.CENTER;
            this._progressBar.mode = ProgressBarMode.MANUAL;
            this._progressBar.indeterminate = indeterminate;
            group.addElement(this._progressBar);

            if (cancelable)
            {
                var cancelButton:Button = new Button;
                cancelButton.label = "Cancel";
                cancelButton.addEventListener(MouseEvent.CLICK, handleCancel);
                group.addElement(cancelButton);
            }
        }

        private function handleCancel(event:MouseEvent):void
        {
            trace("ProgressDialog.handleCancel");
            // Instead of closing the dialog here, dispatch event and let owner window decide.
            // Owner might want to prompt "Are you sure you want to cancel?".
            this.dispatchEvent(new Event(Event.CANCEL));
        }

        public function setProgress(completed:Number, total:Number):void
        {
            this._progressBar.setProgress(completed, total);
        }

        public function close():void
        {
            PopUpManager.removePopUp(this);  // a little weird having this here; could have entered using 'new' not 'show'
        }
    }
}
