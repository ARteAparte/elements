/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements.views {

import spark.components.Group;
import spark.components.HGroup;
import spark.components.Label;
import spark.components.VGroup;

import dse.DataSet;
import dse.MetaData;

import elements.DataDisplayOptions;
import elements.DocEvents;
import elements.DocHandlers;
import elements.DocState;

import gui.AcmeManagedRef;
import gui.IManagedRef;

import pdt.IPMap;

import plugins.DateAndTime;
import plugins.Location;

import units.NoUnits;
import units.UnitConversions;
import units.UnitInfo;
import units.UnitSystems;

import utils.FormatUtils;

public class DataHeaderGroup extends Group {
  private var _docState:IManagedRef;
  /* TODO: The header needs to "self-build" depending
  on what things the community member would like to see
  (which can be specified in preferences, presumably).
  For now, this is hard-coded */
  private var _siteNameLabelText:Label;
  private var _siteNameLabel:Label;
  private var _latitudeLabelText:Label;
  private var _latitudeLabel:Label;
  private var _longitudeLabelText:Label;
  private var _longitudeLabel:Label;
  private var _timeZoneLabelText:Label;
  private var _timeZoneLabel:Label;
  private var _elevationLabelText:Label;
  private var _elevationLabel:Label;
  private var _typeConversions:Object;

  public function DataHeaderGroup(docState:IManagedRef) {
    super();
    this._docState = docState;
    this._docState.registerSubscriberFunction(_onStateUpdate);
    var myds:DocState = DocHandlers.readDocState(_docState);
    var us:String = myds.displayUnitSystem;
    //trace('DataHeaderGroup :: Unit System = ', us);
    var vg:VGroup = makeVGroup();
    var hg1:HGroup = makeHGroup();
    var hg2:HGroup = makeHGroup();
    var hg3:HGroup = makeHGroup();
    /* TODO: Ideally, hook these up to be able to specify units --
    or have unit interpretation in the dialogue?
    TODO: Hook the SeriesMeta in to also work for the data header
    labels, too, so that they can have unit conversions, etc. */
    _siteNameLabelText = new Label();
    _siteNameLabel =
      addInputPair(hg1, "Site Name:", 400, Location.CITY,
      _siteNameLabelText);
    _latitudeLabelText = new Label();
    _latitudeLabel = addInputPair(hg2, "Latitude [deg]:", 50,
      Location.LATITUDE_deg, _latitudeLabelText);
    _longitudeLabelText = new Label();
    _longitudeLabel = addInputPair(hg2, "Longitude [deg]:", 50,
      Location.LONGITUDE_deg, _longitudeLabelText);
    _timeZoneLabelText = new Label();
    _timeZoneLabel = addInputPair(hg3, "Time Zone:", 50,
      DateAndTime.TIME_ZONE, _timeZoneLabelText);
    _elevationLabelText = new Label();
    _elevationLabel = addInputPair(hg3, "Elevation [m]:", 50,
      Location.ELEVATION_m, _elevationLabelText);
    if (us == UnitSystems.IP) {
      updateLabel(_elevationLabelText, _elevationLabel, Location.ELEVATION_m,
        "Elevation", true);
    }
    vg.addElement(hg1);
    vg.addElement(hg2);
    vg.addElement(hg3);
    this.addElement(vg);
    /* TODO: This needs to go somewhere else -- ideally, this should
    be part of the SeriesMeta (what type of data is it -- string,
    number, sequence, etc.) and the header should self-build here
    depending on the data in the header. */
    _typeConversions = new Object();
    _typeConversions[Location.CITY] = String;
    _typeConversions[Location.LATITUDE_deg] = Number;
    _typeConversions[Location.LONGITUDE_deg] = Number;
    _typeConversions[DateAndTime.TIME_ZONE] = Number;
    _typeConversions[Location.ELEVATION_m] = Number;
    _onStateUpdate(DocEvents.EVENT_ALL_CHANGED);
  }
  public function _onStateUpdate(topic:String):void {
    //trace('DataHeaderGroup._onStateUpdate('+topic+')');
    switch (topic) {
      case DocEvents.EVENT_HEADER_CHANGED:
      case DocEvents.EVENT_ALL_CHANGED:
      case AcmeManagedRef.EVENT_STATE_CHANGED:
        //trace('case :', topic);
        updateLabel(_elevationLabelText, _elevationLabel,
          Location.ELEVATION_m, "Elevation", true);
        updateLabel(_latitudeLabelText, _latitudeLabel, Location.LATITUDE_deg,
          "Latitude", false);
        updateLabel(_longitudeLabelText, _longitudeLabel,
          Location.LONGITUDE_deg, "Longitude", false);
        updateLabel(_siteNameLabelText, _siteNameLabel, Location.CITY,
          "Site Name");
        updateLabel(_timeZoneLabelText, _timeZoneLabel, DateAndTime.TIME_ZONE,
          "Time Zone");
        break;
      case DocEvents.EVENT_DISPLAY_UNITS_CHANGED:
        updateLabel(_elevationLabelText, _elevationLabel, Location.ELEVATION_m,
          "Elevation", true);
      default:
        // trace?
        break;
    }
  }
  private function makeVGroup():VGroup {
    var vg:VGroup = new VGroup();
    /*
    vg.paddingBottom = 12;
    vg.paddingTop = 10;
    vg.paddingLeft = 10;
    vg.paddingRight = 10;
    vg.percentHeight = 100;
    vg.percentWidth = 100;
    */
    return vg;
  }
  private function makeHGroup():HGroup {
    var hg:HGroup = new HGroup();
    hg.percentWidth = 100;
    hg.verticalAlign = "middle";
    return hg;
  }
  private function
    addInputPair(gr:Group, labelText:String, width:int,
                 headerAttribute:String, label:Label=null):Label {
    //trace('DataHeaderGroup.addInputPair');
    if (label == null) label = new Label();
    label.text = labelText;
    label.width = 120;
    gr.addElement(label);
    var val:Label = new Label();
    var docState:DocState = DocHandlers.readDocState(_docState);
    var dataSet:DataSet = docState.dataSet;
    val.text = dataSet.headerValueFor(headerAttribute);
    val.width = width;
    gr.addElement(val);
    return val;
  }
  private function updateLabel(txt:Label, lab:Label, headerAttr:String,
                               msg:String, convertUnits:Boolean=true):void {
    //trace('DataHeaderGroup :: updateLabel');
    var docState:DocState = DocHandlers.readDocState(_docState);
    var dataSet:DataSet = docState.dataSet;
    var hdr:Object = dataSet.header;
    var hmd:Object = dataSet.headerMetaData;
    if (hdr.hasOwnProperty(headerAttr)) {
      var val:* = hdr[headerAttr];
      var kind:String;
      var md:MetaData = null;
      if (hmd.hasOwnProperty(headerAttr)) {
        md = hmd[headerAttr] as MetaData;
        kind = md.dataType;
      } else {
        kind = MetaData.DATA_TYPE__STRING;
      }
      var viewOptMap:IPMap = docState.displayOptionsMap;
      var precision:int;
      if (viewOptMap.containsKey(headerAttr)) {
        var viewOpts:DataDisplayOptions =
          viewOptMap[headerAttr] as DataDisplayOptions;
        precision = viewOpts.precision;
      } else {
        precision = DataDisplayOptions.DEFAULT_PRECISION;
      }
      if ((md != null) && (md.baseUnits != NoUnits.NONE) && convertUnits) {
        var qom:String = md.quantityOfMeasure;
        var us:String = docState.displayUnitSystem;
        var targetUnit:String = UnitInfo.unitsBySystemAndQOM(us, qom);
        var tag:String = UnitConversions.tag(md.baseUnits, targetUnit);
        var f:Function;
        if (UnitConversions.CONVERT.hasOwnProperty(tag)) {
          f = UnitConversions.CONVERT[tag] as Function;
        } else {
          f = function(n:Number):Number { return n; };
        }
        val = f(val as Number);
        txt.text = msg + " [" + targetUnit + "]:";
      } else {
        if ((md != null) && (md.baseUnits != NoUnits.NONE)) {
          txt.text = msg + " [" + md.baseUnits + "]:";
        } else {
          txt.text = msg + ":";
        }
      }
      if (kind == MetaData.DATA_TYPE__NUMBER) {
        val = Number(val);
        lab.text = FormatUtils.numberToFormattedString(val, precision);
      } else {
        lab.text = val.toString();
      }
    } else {
      lab.text = "";
      txt.text = msg + ":";
    }
  }
}
}