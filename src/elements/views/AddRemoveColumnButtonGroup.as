/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements.views {
import flash.display.DisplayObject;
import flash.display.DisplayObjectContainer;
import flash.events.Event;
import flash.events.MouseEvent;

import mx.collections.ArrayCollection;
import mx.collections.IList;
import mx.managers.PopUpManager;

import spark.components.Button;
import spark.components.DropDownList;
import spark.components.Group;
import spark.components.HGroup;
import spark.components.Label;

import dse.MetaData;
import dse.SeriesSelection;

import elements.AppHandlers;
import elements.DocEvents;
import elements.DocHandlers;
import elements.DocState;
import elements.commands.AppSetModal;
import elements.commands.DocAddDataColumn;
import elements.commands.DocRemoveDataColumn;
import elements.commands.DocSetSelections;
import elements.commands.DocShiftSeriesColumnTowardsHead;
import elements.commands.DocShiftSeriesColumnTowardsTail;

import gui.IManagedRef;

import pdt.PersistentVector;

import plugins.DateAndTime;
import plugins.PlugInfo;

import utils.ArrayUtils;
import utils.MapUtils;

public class AddRemoveColumnButtonGroup extends Group {
    public var moveLeftButton:Button;
    public var addButton:Button;
    public var hideButton:Button;
    public var moveRightButton:Button;
    public var seriesSelectMenu:DropDownList;
    private var _docSC:IManagedRef;
    private var _seriesTags:ArrayCollection;
    public var _seriesHumanNames:ArrayCollection;
    public var _tagToNameMap:Object;

    public function AddRemoveColumnButtonGroup(docSC:IManagedRef) {
        super();
        _docSC = docSC;
        var columnsLabel:Label = new Label();
        columnsLabel.text = "Columns:";
        columnsLabel.styleName = "heading";
        columnsLabel.height = 10;
        moveLeftButton = new Button();
        moveLeftButton.label = "Move Left";
        moveLeftButton.addEventListener(
            MouseEvent.CLICK, moveColumnToTheLeftHandler);
        addButton = new Button();
        addButton.label = "Add";
        addButton.addEventListener(MouseEvent.CLICK, addColumnHandler);
        hideButton = new Button();
        hideButton.label = "Remove";
        hideButton.addEventListener(
            MouseEvent.CLICK, removeColumnHandler);
        var spacer:HGroup = new HGroup();
        spacer.width = 5;
        moveRightButton = new Button();
        moveRightButton.label = "Move Right";
        moveRightButton.addEventListener(
            MouseEvent.CLICK, moveColumnToTheRightHandler);
        seriesSelectMenu = new DropDownList();
        seriesSelectMenu.width = 160;
        var seriesMeta:Object = PlugInfo.knownSeriesMetaData();
        var attrs:Array = PlugInfo.allDisplayableNumericSeries();
        refreshTags();
        var hg:HGroup = ViewUtils.makeHGroup();
        hg.addElement(columnsLabel);
        hg.addElement(this.addButton);
        hg.addElement(this.hideButton);
        hg.addElement(spacer);
        hg.addElement(this.moveLeftButton);
        hg.addElement(this.moveRightButton);
        this.addElement(hg);
        _docSC.registerSubscriberFunction(update);
    }
    public function refreshTags():void {
      var attrs:Array = PlugInfo.allDisplayableNumericSeries();
      _seriesTags = new ArrayCollection();
      _seriesHumanNames = new ArrayCollection();
      var ds:DocState = DocHandlers.readDocState(_docSC);
      var shown:Array = ds.shownDataAsArray;
      for each (var attr:String in attrs) {
        if (ArrayUtils.contains(shown, attr)) continue;
        _seriesTags.addItem(attr);
        var md:MetaData = ds.dataSet.metaDataFor(attr);
        _seriesHumanNames.addItem(md.fullName);
      }
      _tagToNameMap = MapUtils.makeFromKeysAndVals(
        _seriesHumanNames.source, _seriesTags.source);
      _seriesHumanNames.source = _seriesHumanNames.source.sort();
      seriesSelectMenu.dataProvider = _seriesHumanNames;
      seriesSelectMenu.selectedIndex = 0;
    }
    private function greatGrandParent():DisplayObjectContainer {
      return this.parent.parent.parent;
    }
    private function addColumnHandler(e:Event):void {
      var ds:DocState = DocHandlers.readDocState(_docSC);
      var hasData:Boolean = DocHandlers.hasData(ds);
      if (!hasData) return;
      var nl:NameList;
      var onAddOK:Function = function(e:Event):void {
        var idx:int = nl.value;
        if (idx >= 0) {
          //trace('adding column');
          var tag:String = _seriesHumanNames.getItemAt(idx) as String;
          var seriesName:String = _tagToNameMap[tag] as String;
          //trace('SeriesName is ', seriesName);
          //trace('Column ', seriesName, ' selected.');
          _docSC.handleCommand(new DocAddDataColumn(seriesName));
        } else {
          //trace('not adding column');
        }
        PopUpManager.removePopUp(nl);
        AppHandlers.instance().handleCommand(new AppSetModal(false));
        _docSC.handleCommand(new DocSetSelections(ds.selections));
      };
      var onAddCancel:Function = function(e:Event):void {
        PopUpManager.removePopUp(nl);
        AppHandlers.instance().handleCommand(new AppSetModal(false));
        _docSC.handleCommand(new DocSetSelections(ds.selections));
      };
      nl = new NameList(_seriesHumanNames as IList, onAddOK, onAddCancel);
      nl.title = 'Add Column';
      AppHandlers.instance().handleCommand(new AppSetModal(true));
      PopUpManager.addPopUp(nl, this.parentDocument as DisplayObject, true);
      PopUpManager.centerPopUp(nl);
      //nl.validateNow();
    }

    private function removeColumnHandler(e:Event):void {
        var ds:DocState = DocHandlers.readDocState(_docSC);
        if (!DocHandlers.hasData(ds)) return;
        if (!SeriesSelection.onlyOneSeriesSelected(ds.selections)) return;
        var selected:String = ds.selectionN(0).selectedSeries;
        if (selected == DateAndTime.DATE_TIME) return;
        _docSC.handleCommand(new DocRemoveDataColumn(selected));
        _docSC.handleCommand(new DocSetSelections(PersistentVector.EMPTY));
    }

    private function moveColumnToTheRightHandler(e:Event):void {
        var ds:DocState = DocHandlers.readDocState(_docSC);
        if (!DocHandlers.hasData(ds)) return;
        if (!SeriesSelection.onlyOneSeriesSelected(ds.selections)) return;
        var selected:String = ds.selectionN(0).selectedSeries;
        _docSC.handleCommand(new DocShiftSeriesColumnTowardsTail(selected));
        _docSC.handleCommand(new DocSetSelections(ds.selections));
    }

    private function moveColumnToTheLeftHandler(e:Event):void {
        var ds:DocState = DocHandlers.readDocState(_docSC);
        if (!DocHandlers.hasData(ds)) return;
        if (!SeriesSelection.onlyOneSeriesSelected(ds.selections)) return;
        var selected:String = ds.selectionN(0).selectedSeries;
        _docSC.handleCommand(new DocShiftSeriesColumnTowardsHead(selected));
    }

    public function update(event:String):void {
      var ds:DocState = DocHandlers.readDocState(_docSC);
      var md:MetaData = ds.metaDataForSelectedSeries;

      hideButton.enabled = false;
      moveLeftButton.enabled = false;
      moveRightButton.enabled = false;

      if (event == DocEvents.EVENT_DISPLAYED_DATA_COLUMNS_CHANGED) {
        trace('refreshing tags');
        refreshTags();
      } else if (event == DocEvents.EVENT_REQUEST_ADD_COLUMN) {
        addColumnHandler(new Event(''));
      } else if (event == DocEvents.EVENT_SELECTION_CHANGED) {
        if (SeriesSelection.onlyOneSeriesSelected(ds.selections) && ds.selectionN(0).numberOfValues == 8760 && md.name != "dateTime") {
          // WARNING: Checking the hard value '8760' will break if files can have a variable number of rows.
          hideButton.enabled = true;
          moveLeftButton.enabled = true;
          moveRightButton.enabled = true;
        }
      }
    }
}
}