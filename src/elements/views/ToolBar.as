/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements.views {
  import flash.display.DisplayObject;
  import flash.display.DisplayObjectContainer;
  import flash.events.Event;
  import flash.events.MouseEvent;

  import mx.events.CloseEvent;
  import mx.managers.PopUpManager;

  import spark.components.Button;
  import spark.components.Group;
  import spark.components.HGroup;
  import spark.components.Label;
  import spark.components.TitleWindow;

  import dse.MetaData;
  import dse.SeriesSelection;

  import elements.AppHandlers;
  import elements.DataDisplayOptions;
  import elements.DocEvents;
  import elements.DocHandlers;
  import elements.DocState;
  import elements.NormWizData;
  import elements.commands.AppSetModal;
  import elements.commands.DocNormalizeSelected;
  import elements.commands.DocNormalizeSelectedTemperature;
  import elements.commands.DocOffsetSelected;
  import elements.commands.DocScaleSelected;
  import elements.commands.DocSetSelection;
  import elements.components.ModalSumDialogBox;
  import elements.components.NormWizardSolar;
  import elements.components.NormalizationWizard;

  import gui.IManagedRef;

  import plugins.PlugInfo;

  import units.IrradianceUnits;
  import units.IrradiationUnits;
  import units.TemperatureUnits;
  import units.UnitConversions;

  import utils.ArrayUtils;

  public class ToolBar extends Group {
    private var _docSC:IManagedRef;
    private var _offsetButton:Button;
    private var _scaleButton:Button;
    private var _normalizeButton:Button;
    private var _normMonthlyButton:Button;

    private var _win:BaseWindow;

    public function ToolBar(docState:IManagedRef) {
      super();
      this._docSC = docState;
      docState.registerSubscriberFunction(update);
      //this.layout = new HorizontalLayout();
      var hg:HGroup = ViewUtils.makeHGroup();
      var toolsLabel:Label = new Label();
      toolsLabel.text = "Tools:";
      toolsLabel.styleName = "heading";
      toolsLabel.height = 10;
      hg.addElement(toolsLabel);
      _offsetButton = new Button();
      _offsetButton.label = "Offset";
      _offsetButton.addEventListener(MouseEvent.CLICK, offsetHandler);
      hg.addElement(_offsetButton);
      _scaleButton = new Button();
      _scaleButton.label = "Scale";
      _scaleButton.addEventListener(MouseEvent.CLICK, scaleHandler);
      hg.addElement(_scaleButton);
      _normalizeButton = new Button();
      _normalizeButton.label = "Normalize";
      _normalizeButton.addEventListener(MouseEvent.CLICK, normalizeHandler);
      hg.addElement(_normalizeButton);
      _normMonthlyButton = new Button();
      _normMonthlyButton.label = "Normalize By Month";
      _normMonthlyButton.addEventListener(MouseEvent.CLICK, normalizeMonthlyHandler);
      hg.addElement(_normMonthlyButton);
      this.addElement(hg);
      _win = null;
    }
    private function greatGreatGrandParent():DisplayObjectContainer {
      return this.parent.parent.parent;
    }
    private function normalizeMonthlyHandler(event:Event):void {
      //trace('normalizeMonthlyHandler');
      var ds:DocState = DocHandlers.readDocState(_docSC);
      if (!DocHandlers.hasData(ds)) return;
      // Only acts on one time series so only works if a single column is
      // selected. Otherwise, nothing happens.
      if (!SeriesSelection.onlyOneSeriesSelected(ds.selections)) return;
      if (!ArrayUtils.contains(PlugInfo.allDisplayableNumericSeries(),
        ds.selectionN(0).selectedSeries)) return;
      //trace('ToolBar.normalizeHandler :: selectedSeries => '+
      //  ds.selectionN(0).selectedSeries);
      var name:String = ds.selectionN(0).selectedSeries;
      var md:MetaData = ds.dataSet.metaDataFor(name);
      var appSC:IManagedRef = AppHandlers.instance();
      var dialogue:TitleWindow = new TitleWindow();
      dialogue.title = "Normalize By Month";
      dialogue.width = 720;
      dialogue.height = 500;
      AppHandlers.instance().handleCommand(new AppSetModal(true));
      var onCompleteFn:Function = function():void {
          PopUpManager.removePopUp(dialogue);
          AppHandlers.instance().handleCommand(new AppSetModal(false));
          _docSC.handleCommand(new DocSetSelection(ds.selectionN(0)));
      };
      var config:NormWizData = DocHandlers.getScaleByMonthPanelData(ds);
      if ((md.quantityOfMeasure == IrradianceUnits.QUANTITY_OF_MEASURE) ||
          (md.quantityOfMeasure == IrradiationUnits.QUANTITY_OF_MEASURE)) {
        var solarNormWiz:NormWizardSolar = new NormWizardSolar();
        dialogue.addElement(solarNormWiz);
        dialogue.addEventListener(CloseEvent.CLOSE, solarNormWiz.onComplete);
        PopUpManager.addPopUp(
          dialogue, this.parentDocument as DisplayObject, true);
        PopUpManager.centerPopUp(dialogue);
        dialogue.validateNow();
        solarNormWiz.init(config, _docSC, onCompleteFn);
      } else {
        var normWiz:NormalizationWizard = new NormalizationWizard();
        dialogue.addElement(normWiz);
        dialogue.addEventListener(CloseEvent.CLOSE, normWiz.onComplete);
        PopUpManager.addPopUp(
          dialogue, this.parentDocument as DisplayObject, true);
        PopUpManager.centerPopUp(dialogue);
        dialogue.validateNow();
        normWiz.init(config, _docSC, onCompleteFn);
      }
    }
    private function offsetHandler(event:Event):void {
      var ds:DocState = DocHandlers.readDocState(_docSC);
      if (!SeriesSelection.onlyOneSeriesSelected(ds.selections)) return;
      if (!ArrayUtils.contains(PlugInfo.allDisplayableNumericSeries(),
        ds.selectionN(0).selectedSeries)) return;
      //trace('ToolBar.normalizeHandler :: selectedSeries => '+
      //  ds.selectionN(0).selectedSeries);
      modalLaunch('Offset', 'Offset Selected', function (value:Number):void {
        _docSC.handleCommand(new DocOffsetSelected(value));
      });
    }
    private function scaleHandler(event:Event):void {
      var ds:DocState = DocHandlers.readDocState(_docSC);
      if (!SeriesSelection.onlyOneSeriesSelected(ds.selections)) return;
      if (!ArrayUtils.contains(PlugInfo.allDisplayableNumericSeries(),
        ds.selectionN(0).selectedSeries)) return;
      //trace('ToolBar.normalizeHandler :: selectedSeries => '+
      //  ds.selectionN(0).selectedSeries);
      modalLaunch('Scale Factor', 'Scale Selected',
        function (value:Number):void {
          _docSC.handleCommand(new DocScaleSelected(value));
        });
    }
    private function getDisplayUnits(dataField:String):String {
      var ds:DocState = DocHandlers.readDocState(_docSC);
      return ds.displayUnitsFor(dataField);
    }
    private function getSeriesMeta(dataField:String):MetaData {
      var ds:DocState = DocHandlers.readDocState(_docSC);
      return ds.dataSet.metaDataFor(dataField);
    }
    private function getDataDisplayOptions(field:String):DataDisplayOptions {
      var ds:DocState = DocHandlers.readDocState(_docSC);
      return ds.displayOptionsFor(field);
    }
    private function getDisplayToBaseConverter(field:String):Function {
      var sm:MetaData = getSeriesMeta(field);
      var displayUnits:String = getDisplayUnits(field);
      var ddo:DataDisplayOptions = getDataDisplayOptions(field);
      return function (displayVal:Number):Number {
        return UnitConversions.displayToBase(displayVal, displayUnits,
          ddo.scale, sm.baseUnits);
      };
    }
    private function normalizeHandler(event:Event):void {
      var ds:DocState = DocHandlers.readDocState(_docSC);
      if (!SeriesSelection.onlyOneSeriesSelected(ds.selections)) return;
      if (!ArrayUtils.contains(PlugInfo.allDisplayableNumericSeries(),
        ds.selectionN(0).selectedSeries)) return;
      //trace('ToolBar.normalizeHandler :: selectedSeries => '+
      //  ds.selectionN(0).selectedSeries);
      var md:MetaData = ds.dataSet.metaDataFor(ds.selectionN(0).selectedSeries);
      var qom:String = md.quantityOfMeasure;
      // NORMALIZE TEMPERATURE
      if (qom == TemperatureUnits.QUANTITY_OF_MEASURE)
      {
        modalLaunch(
          'Average',
          'Normalize Selected',
          function (value:Number):void
          {
            var ss:SeriesSelection = ds.selectionN(0);
            var ucf:Function = getDisplayToBaseConverter(ss.selectedSeries);
            var valueInBaseUnits:Number = ucf(value);
            _docSC.handleCommand(
              new DocNormalizeSelectedTemperature(valueInBaseUnits));
          }
        );
      }
      // NORMALIZE SUM (SOLAR)
      else if ((qom == IrradiationUnits.QUANTITY_OF_MEASURE) ||
               (qom == IrradianceUnits.QUANTITY_OF_MEASURE))
      {
        modalLaunchSum(
          'Normalize Selected',
          function (value:Number):void
          {
            _docSC.handleCommand(new DocScaleSelected(value));
          },
          function (value:Number):void
          {
            _docSC.handleCommand(new DocOffsetSelected(value));
          }
        );
      }
      else
      {
        modalLaunch(
          'Average',
          'Normalize Selected',
          function (value:Number):void
          {
            var ss:SeriesSelection = ds.selectionN(0);
            var ucf:Function = getDisplayToBaseConverter(ss.selectedSeries);
            var valueInBaseUnits:Number = ucf(value);
            _docSC.handleCommand(new DocNormalizeSelected(valueInBaseUnits));
          }
        );
      }
    }
    private function modalLaunch(
      labelText:String, winTitle:String, runFunc:Function):void
    {
      var ds:DocState = DocHandlers.readDocState(_docSC);
      if (!SeriesSelection.onlyOneSeriesSelected(ds.selections)) return;
      var label:Label = new Label();
      label.text = labelText;
      var dialogue:ModalDialogBox;
      var onOK:Function = function(event:Event):void {
        var displayVal:Number = parseFloat(dialogue.value);
        if (!isNaN(displayVal)) {
          runFunc(displayVal);
        }
        PopUpManager.removePopUp(dialogue);
        AppHandlers.instance().handleCommand(new AppSetModal(false));
        //_docSC.handleRequest(
        //  DocHandlers.RQST_SET_SELECTION,
        //  {   selection: ds.selectionN(0)});
      };
      var onCancel:Function = function(event:Event):void {
        PopUpManager.removePopUp(dialogue);
        AppHandlers.instance().handleCommand(new AppSetModal(false));
        //_docSC.handleRequest(
        //  DocHandlers.RQST_SET_SELECTION,
        //  {   selection: ds.selectionN(0)});
      };
      dialogue = new ModalDialogBox(label, winTitle, onOK, onCancel);
      AppHandlers.instance().handleCommand(new AppSetModal(true));
      PopUpManager.addPopUp(
        dialogue, this.parentDocument as DisplayObject, true);
      PopUpManager.centerPopUp(dialogue);
      //dialogue.validateNow();
    }
    private function
    modalLaunchSum(winTitle:String, scaleFn:Function, offsetFn:Function):void {
      var ds:DocState = DocHandlers.readDocState(_docSC);
      if (!SeriesSelection.onlyOneSeriesSelected(ds.selections)) return;
      var dialogue:ModalSumDialogBox;
      var onOK:Function = function(event:Event):void {
        var scale:Number = dialogue.scaleValue;
        if (!isNaN(scale)) {
          if (dialogue._sum == 0.0) {
            var offset:Number = dialogue.offsetValue;
            if (!isNaN(offset)) {
              offsetFn(offset);
            }
          } else {
            scaleFn(scale);
          }
        }
        PopUpManager.removePopUp(dialogue);
        AppHandlers.instance().handleCommand(new AppSetModal(false));
      };
      var onCancel:Function = function(event:Event):void {
        PopUpManager.removePopUp(dialogue);
        AppHandlers.instance().handleCommand(new AppSetModal(false));
      };
      dialogue = new ModalSumDialogBox();
      dialogue.dsRef = _docSC;
      dialogue.onOK = onOK;
      dialogue.onCancel = onCancel;
      dialogue.title = "Normalize Selected";
      AppHandlers.instance().handleCommand(new AppSetModal(true));
      PopUpManager.addPopUp(
        dialogue, this.parentDocument as DisplayObject, true);
      PopUpManager.centerPopUp(dialogue);
      //dialogue.validateNow();
    }
    private function update(topic:String):void {
      var ds:DocState = DocHandlers.readDocState(_docSC);
      var md:MetaData = ds.metaDataForSelectedSeries;

      _offsetButton.enabled = false;
      _scaleButton.enabled = false;
      _normalizeButton.enabled = false;
      _normMonthlyButton.enabled = false;

      if (topic == DocEvents.EVENT_REQUEST_OFFSET) {
        offsetHandler(new Event(''));
      } else if (topic == DocEvents.EVENT_REQUEST_SCALE) {
        scaleHandler(new Event(''));
      } else if (topic == DocEvents.EVENT_REQUEST_NORMALIZE) {
        normalizeHandler(new Event(''));
      } else if (topic == DocEvents.EVENT_REQUEST_NORMALIZE_MONTHLY) {
        normalizeMonthlyHandler(new Event(''));
      } else if (topic == DocEvents.EVENT_SELECTION_CHANGED) {
        if (SeriesSelection.onlyOneSeriesSelected(ds.selections) && md.name != "dateTime") {
          _offsetButton.enabled = true;
          _scaleButton.enabled = true;
          _normalizeButton.enabled = true;
        }
        if (SeriesSelection.onlyOneSeriesSelected(ds.selections) && ds.selectionN(0).numberOfValues == 8760 && md.name != "dateTime") {
          _normMonthlyButton.enabled = true;
        }
      }
    }
  }
}
