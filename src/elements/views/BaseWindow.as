/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements.views
{
import flash.desktop.NativeApplication;
import flash.display.NativeWindow;
import flash.events.Event;
import flash.events.KeyboardEvent;
import flash.events.NativeWindowBoundsEvent;
import flash.filesystem.File;
import flash.ui.Keyboard;

import mx.controls.Alert;
import mx.core.IVisualElement;
import mx.events.AIREvent;
import mx.events.CloseEvent;

import spark.components.Window;

import elements.AppEvents;
import elements.AppHandlers;
import elements.AppState;
import elements.DocEvents;
import elements.DocHandlers;
import elements.DocState;
import elements.ICopyPaste;
import elements.commands.AppCloseActiveWin;
import elements.commands.AppCloseWindow;
import elements.commands.AppSetActiveWindow;
import elements.commands.DocSaveFile;

import gui.AcmeManagedRef;
import gui.IManagedRef;

public class BaseWindow extends Window
{
  public static const DEFAULT_WIDTH:int = 1200;
  public static const DEFAULT_HEIGHT:int = 600;
  public static const RESULT_CANCEL:String = 'result_cancel';
  public static const RESULT_CLOSE:String = 'result_close';
  public static const IDLE:String = "idle";
  public static const OPENING:String = "opening";
  public static const SAVING:String = "saving";
  public static const CLOSING:String = "closing";
  public static const EXITING:String = "exiting";
  public var docStateCtrlr:IManagedRef;

  public var checkClose:Boolean;
  public var _menuBar:MenuBar;
  public var _pendingAction:String;
  public var _title:String;
  public var _pretitle:String;
  public var _parentWindow:BaseWindow;
  public var _onCloseFn:Function;
  public var _copyPaste:ICopyPaste;

  public function BaseWindow(
    sc:IManagedRef=null,
    view:IVisualElement=null,
    openWindowActive:Boolean=true,
    resizable:Boolean=true,
    minimizable:Boolean=true,
    maximizable:Boolean=true,
    width:int=DEFAULT_WIDTH,
    height:int=DEFAULT_HEIGHT,
    minWidth:int=DEFAULT_WIDTH,
    minHeight:int=DEFAULT_HEIGHT,
    parentWindow:BaseWindow=null,
    pretitle:String='')
  {
    //trace('BaseWindow.DocumentWindow');
    super();
    this.minimizable = minimizable;  // Must be set before 'open'
    this.maximizable = maximizable;  // Must be set before 'open'
    this.resizable = resizable;  // Must be set before 'open'
    if (!resizable) {
      this.maximizable = false;  // Must be set false or resize will still be allowed
    }
    this.showStatusBar = false;  // Apparently must be before 'open'
    this.addEventListener(AIREvent.WINDOW_ACTIVATE, handleWindowActivate);
    this.addEventListener(AIREvent.WINDOW_DEACTIVATE, handleWindowDeactivate);
    open(openWindowActive);
    this.checkClose = true;
    _pendingAction = IDLE;
    var appSC:IManagedRef = AppHandlers.instance();
    var as0:AppState = AppHandlers.readAppState(appSC);
    _menuBar = as0.menuBar;
    if (NativeWindow.supportsMenu) {
      this.nativeWindow.menu = this._menuBar.menu;
    }
    docStateCtrlr = sc;
    if (docStateCtrlr != null) {
      docStateCtrlr.registerSubscriberFunction(update);
    } else {
      //trace('BaseWindow: null docStateCtrlr');
    }
    this.width = width;
    this.height = height;
    this.minWidth = Math.min(width, minWidth);
    this.minHeight = Math.min(height, minHeight);
    if (view != null)
    {
      addElement(view);
    }
    else
    {
      //trace('BaseWindow: null view');
    }
    updateTitle();
    addEventListener(Event.CLOSING, _handleClosing);
    addEventListener(Event.EXITING, _handleClosing);
    addEventListener(Event.RESIZE, _handleResize);
    this.nativeWindow.addEventListener(
      NativeWindowBoundsEvent.MOVE, _handleMove);
    //this.addEventListener(KeyboardEvent.KEY_DOWN, _handleKeys);
    this._parentWindow = parentWindow;
    this._pretitle = pretitle;
    this._copyPaste = null;
  }
  public function _handleMove(e:Event):void
  {
    AppHandlers.instance().notifySubscribers(
      AppEvents.EVENT_WINDOW_MOVED);
  }
  public function _handleResize(e:Event):void
  {
    AppHandlers.instance().notifySubscribers(
      AppEvents.EVENT_WINDOW_RESIZED);
  }
  public function _handleKeys(e:KeyboardEvent):void
  {
    var ctrl:Boolean = e.ctrlKey || e.commandKey;
    if (ctrl && (e.keyCode == Keyboard.C))
    {
      trace('BaseWindow._handleKeys :: CTRL+C');
      if (_copyPaste == null) return;
      _copyPaste.doCopy();
    }
    else if(ctrl && (e.keyCode == Keyboard.V))
    {
      trace('BaseWindow._handleKeys :: CTRL+V');
      if (_copyPaste == null) return;
      _copyPaste.doPaste();
    }
    else if (ctrl && (e.keyCode == Keyboard.W))
    {
      trace('BaseWindow._handleKeys :: CTRL+W');
      _handleClosing(e);
    }
  }
  public function get tag():String
  {
    if (this.docStateCtrlr == null) return this.title;
    // need to special case Charts so we don't have two "window types"
    // that are keying off of the same file path
    if (this.title.indexOf("Chart for") == 0) return this.title;
    var ds:DocState = DocHandlers.readDocState(this.docStateCtrlr);
    if (ds.file == null) return this.title;
    if (!ds.file.exists) return this.title;
    return ds.file.nativePath;
  }
  public function set copyPaste(cp:ICopyPaste):void
  {
    this._copyPaste = cp;
  }
  public function get copyPaste():ICopyPaste
  {
    return this._copyPaste;
  }
  public function set parentWindow(w:BaseWindow):void
  {
    //trace('BaseWindow: Parent window null?', w == null);
    _parentWindow = w;
  }
  public function set onCloseFn(fn:Function):void
  {
    _onCloseFn = fn;
  }
  public function get parentWindow():BaseWindow
  {
    return _parentWindow;
  }
  override public function set title(t:String):void
  {
    super.title = this._pretitle + t;
  }
  private function handleWindowActivate(e:Event):void
  {
    //trace('BaseWindow.handleWindowActivate');
    var appSC:IManagedRef = AppHandlers.instance();
    appSC.handleCommand(new AppSetActiveWindow(this));
  }
  private function handleWindowDeactivate(e:Event):void
  {
    //trace('BaseWindow.handleWindowDeactivate');
  }
  private function update(event:String):void
  {
    //trace('BaseWindow.update');
    switch (event)
    {
      //case AppHandlers.EVENT_ACTIVE_WINDOW_CHANGED:
      //  var appRef:IManagedRef = AppHandlers.instance();
      //  var appSt:AppState = AppHandlers.readAppState(appRef);
      //  if (appSt.activeWindow === this) {
      //    this.activate();
      //  }
      //  break;
      case DocEvents.EVENT_HEADER_CHANGED:
      case DocEvents.EVENT_ALL_CHANGED:
      case DocEvents.EVENT_FILE_STATUS_CHANGED:
      case DocEvents.EVENT_SERIES_DATA_CHANGED:
      case AcmeManagedRef.EVENT_STATE_CHANGED:
        updateTitle();
        break;
      default:
        //skip
    }
  }
  // NOTE: if the close event is initiated outside (for example,
  // via the MenuBar), this handler will eventually be called again.
  // Thus, it is important to call RQST_CLOSE_WINDOW vs
  // RQST_CLOSE_ACTIVE_WINDOW which will close another window since
  // this will not be the active window when it's about to be closed.
  //
  // NOTE: need to use Event.CLOSING vs Event.CLOSE -- closing is
  // cancelable (which is desired since we want to close from AppState
  // explicitly) vs CLOSE which has already happened.
  public function _handleClosing(e:Event):void
  {
    //trace('BaseWindow Event.CLOSING handler');
    e.preventDefault();
    e.stopImmediatePropagation();
    this.doCheckedClose();
  }
  private function updateTitle():void
  {
    //trace('BaseWindow.updateTitle');
    if (docStateCtrlr == null) return;
    var ds:DocState = DocHandlers.readDocState(docStateCtrlr);
    if (ds == null) return;
    this.title = ds.title;
    //trace('title is', ds.title);
  }
  private function performSave():void
  {
    //trace("BaseWindow.performSave");
    if (docStateCtrlr == null) return;
    var ds:DocState = DocHandlers.readDocState(docStateCtrlr);
    if (DocHandlers.fileExists(ds))
    {
      saveSave(ds.file);
    }
    else
    {
      this.performSaveAs();
    }
  }
  private function saveSave(file:File):void
  {
    if (docStateCtrlr == null) return;
    //trace("BaseWindow.saveSave");
    // Another way to do this:
    // Include a callback function as an argument to SaveCommandEvent
    docStateCtrlr.handleCommand(new DocSaveFile(file));
    // Because ActionScript is single threaded, full event stream
    // should already be completed by now.
    var ds:DocState = DocHandlers.readDocState(docStateCtrlr);
  }
  private function handleCommandCancel(event:Event):void
  {
    //trace("BaseWindow.handleCommandCancel");
    dismissAction();
  }
  private function handleCommandComplete(event:Event):void
  {
    //trace("BaseWindow.handleCommandComplete");
    commitAction();
  }
  private function handleProgressCancel(event:Event):void
  {
    //trace("BaseWindow.handleProgressCancel");
    // Tell currently executing command on this document to stop
    // running. // (Hopefully this is the command we think it is!)
    /*
    ApplicationController.instance().dispatchEvent(
    new CancelCommandEvent(
    CancelCommandEvent.CANCEL, _docStateCtrlr));
    */
  }
  private function performSaveAs():void
  {
    //trace("BaseWindow.performSaveAs");
    // Present a default file path; can include directory path
    var file:File = File.desktopDirectory.resolvePath("Untitled.elements");
    file.addEventListener(Event.SELECT, handleSaveAsDialog);
    file.addEventListener(Event.CANCEL, handleCancelSaveAsDialog);
    // NOTE:  There's a bug with 'browseForSave' that causes the
    // extension to be (sometimes)
    // lost if the user clicks on an existing file shown in the
    // dialog.
    file.browseForSave("Save As...");
  }
  private function handleSaveAsDialog(event:Event):void
  {
    //trace("BaseWindow.handleSaveAsDialog");
    saveSave(event.target as File);
  }
  private function handleCancelSaveAsDialog(event:Event):void
  {
    //trace("BaseWindow.handleCancelSaveAsDialog");
    dismissAction();
  }
  private function handleClose(event:Event):void
  {
    trace("BaseWindow.handleClose");
    var appSC:IManagedRef = AppHandlers.instance();
    appSC.handleCommand(new AppCloseActiveWin());
  }
  public function doCheckedClose():void
  {
    trace("BaseWindow.doCheckedClose");
    if (this.checkClose) {
      this.confirmClosing();
    } else {
      if (_onCloseFn != null) _onCloseFn();
      AppHandlers.instance().handleCommand(new AppCloseWindow(this));
    }
  }
  private function confirmClosing():void
  {
    trace("BaseWindow.confirmClosing");
    if (docStateCtrlr == null) {
      if (_onCloseFn != null) _onCloseFn();
      AppHandlers.instance().handleCommand(new AppCloseWindow(this));
      return;
    }
    var appSC:IManagedRef = AppHandlers.instance();
    var ds:DocState = DocHandlers.readDocState(docStateCtrlr);
    if (ds.fileModified)
    {
      this.activate();
      Alert.show("Your document has been modified. " +
        "Do you want to save it?", "Warning",
        Alert.YES | Alert.NO | Alert.CANCEL,
        this, handleClosingDialog);
    }
    else
    {
      if (_onCloseFn != null) _onCloseFn();
      AppHandlers.instance().handleCommand(new AppCloseWindow(this));
    }
  }
  private function handleClosingDialog(event:CloseEvent):void
  {
    //trace("BaseWindow.handleClosingDialog2");
    if (event.detail == Alert.YES)
    {
      this.performSave();
      if (_onCloseFn != null) _onCloseFn();
      AppHandlers.instance().handleCommand(new AppCloseWindow(this));
    }
    else if (event.detail == Alert.NO)
    {
      if (_onCloseFn != null) _onCloseFn();
      AppHandlers.instance().handleCommand(new AppCloseWindow(this));
    }
  }
  // Executes original action (event?) that was the entry point for
  // this cascade of events. Every path through the chain of dialog
  // events should terminate either with 'commitAction' or
  // 'dismissAction'.
  private function commitAction():void
  {
    //trace("BaseWindow.commitAction");
    if (_pendingAction == CLOSING)
    {
      _pendingAction = IDLE;
      AppHandlers.instance().handleCommand(new AppCloseActiveWin());
    }
    else if (_pendingAction == EXITING)
    {
      _pendingAction = IDLE;
      AppHandlers.instance().handleCommand(new AppCloseActiveWin());
      // Restart exiting procedure
      NativeApplication.nativeApplication.dispatchEvent(
        new Event(Event.EXITING));
    }
    else
    {
      _pendingAction = IDLE;
    }
  }
  private function dismissAction():void
  {
    //trace("BaseWindow.dismissAction");
    if (_pendingAction == OPENING)
    {
      _pendingAction = IDLE;
      // This is a hard close that is not cancelable
      this.nativeWindow.close();
    }
    else
    {
      _pendingAction = IDLE;
    }
  }
}
}