/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements.views {
import mx.charts.AxisRenderer;
import mx.charts.DateTimeAxis;
import mx.charts.HitData;
import mx.charts.LinearAxis;
import mx.charts.chartClasses.CartesianChart;
import mx.charts.chartClasses.IAxis;
import mx.charts.series.LineSeries;
import mx.core.ClassFactory;
import mx.graphics.SolidColorStroke;

import spark.components.BorderContainer;

import elements.Variable;

/*  should be able to handle multiple series with smart features like
automatic changing of scales click events to change selections will be
handled elsewhere.

look at filterCache
http://cookbooks.adobe.com/
post_Disable_filtering_of_Cartesian_chart_data_by_axis-13088.html

good stuff:
http://help.adobe.com/en_US/FlashPlatform/reference/actionscript/3/mx/charts/
chartClasses/IAxis.html#filterCache%28%29

Using the PixelBender toolkit, you can utilize a Shader to perform math
operations on a separate thread. */

public class MixedSeriesChart extends CartesianChart {
    public static const TIME_FIELD:String = 'dateTime';//default time field name
    public var timeField:String; // make time field name "pluggable"
    // change to _hAxis
    //protected var _xAxis:LinearAxis = new LinearAxis;
    protected var _xAxis:DateTimeAxis = new DateTimeAxis();
    protected var _xAxisRenderer:AxisRenderer = new AxisRenderer();
    protected var _vAxisRenderer:AxisRenderer = new AxisRenderer();
    // Chart already has a _selectedSeries variable!!
    protected var _selectedTimeSeries:Variable = null;
    // connects LineSeries with its AxisRenderer
    protected var _seriesConnectors:Array = new Array();
    /* TO DO:
    - value formatters:  precision, match line series color
    - methods for selecting a time period (as overlay)
    */
    public function MixedSeriesChart() {
        super();
        timeField = TIME_FIELD;
        // turning these on make everything much slower for scrolling
        // and zooming!
        //_xAxis.dataUnits = "hours";
        // fixes bug with dates reversing order, if I ever see that
        // happen again
        _xAxis.disabledDays = [];
        this.showDataTips = true;
        // literally includes ALL tips on all nearby points at once
        //this.showAllDataTips = true;
        this.horizontalAxis = _xAxis;
        _xAxisRenderer.axis = _xAxis;
        _xAxisRenderer.setStyle("showLabels", true);
        _xAxisRenderer.setStyle("showLine", false);
        _xAxisRenderer.setStyle("tickPlacement", "outside");
        _xAxisRenderer.setStyle("tickStroke",
            new SolidColorStroke(0x999999, 1));
        _xAxisRenderer.setStyle("tickLength", 3);
        this.horizontalAxisRenderers = [_xAxisRenderer];
        LinearAxis(this.verticalAxis).minimum = 0;
        // This is only way to hide default vAxis.  Better to recycle it when
        // a series gets added.
        //var vAxisRenderer:AxisRenderer = new AxisRenderer();
        _vAxisRenderer.axis = this.verticalAxis;
        _vAxisRenderer.visible = false;
        _vAxisRenderer.setStyle("showLabels", false);
        _vAxisRenderer.setStyle("tickPlacement", "outside");
        _vAxisRenderer.setStyle("axisStroke",
            new SolidColorStroke(0xFF0000, 0));
        _vAxisRenderer.setStyle("tickStroke",
            new SolidColorStroke(0xFFFFFF, 1));
        // CustomChartLabel sets the text color
        var cf:ClassFactory = new ClassFactory(CustomChartLabel);
        // hide the labels by making them white
        cf.properties = { color: 0xFFFFFF };
        //Label(cf).width = 200;
        _vAxisRenderer.labelRenderer = cf;
        // these might work instead of a couple of the above, like axisStroke:
        //verticalAxisRenderer.height = 0;
        //verticalAxisRenderer.width = 0;
        this.verticalAxisRenderers = [_vAxisRenderer];  // only this works
        // DataTip stuff
        // only show tip for one variable at a time
        this.dataTipMode = "single";
        //this.dataTipFunction= formatDataTip;
        //_yAxis.labelFunction = formatLabel;  // works
        // could work with more investigation
        //_yAxisRenderer.labelFunction = formatLabel;
        // sets whether chart.selectedChartItem does anything
        this.selectionMode = "single";
        // don't see any difference in behavior or performance
        //this.clipContent = false;
        // this works!  but applies the scaling to all data on that axis,
        // not just a series.
        //_yAxis.parseFunction = scaleData;
        // this does not affect the dataTip values.
        // could generate many invisible axis for each series, each of which
        // is scaled.  One visible axis shows selected series units.
        this.setStyle("gutterTop", 0);
        this.setStyle("gutterRight", 0);
        this.setStyle("gutterBottom", 20);
        // messes up positioning of vertical labels
        //this.setStyle("gutterLeft", 40);
        var canvas:BorderContainer = new BorderContainer();
        canvas.percentWidth = 100;
        canvas.percentHeight = 100;
        //canvas.height = this.height - 20;  // no effect
        canvas.setStyle("borderStyle", "solid");
        canvas.setStyle("borderColor", 0x999999);
        canvas.setStyle("borderThickness", 1);
        // not sure if needed...but be necessary for series to show through
        canvas.setStyle("backgroundAlpha", 0);
        this.annotationElements = [canvas];
        this._addAxis("dummy", "dummy variable", 0);
    }

    private function scaleData(value:Number):Number {
        return(value * 10.0);
    }

    private function
    formatLabel(labelValue:Object, previousValue:Object, axis:IAxis):String {
        // Use this to apply:
        //   Units (unnormalized from 0.0 to 1.0)
        //   Precision
        //   Color?
        // for example:  divides values by 10
        //return(String(Number(labelValue) / 10.0));
        return(String(labelValue));
    }

    private function formatDataTip(hitData:HitData):String {
        // Need a way to lookup the SeriesDefinition in order to get: label,
        // units, precision, color
        return('no tip for you');
    }
    
    // This function adds a "dummy" time series to force the chart to
    // pre-render to the correct size to account for left axis
    public function _addAxis(name:String,fullName:String,color:uint):void {
      var series:Array = this.series;
      for (var i:uint = 0; i < series.length; i++) {
        if (series[i].yField == name) {
          trace("MixedTimeSeriesChart.addTimeSeries: " +
            " variable already added");
          return;
        }
      }
      var stroke:SolidColorStroke = new SolidColorStroke(color);
      // referenced by LineSeries and AxisRenderer
      var vAxis:LinearAxis = new LinearAxis();
      var line:LineSeries = new LineSeries();
      line.verticalAxis = vAxis;
      line.dataProvider = this.dataProvider;
      line.xField = timeField;
      line.yField = name;
      line.displayName = fullName;
      line.setStyle('lineStroke', stroke);
      line.visible = false;  // don't make visible until 'show' is called
      var renderer:AxisRenderer = new AxisRenderer();
      // Set permanent properties that don't change when hidden
      renderer.axis = vAxis;
      renderer.placement = "left";
      renderer.setStyle("showLine", false);  // line
      renderer.setStyle("canStagger", false);  // labels
      // has strange effect on label font size
      //renderer.setStyle("canDropLabels", false);
      renderer.setStyle("tickPlacement", "outside");  // ticks
      renderer.setStyle("tickStroke", new SolidColorStroke(0x999999, 1));
      hideAxis(renderer);
      // CustomChartLabel sets the text color
      var cf:ClassFactory = new ClassFactory(CustomChartLabel);
      cf.properties = { color: color, myWidth: 200 };
      //Label(cf).width = 200;
      renderer.labelRenderer = cf;
      //_yAxisRenderer.labelRenderer = new ClassFactory(CustomChartLabel);
      this.verticalAxisRenderers.push(renderer);
      // could make this an object later
      //var connector:Array = [null, line, renderer];
      //_seriesConnectors.push(connector);
      this.series.push(line);
    }

    // some of this does not belong--too specific to weather tool
    // LineSeries should be created outside
    public function addTimeSeries(variable:Variable):void {
        // check that this variable is not already present
        var series:Array = this.series;
        for (var i:uint = 0; i < series.length; i++) {
            if (series[i].yField == variable.name) {
                trace("MixedTimeSeriesChart.addTimeSeries: " +
                    " variable already added");
                return;
            }
        }
        var stroke:SolidColorStroke = new SolidColorStroke(variable.color);
        // referenced by LineSeries and AxisRenderer
        var vAxis:LinearAxis = new LinearAxis();
        var line:LineSeries = new LineSeries();
        line.verticalAxis = vAxis;
        line.dataProvider = this.dataProvider;
        line.xField = timeField;
        line.yField = variable.name;
        line.displayName = variable.fullName;
        line.setStyle('lineStroke', stroke);
        line.visible = false;  // don't make visible until 'show' is called
        var renderer:AxisRenderer = new AxisRenderer();
        // Set permanent properties that don't change when hidden
        renderer.axis = vAxis;
        renderer.placement = "left";
        renderer.setStyle("showLine", false);  // line
        renderer.setStyle("canStagger", false);  // labels
        // has strange effect on label font size
        //renderer.setStyle("canDropLabels", false);
        renderer.setStyle("tickPlacement", "outside");  // ticks
        renderer.setStyle("tickStroke", new SolidColorStroke(0x999999, 1));
        hideAxis(renderer);
        // CustomChartLabel sets the text color
        var cf:ClassFactory = new ClassFactory(CustomChartLabel);
        cf.properties = { color: variable.color, myWidth: 200 };
        //Label(cf).width = 200;
        renderer.labelRenderer = cf;
        //_yAxisRenderer.labelRenderer = new ClassFactory(CustomChartLabel);
        this.verticalAxisRenderers.push(renderer);
        // could make this an object later
        var connector:Array = [variable, line, renderer];
        _seriesConnectors.push(connector);
        this.series.push(line);
    }

    // Needed to refresh the series list and draw any news that were added
    public function updateTimeSeries():void {
        var tmp:Array = this.series;
        this.series = tmp;
        this.invalidateSeriesStyles();
    }

    // not really used
    public function removeTimeSeries(variable:Variable):void {
        for (var i:uint = 0; i < this.series.length; i++) {
            if (this.series[i].yField == variable.name) {
                var tmp:Array = this.series;
                tmp.splice(i, 1);
                this.series = tmp;
                this.invalidateSeriesStyles();
                break;
            }
        }
    }

    // Show/Hide are much faster...but might be penalty when trying to move
    // all that data
    public function showTimeSeries(variable:Variable):void {
        var line:LineSeries;
        var renderer:AxisRenderer;
        for (var i:uint = 0; i < _seriesConnectors.length; i++) {
            if (_seriesConnectors[i][0] == variable) {
                line = _seriesConnectors[i][1];
                renderer = _seriesConnectors[i][2];
                line.visible = true;
                break;
            }
        }
    }

    public function hideTimeSeries(variable:Variable):void {
        if (variable == _selectedTimeSeries) {
            _selectedTimeSeries = null;
        }
        var line:LineSeries;
        var renderer:AxisRenderer;
        for (var i:uint = 0; i < _seriesConnectors.length; i++) {
            if (_seriesConnectors[i][0] == variable) {
                line = _seriesConnectors[i][1];
                renderer = _seriesConnectors[i][2];
                line.visible = false;
                hideAxis(renderer);
                break;
            }
        }
    }

    // only select one variable
    public function setSelection(variableName:String):void {
        unselectAll();
        var variable:Variable;
        var line:LineSeries;
        var renderer:AxisRenderer;
        for (var i:uint = 0; i < _seriesConnectors.length; i++) {
            if (_seriesConnectors[i][0].name == variableName) {
                variable = _seriesConnectors[i][0];
                line = _seriesConnectors[i][1];
                renderer = _seriesConnectors[i][2];
                _selectedTimeSeries = variable;
                line.setStyle('lineStroke',
                    new SolidColorStroke(variable.color, 3));
                showAxis(renderer);
            }
        }
    }

    public function unselectAll():void {
        _selectedTimeSeries = null;
        var variable:Variable;
        var line:LineSeries;
        var renderer:AxisRenderer;
        for (var i:uint = 0; i < _seriesConnectors.length; i++) {
            variable = _seriesConnectors[i][0];
            line = _seriesConnectors[i][1];
            renderer = _seriesConnectors[i][2];
            line.setStyle('lineStroke',
                new SolidColorStroke(variable.color, 1));
            hideAxis(renderer);
        }
    }

    private function showAxis(renderer:AxisRenderer):void {
        renderer.visible = true;
        renderer.setStyle("showLine", false);
        renderer.setStyle("showLabels", true);
        renderer.setStyle("tickLength", 3);
        renderer.setStyle("labelGap", 0);
        //renderer.left = 0;
        //renderer.includeInLayout = false;
        renderer.width = 200;
        renderer.minWidth = 200;
        renderer.invalidateSize();
        renderer.invalidateProperties();
        renderer.invalidateDisplayList();
        //renderer.gutters = new Rectangle(0, 0, 200, 400);
        _vAxisRenderer.visible = false;  // required
        _vAxisRenderer.setStyle("showLine", false);  // required
        _vAxisRenderer.setStyle("showLabels", false);  // required
        _vAxisRenderer.setStyle("tickLength", 0);  // required
        _vAxisRenderer.setStyle("labelGap", 0);  // required
        // this is required to get extra axis's to show up
        var tmp:Array = this.verticalAxisRenderers;
        this.verticalAxisRenderers = tmp;
    }

    private function hideAxis(renderer:AxisRenderer):void {
        renderer.visible = false;  // required
        renderer.setStyle("showLine", false);  // required
        renderer.setStyle("showLabels", false);  // required
        renderer.setStyle("tickLength", 0);  // required
        renderer.setStyle("labelGap", 0);  // required
        renderer.setStyle("minorTickLength", 0);
        //renderer.height = 0;
        //renderer.width = 0;  // does nothing?
        //renderer.left = 0;
        //renderer.includeInLayout = false;
        //renderer.minWidth = 100;
        //renderer.invalidateSize();
        //renderer.gutters = new Rectangle(0, 0, 200, 400);
        // show default v axis just so that same width is maintained
        _vAxisRenderer.visible = true;
        _vAxisRenderer.setStyle("showLine", false);
        _vAxisRenderer.setStyle("showLabels", true);
        _vAxisRenderer.setStyle("tickLength", 3);
        _vAxisRenderer.setStyle("labelGap", 0);
        // this is required to get extra axis's to show up
        var tmp:Array = this.verticalAxisRenderers;
        this.verticalAxisRenderers = tmp;
    }
}
}