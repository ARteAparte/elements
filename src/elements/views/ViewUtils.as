/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements.views {
import spark.components.HGroup;
import spark.components.Label;
import spark.components.VGroup;

public class ViewUtils {
    public function ViewUtils() {
    }

    public static function makeVGroup():VGroup {
        var vg:VGroup = new VGroup();
        vg.paddingBottom = 10;
        vg.paddingTop = 10;
        vg.paddingLeft = 10;
        vg.paddingRight = 10;
        vg.percentHeight = 100;
        vg.percentWidth = 100;
        return vg;
    }

    public static function makeHGroup():HGroup {
        var hg:HGroup = new HGroup();
        hg.paddingBottom = 10;
        hg.paddingTop = 0;
        hg.paddingLeft = 10;
        hg.paddingRight = 10;
        hg.percentHeight = 100;
        hg.percentWidth = 100;
        hg.verticalAlign = "middle";
        return hg;
    }

    public static function makeTitleLabel(text:String):Label {
        var label:Label = makeLabel(text);
        label.styleName = "title";
        return label;
    }

    public static function makeFormLabel(text:String):Label {
        var label:Label = makeLabel(text);
        label.styleName = "formLabel";
        return label;
    }

    public static function makeLabel(text:String):Label {
        var label:Label = new Label();
        label.text = text;
        return label;
    }
}
}