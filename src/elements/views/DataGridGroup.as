/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements.views {
import flash.events.Event;
import flash.events.KeyboardEvent;
import flash.ui.Keyboard;
import flash.utils.getTimer;

import mx.collections.ArrayCollection;
import mx.collections.IList;
import mx.core.IFactory;

import spark.components.DataGrid;
import spark.components.Group;
import spark.components.VGroup;
import spark.components.gridClasses.GridColumn;
import spark.components.gridClasses.GridSelectionMode;
import spark.events.GridEvent;
import spark.events.GridSelectionEvent;
import spark.layouts.VerticalLayout;

import dse.DataSet;
import dse.MetaData;
import dse.SeriesSelection;

import elements.DataDisplayOptions;
import elements.DataGridHelpers;
import elements.DocEvents;
import elements.DocHandlers;
import elements.DocState;
import elements.commands.DocAddSeriesToSelection;
import elements.commands.DocCopySelected;
import elements.commands.DocPaste;
import elements.commands.DocSetGridSelection;
import elements.commands.DocSetSelectionToSeries;
import elements.selection.DataGridSelectionUtils;

import gui.AcmeManagedRef;
import gui.IManagedRef;

import pdt.IPMap;
import pdt.IPVec;

public class DataGridGroup extends Group {
  public const IS_EDITABLE:Boolean = true;
  public var dataGrid:DataGrid;
  private var _lastClickTimeInMillisecs:int;
  private var _doubleClickTimeInMillisecs:int;
  private var _docSC:IManagedRef;
  private var _ignoreSelectionChange:Boolean;

  public function DataGridGroup(docSC:IManagedRef) {
    super();
    _ignoreSelectionChange = false;
    _docSC = docSC;
    _docSC.registerSubscriberFunction(update);
    initializeMouseClickVariables();
    doLayout();
  }
  private function initializeMouseClickVariables():void {
    _lastClickTimeInMillisecs = getTimer();
    _doubleClickTimeInMillisecs = 300;
  }
  private function doLayout():void {
    percentWidth = 100;
    percentHeight = 100;
    layout = new VerticalLayout();
    var vg:VGroup = ViewUtils.makeVGroup();
    dataGrid = makeDataGrid(getDataSet());
    updateColumnLabels();
    vg.addElement(this.dataGrid);
    addElement(vg);
  }
  private function makeDataGrid(dataSet:DataSet):DataGrid {
    //trace('DataGridGroup.makeDataGrid');
    var dg:DataGrid = new DataGrid();
    dg.percentHeight = 100.0;
    dg.percentWidth = 100.0;
    dg.selectionMode = GridSelectionMode.MULTIPLE_CELLS;
    dg.editable = IS_EDITABLE;
    dg.dataProvider = dataSet.pointsAsIList;
    dg.columns = makeDataGridColumns(dataSet) as IList;
    dg.addEventListener(GridSelectionEvent.SELECTION_CHANGE,
      function (event:Event):void {
        _ignoreSelectionChange = true;
        _docSC.handleCommand(
          new DocSetGridSelection(dg.selectedCells, dg.columns));
      });
    dg.addEventListener(GridEvent.GRID_CLICK, onClick);
    dg.addEventListener(KeyboardEvent.KEY_DOWN, onKeyDown);
    return dg;
  }
  public function _getRowIndex():int {
    var rowIdx:int = -1;
    var ss:IPVec = this.getSelection();
    for (var idx:int=0; idx<ss.length; idx++) {
      var selection:SeriesSelection = ss.getItemAt(idx) as SeriesSelection;
      if (selection.startIndex > rowIdx) {
        rowIdx = selection.startIndex;
      }
    }
    return rowIdx;
  }
  private function makeDataGridColumns(dataSet:DataSet):ArrayCollection {
    var columns:ArrayCollection = new ArrayCollection();
    var docState:DocState = DocHandlers.readDocState(_docSC);
    var opts:IPMap = docState.displayOptionsMap;
    var seriesMetaMap:Object = dataSet.seriesMetaData;
    var seriesNames:ArrayCollection =
      new ArrayCollection(docState.shownDataAsArray);
    for each (var name:String in seriesNames) {
      var seriesMeta:MetaData = seriesMetaMap[name] as MetaData;
      var column:GridColumn = new GridColumn();
      column.dataField = name;
      column.headerRenderer = new GridHeaderFactory();
      if (opts.containsKey(name)) {
        var optSet:DataDisplayOptions =
          opts.valAt(name) as DataDisplayOptions;
        if (optSet != null) {
          column.labelFunction = optSet.makeLabelFunction(_docSC);
          if (seriesMetaMap.hasOwnProperty(name)
            &&(seriesMetaMap[name] as MetaData).editable) {
            column.itemEditor = optSet.itemEditorMaker(_docSC) as IFactory;
          } else {
            column.editable = false;
          }
        }
      }
      column.sortable = false;
      columns.addItem(column);
    }
    return columns;
  }
  private function getDS():DocState {
    return DocHandlers.readDocState(_docSC);
  }
  private function getDataSet():DataSet {
    return getDS().dataSet;
  }
  private function getDisplayUnits():IPMap {
    return getDS().displayUnits;
  }
  private function getSelection():IPVec {
    return getDS().selections;
  }
  private function updateColumnLabels():void {
    var ds:DocState = getDS();
    for each (var col:GridColumn in dataGrid.columns) {
      col.headerText = DataGridHelpers.makeHeaderText(ds, col.dataField);
    }
  }
  private function onClick(event:GridEvent):void {
    //trace('DataGridGroup :: mouse clicked');
    //trace('DataGridGroup :: event phase:', event.eventPhase);
    var now:int = getTimer();
    if ((now - _lastClickTimeInMillisecs) < _doubleClickTimeInMillisecs) {
      _lastClickTimeInMillisecs = now;
      onDoubleClick(event);
    } else {
      // first click of double-click OR single click...
      _lastClickTimeInMillisecs = now;
      onSingleClick(event);
    }
  }
  private function onSingleClick(event:GridEvent):void {
    //trace('onSingleClick');
    if (event.columnIndex != -1 && event.rowIndex == -1) {
      selectColumn(event);
    }
  }
  private function onDoubleClick(event:GridEvent):void {
    //trace('onDoubleClick');
    if((event.columnIndex > -1) && (event.rowIndex > -1)) {
      if(event.column.editable) {
        dataGrid.startItemEditorSession(
          event.rowIndex, event.columnIndex);
      }
    }
  }
  private function selectColumn(event:GridEvent):void {
    //trace('selectColumn', event.column.dataField);
    if(event.ctrlKey) {
      _docSC.handleCommand(
        new DocAddSeriesToSelection(event.column.dataField));
    } else {
      _docSC.handleCommand(new DocSetSelectionToSeries(event.column.dataField));
    }
  }
  public function doCopy():void {
    _docSC.handleCommand(new DocCopySelected());
  }
  public function doPaste():void {
    _docSC.handleCommand(new DocPaste());
  }
  // from http://help.adobe.com/en_US/flex/using/
  // WS0ab2a460655f2dc3-427f401412c60d04dca-7ff8.html
  private function onKeyDown(event:KeyboardEvent):void {
    var ctrl:Boolean = event.ctrlKey || event.commandKey;
    if (ctrl && (event.keyCode == Keyboard.C)) {
      doCopy();
    } else if((event.ctrlKey || event.commandKey)
      && (event.keyCode == Keyboard.A)) {
      selectAll();
    } else if((event.ctrlKey || event.commandKey)
      && (event.keyCode == Keyboard.V)) {
      doPaste();
    }
  }
  private function selectAll():void {
    //trace('DataGridGroup - selectAll');
  }
  // for when data changed but not columns
  public function refreshGrid():void {
    //trace('DataGridGroup.refreshGrid');
    dataGrid.dataProvider = new ArrayCollection(getDataSet().pointsAsArray);
    /*
    DataGridSelectionUtils.setDataGridSelection(
    getSelection(), dataGrid);
    */
    updateColumnLabels();
    //dataGrid.invalidateDisplayList();
    dataGrid.validateNow();
  }
  private function refreshSelection():void {
    //trace('DataGridGroup.refreshSelection');
    DataGridSelectionUtils.setDataGridSelection(
      new ArrayCollection(getSelection().toArray()), this.dataGrid);
    //if (_vscrollbarPosition != -1) {
    //  dataGrid.scroller.verticalScrollBar.value = _vscrollbarPosition;
    //  _vscrollbarPosition = -1;
    //}
  }
  // for data and even columns changing
  private function redrawAll():void {
    //trace('DataGridGroup.redrawAll');
    // save the current view
    //var rowIndex:int = -1;
    //if ((dataGrid != null) && (dataGrid.grid != null)) {
    //  rowIndex = dataGrid.grid.verticalScrollPosition;
    //}
    var docSt:DocState = getDS();
    var dataSet:DataSet = getDataSet();
    dataGrid.dataProvider = new ArrayCollection(dataSet.pointsAsArray);
    dataGrid.columns = makeDataGridColumns(dataSet) as IList;
    updateColumnLabels();
    dataGrid.validateNow();
  }
  public function update(event:String):void {
    //trace('DataGridGroup.update(', event,')');
    var ke:KeyboardEvent;
    var vs:Object = _docSC;
    if ((event == DocEvents.EVENT_SELECTION_CHANGED)
        && _ignoreSelectionChange) {
      _ignoreSelectionChange = false;
      return;
    }
    _ignoreSelectionChange = false;
    switch (event) {
      case DocEvents.EVENT_SELECTION_CHANGED:
        //trace('DataGridGroup.update :: case', event);
        refreshSelection();
        break;
      case DocEvents.EVENT_HEADER_CHANGED:
      case DocEvents.EVENT_ALL_CHANGED:
      case AcmeManagedRef.EVENT_STATE_CHANGED:
      case DocEvents.EVENT_SERIES_DATA_CHANGED:
        //trace('DataGridGroup.update :: case', event);
        redrawAll();
        refreshSelection();
        break;
      case DocEvents.EVENT_DISPLAY_UNITS_CHANGED:
        //trace('DataGridGroup.update :: case', event);
        updateColumnLabels();
        break;
      case DocEvents.EVENT_SERIES_DATA_CHANGED:
        //trace('DataGridGroup.update :: case', event);
        // unfortunately, this doesn't work, Paste typically pastes new data
        // outside of the selection...
        /*
        var ds:DocState = getDS();
        var sels:IPVec = ds.selections;
        var pts:IPVec = ds.dataSet.points;
        var rows:Object = {};
        for (var sel_idx:int=0; sel_idx<sels.length; sel_idx++) {
          var ss:SeriesSelection = sels.getItemAt(sel_idx) as SeriesSelection;
          for each (var row_idx:int in ss.selectedRows) {
            if (rows.hasOwnProperty(row_idx)) continue;
            rows[row_idx] = true; // simulating a set of rows
            dataGrid.dataProvider.setItemAt(pts.getItemAt(row_idx), row_idx);
          }
          dataGrid.validateNow();
        }
        //redrawAll();
        //refreshSelection();
        */
        break;
      case DocEvents.EVENT_DISPLAYED_DATA_COLUMNS_CHANGED:
        //trace('DataGridGroup.update :: case', event);
        redrawAll();
        break;
      default:
        //trace('DataGridGroup.update :: case', event);
        //trace('unhandled event: ', event);
    }
  }
}
}