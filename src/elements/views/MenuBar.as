/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements.views
{
import flash.desktop.NativeApplication;
import flash.display.NativeMenu;
import flash.display.NativeMenuItem;
import flash.display.NativeWindow;
import flash.events.Event;
import flash.filesystem.File;
import flash.system.Capabilities;

import mx.managers.PopUpManager;

import dse.SeriesSelection;

import elements.AppEvents;
import elements.AppHandlers;
import elements.AppState;
import elements.DataGridCopyPaste;
import elements.DocEvents;
import elements.DocHandlers;
import elements.DocState;
import elements.PreferencesListener;
import elements.WindowOptions;
import elements.WindowOptionsBuilder;
import elements.commands.AppImportIntoNewWin;
import elements.commands.AppNewDocument;
import elements.commands.AppNewHTMLDocument;
import elements.commands.AppSetActiveWindow;
import elements.commands.AppSetModal;
import elements.commands.AppSetPreferences;
import elements.commands.DocImportData;
import elements.commands.DocPaste;
import elements.commands.DocRemoveDataColumn;
import elements.commands.DocSaveFile;
import elements.commands.DocSetDisplayUnitSys;
import elements.commands.DocSetSelections;
import elements.commands.DocShiftSeriesColumnTowardsHead;
import elements.commands.DocShiftSeriesColumnTowardsTail;
import elements.components.SaveAsWarning;

import gui.IManagedRef;

import pdt.IPVec;
import pdt.PersistentVector;

import plugins.DateAndTime;

import units.UnitSystems;

import utils.ArrayUtils;
import utils.NetUtils;

import weatherfile.AllFilesFormat;
import weatherfile.DOE2BinFormat;
import weatherfile.DOE2FmtFormat;
import weatherfile.ElementsNativeFormat;
import weatherfile.EnergyPlusEPWFormat;
import weatherfile.exporters.ExporterDOE2BIN;
import weatherfile.exporters.ExporterDOE2FMT;
import weatherfile.exporters.ExporterEnergyPlusEPW;
import weatherfile.exporters.ExporterNativeFormat;
import weatherfile.exporters.IExporter;
import weatherfile.importers.IImporter;
import weatherfile.importers.ImporterDOE2BIN;
import weatherfile.importers.ImporterDOE2FMT;
import weatherfile.importers.ImporterEnergyPlusEPW;
import weatherfile.importers.ImporterNativeFormat;

public class MenuBar {
  public static const CLOSE_MENU_ITEM:String = "Close";
  public static const USER_DOCUMENTATION:String = 'User Guide';
  public static const ONLINE_RESOURCES:String = 'Online Resources';
  public static const UNDO_MENU_ITEM:String = "Undo";
  public static const REDO_MENU_ITEM:String = "Redo";
  public static const SAVE_MENU_ITEM:String = "Save";
  public var ABOUT:String;
  public var QUIT:String;
  public var disabled:Array;
  public var menu:NativeMenu;
  public var isApplicationMenu:Boolean;
  public var _windowMenu:NativeMenu;
  public var _menuItems:Object;

  public function MenuBar()
  {
    //trace('MenuBar.MenuBar');
    _menuItems = new Object();
    disabled = [];
    var appSC:IManagedRef = AppHandlers.instance();
    var as1:AppState = AppHandlers.readAppState(appSC);
    ABOUT = "About " + as1.applicationName;
    QUIT = "Quit " + as1.applicationName;
    isApplicationMenu = NativeApplication.supportsMenu;
    if (isApplicationMenu)
    {
      this.menu = NativeApplication.nativeApplication.menu;
      _setupApplicationMenu();
    }
    else
    {
      this.menu = new NativeMenu();
    }
    addFileMenu(this.menu);
    addEditMenu(this.menu);
    addToolsMenu(this.menu);
    addViewMenu(this.menu);
    addWindowMenu(this.menu);
    addHelpMenu(this.menu);
    appSC.registerSubscriberFunction(update);
  }

  public function addViewMenu(parent:NativeMenu):void
  {
    var submenu:NativeMenu = new NativeMenu();
    parent.addSubmenu(submenu, "View");
    addMenuItem(submenu, "Header", handleViewHeader);
    addMenuItem(submenu, "Chart", handleViewChart);
    addSeparator(submenu);
    createSubmenu(submenu, "Columns",
      ["Add", "Remove", "Move Left", "Move Right"],
      [handleAddColumn, handleRemoveColumn, handleMoveColumnLeft, handleMoveColumnRight]);
    createSubmenu(submenu, "Units",
      [ "SI", "IP" ], [ handleSetSI, handleSetIP]);
  }

  public function handleViewHeader(e:Event):void
  {
    var appSt:AppState = AppHandlers.readAppState(AppHandlers.instance());
    var docRef:IManagedRef = appSt.activeDocState;
    docRef.notifySubscribers(DocEvents.EVENT_REQUEST_HEADER_EDIT);
  }

  public function handleViewChart(e:Event):void
  {
    var appSt:AppState = AppHandlers.readAppState(AppHandlers.instance());
    var docRef:IManagedRef = appSt.activeDocState;
    docRef.notifySubscribers(DocEvents.EVENT_REQUEST_VIEW_CHART);
  }

  public function handleSetSI():void
  {
    var appSt:AppState = AppHandlers.readAppState(AppHandlers.instance());
    var docRef:IManagedRef = appSt.activeDocState;
    docRef.handleCommand(new DocSetDisplayUnitSys(UnitSystems.SI));
  }

  public function handleSetIP():void
  {
    var appSt:AppState = AppHandlers.readAppState(AppHandlers.instance());
    var docRef:IManagedRef = appSt.activeDocState;
    docRef.handleCommand(new DocSetDisplayUnitSys(UnitSystems.IP));
  }

  public function handleMoveColumnLeft():void
  {
    // get active doc state
    var appSt:AppState = AppHandlers.readAppState(AppHandlers.instance());
    var docRef:IManagedRef = appSt.activeDocState;
    var ds:DocState = DocHandlers.readDocState(docRef);
    if (!DocHandlers.hasData(ds)) return;
    if (!SeriesSelection.onlyOneSeriesSelected(ds.selections)) return;
    var selected:String = ds.selectionN(0).selectedSeries;
    docRef.handleCommand(new DocShiftSeriesColumnTowardsHead(selected));
  }

  public function handleMoveColumnRight():void
  {
    // get active doc state
    var appSt:AppState = AppHandlers.readAppState(AppHandlers.instance());
    var docRef:IManagedRef = appSt.activeDocState;
    var ds:DocState = DocHandlers.readDocState(docRef);
    if (!DocHandlers.hasData(ds)) return;
    if (!SeriesSelection.onlyOneSeriesSelected(ds.selections)) return;
    var selected:String = ds.selectionN(0).selectedSeries;
    docRef.handleCommand(new DocShiftSeriesColumnTowardsTail(selected));
  }

  public function handleAddColumn():void
  {
    var appSt:AppState = AppHandlers.readAppState(AppHandlers.instance());
    var docRef:IManagedRef = appSt.activeDocState;
    var ds:DocState = DocHandlers.readDocState(docRef);
    if (!DocHandlers.hasData(ds)) return;
    docRef.notifySubscribers(DocEvents.EVENT_REQUEST_ADD_COLUMN);
  }

  public function handleRemoveColumn():void {
    var appSt:AppState = AppHandlers.readAppState(AppHandlers.instance());
    var docRef:IManagedRef = appSt.activeDocState;
    var ds:DocState = DocHandlers.readDocState(docRef);
    if (!DocHandlers.hasData(ds)) return;
    if (!SeriesSelection.onlyOneSeriesSelected(ds.selections)) return;
    var selected:String = ds.selectionN(0).selectedSeries;
    if (selected == DateAndTime.DATE_TIME) return;
    docRef.handleCommand(new DocRemoveDataColumn(selected));
    docRef.handleCommand(new DocSetSelections(PersistentVector.EMPTY));
  }

  public function addToolsMenu(parent:NativeMenu):void
  {
    var submenu:NativeMenu = new NativeMenu();
    parent.addSubmenu(submenu, "Tools");
    addMenuItem(submenu, "Offset", handleOffset);
    addMenuItem(submenu, "Scale", handleScale);
    addMenuItem(submenu, "Normalize", handleNormalize);
    addMenuItem(submenu, "Normalize By Month", handleNormalizeMonthly);
  }

  public function handleOffset(event:Event):void
  {
    var appSt:AppState = AppHandlers.readAppState(AppHandlers.instance());
    var docRef:IManagedRef = appSt.activeDocState;
    docRef.notifySubscribers(DocEvents.EVENT_REQUEST_OFFSET);
  }

  public function handleScale(event:Event):void
  {
    var appSt:AppState = AppHandlers.readAppState(AppHandlers.instance());
    var docRef:IManagedRef = appSt.activeDocState;
    docRef.notifySubscribers(DocEvents.EVENT_REQUEST_SCALE);
  }

  public function handleNormalize(event:Event):void
  {
    var appSt:AppState = AppHandlers.readAppState(AppHandlers.instance());
    var docRef:IManagedRef = appSt.activeDocState;
    docRef.notifySubscribers(DocEvents.EVENT_REQUEST_NORMALIZE);
  }

  public function handleNormalizeMonthly(event:Event):void
  {
    var appSt:AppState = AppHandlers.readAppState(AppHandlers.instance());
    var docRef:IManagedRef = appSt.activeDocState;
    docRef.notifySubscribers(DocEvents.EVENT_REQUEST_NORMALIZE_MONTHLY);
  }

  public function _setupApplicationMenu():void
  {
    //trace('MenuBar._setupApplicationMenu');
    var appSC:IManagedRef = AppHandlers.instance();
    // Remove all menus except for the application menu.
    while (this.menu.numItems != 1)
    {
      this.menu.removeItemAt(this.menu.numItems - 1);
    }
    var applicationMenuItem:NativeMenuItem = this.menu.getItemAt(0);
    var appMenu:NativeMenu = applicationMenuItem.submenu;
    appMenu.removeAllItems();
    var aboutItem:NativeMenuItem = new NativeMenuItem(ABOUT);
    aboutItem.addEventListener(Event.SELECT, handleAbout);
    _menuItems[ABOUT] = aboutItem;
    appMenu.addItem(aboutItem);
    appMenu.addItem(newSeparator());
    var quitItem:NativeMenuItem = new NativeMenuItem();
    quitItem.label = QUIT;
    quitItem.keyEquivalent = "q";
    quitItem.addEventListener(Event.SELECT, handleExit);
    appMenu.addItem(quitItem);
  }

  public function handleNew(event:Event):void
  {
    //trace('MenuBar.handleNew');
    AppHandlers.instance().handleCommand(new AppNewDocument());
  }

  private function handleClose(event:Event):void
  {
    //trace('MenuBar.handleClose');
    var appState:AppState = AppHandlers.readAppState(AppHandlers.instance());
    var w:BaseWindow = appState.activeWindow as BaseWindow;
    var nw:NativeWindow = NativeApplication.nativeApplication.activeWindow;
    if (w.nativeWindow != nw)
    {
      if (nw != null)
      {
        nw.close();
        return;
      }
    }
    if (w != null)
    {
      w.doCheckedClose();
    }
    else
    {
      //trace('MenuBar.handleClose :: attempt to close null window',
      //  'OR no windows open');
    }
  }

  private function addSeparator(parent:NativeMenu):void
  {
    //trace('MenuBar.addSeparator');
    parent.addItem(newSeparator());
  }

  private function handleSave(event:Event):void
  {
    //trace('MenuBar.handleSave');
    var appSC:IManagedRef = AppHandlers.instance();
    var ast:AppState = AppHandlers.readAppState(appSC);
    var w:Object = ast.activeWindow;
    if (w != null)
    {
      var w_:BaseWindow = w as BaseWindow;
      var ds:DocState = DocHandlers.readDocState(w_.docStateCtrlr);
      if (ds.file == null)
      {
        handleSaveAs(event);
      }
      w_.docStateCtrlr.handleCommand(new DocSaveFile(null));
    }
  }

  private function currentDocRef():IManagedRef
  {
    var appSC:IManagedRef = AppHandlers.instance();
    var ast:AppState = AppHandlers.readAppState(appSC);
    var w:Object = ast.activeWindow;
    if (w != null)
    {
      var w_:BaseWindow = w as BaseWindow;
      return w_.docStateCtrlr;
    }
    return null;
  }

  private function handleSaveAs(event:Event):void
  {
    //trace('MenuBar.handleSaveAs');
    var appSC:IManagedRef = AppHandlers.instance();
    var ast:AppState = AppHandlers.readAppState(appSC);
    var w:Object = ast.activeWindow;
    if (w != null)
    {
      var w_:BaseWindow = w as BaseWindow;
      // Query the preferences file for lastPath entry
      var fileToSaveAs:File = PreferencesListener.lastPath();
      //var fileToSaveAs:File = File.documentsDirectory;
      fileToSaveAs.addEventListener(Event.SELECT,
        function(e:Event):void {
          var appSC:IManagedRef = AppHandlers.instance();
          var appState:AppState = AppHandlers.readAppState(appSC);
          var dw:BaseWindow = appState.activeWindow as BaseWindow;
          if (dw == null) return;
          // flag if saving before of {null, elements, epw, bin, fmt}
          //                to after of {epw, bin, fmt}
          //                where before != after
          var ds:DocState = DocHandlers.readDocState(dw.docStateCtrlr);
          var fromExt:String;
          if (ds.file != null)
          {
            fromExt = ds.file.extension.toLowerCase();
          }
          else
          {
            fromExt = "";
          }
          var toExt:String = fileToSaveAs.extension.toLowerCase();
          //trace('saveas: toExt = ', toExt);
          //trace('saveas: fromExt = ', fromExt);
          if (((toExt=="epw")||(toExt=="bin")||(toExt=="fmt"))&&
            (toExt!=fromExt))
          {
            // warn user that data loss may be incurred
            //trace('You should be ALERTED!!!');
            var win:SaveAsWarning = new SaveAsWarning();
            win.fileToSaveAs = fileToSaveAs;
            win.onOK = function():void
            {
              dw.docStateCtrlr.handleCommand(
                new DocSaveFile(fileToSaveAs));
              dw.docStateCtrlr.resetUndoRedo();
              PopUpManager.removePopUp(win);
            };
            win.onCancel = function():void
            {
              PopUpManager.removePopUp(win);
            };
            //trace('adding popup');
            PopUpManager.addPopUp(win, dw.contentGroup, true);
            PopUpManager.centerPopUp(win);
          }
          else
          {
            dw.docStateCtrlr.handleCommand(
              new DocSaveFile(fileToSaveAs));
            dw.docStateCtrlr.resetUndoRedo();
          }
        });
      fileToSaveAs.browseForSave("Save As");
    }
  }

  private function handleExit(event:Event):void
  {
    //trace('MenuBar.handleExit');
    NativeApplication.nativeApplication.dispatchEvent(
      new Event(Event.EXITING, false, true));
  }

  public function handleImportType(imp:IImporter):Function
  {
    //trace('MenuBar.handleImportType');
    return function(runAfter:Function=null):void
    {
      var fileToImport:File = File.documentsDirectory;
      fileToImport.addEventListener(
        Event.SELECT,
        function(e:Event):void
        {
          var appSC:IManagedRef = AppHandlers.instance();
          appSC.handleCommand(new AppNewDocument());
          var appState:AppState = AppHandlers.readAppState(appSC);
          var dw:BaseWindow = appState.activeWindow as BaseWindow;
          dw.docStateCtrlr.handleCommand(
            new DocImportData(imp, fileToImport));
          dw.docStateCtrlr.resetUndoRedo();
          if (runAfter != null)
          {
            runAfter();
          }
        });
      fileToImport.browseForOpen("Import " +
        imp.fileFormat.fileDescription, [imp.fileFormat.fileFilter]);
    };
  }

  private function handleExportType(exp:IExporter):Function
  {
    //trace('MenuBar.handleExportType');
    return function():void
    {
      var appSC:IManagedRef = AppHandlers.instance();
      var appState:AppState = AppHandlers.readAppState(appSC);
      var dw:BaseWindow = appState.activeWindow as BaseWindow;
      var file:File;
      var ds:DocState = DocHandlers.readDocState(dw.docStateCtrlr);
      if (ds.file != null && ds.file.exists && !ds.file.isDirectory)
      {
        // note: must use parent directory to avoid Mac bug where
        // files treated like directories
        if (Capabilities.os.indexOf("Mac") >= 0)
        {
          file = ds.file.parent;
        }
        else
        {
          file = ds.file.clone();
          if (file.extension != exp.fileFormat.fileRawExtension)
          {
            //trace('orig native path:', file.nativePath);
            var re:RegExp = new RegExp("\\."+file.extension+"$");
            //trace('re', re);
            var repl:String = "."+exp.fileFormat.fileRawExtension;
            //trace('repl', repl);
            file = new File(file.nativePath.replace(re, repl));
            //trace('new native path:', file.nativePath);
          }
        }
      }
      else
      {
        var fname:String = ds.fileName + "."
          + exp.fileFormat.fileRawExtension;
        file = File.documentsDirectory.resolvePath(fname);
      }
      file.addEventListener(Event.SELECT,
        function(e:Event):void
        {
          dw.docStateCtrlr.handleCommand(new DocSaveFile(file));
          AppHandlers.instance().notifySubscribers(
            AppEvents.EVENT_NEW_DOCUMENT);
          //dw.docStateCtrlr.handleCommand(new DocExportData(exp, file));
        });
      var s:String = "Save As " + exp.fileFormat.fileDescriptionNoWildcard;
      file.browseForSave(s);
    };
  }

  private function addFileMenu(parent:NativeMenu):void
  {
    //trace('MenuBar.addFileMenu');
    var submenu:NativeMenu = new NativeMenu();
    parent.addSubmenu(submenu, "File");
    addMenuItem(submenu, "New", handleNew, "n");
    addMenuItem(submenu, "Open...", handleOpen, "o");
    addSeparator(submenu);
    addMenuItem(submenu, MenuBar.CLOSE_MENU_ITEM, handleClose, "w");
    addSeparator(submenu);
    addMenuItem(submenu, MenuBar.SAVE_MENU_ITEM, handleSave, "s");
    createSubmenu(submenu, "Save As...",
      [ DOE2BinFormat.FORMAT.fileDescriptionNoWildcard,
        DOE2FmtFormat.FORMAT.fileDescriptionNoWildcard,
        EnergyPlusEPWFormat.FORMAT.fileDescriptionNoWildcard,
        ElementsNativeFormat.FORMAT.fileDescriptionNoWildcard],
      [ handleExportType((new ExporterDOE2BIN) as IExporter),
        handleExportType((new ExporterDOE2FMT) as IExporter),
        handleExportType((new ExporterEnergyPlusEPW) as IExporter),
        handleExportType((new ExporterNativeFormat) as IExporter)]);
    if (!this.isApplicationMenu)
    {
      submenu.addItem(newSeparator());
      addMenuItem(submenu, "Exit", handleExit, "");
    }
  }

  public function addMenuItem(
    parent:NativeMenu,
    label:String,
    handler:Function,
    shortcut:String="",
    active:Boolean=true):void
  {
    //trace('MenuBar.addMenuItem');
    var menuItem:NativeMenuItem = new NativeMenuItem(label);
    if (shortcut != "")
    {
      menuItem.keyEquivalent = shortcut;
    }
    menuItem.addEventListener(Event.SELECT, handler);
    parent.addItem(menuItem);
    this._menuItems[label] = menuItem;
    if (!active) disabled.push(menuItem);
    menuItem.enabled = active;
  }

  public function createSubmenu(
    parent:NativeMenu,
    categoryName:String,
    itemLabels:Array,
    itemHandlers:Array):void
  {
    //trace('MenuBar.createSubmenu');
    var category:NativeMenuItem = new NativeMenuItem(categoryName);
    category.submenu = new NativeMenu();
    var addItem:Function = function(
      label:String,
      idx:int,
      array:Array):void
    {
      var item:NativeMenuItem = new NativeMenuItem(label);
      item.addEventListener(Event.SELECT, function(e:Event):void
      {
        itemHandlers[idx]();
      });
      category.submenu.addItem(item);
      _menuItems[categoryName + ":" + label] = item;
    };
    itemLabels.forEach(addItem);
    parent.addItem(category);
  }

  private function addEditMenu(parent:NativeMenu):void
  {
    //trace('MenuBar.addEditMenu');
    var submenu:NativeMenu = new NativeMenu();
    parent.addSubmenu(submenu, "Edit");
    addMenuItem(submenu, MenuBar.UNDO_MENU_ITEM, handleUndo, "z");
    addMenuItem(submenu, MenuBar.REDO_MENU_ITEM, handleRedo, "Z");
    addSeparator(submenu);
    addMenuItem(submenu, "Cut", handleCut, "x", false);
    addMenuItem(submenu, "Copy", handleCopy, "c");
    addMenuItem(submenu, "Paste", handlePaste, "v");
    addMenuItem(submenu, "Select All", handleSelectAll, "a", false);
  }

  private function handleCut(e:Event):void
  {
    trace('cut');
  }

  private function handleCopy(e:Event):void
  {
    // trace('MenuBar :: handleCopy');
    var apSt:AppState = AppHandlers.readAppState(AppHandlers.instance());
    var w:BaseWindow = apSt.activeWindow as BaseWindow;
    if (w == null) return;
    if (w.copyPaste == null) return;
    w.copyPaste.doCopy();
  }

  private function handlePaste(e:Event):void
  {
    // trace('MenuBar :: handlePaste');
    var ref:IManagedRef = currentDocRef();
    if (ref == null) return;
    ref.handleCommand(new DocPaste());
  }

  private function handleSelectAll(e:Event):void
  {
    // trace('select all');
  }

  private function getDocStateController():IManagedRef
  {
    //trace('MenuBar.getDocStateController');
    var appSC:IManagedRef = AppHandlers.instance();
    var as0:AppState = AppHandlers.readAppState(appSC);
    var aw:BaseWindow = as0.activeWindow as BaseWindow;
    if (aw == null) return null;
    return aw.docStateCtrlr;
  }

  private function handleUndo(e:Event):void
  {
    //trace('MenuBar.handleUndo');
    var sc:IManagedRef = getDocStateController();
    if (sc == null) return;
    sc.undo();
  }

  private function handleRedo(e:Event):void
  {
    //trace('MenuBar.handleRedo');
    var sc:IManagedRef = getDocStateController();
    if (sc == null) return;
    sc.redo();
  }

  private function addWindowMenu(parent:NativeMenu):void
  {
    //trace('MenuBar.addWindowMenu');
    var menuItem:NativeMenuItem;
    var submenu:NativeMenu = new NativeMenu;
    parent.addSubmenu(submenu, "Window");
    _windowMenu = submenu;
  }

  private function addHelpMenu(parent:NativeMenu):void
  {
    //trace('MenuBar.addHelpMenu');
    var menuItem:NativeMenuItem;
    var submenu:NativeMenu = new NativeMenu;
    parent.addSubmenu(submenu, "Help");
    addMenuItem(
      submenu,
      MenuBar.USER_DOCUMENTATION,
      MenuBar.handleUserDoc,
      '');
    addSeparator(submenu);
    addMenuItem(
      submenu,
      MenuBar.ONLINE_RESOURCES,
      MenuBar.handleOnlineResources,
      '');
    if (!this.isApplicationMenu)
    {
      addSeparator(submenu);
      addMenuItem(submenu, this.ABOUT, handleAbout, '');
    }
  }

  public function handleAbout(e:Event):void
  {
    //trace('MenuBar.handleAbout');
    var w:NativeWindow = NativeApplication.nativeApplication.activeWindow;
    var app:AppState = AppHandlers.readAppState(AppHandlers.instance());
    var wind:BaseWindow = app.activeWindow as BaseWindow;
    AppHandlers.instance().handleCommand(new AppSetModal(true));
    var f:File = File.applicationDirectory.resolvePath("htmldocs/about/about.html");
    var ver:String = AppHandlers.getAppVersion();
    var replace:Object = {};
    replace['VERSION'] = ver;
    var opts:WindowOptions = new WindowOptionsBuilder()
      .withWidth(372)
      .withHeight(570)
      .withResizable(false)
      .withMinimizable(false)
      .withReplace(replace)
      .withTitle("About Elements")
      .withPlainTitle(true)
      .withOnCloseFn(function():void {
        AppHandlers.instance().handleCommand(new AppSetModal(false));
      })
      .build();
    AppHandlers.instance().handleCommand(new AppNewHTMLDocument(f, opts));
  }

  public static function handleUserDoc(e:Event=null):void
  {
    //trace('MenuBar.handleUserDoc');
    var url:String = File.applicationDirectory.resolvePath("htmldocs/user-guide/index.html").url;
    NetUtils.openExternalURL(url);
  }

  public static function handleOnlineResources(e:Event=null):void
  {
    //trace('MenuBar.handleOnlineResources');
    NetUtils.openExternalURL(
      "http://bigladdersoftware.com/projects/elements/docs/");
  }

  public function handlePreferences(e:Event):void
  {
    //trace('MenuBar.handlePreferences');
    var preferences:Object = {};
    AppHandlers.instance().handleCommand(new AppSetPreferences(preferences));
  }

  private function newSeparator():NativeMenuItem
  {
    return new NativeMenuItem("", true);
  }

  public function updateActiveWindow():void
  {
    //trace('MenuBar.updateActiveWindow');
    var appSC:IManagedRef = AppHandlers.instance();
    var as0:AppState = AppHandlers.readAppState(appSC);
    var w:Object = as0.activeWindow;
    if (w == null) return;
    for each (var menuItem:NativeMenuItem in _windowMenu.items)
    {
      if (AppHandlers.equals(menuItem.data, w))
      {
        menuItem.checked = true;
      }
      else
      {
        menuItem.checked = false;
      }
    }
  }

  public function refreshWindowList():void
  {
    //trace('MenuBar.refreshWindowList');
    _windowMenu.removeAllItems();
    var appSC:IManagedRef = AppHandlers.instance();
    var as0:AppState = AppHandlers.readAppState(appSC);
    var menuItem:NativeMenuItem;
    var ws:IPVec = as0.openWindows;
    var aw:Object = as0.activeWindow;
    var makeHandler:Function = function(w:Object):Function
    {
      return function(e:Event):void
      {
        //trace('MenuBar.makeHandler (window selected handler)');
        AppHandlers.instance().handleCommand(new AppSetActiveWindow(w));
      };
    };
    for (var i:int=0; i<ws.length; i++)
    {
      var window:Object = ws.valAt(i);
      menuItem = new NativeMenuItem(window.title);
      if (as0.modal)
      {
        menuItem.enabled = false;
      }
      else
      {
        menuItem.enabled = true;
      }
      menuItem.data = window;
      menuItem.addEventListener(
        Event.SELECT,
        makeHandler(window));
      if (AppHandlers.equals(window, aw))
      {
        menuItem.checked = true;
      }
      else
      {
        menuItem.checked = false;
      }
      _windowMenu.addItem(menuItem);
    }
    //trace('MenuBar.refreshWindowList :: there are', ws.length, 'windows');
  }

  private function getActiveWindow():Object
  {
    //trace('MenuBar.getActiveWindow');
    var appSC:IManagedRef = AppHandlers.instance();
    var appState:AppState = AppHandlers.readAppState(appSC);
    return appState.activeWindow;
  }

  public function handleOpen(
    event:Event=null,
    runAfter:Function=null):void
  {
    // trace('MenuBar.handleOpen');
    // need to query preferences to see if lastDirectory exists
    var file:File = PreferencesListener.lastPath();
    trace('file path:', file.nativePath);
    //var file:File = File.documentsDirectory;
    file.addEventListener(Event.SELECT, function (e:Event):void {
      trace('Opening', file.extension);
      var imp:IImporter;
      switch (file.extension.toLowerCase())
      {
        case ".elements":
        case "elements":
          //trace('opening native format');
          imp = new ImporterNativeFormat();
          break;
        case ".bin":
        case "bin":
          //trace('opening DOE2 BIN');
          imp = new ImporterDOE2BIN();
          break;
        case ".fmt":
        case "fmt":
          //trace('opening DOE2 FMT');
          imp = new ImporterDOE2FMT();
          break;
        case ".epw":
        case "epw":
          //trace('opening EPW');
          imp = new ImporterEnergyPlusEPW();
          break;
        default:
          // do nothing
      }
      AppHandlers.instance().handleCommand(
        new AppImportIntoNewWin(imp, file));
      PreferencesListener.setLastPath(e.target as File);
      if (runAfter != null)
      {
        runAfter();
      }
    });
    var filters:Array = [
      ElementsNativeFormat.FORMAT.fileFilter,
      EnergyPlusEPWFormat.FORMAT.fileFilter,
      DOE2BinFormat.FORMAT.fileFilter,
      DOE2FmtFormat.FORMAT.fileFilter];
    if (Capabilities.os.indexOf("Windows") >= 0)
    {
      filters.unshift(AllFilesFormat.FORMAT.fileFilter);
    }
    file.browseForOpen("Open", filters);
  }

  public static function doImport(
    imp:IImporter,
    file:File):void
  {
    // trace('MenuBar.doImport');
    var appSC:IManagedRef = AppHandlers.instance();
    appSC.handleCommand(new AppNewDocument());
    var appState:AppState = AppHandlers.readAppState(appSC);
    var dw:BaseWindow = appState.activeWindow as BaseWindow;
    dw.docStateCtrlr.handleCommand(
      new DocImportData(imp, file));
    dw.docStateCtrlr.resetUndoRedo();
    dw.copyPaste = new DataGridCopyPaste(dw.docStateCtrlr);
  }

  public function dispose():void
  {
  }

  public function update(event:String):void
  {
    //trace('MenuBar.update');
    switch (event)
    {
      case (AppEvents.EVENT_ACTIVE_WINDOW_CHANGED):
        updateActiveWindow();
        updateAvailableMenus();
        break;
      case (AppEvents.EVENT_NEW_DOCUMENT):
      case (AppEvents.EVENT_OPEN_WINDOWS_CHANGED):
      case (AppEvents.EVENT_STARTUP_MODE_CHANGED):
      case (AppEvents.EVENT_MODALITY_CHANGED):
        refreshWindowList();
        updateAvailableMenus();
        break;
      case (DocEvents.EVENT_FILE_STATUS_CHANGED):
        updateAvailableMenus();
      default:
        break;
    }
  }
  /**
   * Disable menus that depend on having at least one active window. That is,
   * change which menus are "available" to select.
   */
  public function updateAvailableMenus():void
  {
    //trace('MenuBar.updateAvailableMenus');
    var appState:AppState = AppHandlers.readAppState(AppHandlers.instance());

    var numWins:int = appState.numberOfOpenWindows;
    var mi:NativeMenuItem;
    if (appState.startupMode||appState.modal)
    {
      var userDocsMI:NativeMenuItem = _menuItems[MenuBar.USER_DOCUMENTATION];
      var aboutMI:NativeMenuItem = _menuItems[this.ABOUT];
      var exitMI:NativeMenuItem;
      if (_menuItems.hasOwnProperty("Exit"))
      {
        exitMI = _menuItems["Exit"] as NativeMenuItem;
      }
      for each (mi in _menuItems)
      {
        if (userDocsMI === mi) continue;
        //if (aboutMI === mi) continue;
        if (mi.label == MenuBar.CLOSE_MENU_ITEM) continue;
        mi.enabled = false;
      }
      if (appState.startupMode && !appState.modal)
      {
        aboutMI.enabled = true;
      }
      if (exitMI != null)
      {
        exitMI.enabled = true;
      }
    }
    else
    {
      for each (mi in _menuItems)
      {
        if (ArrayUtils.contains(disabled, mi))
        {
          mi.enabled = false
        }
        else
        {
          mi.enabled = true;
        }
      }
      mi = _menuItems[MenuBar.CLOSE_MENU_ITEM];
      if (numWins == 0)
      {
        mi.enabled = false;
      }
      else
      {
        mi.enabled = true;
      }
    }
    if (!appState.startupMode)
    {
      // always allow UNDO/REDO to be available because some modal
      // dialogues could benefit from it
      (_menuItems[MenuBar.UNDO_MENU_ITEM] as NativeMenuItem).enabled = true;
      (_menuItems[MenuBar.REDO_MENU_ITEM] as NativeMenuItem).enabled = true;
    }
    var w:BaseWindow = appState.activeWindow as BaseWindow;
    var ds:DocState;
    if (w == null)
    {
      ds = null;
    }
    else
    {
      ds = DocHandlers.readDocState(w.docStateCtrlr);
    }
    if ((ds == null) || (ds.file == null) || !ds.fileModified)
    {
      // can't save if there's no default file, no default window,
      // or if file isn't modified... must "save as" instead
      (_menuItems[MenuBar.SAVE_MENU_ITEM] as NativeMenuItem).enabled = false;
    }
  }
}
}