/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements.views {
import mx.charts.AxisRenderer;
import mx.charts.GridLines;
import mx.containers.Canvas;
import mx.graphics.SolidColorStroke;

public class ThumbnailChart extends MixedSeriesChart {

    public function ThumbnailChart() {
        super();
        this.showDataTips = false;
        var hAxisRenderer:AxisRenderer = this.horizontalAxisRenderers[0];
        hAxisRenderer.visible = false;
        hAxisRenderer.setStyle("showLabels", false);
        hAxisRenderer.setStyle("tickPlacement", "none");
        hAxisRenderer.setStyle("axisStroke",
            new SolidColorStroke(0xFF0000, 0));
        this.setStyle("gutterBottom", 0);
        var gridLines:GridLines = new GridLines();
        gridLines.visible = false;
        this.backgroundElements = [gridLines];
        var canvas:Canvas = new Canvas();
        canvas.percentWidth = 100;
        canvas.percentHeight = 100;
        canvas.setStyle("borderStyle", "solid");
        canvas.setStyle("borderColor", 0x999999);
        canvas.setStyle("borderThickness", 1);
        // not sure if needed...may be necessary for series to show through
        canvas.setStyle("backgroundAlpha", 0);
        this.annotationElements = [canvas];
    }
}
}