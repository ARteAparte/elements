/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements.views {
import mx.collections.ArrayCollection;

import spark.components.DropDownList;
import spark.components.Group;
import spark.components.Label;
import spark.events.IndexChangeEvent;

import dse.MetaData;

import elements.DocEvents;
import elements.DocHandlers;
import elements.DocState;
import elements.commands.DocSetActiveSettingSet;

import gui.AcmeManagedRef;
import gui.IManagedRef;

import pdt.IPMap;

public class ActiveSettingSetPulldown extends Group {
    public var label:Label;
    public var pulldown:DropDownList;
    public var _docSC:IManagedRef;

    public function ActiveSettingSetPulldown(docSC:IManagedRef) {
        super();
        var vg:Group = ViewUtils.makeHGroup();
        _docSC = docSC;
        var ds:DocState = DocHandlers.readDocState(_docSC);
        label = new Label();
        label.text = 'Variables to Hold Constant:';
        label.styleName = "heading";
        label.height = 10;
        pulldown = new DropDownList();
        pulldown.requireSelection = true;
        pulldown.width = 300;
        pulldown.addEventListener(IndexChangeEvent.CHANGE, onIndexChanged);
        if (ds.selections.length == 0) {
            pulldown.enabled = false;
        }
        vg.addElement(label);
        vg.addElement(pulldown);
        this.addElement(vg);
        update(DocEvents.EVENT_ACTIVE_SETTING_SET_CHANGED);
        _docSC.registerSubscriberFunction(update);
    }

    public function onIndexChanged(e:IndexChangeEvent):void {
        // change index of setting set for selected series data
        var ds:DocHandlers = _docSC.read() as DocHandlers;
        _docSC.handleCommand(
          new DocSetActiveSettingSet(pulldown.selectedIndex));
    }

    public function update(event:String):void {
        var selectedIdx:int;
        if ((event == DocEvents.EVENT_SELECTION_CHANGED) ||
            (event == AcmeManagedRef.EVENT_STATE_CHANGED)) {
            var ds:DocState = DocHandlers.readDocState(_docSC);
            var NA:String = 'Not Applicable';
            if (DocHandlers.singleSeriesSelection(ds)) {
                var sssm:Object = ds.dataSet.settingSetStringMap;
                var md:MetaData = ds.metaDataForSelectedSeries;
                var settingSets:Array = sssm[md.name];
                pulldown.dataProvider = new ArrayCollection(settingSets);
                if (settingSets.length == 0) {
                    pulldown.enabled = false;
                } else {
                    pulldown.enabled = true;
                }
                (pulldown.dataProvider as ArrayCollection).refresh();
                var activeSettingSets:IPMap = ds.activeSettingSets;
                if (activeSettingSets.containsKey(md.name)) {
                    //trace('PREVIOUS SETTING SET DETECTED');
                    selectedIdx = activeSettingSets.valAt(md.name) as int;
                    pulldown.selectedIndex = selectedIdx;
                } else {
                    //trace('NO PREVIOUS SETTINGS DETECTED');
                    pulldown.selectedIndex = 0;
                }
            } else {
                pulldown.enabled = false;
                pulldown.selectedIndex = 0;
            }
        } else if (event == DocEvents.EVENT_ACTIVE_SETTING_SET_CHANGED) {
            ds = DocHandlers.readDocState(_docSC);
            if (DocHandlers.singleSeriesSelection(ds)) {
                md = ds.metaDataForSelectedSeries;
                activeSettingSets = ds.activeSettingSets;
                if (activeSettingSets.containsKey(md.name)) {
                    selectedIdx = activeSettingSets.valAt(md.name) as int;
                    if (pulldown.selectedIndex == selectedIdx) {
                        return;
                    } else {
                        pulldown.selectedIndex = selectedIdx;
                    }
                }
            }
        }
    }
}
}