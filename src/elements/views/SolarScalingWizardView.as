/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements.views {
import mx.collections.ArrayCollection;
import mx.core.IFactory;

import spark.components.DataGrid;
import spark.components.Group;
import spark.components.Label;
import spark.components.VGroup;
import spark.components.gridClasses.GridColumn;
import spark.components.gridClasses.GridSelectionMode;
import spark.layouts.VerticalLayout;

import dse.MetaData;

import elements.DataDisplayOptions;
import elements.DocState;
import elements.DocHandlers;
import elements.SolarScaleGridItemEditorFactory;

import gui.AcmeManagedRef;
import gui.IManagedRef;

import units.IrradiationUnits;
import units.NoUnits;

import utils.FormatUtils;
import utils.MapUtils;

public class SolarScalingWizardView extends Group {
    /* monthlyAveragedDailyRadiations :: Map MonthName [Number]
    type MonthName = String */
    private var _data:ArrayCollection;
    private var _dataGrid:DataGrid;
    private var _docSC:IManagedRef;

    public function SolarScalingWizardView(docSC:IManagedRef) {
        super();
        _docSC = docSC;
        var ds:DocState = DocHandlers.readDocState(_docSC);
        percentWidth = 100;
        percentHeight = 100;
        layout = new VerticalLayout();
        var vg:VGroup = ViewUtils.makeVGroup();
        var lab:Label = new Label();
        lab.text = "Monthly Averaged Daily Solar Insolation Scaler";
        vg.addElement(lab);
        _docSC.registerSubscriberFunction(update);
        _dataGrid = new DataGrid();
        _dataGrid.percentHeight = 100.0;
        _dataGrid.percentWidth = 100.0;
        _dataGrid.selectionMode = GridSelectionMode.SINGLE_CELL;
        _dataGrid.editable = true;
        _dataGrid.dataProvider =
            DocHandlers.deriveMonthlyAverageDailyIrradiation(ds);
        _dataGrid.columns = makeDataGridColumns();
        vg.addElement(_dataGrid);
        addElement(vg);
    }

    public function
    update(event:String):void {
        switch (event) {
        case DocHandlers.EVENT_ALL_CHANGED:
        case DocHandlers.EVENT_SERIES_DATA_CHANGED:
        case AcmeManagedRef.EVENT_STATE_CHANGED:
            var ds:DocState = DocHandlers.readDocState(_docSC);
            _dataGrid.dataProvider =
              DocHandlers.deriveMonthlyAverageDailyIrradiation(ds);
            _dataGrid.invalidateDisplayList();
            _dataGrid.validateNow();
            break;
        default:
            trace('unhandled event for SolarScalingWizardView', event);
        }
    }

    private function
    makeDataGridColumns():ArrayCollection {
        trace('SolarScalingWizardView.makeDataGridColumns');
        var columns:ArrayCollection = new ArrayCollection();
        var docState:DocState = DocHandlers.readDocState(_docSC);
        var opts:Object = {
            monthYear: new DataDisplayOptions(),
            Hgh_MJ__m2: new DataDisplayOptions(),
            HghDesired_MJ__m2: new DataDisplayOptions()
        };
        var precision:int = 2;
        var makeLabelFunc:Function = function (name:String):Function {
            return function (item:Object, c:GridColumn):String {
                //trace('name:', name);
                return FormatUtils.formatFloat(item[name], precision);
            };
        };
        (opts.monthYear as DataDisplayOptions).makeLabelFunction =
            function (item:Object, c:GridColumn):String {
                //trace('label function for monthYear');
                //if (item == null) {
                //    return "item null";
                //}
                //MapUtils.traceObject(item, 'item');
                var mStr:String = FormatUtils.fixStringLength(
                    2, item.month.toString(), '0', FormatUtils.PAD_LEFT);
                return mStr + "/" + item.year.toString();
            };
        (opts.Hgh_MJ__m2 as DataDisplayOptions).makeLabelFunction =
            makeLabelFunc('Hgh_MJ__m2');
        (opts.HghDesired_MJ__m2 as DataDisplayOptions).makeLabelFunction =
            makeLabelFunc('Hgh_MJ__m2');
        (opts.HghDesired_MJ__m2 as DataDisplayOptions).itemEditorMaker =
            function(docSC:IManagedRef):IFactory {
                return new SolarScaleGridItemEditorFactory(docSC) as IFactory;
            };
        var seriesMetaMap:Object = {
            monthYear: new MetaData('monthYear', 'Month/Year',
                NoUnits.NONE, NoUnits.QUANTITY_OF_MEASURE,
                MetaData.makeValueGetter('monthYear'),
                MetaData.DATA_TYPE__STRING, null, null, null, false,
                []),
            Hgh_MJ__m2: new MetaData('Hgh_MJ__m2',
                'Monthly Averaged Daily Radiation',
                IrradiationUnits.MJ__M2,
                IrradiationUnits.QUANTITY_OF_MEASURE,
                MetaData.makeValueGetter('Hgh_MJ__m2'),
                MetaData.DATA_TYPE__NUMBER,
                [{}], null, null, false, IrradiationUnits.AVAILABLE_UNITS),
            HghDesired_MJ__m2: new MetaData('HghDesired_MJ__m2',
                'Desired Monthly Averaged Daily Radiation',
                IrradiationUnits.MJ__M2,
                IrradiationUnits.QUANTITY_OF_MEASURE,
                MetaData.makeValueGetterWithDefault(
                    'HghDesired_MJ__m2', 0.0),
                MetaData.DATA_TYPE__NUMBER, null, null, null,
                true, IrradiationUnits.AVAILABLE_UNITS)
        };
        var seriesNames:ArrayCollection = new ArrayCollection([
            'monthYear', 'Hgh_MJ__m2', 'HghDesired_MJ__m2'
        ]);
        for each (var name:String in seriesNames) {
            var seriesMeta:MetaData = seriesMetaMap[name] as MetaData;
            var column:GridColumn = new GridColumn();
            column.dataField = seriesMeta.fullName;
            if (opts.hasOwnProperty(name)) {
                var optSet:DataDisplayOptions =
                    opts[name] as DataDisplayOptions;
                if (optSet != null) {
                    trace('setting column editors and renderers:', name);
                    column.labelFunction = optSet.makeLabelFunction;
                    //    optSet.makeLabelFunction(_docSC);
                    if (seriesMetaMap.hasOwnProperty(name)
                        &&(seriesMetaMap[name] as MetaData).editable) {
                        trace(name, 'IS editable');
                        column.itemEditor =
                            optSet.itemEditorMaker(_docSC);
                    } else {
                        trace(name, 'is NOT editable');
                        column.editable = false;
                    }
                }
            }
            column.sortable = false;
            columns.addItem(column);
        }
        return columns;
    }
}
}