/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements.commands {
import elements.DocEvents;
import elements.DocHandlers;
import elements.DocState;

import gui.HandlerReturn;
import gui.IUndoableCommand;

public class DocScaleByMinMax implements IUndoableCommand {
  public var _oldMin:Number;
  public var _oldMax:Number;
  public var _newMin:Number;
  public var _newMax:Number;
  // oldMin, oldMax, newMin, newMax are in base units
  public function DocScaleByMinMax(oldMin:Number, oldMax:Number, newMin:Number,
                                   newMax:Number) {
    _oldMin = oldMin;
    _oldMax = oldMax;
    _newMin = newMin;
    _newMax = newMax;
  }
  public function run(state:*):HandlerReturn {
    var ds:DocState = state as DocState;
    var args:Object = {
      oldMin:_oldMin, oldMax:_oldMax,
      newMin:_newMin, newMax:_newMax};
    if (!(DocHandlers.validArgsForScaleByMinMax(args))) {
      return new HandlerReturn(ds, []);
    }
    var f:Function = function(x:Number):Number {
      if (_oldMin == _oldMax) {
        return (_newMin + _newMax) / 2.0;
      } else if (_newMin == _newMax) {
        return _newMin;
      }
      var out:Number =
        _newMin + (x - _oldMin) * (_newMax - _newMin) / (_oldMax - _oldMin);
      return out;
    };
    var hr:HandlerReturn = (new DocSetFileModified()).run(
      DocHandlers.mapSelected(ds, f));
    var events:Array = [DocEvents.EVENT_SERIES_DATA_CHANGED];
    if (ds.fileModified != (hr.state as DocState).fileModified) {
      events.push(DocEvents.EVENT_FILE_STATUS_CHANGED);
    }
    return new HandlerReturn(hr.state as DocState, events);
  }
  public function get undoable():Boolean {
    return true;
  }
}
}