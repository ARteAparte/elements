/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements.commands {
import elements.AppEvents;
import elements.AppHandlers;
import elements.WindowOptions;
import elements.WindowOptionsBuilder;

import gui.HandlerReturn;
import gui.IUndoableCommand;

public class AppNewDocument implements IUndoableCommand {
  public function AppNewDocument() {
  }
  public function run(state:*):HandlerReturn {
    var opts:WindowOptions = (new WindowOptionsBuilder())
      .withHeight(600)
      .withWidth(1200)
      .withMinHeight(300)
      //.withMinWidth(600)
      .build();
    return new HandlerReturn(
      AppHandlers.addNewDocumentWindowAndSetActive(state, opts),
      [AppEvents.EVENT_OPEN_WINDOWS_CHANGED])
  }
  public function get undoable():Boolean {
    return false;
  }
}
}