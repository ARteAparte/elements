/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements.commands {
import flash.filesystem.File;

import dse.DataSet;

import elements.DocEvents;
import elements.DocState;
import elements.PreferencesListener;

import gui.HandlerReturn;
import gui.IUndoableCommand;

import weatherfile.exporters.ExporterDOE2BIN;
import weatherfile.exporters.ExporterDOE2FMT;
import weatherfile.exporters.ExporterEnergyPlusEPW;
import weatherfile.exporters.ExporterNativeFormat;
import weatherfile.exporters.IExporter;

public class DocSaveFile implements IUndoableCommand {
  public var _file:File;
  public function DocSaveFile(file:File) {
    _file = file;
  }
  public function run(state:*):HandlerReturn {
    var ds:DocState = state as DocState;
    var file:File;
    if (_file == null) {
      file = ds.file;
    } else {
      file = _file;
    }
    if (file == null) {
      return new HandlerReturn(ds, []);
    }
    // Switch based on file type...
    var exp:IExporter;
    switch (file.extension.toLowerCase()) {
      case "elements":
        trace('saving to elements');
        exp = new ExporterNativeFormat();
        break;
      case "epw":
        trace('saving to epw');
        exp = new ExporterEnergyPlusEPW();
        break;
      case "bin":
        trace('saving to bin');
        exp = new ExporterDOE2BIN();
        break;
      case "fmt":
        trace('saving to fmt');
        exp = new ExporterDOE2FMT();
        break;
      default:
        // unknown format... return
        trace('Unknown file format... returning');
        return new HandlerReturn(ds, []);
    }
    var dataSet:DataSet = ds.dataSet;
    exp.write(file, dataSet);
    var hr:HandlerReturn = (new DocSetFileUnModified(file)).run(ds);
    var newDS:DocState = hr.state as DocState;
    var events:Array = [];
    events.push(DocEvents.EVENT_FILE_STATUS_CHANGED);
    // Update preferences with lastPath
    PreferencesListener.setLastPath(file);
    return new HandlerReturn(newDS, events);
  }
  public function get undoable():Boolean {
    return false;
  }
}
}