/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements.commands {
import elements.DocEvents;
import elements.DocState;

import gui.HandlerReturn;
import gui.IUndoableCommand;

import plugins.PlugInfo;

import units.UnitInfo;
import units.UnitSystems;

import utils.MapUtils;

public class DocSetDisplayUnitSys implements IUndoableCommand {
  public var _unitSystem:String;
  public function DocSetDisplayUnitSys(unitSystem:String) {
    //trace('DocSetDisplayUnitSys: unitSystem is ', unitSystem);
    _unitSystem = unitSystem;
  }
  public function run(state:*):HandlerReturn {
    //trace('DocSetDisplayUnitSys.run');
    //trace('DocSetDisplayUnitSys.run :: _unitSystem', _unitSystem);
    var ds:DocState = state as DocState;
    var events:Array = [DocEvents.EVENT_DISPLAY_UNITS_CHANGED];
    if (_unitSystem == UnitSystems.CUSTOM) {
      var newDS:DocState = ds.settingDisplayUnitsTo(
        ds.displayUnits.assoc(UnitSystems.UNIT_SYSTEM,
          UnitSystems.CUSTOM));
      return new HandlerReturn(newDS, events);
    }
    return new HandlerReturn(
      ds.settingDisplayUnitsTo(
        UnitInfo.makeDisplayUnitsForUnitSystem(
          _unitSystem,
          MapUtils.merge(
            PlugInfo.knownHeaderMetaData(),
            PlugInfo.knownSeriesMetaData()))),
      events);
  }
  public function get undoable():Boolean {
    return false;
  }
}
}