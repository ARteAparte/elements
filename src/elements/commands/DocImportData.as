/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements.commands {
import flash.filesystem.File;

import dse.DataSet;

import elements.DataDisplayOptions;
import elements.DocEvents;
import elements.DocHandlers;
import elements.DocState;
import elements.DocStateBuilder;

import gui.HandlerReturn;
import gui.IUndoableCommand;

import pdt.IPMap;
import pdt.PersistentArrayMap;
import pdt.PersistentVector;

import weatherfile.importers.IImporter;

public class DocImportData implements IUndoableCommand {
  public var _importer:IImporter;
  public var _file:File;
  public function DocImportData(importer:IImporter, file:File) {
    _importer = importer;
    _file = file;
  }
  public function run(state:*):HandlerReturn {
    var ds:DocState = state as DocState;
    var dataSet:DataSet;
    var imp:IImporter = _importer;
    if ((_file != null) && ((_file as File).exists)) {
      dataSet = imp.read(_file);
      var fileName:String = _file.name;
      var smd:Object = dataSet.seriesMetaData;
      var dom:IPMap = DataDisplayOptions.makeDefaultDisplayOptions(smd);
      var newDS:DocState = (new DocStateBuilder())
        .withDataSet(dataSet)
        .withDisplayUnits(ds.displayUnits)
        .withSelections(PersistentVector.EMPTY)
        .withDisplayOptionsMap(dom)
        .withShownData(DocHandlers.defaultShownDataColumns(smd))
        .withFileName(fileName)
        .withTitle(fileName)
        .withFile(ds.file)
        .withFileModified(false)
        .withActiveSettingSets(PersistentArrayMap.EMPTY)
        .build();
      var events:Array = [DocEvents.EVENT_ALL_CHANGED];
      return new HandlerReturn(newDS, events);
    }
    return new HandlerReturn(ds, []);
  }
  public function get undoable():Boolean {
    return false;
  }
}
}