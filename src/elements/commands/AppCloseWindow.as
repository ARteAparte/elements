/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements.commands {
import elements.AppEvents;
import elements.AppHandlers;
import elements.AppState;
import elements.views.BaseWindow;

import gui.HandlerReturn;
import gui.IUndoableCommand;

public class AppCloseWindow implements IUndoableCommand {
  public var _w:Object;

  public function AppCloseWindow(w:Object) {
    _w = w;
  }
  public function run(state:*):HandlerReturn {
    var apSt:AppState = state as AppState;
    var ws:Array = apSt.openWindows.toArray();
    var apStNext:AppState = apSt;
    var hr:HandlerReturn;
    for each (var win:Object in ws) {
      //trace('checking win to close');
      var baseWin:BaseWindow = win as BaseWindow;
      var baseWinToClose:BaseWindow = _w as BaseWindow;
      if (baseWin.parentWindow != null) {
        //trace('found window with parent != null');
        if (baseWin.parentWindow === baseWinToClose) {
          //trace('closing window');
          apStNext = AppHandlers.removeWindow(apStNext, baseWin);
          hr = (new AppSetActiveWindow(apStNext.activeWindow)).run(apStNext);
        }
      }
    }
    apStNext = AppHandlers.removeWindow(apStNext, _w);
    hr = (new AppSetActiveWindow(apStNext.activeWindow)).run(apStNext);
    var events:Array = hr.events;
    events.push(AppEvents.EVENT_OPEN_WINDOWS_CHANGED);
    return new HandlerReturn(
      hr.state,
      events);
  }
  public function get undoable():Boolean {
    return false;
  }
}
}