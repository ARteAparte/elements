/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements.commands {
import elements.DocEvents;
import elements.DocState;

import gui.HandlerReturn;
import gui.IUndoableCommand;

import pdt.IPVec;
import pdt.PersistentVector;

public class DocShiftSeriesColumnToHead implements IUndoableCommand {
  public var _seriesName:String;
  public function DocShiftSeriesColumnToHead(seriesName:String) {
    _seriesName = seriesName;
  }
  public function run(state:*):HandlerReturn {
    var ds:DocState = state as DocState;
    var events:Array = [DocEvents.EVENT_DISPLAYED_DATA_COLUMNS_CHANGED];
    var shownDataColumns:IPVec = ds.shownData;
    var mutableCols:Array = new Array();
    var start:String = null;
    for (var idx:int=0; idx < shownDataColumns.length; idx++) {
      var seriesName:String = shownDataColumns.getItemAt(idx) as String;
      if (_seriesName == seriesName) {
        if (idx == 0) {
          return new HandlerReturn(ds, []);
        } else {
          start = seriesName;
        }
      } else {
        mutableCols.push(seriesName);
      }
    }
    if (start != null) {
      mutableCols.unshift(start);
    }
    var newDS:DocState = ds.settingShownDataTo(
      PersistentVector.create(mutableCols));
    return new HandlerReturn(newDS, events);
  }
  public function get undoable():Boolean {
    return false;
  }
}
}