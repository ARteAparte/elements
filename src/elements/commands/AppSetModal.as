/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements.commands {
import elements.AppEvents;
import elements.AppState;

import gui.HandlerReturn;
import gui.IUndoableCommand;

public class AppSetModal implements IUndoableCommand {
  public var _modal:Boolean;
  public function AppSetModal(modal:Boolean) {
    _modal = modal;
  }

  public function run(state:*):HandlerReturn {
    var appSt:AppState = state as AppState;
    return (new HandlerReturn(appSt.withModal(_modal),
      [AppEvents.EVENT_MODALITY_CHANGED]));
  }

  public function get undoable():Boolean {
    return false;
  }
}
}