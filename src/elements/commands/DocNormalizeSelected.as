/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements.commands {
import elements.DocEvents;
import elements.DocHandlers;
import elements.DocState;

import gui.HandlerReturn;
import gui.IUndoableCommand;

public class DocNormalizeSelected implements IUndoableCommand {
  public var _mean:Number;
  public function DocNormalizeSelected(mean:Number) {
    _mean = mean;
  }
  public function run(state:*):HandlerReturn {
    var ds:DocState = state as DocState;
    var args:Object = {mean: _mean};
    if (!DocHandlers.isProperToolCall(ds, args, 'mean')) {
      return new HandlerReturn(ds, []);
    }
    var g:Function = function(sumAndCount:Object, x:Number):Object {
      sumAndCount.sum += x;
      sumAndCount.count += 1.0;
      return sumAndCount;
    };
    var sumAndCount:Object = DocHandlers.reduceSelected(
      ds, {sum:0.0, count:0.0}, g);
    var currentMean:Number = sumAndCount.sum / sumAndCount.count;
    var dsNew:DocState;
    if (currentMean == 0.0) {
      var ucfs:Object = ds.unitConversionFunctionsForSelection;
      var hrOffset:HandlerReturn = (
        new DocOffsetSelected(ucfs.b2d(_mean))).run(ds);
      dsNew = hrOffset.state as DocState;
    } else {
      var scaleFactor:Number = _mean / currentMean;
      var hrScaled:HandlerReturn = (new DocScaleSelected(scaleFactor)).run(ds);
      dsNew = hrScaled.state as DocState;
    }
    var hr:HandlerReturn = (new DocSetFileModified()).run(dsNew);
    var events:Array = [
      DocEvents.EVENT_SERIES_DATA_CHANGED,
      DocEvents.EVENT_SELECTION_CHANGED
    ];
    if (ds.fileModified != (hr.state as DocState).fileModified) {
      events.push(DocEvents.EVENT_FILE_STATUS_CHANGED);
    }
    return new HandlerReturn(hr.state as DocState, events);
  }
  public function get undoable():Boolean {
    return true;
  }
}
}