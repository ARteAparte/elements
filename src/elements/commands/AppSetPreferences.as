/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements.commands {
import elements.AppEvents;
import elements.AppState;

import gui.HandlerReturn;
import gui.IUndoableCommand;

import pdt.IPMap;
import pdt.PersistentArrayMap;

public class AppSetPreferences implements IUndoableCommand {
  public var _prefs:Object;
  public function AppSetPreferences(preferences:Object) {
    _prefs = preferences;
  }
  public function run(state:*):HandlerReturn {
    var aSt:AppState = state as AppState;
    var prefs:IPMap = PersistentArrayMap.createFromObj(_prefs);
    return new HandlerReturn(
      aSt.withPreferences(prefs), [AppEvents.EVENT_PREFERENCES_CHANGED]);
  }
  public function get undoable():Boolean {
    return false;
  }
}
}