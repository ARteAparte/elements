/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements.commands {
  import elements.DocEvents;
  import elements.DocHandlers;
  import elements.DocState;

  import gui.HandlerReturn;
  import gui.IUndoableCommand;

  import units.UnitConversions;

  public class DocScaleSelectedTemperatures implements IUndoableCommand {
    public var _avg:Number;
    public var _tgtAvg:Number;
    // input average and targetAverage in base units
    public function DocScaleSelectedTemperatures(average:Number,
                                                 targetAverage:Number) {
      _avg = average;
      _tgtAvg = targetAverage;
    }
    public function run(state:*):HandlerReturn {
      //trace('DocScaleSelectedTemperatures');
      //trace('input average:', _avg);
      //trace('input target average:', _tgtAvg);
      var ds:DocState = state as DocState;
      var ucfs:Object = ds.unitConversionFunctionsForSelection;
      var C_to_K:Function = UnitConversions.C_to_K;
      var K_to_C:Function = UnitConversions.K_to_C;
      var scaleFactor:Number = C_to_K(_tgtAvg) / C_to_K(_avg);
      //trace('scale factor:', scaleFactor);
      if (!DocHandlers.isProperToolCall(ds, {scaleFactor:scaleFactor},
        'scaleFactor')) {
        return new HandlerReturn(ds, []);
      }
      var f:Function = function(x:Number):Number {
        // note: assumes base units is C
        var absVal:Number = C_to_K(x);
        var scaledAbsVal:Number = absVal * scaleFactor;
        return K_to_C(scaledAbsVal);
      };
      var hr:HandlerReturn = (new DocSetFileModified()).run(
        DocHandlers.mapSelected(ds, f));
      return new HandlerReturn(
        hr.state as DocState,
        [ DocEvents.EVENT_SERIES_DATA_CHANGED,
          DocEvents.EVENT_FILE_STATUS_CHANGED,
          DocEvents.EVENT_SELECTION_CHANGED
        ]);
    }
    public function get undoable():Boolean {
      return true;
    }
  }
}