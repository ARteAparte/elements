/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements.commands {
import dse.SeriesSelection;

import elements.DocEvents;
import elements.DocState;

import gui.HandlerReturn;
import gui.IUndoableCommand;

import pdt.IPVec;
import pdt.PersistentVector;

public class DocSetSelectionToSeries implements IUndoableCommand {
  public var _name:String;
  public function DocSetSelectionToSeries(name:String) {
    _name = name;
  }
  public function run(state:*):HandlerReturn {
    var ds:DocState = state as DocState;
    var points:IPVec = ds.dataSet.points;
    var newDS:DocState = ds.settingSelectionsTo(PersistentVector.create([
      new SeriesSelection(points, _name, 0, points.length)]));
    var events:Array = [];
    if (ds.selections != newDS.selections) {
      events.push(DocEvents.EVENT_SELECTION_CHANGED);
    }
    return new HandlerReturn(newDS, events);
  }
  public function get undoable():Boolean {
    return false;
  }
}
}