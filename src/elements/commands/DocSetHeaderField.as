/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements.commands {
import elements.DocEvents;
import elements.DocState;

import gui.HandlerReturn;
import gui.IUndoableCommand;

import utils.MapUtils;

public class DocSetHeaderField implements IUndoableCommand {
  public var _name:String;
  public var _value:*;
  public function DocSetHeaderField(name:String, value:*) {
    _name = name;
    _value = value;
  }
  public function run(state:*):HandlerReturn {
    var ds:DocState = state as DocState;
    var hdr:Object = ds.dataSet.header;
    var newHdr:Object = MapUtils.copyObject(hdr);
    newHdr[_name] = _value;
    var hr:HandlerReturn = (new DocSetFileModified()).run(
      ds.settingDataSetTo(ds.dataSet.settingHeaderTo(newHdr)));
    var events:Array = [DocEvents.EVENT_HEADER_CHANGED];
    if (ds.fileModified != (hr.state as DocState).fileModified) {
      events.push(DocEvents.EVENT_FILE_STATUS_CHANGED);
    }
    return new HandlerReturn(hr.state, events);
  }
  public function get undoable():Boolean {
    return true;
  }
}
}