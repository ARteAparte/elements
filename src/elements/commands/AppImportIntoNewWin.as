/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements.commands {
import flash.filesystem.File;

import dse.DataSet;

import elements.AppEvents;
import elements.AppHandlers;
import elements.AppState;
import elements.DataDisplayOptions;
import elements.DataGridCopyPaste;
import elements.DocHandlers;
import elements.DocState;
import elements.DocStateBuilder;
import elements.DocumentMaker;
import elements.PreferencesListener;
import elements.WindowOptions;
import elements.WindowOptionsBuilder;
import elements.views.AcmeMainView;

import gui.HandlerReturn;
import gui.IManagedRef;
import gui.IUndoableCommand;

import pdt.IPMap;
import pdt.PersistentArrayMap;
import pdt.PersistentVector;

import units.UnitSystems;

import weatherfile.importers.IImporter;

public class AppImportIntoNewWin implements IUndoableCommand
{
  public var _imp:IImporter;
  public var _file:File;
  public function AppImportIntoNewWin(importer:IImporter, file:File)
  {
    _imp = importer;
    _file = file;
  }
  public function run(state:*):HandlerReturn
  {
    var apSt:AppState = state as AppState;
    if ((_file != null) && ((_file as File).exists))
    {
      var dataSet:DataSet = _imp.read(_file);
      var prefs:Object = DocumentMaker.loadPreferences();
      var units:IPMap = PersistentArrayMap.EMPTY;
      if (prefs.hasOwnProperty(UnitSystems.UNIT_SYSTEM))
      {
        units = new PersistentArrayMap(
          [UnitSystems.UNIT_SYSTEM, prefs[UnitSystems.UNIT_SYSTEM]]);
      }
      var fileName:String = _file.name;
      var smd:Object = dataSet.seriesMetaData;
      var dom:IPMap = DataDisplayOptions.makeDefaultDisplayOptions(smd);
      var newDS:DocState = (new DocStateBuilder())
        .withDataSet(dataSet)
        .withDisplayUnits(units)
        .withSelections(PersistentVector.EMPTY)
        .withDisplayOptionsMap(dom)
        .withShownData(DocHandlers.defaultShownDataColumns(smd))
        .withFileName(fileName)
        .withTitle(fileName)
        .withFile(_file)
        .withFileModified(false)
        .withActiveSettingSets(PersistentArrayMap.EMPTY)
        .build();
      var ref:IManagedRef = DocHandlers.makeControlledDocState(newDS);
      var pl:PreferencesListener = new PreferencesListener(ref);
      var view:AcmeMainView = new AcmeMainView(ref);
      var opts:WindowOptions = new WindowOptionsBuilder()
        .withHeight(600)
        .withWidth(1200)
        .withMinHeight(300)
        //.withMinWidth(600)
        .withCopyPaste(new DataGridCopyPaste(ref))
        .build();
      var w:Object = DocumentMaker.newDocumentFromView(view, ref, true, opts);
      var events:Array = [AppEvents.EVENT_OPEN_WINDOWS_CHANGED];
      var newAppSt:AppState = AppHandlers.addWindowAndSetActive(apSt, w);
      return new HandlerReturn(newAppSt, events);
    }
    return new HandlerReturn(apSt, []);
  }
  public function get undoable():Boolean
  {
    return false;
  }
}
}