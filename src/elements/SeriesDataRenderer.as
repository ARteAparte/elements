/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements {
import spark.skins.spark.DefaultGridItemRenderer;

import dse.DataSet;
import dse.MetaData;

import gui.IManagedRef;

import utils.FormatUtils;

public class SeriesDataRenderer extends DefaultGridItemRenderer  {
    private var _docSC:IManagedRef;

    public function SeriesDataRenderer(docSC:IManagedRef) {
        super();
        _docSC = docSC;
    }

    override public function prepare(willBeRecycled:Boolean):void {
        var ds:DocState = DocHandlers.readDocState(_docSC);
        var hdr:Object = ds.dataSet.header;
        var name:String = this.column.dataField;
        var displayOpts:DataDisplayOptions = ds.displayOptionsFor(name);
        var vals:Object = this.data;
        var dataSet:DataSet = ds.dataSet;
        var smd:Object = dataSet.seriesMetaData;
        var sm:MetaData = dataSet.metaDataFor(name);
        if (sm.dataType == MetaData.DATA_TYPE__NUMBER) {
            var val:Number = sm.getter(hdr, vals) as Number;
            var precision:int = displayOpts.precision;
            this.label = FormatUtils.numberToFormattedString(val, precision);
        } else {
            this.label = sm.getter(hdr, vals) as String;
        }
    }
}
}