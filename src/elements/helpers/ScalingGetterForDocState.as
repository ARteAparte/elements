/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements.helpers {
import elements.DataDisplayOptions;
import elements.DocState;
import elements.DocHandlers;

import gui.IManagedRef;

public class ScalingGetterForDocState implements IGetScaling {
    public var _sc:IManagedRef;
    public function ScalingGetterForDocState(sc:IManagedRef) {
        _sc = sc;
    }

    public function getScalingFor(name:String):Number {
        var docState:DocState = DocHandlers.readDocState(_sc);
        var ddo:DataDisplayOptions = docState.displayOptionsFor(name);
        return ddo.scale;
    }
}
}