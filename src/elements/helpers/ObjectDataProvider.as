/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements.helpers {
import mx.collections.ArrayCollection;
import mx.collections.IList;

import utils.ArrayUtils;
import utils.MapUtils;

public class ObjectDataProvider implements IProvideData {
    private var _obj:Object;
    private var _data:IList;

    public function ObjectDataProvider(obj:Object) {
        _obj = obj;
        var attrs:Array = MapUtils.keys(obj);
        var dataList:Array = [];
        for each (var attr:String in attrs) {
            dataList.push(MapUtils.makeObject(
                ['property', attr, 'value', obj[attr]]));
        }
        _data = new ArrayCollection(dataList);
    }

    public function provideData():IList {
        return _data;
    }

    public function provideGivenData(items:Array):IList {
      var obj:Object = _obj;
      var attrs:Array = MapUtils.keys(obj);
      var dataList:Array = [];
      for each (var attr:String in attrs) {
        if (ArrayUtils.contains(items, attr)) {
          dataList.push(MapUtils.makeObject(
            ['property', attr, 'value', obj[attr]]));
        }
      }
      return new ArrayCollection(dataList);
    }
}
}