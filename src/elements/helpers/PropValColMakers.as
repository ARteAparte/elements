/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements.helpers
{
import mx.collections.ArrayCollection;
import mx.collections.IList;

import spark.components.gridClasses.GridColumn;

import dse.MetaData;

import elements.DataDisplayOptions;
import elements.DocHandlers;
import elements.DocState;
import elements.PropValGridItemEditorFactory;

import gui.IManagedRef;

import units.NoUnits;
import units.UnitConversions;
import units.UnitInfo;

import utils.FormatUtils;

public class PropValColMakers implements IMkCols
{
  public var _hdr:Object;
  public var _hdrMD:Object;
  public var _dsRef:IManagedRef; // units instead?
  public function PropValColMakers(
    hdr:Object,
    hdrMD:Object,
    dsRef:IManagedRef)
  {
    _hdr = hdr;
    _hdrMD = hdrMD;
    _dsRef = dsRef;
  }
  public function mkCols():IList
  {
    var propCol:GridColumn = new GridColumn();
    var valuCol:GridColumn = new GridColumn();
    propCol.dataField = 'property';
    propCol.headerText = 'Property';
    propCol.labelFunction = propertyLabelFunction;
    propCol.editable = false;
    propCol.sortable = false;
    valuCol.dataField = 'value';
    valuCol.headerText = 'Value';
    valuCol.labelFunction = valueLabelFunction;
    valuCol.editable = true;
    valuCol.itemEditor = new PropValGridItemEditorFactory(_dsRef);
    valuCol.sortable = false;
    return new ArrayCollection([propCol, valuCol]);
  }
  public function propertyLabelFunction(item:Object, col:GridColumn):String
  {
    var prop:String = item.property;
    var name:String = prop;
    if (_hdrMD.hasOwnProperty(prop))
    {
      var md:MetaData = _hdrMD[prop] as MetaData;
      name = md.fullName;
      if (md.dataType == MetaData.DATA_TYPE__NUMBER)
      {
        if (md.baseUnits != NoUnits.NONE)
        {
          var ds:DocState = DocHandlers.readDocState(_dsRef);
          var us:String = ds.displayUnitSystem;
          var targetUnit:String =
            UnitInfo.unitsBySystemAndQOM(us, md.quantityOfMeasure);
          name = name + " [" + targetUnit + "]";
        }
      }
    }
    return name;
  }
  public function valueLabelFunction(item:Object, col:GridColumn):String
  {
    var val:String = item.value.toString();
    if (_hdrMD.hasOwnProperty(item.property))
    {
      var md:MetaData = _hdrMD[item.property] as MetaData;
      var ds:DocState = DocHandlers.readDocState(_dsRef);
      var us:String = ds.displayUnitSystem;
      var targetUnit:String =
        UnitInfo.unitsBySystemAndQOM(us, md.quantityOfMeasure);
      if (md.dataType == MetaData.DATA_TYPE__NUMBER)
      {
        var tag:String = UnitConversions.tag(md.baseUnits, targetUnit);
        if (UnitConversions.CONVERT.hasOwnProperty(tag))
        {
          var f:Function = UnitConversions.CONVERT[tag];
          val = FormatUtils.numberToFormattedString(
            f(item.value as Number),
            DataDisplayOptions.DEFAULT_PRECISION
          );
        }
      }
    }
    return val;
  }
}
}