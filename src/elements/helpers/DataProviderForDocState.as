/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements.helpers {
import mx.collections.ArrayCollection;
import mx.collections.IList;

import dse.MetaData;

import elements.DocHandlers;
import elements.DocState;
import elements.views.MixedSeriesChart;

import gui.IManagedRef;

import pdt.IPMap;

import plugins.DateAndTime;
import plugins.PlugInfo;

import utils.ArrayUtils;
import utils.MapUtils;

public class DataProviderForDocState implements IProvideData {
    public var _sc:IManagedRef;
    public var _lastDocState:DocState;
    public var _data:ArrayCollection;
    public var _items:Array;

    public function DataProviderForDocState(sc:IManagedRef) {
        _sc = sc;
        _lastDocState = null;
        _data = null;
        _items = null;
    }

    public function provideData():IList {
        var docState:DocState = DocHandlers.readDocState(_sc);
        if ((docState == _lastDocState) && (_data != null)) {
            return _data;
        }
        var seriesMeta:Object = PlugInfo.knownSeriesMetaData();
        var allMetrics:Array = MapUtils.keys(seriesMeta);
        allMetrics.sort();
        if (!ArrayUtils.contains(allMetrics, DateAndTime.ELAPSED_TIME_hrs)) {
            allMetrics.push(DateAndTime.ELAPSED_TIME_hrs);
        }
        var data:Array = [];
        var keyVals:Array;
        var hdr:Object = docState.dataSet.header;
        var pts:Array = docState.dataSet.pointsAsArray;
        var refYr:int = MapUtils.atKeyOrAlt(
            hdr, DateAndTime.REFERENCE_YEAR, 2012);
        var refMo:int = MapUtils.atKeyOrAlt(
            hdr, DateAndTime.REFERENCE_MONTH, 1);
        var refDay:int = MapUtils.atKeyOrAlt(
            hdr, DateAndTime.REFERENCE_DAY, 1);
        var refHr:int = MapUtils.atKeyOrAlt(
            hdr, DateAndTime.REFERENCE_HOUR, 0);
        var refMin:int = MapUtils.atKeyOrAlt(
            hdr, DateAndTime.REFERENCE_MINUTE, 0);
        var refSec:int = MapUtils.atKeyOrAlt(
            hdr, DateAndTime.REFERENCE_SECOND, 0);
        var refDate:Date = new Date(
            refYr, refMo-1, refDay, refHr, refMin, refSec);
        var refTime_msec:Number = refDate.getTime();
        var millisecondsPerHr:Number = 3600.0 * 1e3;
        for (var idx:int = 0; idx < pts.length; idx++) {
            var pt:Object = pts[idx];
            keyVals = [];
            for each (var metricName:String in allMetrics) {
                var md:MetaData = docState.dataSet.metaDataFor(metricName);
                var getter:Function = md.getter;
                if (metricName == DateAndTime.ELAPSED_TIME_hrs) {
                    keyVals.push(MixedSeriesChart.TIME_FIELD);
                    var elapsedTime_hrs:Number = getter(hdr, pt);
                    keyVals.push(
                        new Date(
                            refTime_msec +
                            elapsedTime_hrs * millisecondsPerHr));
                } else if (md.dataType == MetaData.DATA_TYPE__STRING) {
                    continue;
                } else {
                    keyVals.push(metricName);
                    keyVals.push(getter(hdr, pt));
                }
            }
            keyVals.push("index");
            keyVals.push(idx);
            data.push(MapUtils.makeObject(keyVals));
        }
        _lastDocState = docState;
        _data = new ArrayCollection(data);
        return _data;
    }

    public function provideGivenData(items:Array):IList {
      //trace('DataProviderForDocState.provideGivenData');
      //trace('items => ', items);
      var docState:DocState = DocHandlers.readDocState(_sc);
      if ((docState == _lastDocState) && (_data != null)) {
        if (_items == null) {
          // first time through so set _items
          _items = ArrayUtils.copyArray(items);
        } else if (ArrayUtils.equals(_items, items)) {
          // same request as before so return previous data
          return _data;
        } else {
          // otherwise, memoize items into _items
          _items = ArrayUtils.copyArray(items);
        }
      }
      var shownMetrics:Array = ArrayUtils.copyArray(items);
      if (!ArrayUtils.contains(shownMetrics, DateAndTime.ELAPSED_TIME_hrs)) {
        shownMetrics.push(DateAndTime.ELAPSED_TIME_hrs);
        shownMetrics.push(DateAndTime.DATE_TIME);
      }
      var data:Array = [];
      var keyVals:Array;
      var hdr:Object = docState.dataSet.header;
      var pts:Array = docState.dataSet.pointsAsArray;
      var refYr:int = MapUtils.atKeyOrAlt(
        hdr, DateAndTime.REFERENCE_YEAR, 2012);
      var refMo:int = MapUtils.atKeyOrAlt(
        hdr, DateAndTime.REFERENCE_MONTH, 1);
      var refDay:int = MapUtils.atKeyOrAlt(
        hdr, DateAndTime.REFERENCE_DAY, 1);
      var refHr:int = MapUtils.atKeyOrAlt(
        hdr, DateAndTime.REFERENCE_HOUR, 0);
      var refMin:int = MapUtils.atKeyOrAlt(
        hdr, DateAndTime.REFERENCE_MINUTE, 0);
      var refSec:int = MapUtils.atKeyOrAlt(
        hdr, DateAndTime.REFERENCE_SECOND, 0);
      var refDate:Date = new Date(
        refYr, refMo-1, refDay, refHr, refMin, refSec);
      var refTime_msec:Number = refDate.getTime();
      var millisecondsPerHr:Number = 3600.0 * 1e3;
      for (var idx:int = 0; idx < pts.length; idx++) {
        var pt:Object = pts[idx];
        keyVals = [];
        for each (var metricName:String in shownMetrics) {
          var md:MetaData = docState.dataSet.metaDataFor(metricName);
          var getter:Function = md.getter;
          if (metricName == DateAndTime.ELAPSED_TIME_hrs) {
            keyVals.push(MixedSeriesChart.TIME_FIELD);
            var elapsedTime_hrs:Number = getter(hdr, pt);
            keyVals.push(
              new Date(
                refTime_msec +
                elapsedTime_hrs * millisecondsPerHr));
          } else if (md.dataType == MetaData.DATA_TYPE__STRING) {
            continue;
          } else {
            keyVals.push(metricName);
            keyVals.push(getter(hdr, pt));
          }
        }
        keyVals.push("index");
        keyVals.push(idx);
        data.push(MapUtils.makeObject(keyVals));
      }
      _lastDocState = docState;
      _data = new ArrayCollection(data);
      return _data;
    }
}
}