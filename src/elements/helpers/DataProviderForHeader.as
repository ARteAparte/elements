/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements.helpers
{
import mx.collections.ArrayCollection;
import mx.collections.IList;

import elements.DocHandlers;
import elements.DocState;

import gui.IManagedRef;

import utils.ArrayUtils;
import utils.MapUtils;

public class DataProviderForHeader implements IProvideData
{
  public var _docStateRef:IManagedRef;
  public function DataProviderForHeader(docStateRef:IManagedRef)
  {
    _docStateRef = docStateRef;
  }
  public function provideData():IList
  {
    var docState:DocState = DocHandlers.readDocState(_docStateRef);
    var header:Object = docState.dataSet.header;
    var attrs:Array = MapUtils.keys(header);
    var dataList:Array = [];
    for each (var attr:String in attrs)
    {
      dataList.push(
        MapUtils.makeObject(['property', attr, 'value', header[attr]])
      );
    }
    return new ArrayCollection(dataList);
  }
  public function provideGivenData(items:Array):IList
  {
    var docState:DocState = DocHandlers.readDocState(_docStateRef);
    var header:Object = docState.dataSet.header;
    var attrs:Array = MapUtils.keys(header);
    var dataList:Array = [];
    for each (var attr:String in attrs)
    {
      if (ArrayUtils.contains(items, attr))
      {
        dataList.push(
          MapUtils.makeObject(['property', attr, 'value', header[attr]])
        );
      }
    }
    return new ArrayCollection(dataList);
  }
}
}