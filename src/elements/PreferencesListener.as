/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements {
  import flash.filesystem.File;
  import flash.filesystem.FileMode;
  import flash.filesystem.FileStream;
  
  import elements.views.BaseWindow;
  
  import gui.IManagedRef;

public class PreferencesListener {
  public static const FILE_NAME:String = "prefs.xml";
  public var _fileName:String;
  public var _docRef:IManagedRef;
  public var _fsOut:FileStream;
  public var _fsIn:FileStream;

  public function PreferencesListener(
    docRef:IManagedRef,
    name:String=FILE_NAME)
  {
    this._fileName = name;
    this._docRef = docRef;
    this._docRef.registerSubscriberFunction(update);
    try {
      AppHandlers.instance().registerSubscriberFunction(update);
    } catch (e:Error) {
      // do nothing -- allows unit tests to work without starting app instance
    }
  }
  public function update(event:String):void {
    switch (event) {
      case DocEvents.EVENT_DISPLAY_UNITS_CHANGED:
        //trace('saving preferences for display units changed');
        savePreferences();
        break;
      case AppEvents.EVENT_OPEN_WINDOWS_CHANGED:
        //trace('saving preferences for open windows changed');
        savePreferences();
        break;
      case AppEvents.EVENT_WINDOW_RESIZED:
        //trace('saving preferences for window resize');
        savePreferences();
        break;
      case AppEvents.EVENT_WINDOW_MOVED:
        //trace('saving preferences for window move');
        savePreferences();
        break;
      default:
        break;
    }
  }
  public static function readPrefs():XML
  {
    var f:File = File.applicationStorageDirectory.resolvePath(FILE_NAME);
    var prefs:XML;
    var defaultPrefs:XML =
      <elements>
        <unitSystem>SI</unitSystem>
        <lastPath></lastPath>
        <windowPositions></windowPositions>
      </elements>
    if (f.exists)
    {
      var fsInput:FileStream = new FileStream();
      fsInput.open(f, FileMode.READ);
      var str:String = fsInput.readUTFBytes(fsInput.bytesAvailable);
      if ((str == "") || (str.indexOf("<?xml") != 0))
      {
        prefs = defaultPrefs;
      } else {
        prefs = new XML(str);
      }
      fsInput.close();
    } else {
      prefs = defaultPrefs;
    }
    return prefs;
  }
  public static function savePrefs(x:XML):void
  {
    var f:File = File.applicationStorageDirectory.resolvePath(FILE_NAME);
    var fsOutput:FileStream = new FileStream();
    try
    {
      fsOutput.open(f, FileMode.WRITE);
    } catch (e:Error) {
      fsOutput.close();
      return;
    }
    var output:String = '<?xml version="1.0" encoding="utf-8"?>\n';
    output += x.toXMLString();
    fsOutput.writeUTFBytes(output);
    fsOutput.close();
  }
  public static function lastPath():File
  {
    var x:XML = readPrefs();
    if (x.hasOwnProperty('lastPath') && x.lastPath != '')
    {
      var f:File = new File(x.lastPath);
      if (f.exists && f.isDirectory)
      {
        return f;
      } else if (f.exists) {
        return f.parent;
      }
    }
    return File.documentsDirectory;
  }
  public static function setLastPath(f:File):void
  {
    var x:XML = readPrefs();
    if (!x.hasOwnProperty('lastPath'))
    {
      x.appendChild(<lastPath/>);
    }
    if (f.isDirectory && f.exists)
    {
      x.lastPath = f.nativePath;
    } else if (f.exists && f.parent != null) {
      x.lastPath = f.parent.nativePath;
    } else {
      // f doesn't exist or parent is null so just return without saving
      return;
    }
    savePrefs(x);
  }
  public function savePreferences():void {
    //trace('PreferencesListener.savePreferences()');
    //trace('-------------------------------------');
    var f:File = File.applicationStorageDirectory.resolvePath(_fileName);
    var ds:DocState = DocHandlers.readDocState(_docRef);
    var prefs:XML;
    var defaultPrefs:XML =
        <elements>
          <unitSystem>SI</unitSystem>
          <lastPath></lastPath>
          <windowPositions></windowPositions>
        </elements>
    if (f.exists) {
      _fsIn = new FileStream();
      _fsIn.open(f, FileMode.READ);
      var str:String = _fsIn.readUTFBytes(_fsIn.bytesAvailable);
      if ((str == "") || (str.indexOf("<?xml") != 0)) {
        prefs = defaultPrefs;
      } else {
        prefs = new XML(str);
      }
      _fsIn.close();
    } else {
      prefs = defaultPrefs;
    }
    //trace('prefs: \n' + prefs.toString());
    // read the existing prefs and get the windowPrefs data structure
    // get appstate, go through open windows, for each window name
    // save the window x, window y, window dx, window dy
    // updating the datastructure if already there or adding a new entry if
    // not there
    _fsOut = new FileStream();
    try {
      _fsOut.open(f, FileMode.WRITE);
    } catch (e:Error) {
      _fsIn.close();
      _fsOut.close();
      return;
    }
    var ws:Array = [];
    try {
      var apSt:AppState = AppHandlers.readAppState(AppHandlers.instance());
      ws = apSt.openWindows.toArray();
    } catch (e:Error) {
      // do nothing -- allows testing without an application instance
    }
    var win:XML;
    for each (var w:BaseWindow in ws) {
      var addNew:Boolean = true;
      if (prefs.windowPositions.hasOwnProperty('win')) {
        //trace('has existing windows');
        for each (var winNode:XML in prefs.windowPositions.win) {
          //trace('node: \n', winNode.toXMLString());
          var winTitle:XMLList = winNode.child("title");
          if (winTitle.toString() == w.tag) {
            //trace('found existing window');
            addNew = false;
            winNode.x = w.nativeWindow.x;
            winNode.y = w.nativeWindow.y;
            winNode.width = w.nativeWindow.width;
            winNode.height = w.nativeWindow.height;
            break;
          }
        }
      }
      if (addNew) {
        //trace('adding new entry');
        win = <win>
                <title>{ w.tag }</title>
                <x>{ w.nativeWindow.x }</x>
                <y>{ w.nativeWindow.y }</y>
                <width>{ w.nativeWindow.width }</width>
                <height>{ w.nativeWindow.height }</height>
              </win>
        //trace(win.toXMLString());
        if (!prefs.hasOwnProperty('windowPositions')) {
          var wp:XML = <windowPositions/>
          prefs.appendChild(wp);
        }
        var winPositions:XMLList = prefs.windowPositions;
        winPositions.appendChild(win);
      }
    }
    prefs.unitSystem = ds.displayUnitSystem;
    var output:String = '<?xml version="1.0" encoding="utf-8"?>\n';
    output += prefs.toXMLString();
    _fsOut.writeUTFBytes(output);
    _fsOut.close();
    //trace('final prefs:\n', prefs.toXMLString());
    //trace('^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^');
  }
}
}