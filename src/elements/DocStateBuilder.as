/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements {
  import flash.filesystem.File;

  import dse.DataSet;
  import dse.DataSetBuilder;

  import elements.helpers.WindowNaming;

  import pdt.IPMap;
  import pdt.IPVec;
  import pdt.PersistentArrayMap;
  import pdt.PersistentVector;

  import plugins.DateAndTime;
  import plugins.Psych;
  import plugins.Solar;
  import plugins.Wind;

  import utils.MapUtils;

public class DocStateBuilder {
  internal var dataSet:DataSet;
  internal var displayUnits:IPMap;
  internal var selections:IPVec;
  internal var displayOptionsMap:IPMap;
  internal var shownData:IPVec;
  internal var fileName:String;
  internal var title:String;
  internal var alwaysPlainTitle:Boolean;
  internal var file:File;
  internal var fileModified:Boolean;
  internal var activeSettingSets:IPMap;
  public static const DEFAULT_SHOWN_COLUMNS:IPVec = PersistentVector.create([
    DateAndTime.DATE_TIME,
    Psych.DRY_BULB_TEMP_C,
    Psych.WET_BULB_TEMP_C,
    Psych.PRESSURE_kPa,
    Psych.RELATIVE_HUMIDITY,
    Psych.DEW_POINT_TEMP_C,
    Solar.GLOBAL_HORIZ_Wh__m2,
    Solar.BEAM_NORMAL_Wh__m2,
    Solar.DIFFUSE_HORIZONTAL_Wh__m2,
    Wind.SPEED_m__s]);
  public function DocStateBuilder() {
    dataSet = new DataSetBuilder().build();
    displayUnits = PersistentArrayMap.EMPTY;
    selections = PersistentVector.EMPTY;
    var smd:Object = dataSet.seriesMetaData;
    displayOptionsMap = DataDisplayOptions.makeDefaultDisplayOptions(smd);
    shownData = defaultShownDataColumns(smd);
    var name:String = WindowNaming.BASE_NAME;
    file = null;
    fileName = name;
    title = name;
    alwaysPlainTitle = false;
    fileModified = false;
    activeSettingSets = PersistentArrayMap.EMPTY;
  }
  public static function
  createDefault(ds:DataSet, file:File=null):DocStateBuilder {
    var b:DocStateBuilder = new DocStateBuilder();
    b.dataSet = ds;
    b.displayUnits = PersistentArrayMap.EMPTY;
    b.selections = PersistentVector.EMPTY;
    var smd:Object = ds.seriesMetaData;
    b.displayOptionsMap = DataDisplayOptions.makeDefaultDisplayOptions(smd);
    b.shownData = defaultShownDataColumns(smd);
    if (file == null) {
      var name:String = WindowNaming.nextName();
      b.fileName = name;
      b.title = name;
      b.fileModified = true;
    } else {
      b.fileName = file.name;
      b.title = file.name;
      b.fileModified = false;
    }
    b.activeSettingSets = PersistentArrayMap.EMPTY;
    return b;
  }
  public function withDefaultDisplayOptions():DocStateBuilder {
    var smd:Object = dataSet.seriesMetaData;
    displayOptionsMap = DataDisplayOptions.makeDefaultDisplayOptions(smd);
    return this;
  }
  public function withDataSet(ds:DataSet):DocStateBuilder {
    this.dataSet = ds;
    return this;
  }
  public function withDisplayUnits(du:IPMap):DocStateBuilder {
    this.displayUnits = du;
    return this;
  }
  public function withSelections(ss:IPVec):DocStateBuilder {
    this.selections = ss;
    return this;
  }
  public function withDisplayOptionsMap(dispOpts:IPMap):DocStateBuilder {
    this.displayOptionsMap = dispOpts;
    return this;
  }
  public function withShownData(sd:IPVec):DocStateBuilder {
    this.shownData = sd;
    return this;
  }
  public function withFileName(name:String):DocStateBuilder {
    this.fileName = name;
    return this;
  }
  public function withTitle(title:String):DocStateBuilder {
    this.title = title;
    return this;
  }
  public function withAlwaysPlainTitle(flag:Boolean):DocStateBuilder {
    this.alwaysPlainTitle = flag;
    return this;
  }
  public function withFile(f:File):DocStateBuilder {
    this.file = f;
    return this;
  }
  public function withFileModified(mod:Boolean):DocStateBuilder {
    this.fileModified = mod;
    return this;
  }
  public function withActiveSettingSets(ss:IPMap):DocStateBuilder {
    this.activeSettingSets = ss;
    return this;
  }
  public function build():DocState {
    return new DocState(this);
  }
  public static function fromDocState(ds:DocState):DocStateBuilder {
    return (new DocStateBuilder())
      .withActiveSettingSets(ds._activeSettingSets)
      .withDataSet(ds._dataSet)
      .withDisplayOptionsMap(ds._displayOptionsMap)
      .withDisplayUnits(ds._displayUnits)
      .withFile(ds._file)
      .withFileModified(ds._fileModified)
      .withFileName(ds._fileName)
      .withTitle(ds._title)
      .withAlwaysPlainTitle(ds._alwaysPlainTitle)
      .withSelections(ds._selections)
      .withShownData(ds._shownData);
  }
  public static function defaultShownDataColumns(seriesMetas:Object):IPVec {
    //trace('DocState.defaultShownDataColumns');
    var out:Array = new Array();
    var desiredDefaults:Array = DEFAULT_SHOWN_COLUMNS.toArray();
    for each (var deflt:String in desiredDefaults) {
      for each (var name:String in MapUtils.keys(seriesMetas)) {
        if (name == deflt) {
          out.push(name);
        }
      }
    }
    return PersistentVector.create(out);
  }
}
}