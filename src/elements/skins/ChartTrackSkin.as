/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements.skins {
import mx.core.UIComponent;

public class ChartTrackSkin extends UIComponent {
    private static const HEIGHT:uint = 38;

    public function ChartTrackSkin() {
        super();
    }

    // Setting height AND measuredHeight is key to getting the track to line
    // up with where it's supposed to.
    override public function get height():Number {
        return HEIGHT;
    }

    override public function get measuredHeight():Number {
        return HEIGHT;
    }

    override protected function
    updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void {
        super.updateDisplayList(unscaledWidth, unscaledHeight);
        /*
        trace('height: ' + String(this.height));
        trace('measuredHeight: ' + String(this.measuredHeight));
        trace('unscaledHeight: ' + String(this.unscaledHeight));
        trace('minHeight: ' + String(this.minHeight));
        trace('maxHeight: ' + String(this.maxHeight));
        */
        with (graphics) {
            clear();
            lineStyle(1, 0x0000FF, 0);
            //beginFill(0x999999, 0.25);
            //drawRect(0, -5, w, h);
            //drawRect(0, -5, w, h + 11);
            drawRect(0, 0, unscaledWidth, unscaledHeight);
        }
    }
}
}