/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements.skins {
import mx.core.UIComponent;
// Peter customized

//SliderThumbNoGripHighlightSkin
public class HighlightSkin extends UIComponent {
    private static const HEIGHT:uint = 38;

    public function HighlightSkin() {
        super();
    }

    // Should NOT set width; it's determined by the slider code
    /*
    override public function get width():Number
    {
        return(4);
    }

    override public function get measuredWidth():Number
    {
        return(4);
    }
    */

    override public function get height():Number {
        return(HEIGHT);
    }

    override public function get measuredHeight():Number {
        return(HEIGHT);
    }

    override protected function
    updateDisplayList(unscaledWidth:Number, unscaledHeight:Number):void {
        super.updateDisplayList(unscaledWidth, unscaledHeight);
        graphics.clear();
        graphics.lineStyle(1, 0x666666);
        graphics.beginFill(0x999999, 0.25);
        graphics.drawRect(0, 0, unscaledWidth, unscaledHeight);
    }
}
}