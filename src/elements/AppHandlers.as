/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package elements {
import flash.desktop.NativeApplication;
import flash.display.NativeWindow;
import flash.filesystem.File;

import mx.core.IVisualElement;
import mx.core.IWindow;
import mx.utils.ObjectUtil;

import elements.commands.AppAddMenuBar;
import elements.commands.AppNewCustomDoc;
import elements.commands.AppOpenDocument;
import elements.commands.AppSetStartupMode;
import elements.commands.DocRefreshAll;
import elements.components.StartWizard;
import elements.views.BaseWindow;

import gui.AcmeManagedRef;
import gui.IManagedRef;

import pdt.IPVec;
import pdt.PersistentVector;

public class AppHandlers {
  /* HELPER FUNCTIONS
  =================================================================== */
  public static function getAppVersion():String {
    var appXml:XML = NativeApplication.nativeApplication.applicationDescriptor;
    var ns:Namespace = appXml.namespace();
    var appVersion:String = appXml.ns::versionNumber[0];
    return appVersion;
  }
  public static function readAppState(appSC:IManagedRef):AppState {
    //trace('AppState.readAppState');
    return appSC.read() as AppState;
  }
  public static function getActiveWindowModified(aSt:AppState):Boolean {
    //trace('AppState.getActiveWindowModified');
    var activeWin:BaseWindow = aSt.activeWindow as BaseWindow;
    if (activeWin == null) {
      //trace('AppState.getActiveWindowModified :: active window was null',
      //  ' -> false');
      return false;
    }
    var docSC:IManagedRef = activeWin.docStateCtrlr;
    if (docSC == null){
      //trace('AppState.getActiveWindowModified :: the doc state controller',
      //'is null! This should not be possible');
      return false;
    }
    var docState:DocState = DocHandlers.readDocState(docSC);
    return docState.fileModified;
  }
  public static function indexForWindow(obj:IHasOpenWindows, w:Object):int {
    //trace('AppState.getWindowIndex');
    var ws:IPVec = obj.openWindows;
    for (var i:int=0; i<ws.length; i++) {
      var w_:Object = ws.valAt(i);
      if (equals(w_, w)) return i;
    }
    return -1;
  }
  public static function _isWindowInOpenWindowList(
                             aSt:AppState, w:Object):Boolean {
    //trace('AppState._isWindowInOpenWindowList');
    var currentActiveWindow:Object = aSt.activeWindow;
    if (equals(currentActiveWindow, w)) return true;
    var openWins:IPVec = aSt.openWindows;
    var wIsInOpenWindowsList:Boolean = false;
    for (var i:int; i<openWins.length; i++) {
      if (equals(w, openWins.valAt(i))) {
        wIsInOpenWindowsList = true;
      }
    }
    return wIsInOpenWindowsList;
  }
  public static function equals(a:Object, b:Object):Boolean {
    //trace('AppState.equals');
    if ((a == null) || (b == null)) return false;
    return ObjectUtil.compare(a, b) == 0;
  }
  public static function setActiveWindow(aSt:AppState, w:Object):AppState {
    //trace('AppState.setActiveWindow');
    var currentActiveWindow:Object = aSt.activeWindow;
    if (equals(currentActiveWindow, w)) {
      //trace('AppHandlers.setActiveWindow: w is already current active win');
      return aSt;
    }
    if (w == null) {
      //trace('AppHandlers.setActiveWindow: w is null');
      return aSt;
    }
    var wIsAnOpenWindow:Boolean = _isWindowInOpenWindowList(aSt, w);
    if (!wIsAnOpenWindow) {
      //trace('AppHandlers.setActiveWindow: w is not in open window list')
      return aSt;
    }
    var winIdx:int = indexForWindow(aSt, w);
    return aSt.withActiveWindowIndex(winIdx);
  }
  public static function setActiveWindowIdx(aSt:AppState, idx:int):AppState {
    //trace('AppState.setActiveWindowIdx');
    var ws:IPVec = aSt.openWindows;
    var newIdx:int = _limitActiveIdxToBounds(ws, idx);
    return aSt.withActiveWindowIndex(idx);
  }
  public static function _limitActiveIdxToBounds(ws:IPVec, idx:int):int {
    //trace('AppState._limitActiveIdxToBounds');
    var maxIdx:int = ws.length - 1;
    var minIdx:int = 0;
    if (maxIdx == -1) return -1;
    if (idx > maxIdx) return maxIdx;
    if (idx < minIdx) return minIdx;
    return idx;
  }
  public static function addWindow(aSt:AppState, w:Object):AppState {
    //trace('AppState.addWindow');
    if (_isWindowInOpenWindowList(aSt, w)){
      //trace('AppState.addWindow :: w already in open window list');
      return aSt;
    }
    var ws:IPVec = aSt.openWindows.cons(w);
    var activeIdx:int = aSt.activeWindowIndex;
    var newActiveIdx:int = _limitActiveIdxToBounds(ws, activeIdx);
    return aSt.withOpenWindows(ws).withActiveWindowIndex(newActiveIdx);
  }
  public static function addWindowAndSetActive(
    aSt:AppState,
    w:Object):AppState
  {
    //trace('AppState.addWindowAndSetActive');
    var as1:AppState = addWindow(aSt, w);
    var ws:IPVec = as1.openWindows;
    (w as BaseWindow).docStateCtrlr.handleCommand(new DocRefreshAll());
    return as1.withActiveWindowIndex(ws.length - 1);
  }
  public static function addNewDocumentWindowAndSetActive(
    aSt:AppState,
    opts:WindowOptions=null):AppState
  {
    //trace('AppState.addNewDocumentWindowAndSetActive');
    var docMaker:Function = aSt.newDocMaker;
    var w:Object;
    if (opts == null) {
      w = docMaker() as Object;
    } else {
      w = docMaker(opts);
    }
    return addWindowAndSetActive(aSt, w);
  }
  public static function addNewHTMLWindowAndSetActive(
    aSt:AppState,
    file:File,
    opts:WindowOptions=null):AppState
  {
    //trace('AppState.addNewDocumentWindowAndSetActive');
    var docMaker:Function = DocumentMaker.loadHTMLWindow;
    var w:Object;
    if (opts == null) {
      w = docMaker(file);
    } else {
      w = docMaker(file, opts);
    }
    return addWindowAndSetActive(aSt, w);
  }
  public static function addNewCustomDocAndSetActive(
                             aSt:AppState,
                             view:IVisualElement,
                             sc:IManagedRef,
                             checkClose:Boolean=true,
                             opts:WindowOptions=null,
                             parentWin:BaseWindow=null):AppState {
    //trace('AppState.addNewCustomDocAndSetActive');
    var docFromViewMaker:Function = aSt.newDocFromViewMaker;
    var w:Object = docFromViewMaker(view, sc, checkClose, opts, parentWin) as Object;
    //view.parentWindow = w;
    return addWindowAndSetActive(aSt, w);
  }
  public static function removeWindow(aSt:AppState, w:Object):AppState {
    //trace('AppHandlers.removeWindow');
    try {
      //trace('AppHandlers.removeWindow :: w.title =', w.title);
    } catch(e:Error) {
      //trace('AppHandlers.removeWindow :: could not access w.title');
    }
    var idx:int;
    var tmp:AppState;
    var ws:IPVec = aSt.openWindows;
    if (!_isWindowInOpenWindowList(aSt, w)){
      //trace('AppHandlers.removeWindow :: win not in open window list');
      idx = _limitActiveIdxToBounds(ws, aSt.activeWindowIndex);
      return aSt.withActiveWindowIndex(idx);
    }
    var wsArry:Array = ws.toArray();
    var newWsArry:Array = [];
    var newWs:IPVec;
    for (var i:int=0; i<ws.length; i++) {
      //trace('AppHandlers.removeWindow :: starting w/', ws.length, 'windows');
      var w_:Object = ws.valAt(i);
      if (equals(w_, w)) {
        try {
          // close Window if it's a window;
          // this allows us to pass in non-windows for testing
          var nw:NativeWindow = (w_ as IWindow).nativeWindow;
          // null out the menu so it isn't auto-closed when
          // the window closes; nw is shared between all open windows
          nw.menu = null;
          nw.close();
          //trace('AppHandlers.removeWindow :: closed window');
        } catch (e:Error) {
          // pass
        }
      } else {
        newWsArry.push(w_);
      }
    }
    newWs = PersistentVector.create(newWsArry);
    idx = _limitActiveIdxToBounds(newWs, aSt.activeWindowIndex);
    tmp = aSt.withActiveWindowIndex(idx);
    return tmp.withOpenWindows(newWs);
  }
  /* INITIALIZATION & INSTANCES
  ===================================================================== */
  private static var _APP_STATE:IManagedRef = null;

  public static function instance():IManagedRef {
    //trace('AppState.instance');
    if (_APP_STATE == null) {
      throw new Error("Cannot use an instance of non-initialized app state");
    }
    return _APP_STATE;
  }
  // call this first when Application Starts
  public static function initialize(args:Array=null, dir:File=null):IManagedRef{
    //trace('AppState.initialize');
    var apSt:AppState = new AppStateBuilder().build();
    _APP_STATE = _finalizeInitialization(apSt);
    _APP_STATE.handleCommand(new AppAddMenuBar());
    if ((args == null) || (args.length == 0)){
      // launch StartWizard
      _APP_STATE.handleCommand(new AppSetStartupMode(true));
      var startWiz:StartWizard = new StartWizard();
      _APP_STATE.handleCommand(new AppNewCustomDoc(
        startWiz,
        null,
        false,
        (new WindowOptionsBuilder())
        .withHeight(150)
        .withWidth(400)
        .withResizable(false)
        .withMinimizable(false)
        .withTitle("Elements")
        .withPlainTitle(true)
        .build()));
      var w:Object = AppHandlers.readAppState(_APP_STATE).activeWindow;
      startWiz.parentWindow = w;
      return _APP_STATE;
    }
    // open a file directly
    _APP_STATE.handleCommand(new AppSetStartupMode(false));
    //trace('args =>', args);
    if (dir != null) {
      //trace('dir =>', dir.nativePath);
    }
    var fileToOpen:File = dir.resolvePath(args[0]);
    _APP_STATE.handleCommand(new AppOpenDocument(fileToOpen));
    return _APP_STATE;
  }
  public static function _finalizeInitialization(state:AppState):IManagedRef{
    //trace('AppState._finalizeInitialization');
    if (_APP_STATE != null) {
      //trace('AppState._finalizeInitialization :: Warning! ' +
      //  'AppState already initialized');
      return _APP_STATE;
    }
    _APP_STATE = new AcmeManagedRef(state);
    return _APP_STATE;
  }
}
}