<!DOCTYPE html>
<html lang="en-US">
	<head>
		<!--[if lt IE 9]>
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->

		<title>Document Window - User Guide | Elements</title>
		<meta charset="utf-8">
		<meta name="author" content="Big Ladder Software LLC">
		<meta name="description" content="">
		<link rel="stylesheet" href="css/normalize.css" type="text/css">
		<link rel="stylesheet" href="css/style.css" type="text/css">
		<link rel="icon" href="media/elements_16x16.gif" type="image/gif">
		<link rel="SHORTCUT ICON" href="media/elements_16x16.ico">
		
		<!--Google Analytics script should not count hits for local browsing.-->
		<script type="text/javascript">
      var _gaq = _gaq || [];
      _gaq.push(['_setAccount', 'UA-33302462-1']);
      _gaq.push(['_trackPageview']);

      (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
      })();
    </script>
	</head>
  <body>
	    <div class="container">

        <div id="header_container">
            <div id="logo_container"><div id="logo_img"><a href="http://bigladdersoftware.com/projects/elements/"><img src="media/elements_64x64.png"></a></div></div>
            <div id="name_container"><a href="http://bigladdersoftware.com/projects/elements/">Elements</a></div>
            <div id="title_container">User Guide</div>
        </div>

        <div id="body_container">

          <div id="navigation_container">
            <div id="navigation">
              <li><a href="index.html">Introduction</a></li>
              <ul>
                <li><a href="background.html">Background</a></li>
                <li><a href="install-uninstall.html">Install/Uninstall</a></li>
                <li><a href="support.html">Support</a></li>
                <li><a href="acknowledgments.html">Acknowledgments</a></li>
              </ul>
              <li><a href="usage.html">Usage</a></li>
              <ul>
                <li><a href="document-window.html">Document Window</a></li>
                <li><a href="header-dialog.html">Header Dialog</a></li>
                <li><a href="normalize-by-month-dialog.html">Normalize By Month Dialog</a></li>
                <li><a href="chart-window.html">Chart Window</a></li>
                <li><a href="command-summary.html">Command Summary</a></li>
              </ul>
              
              <li>Tutorials</li>
              <ul>
                <li><a href="tutorial-convert-formats.html">Convert Formats</a></li>
                <li><a href="tutorial-edit-time-series-data.html">Edit Time-Series Data</a></li>
                <li><a href="tutorial-edit-header-data.html">Edit Header Data</a></li>
                <li><a href="tutorial-normalize-by-month.html">Normalize By Month</a></li>
                <li><a href="tutorial-interactive-chart.html">Interactive Chart</a></li>
                <li><a href="tutorial-create-from-scratch.html">Create From Scratch</a></li>
              </ul>

              <li>Reference</li>
              <ul>
                <li><a href="elements-file-format.html">Elements File Format</a></li>
                <li><a href="license.html">License</a></li>
              </ul>
              
              
            </div>
          </div>


          <div id="content">  <!-- Change to 'main' -->
              <h1 id="document-window">Document Window</h1>
              <p>The Document Window is the centerpiece of the <em>Elements</em> user interface. It represents a single weather file comprised of header data and 8760 rows of hourly time-series data.</p>
              <p>The header data include values for (among others):</p>
              <ul>
              <li>Latitude</li>
              <li>Longitude</li>
              <li>Time zone</li>
              <li>Elevation.</li>
              </ul>
              <p>The time-series data include variables for (among others):</p>
              <ul>
              <li>Psychrometrics (temperature, humidity, pressure, density, enthalpy)</li>
              <li>Solar (global, normal, diffuse, solar angles)</li>
              <li>Wind (speed and direction).</li>
              </ul>
              <p>From the Document Window, you can view and edit all of the header and time-series data. You can edit individual values or edit selections of data in bulk. You can also access <a href="command-summary.html">all menu commands</a>.</p>
              <p>Multiple weather files can be open at the same time in separate Document Windows.</p>
              <figure>
              <img src="images/document-window.png" alt="Document Window" /><figcaption>Document Window</figcaption>
              </figure>
              <p>The different parts of the Document Window are described below.</p>
              <h2 id="header-summary">Header Summary</h2>
              <p>The upper left of the Document Window displays a summary of data from the header including site name, latitude, longitude, time zone, and elevation.</p>
              <figure>
              <img src="images/document-window-header-summary.png" alt="Header Summary" /><figcaption>Header Summary</figcaption>
              </figure>
              <p>These values and other header data can be viewed and edited in the <a href="header-dialog.html">Header Dialog</a>. Click the <code>Header</code> button in the upper right of the Document Window to show.</p>
              <h2 id="data-grid">Data Grid</h2>
              <p>The Data Grid is similar to a spreadsheet. The time-series data are shown in a tabular format; each variable is a column of data with a header at the top showing the name and displayed units. The left column shows the hourly date and time stamp for each row of values. The Data Grid can scroll vertically as necessary.</p>
              <figure>
              <img src="images/document-window-data-grid.png" alt="Data Grid" /><figcaption>Data Grid</figcaption>
              </figure>
              <p>To edit an individual data value, simply double click on the cell and type in the new value.</p>
              <p>To edit a range of data values, first make a selection by:</p>
              <ul>
              <li>Clicking on one cell, holding down Shift, and clicking on another cell to get a <em>contiguous</em> selection</li>
              <li>Clicking on one cell, holding down Ctrl (on Windows) or Command (on Mac), and clicking on various other cells to get a <em>discontiguous</em> selection</li>
              <li>Clicking on a column header to select <em>all values</em> in the column.</li>
              </ul>
              <figure>
              <img src="images/document-window-data-grid-selection.png" alt="Data Grid Contiguous Selection" /><figcaption>Data Grid Contiguous Selection</figcaption>
              </figure>
              <p>With a range of data values selected, you can apply the available <a href="#tools">Tools</a> to edit the selection in bulk.</p>
              <p><code>Edit/Copy</code> and <code>Edit/Paste</code> can also be used to edit data. You can copy and paste within the same weather file, between different weather files, and to/from an external spreadsheet program such as <em>Excel</em>.</p>
              <p><code>Edit/Undo</code> and <code>Edit/Redo</code> can be used as necessary if you make a mistake.</p>
              <h3 id="variables-to-hold-constant">Variables to Hold Constant</h3>
              <p>Unlike a normal spreadsheet, the Data Grid understands the relationships between certain variables. For example, the psychrometric variables (dry-bulb temperature, wet-bulb temperature, dew-point temperature, relative humidity, atmospheric pressure, density, enthalpy, and others) all share a relationship with each other through the physics of psychrometry. Similarly, the solar variables (global solar, direct solar, and diffuse solar) also share a relationship through solar angles.</p>
              <p>When editing data in the Data Grid, <em>Elements</em> ensures that related variables maintain consistency with each other. Consequently, if you change the value of one variable, the Data Grid might have to change the values of other variables. For instance, if you change dry-bulb temperature, the Data Grid may also have to change wet-bulb temperature and dew-point temperature to preserve psychometric consistency in order to hold the atmospheric pressure and relative humidity constant. Or the Data Grid could hold dew-point temperature and atmospheric pressure constant and change wet-bulb temperature and relative humidity.</p>
              <p>The upper right of the Document Window shows a drop-down list labeled <code>Variables to Hold Constant</code> that allows you to select which variables are constant and which variables can change when editing data. The contents of the list change depending on which variable is selected for editing.</p>
              <figure>
              <img src="images/document-window-variables-to-hold-constant.png" alt="Variables to Hold Constant Drop-Down List" /><figcaption>Variables to Hold Constant Drop-Down List</figcaption>
              </figure>
              <h3 id="columns">Columns</h3>
              <p>The lower left of the Document Window shows a set of buttons for controlling which variables are displayed as columns in the Data Grid.</p>
              <figure>
              <img src="images/document-window-columns.png" alt="Column Buttons" /><figcaption>Column Buttons</figcaption>
              </figure>
              <p>The <code>Add</code> button adds another variable to the view. Clicking the button displays the Add Column Dialog.</p>
              <figure>
              <img src="images/document-window-add-column.png" alt="Add Column Dialog" /><figcaption>Add Column Dialog</figcaption>
              </figure>
              <p>The <code>Remove</code> button removes the selected column from the view. The <code>Move Left</code> and <code>Move Right</code> buttons move the selected column left or right respectively. An entire column must be selected by clicking on the column header before the buttons will be enabled.</p>
              <p>Clicking <code>Add</code> or <code>Remove</code> does not change any data in the weather file; it only changes which variables are displayed in the current view.</p>
              <h3 id="tools">Tools</h3>
              <p>The upper left of the Document Window shows a set of buttons for tools that edit the data selection in bulk.</p>
              <p>The following tools are available:</p>
              <ul>
              <li><code>Offset</code>: displaces the selected data by adding the user-specified offset to each value</li>
              <li><code>Scale</code>: displaces the selected data by multiplying by the user-specified scale factor</li>
              <li><code>Normalize</code>: adjusts the selected data such that their average equals the specified average. For temperatures, the tool automatically converts to absolute temperature scale first prior to performing normalization. For solar values, the integral of the selected samples is used for scaling.</li>
              <li><code>Normalize By Month</code>: displays the <a href="normalize-by-month-dialog.html">Normalize By Month Dialog</a> to perform normalization functions on a per-month basis.</li>
              </ul>
              <p>All of the data selection must be within the same column before any of the tools buttons will be enabled.</p>
              <p>An entire column must be selected by clicking on the column header before the <code>Normalize By Month</code> button will be enabled.</p>
              <figure>
              <img src="images/document-window-tools.png" alt="Tools Buttons" /><figcaption>Tools Buttons</figcaption>
              </figure>
              <h3 id="units">Units</h3>
              <p>The lower right of the Document Window shows a group of radio buttons to allow you to select the unit system that is displayed. At any time you can conveniently switch between two unit systems: the international system of units (SI) or the inch-pound system of units (IP).</p>
              <figure>
              <img src="images/document-window-units.png" alt="Units Buttons" /><figcaption>Units Buttons</figcaption>
              </figure>
          </div>

        </div>  <!-- End body_container -->
          
        <div id="footer_container">
          <div id="footer">
            <div style="float:left;">Copyright &copy; 2014-2015 Big Ladder Software LLC. The <em>Elements</em> and <em>Big Ladder</em> names and logos are trademarks of Big Ladder Software LLC.</div>
            <div style="float:right;">Version 1.0.5; Updated 2015.10.27</div>
          </div>
        </div>

      </div>
    </body>
</html>
