/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package flexUnitTests.plugins {
import dse.DateTime;
import dse.MetaData;

import flexunit.framework.Assert;

import plugins.DateAndTime;

import utils.MapUtils;

public class TestDateAndTime {
    public var year:int = 2012;
    public var month:int = 2;
    public var day:int = 2;
    public var hr:int = 2;
    public var min:int = 2;
    public var sec:int = 2;

    [Before]
    public function
    setUp():void {
    }

    [Test]
    public function
    testConstraintFunction():void {
        var expected:Number = 0.0;
        var actual:Number = DateAndTime.constraintFunction({}, {});
        Assert.assertEquals(expected, actual);
    }

    [Test]
    public function
    testDatePlusElapsedHoursToDate():void {
        var d:Object = DateAndTime.datePlusElapsedHoursToDate(
            0.0, year, month, day, hr, min, sec);
        //ObjectUtils.traceObject(d, 'd');
        Assert.assertEquals(year, d[DateAndTime.YEAR]);
        Assert.assertEquals(month, d[DateAndTime.MONTH]);
        Assert.assertEquals(day, d[DateAndTime.DAY]);
        Assert.assertEquals(hr, d[DateAndTime.HOUR]);
        Assert.assertEquals(min, d[DateAndTime.MINUTE]);
        Assert.assertEquals(sec, d[DateAndTime.SECOND]);
        d = DateAndTime.datePlusElapsedHoursToDate(
            8760.5, year, month, day, hr, min, sec);
        Assert.assertEquals(year + 1, d[DateAndTime.YEAR]);
        Assert.assertEquals(month, d[DateAndTime.MONTH]);
        Assert.assertEquals(day, d[DateAndTime.DAY]);
        Assert.assertEquals(hr, d[DateAndTime.HOUR]);
        Assert.assertEquals(min + 30, d[DateAndTime.MINUTE]);
        Assert.assertEquals(sec, d[DateAndTime.SECOND]);
    }

    [Test]
    public function
    testDateToCompactString():void {
        var s:String = DateAndTime.dateToCompactString(
            year, month, day, hr, min, sec);
        //trace('compact string:', s);
    }

    [Test]
    public function
    testDateToString():void {
        var s:String = DateAndTime.dateToString(
            year, month, day, hr, min, sec);
        //trace('string:', s);
    }

    [Test]
    public function
    testDayOfYearFromDate():void {
        var n:int = DateAndTime.dayOfYearFromDate(year, month, day);
        //trace('n', n);
        Assert.assertEquals(33, n);
    }

    [Test]
    public function
    testDeriveDayOfYear():void {
        var n:int = DateAndTime.deriveDayOfYear(-1.0, 2012, 1, 1, 0, 0, 0);
        // trace('n', n);
        Assert.assertEquals(365, n);
    }

    [Test]
    public function
    testSeriesMeta():void {
        var hdr:Object = DateAndTime.makeHeaderFromDate(-7,2012,1,1,0,0,0);
        var dp:Object = MapUtils.makeObject([
            DateAndTime.ELAPSED_TIME_hrs, 12]);
        for each (var sm:MetaData in DateAndTime.SERIES_META_DATA) {
            //if (sm != null) trace('sm.name', sm.name);
            var val:* = sm.getter(hdr, dp);
            //trace(sm.seriesName, '=', val);
        }
    }

    [Test]
    public function
    testDeriveElapsedTime_sec():void {
        var s:int = DateAndTime.deriveElapsedTime_sec(10.0/3600.0,
            2012, 1, 1, 0, 0, 0);
        Assert.assertEquals(10, s);
    }

    [Test]
    public function
    testHourOfYearFromDate():void {
        var h:Number = DateAndTime.hourOfYearFromDate(2012, 1, 1, 12, 30, 0);
        Assert.assertEquals(12.5, h);
    }

    [Test]
    public function
    testHourOfYearToDate():void {
        var d:Object = DateAndTime.hourOfYearToDate(0.0, year);
        // ObjectUtils.traceObject(d, 'd');
        Assert.assertEquals(year, d[DateAndTime.YEAR]);
        Assert.assertEquals(1, d[DateAndTime.MONTH]);
        Assert.assertEquals(1, d[DateAndTime.DAY]);
        Assert.assertEquals(0, d[DateAndTime.HOUR]);
        Assert.assertEquals(0, d[DateAndTime.MINUTE]);
        Assert.assertEquals(0, d[DateAndTime.SECOND]);
        d = DateAndTime.hourOfYearToDate(1.0, year);
        Assert.assertEquals(year, d[DateAndTime.YEAR]);
        Assert.assertEquals(1, d[DateAndTime.MONTH]);
        Assert.assertEquals(1, d[DateAndTime.DAY]);
        Assert.assertEquals(1, d[DateAndTime.HOUR]);
        Assert.assertEquals(0, d[DateAndTime.MINUTE]);
        Assert.assertEquals(0, d[DateAndTime.SECOND]);
        d = DateAndTime.hourOfYearToDate(8760.0, year);
        Assert.assertEquals(year + 1, d[DateAndTime.YEAR]);
        Assert.assertEquals(1, d[DateAndTime.MONTH]);
        Assert.assertEquals(1, d[DateAndTime.DAY]);
        Assert.assertEquals(0, d[DateAndTime.HOUR]);
        Assert.assertEquals(0, d[DateAndTime.MINUTE]);
        Assert.assertEquals(0, d[DateAndTime.SECOND]);
        d = DateAndTime.hourOfYearToDate(24.0, year);
        Assert.assertEquals(year, d[DateAndTime.YEAR]);
        Assert.assertEquals(1, d[DateAndTime.MONTH]);
        Assert.assertEquals(2, d[DateAndTime.DAY]);
        Assert.assertEquals(0, d[DateAndTime.HOUR]);
        Assert.assertEquals(0, d[DateAndTime.MINUTE]);
        Assert.assertEquals(0, d[DateAndTime.SECOND]);
        d = DateAndTime.hourOfYearToDate(0.5, year);
        Assert.assertEquals(year, d[DateAndTime.YEAR]);
        Assert.assertEquals(1, d[DateAndTime.MONTH]);
        Assert.assertEquals(1, d[DateAndTime.DAY]);
        Assert.assertEquals(0, d[DateAndTime.HOUR]);
        Assert.assertEquals(30, d[DateAndTime.MINUTE]);
        Assert.assertEquals(0, d[DateAndTime.SECOND]);
        d = DateAndTime.hourOfYearToDate(24 * 40, year);
        // ObjectUtils.traceObject(d, 'd');
        Assert.assertEquals(year, d[DateAndTime.YEAR]);
        Assert.assertEquals(2, d[DateAndTime.MONTH]);
        Assert.assertEquals(41 - 31, d[DateAndTime.DAY]);
        Assert.assertEquals(0, d[DateAndTime.HOUR]);
        Assert.assertEquals(0, d[DateAndTime.MINUTE]);
        Assert.assertEquals(0, d[DateAndTime.SECOND]);
    }

    [Test]
    public function
    testMakeDate():void {
        var d:Object = DateAndTime.makeDate(2012, 7, 4, 12, 57, 30);
        Assert.assertEquals(2012, d[DateAndTime.YEAR]);
        Assert.assertEquals(7, d[DateAndTime.MONTH]);
        Assert.assertEquals(4, d[DateAndTime.DAY]);
        Assert.assertEquals(12, d[DateAndTime.HOUR]);
        Assert.assertEquals(57, d[DateAndTime.MINUTE]);
        Assert.assertEquals(30, d[DateAndTime.SECOND]);
    }

    [Test]
    public function
    testParseCompactString():void {
        var d:Object = DateAndTime.parseCompactString(
            "2012/3/17/18/30/00");
        Assert.assertEquals(17, d[DateAndTime.DAY]);
        Assert.assertEquals(3, d[DateAndTime.MONTH]);
        Assert.assertEquals(18, d[DateAndTime.HOUR]);
        Assert.assertEquals(0, d[DateAndTime.SECOND]);
    }

    [Test]
    public function
    testSecondsSinceYearStart():void {
        var s:int = DateAndTime.secondsSinceYearStart(2012,1,1,0,0,13);
        Assert.assertEquals(13, s);
    }

    [Test]
    public function
    testHeaderAndValueObjectsToDateTime():void {
        var hdr:Object = new Object();
        hdr[DateAndTime.REFERENCE_YEAR] = 2012;
        hdr[DateAndTime.REFERENCE_MONTH] = 6;
        hdr[DateAndTime.REFERENCE_DAY] = 2;
        hdr[DateAndTime.REFERENCE_HOUR] = 16;
        hdr[DateAndTime.REFERENCE_MINUTE] = 54;
        hdr[DateAndTime.REFERENCE_SECOND] = 0;
        var point:Object = new Object();
        point[DateAndTime.ELAPSED_TIME_hrs] = 24.0;
        var dt:DateTime = DateAndTime.headerAndValueObjectsToDateTime(
            hdr, point);
        Assert.assertEquals(2012, dt.year);
        Assert.assertEquals(6, dt.month);
        Assert.assertEquals(3, dt.dayOfMonth);
        Assert.assertEquals(16, dt.hourOfDay);
        Assert.assertEquals(54, dt.minuteOfHour);
        Assert.assertEquals(0, dt.secondOfMinute);
    }

    [Test]
    public function
    testElapsedHoursBetweenDates():void {
        var hrs1:Number = DateAndTime.elapsedHoursBetweenDates(
            2012, 6, 2, 20, 45, 0,
            2011, 6, 2, 20, 45, 0);
        //trace('hrs1=', hrs1);
        var hrs2:Number = DateAndTime.elapsedHoursBetweenDates(
            2012, 6, 2, 20, 45, 0,
            2012, 6, 2, 20, 45, 0);
        //trace('hrs2=', hrs2);
        Assert.assertTrue(Math.abs(hrs1 + 8760.0) < 0.1);
        Assert.assertTrue(Math.abs(hrs2 - 0.0) < 0.1);
    }

    [Test]
    public function
    testDeriveDateParts():void {
        var et:Number = 0.9999999999854481;
        var yr:Number = DateAndTime.deriveCurrentYear(
            et, 2002, 1, 1, 0, 0, 0);
        var month:Number = DateAndTime.deriveCurrentMonth(
            et, 2002, 1, 1, 0, 0, 0);
        var day:Number = DateAndTime.deriveDayOfMonth(
            et, 2002, 1, 1, 0, 0, 0);
        var hr:Number = DateAndTime.deriveHourOfDay(
            et, 2002, 1, 1, 0, 0, 0);
        var min:Number = DateAndTime.deriveMinuteOfHour(
            et, 2002, 1, 1, 0, 0, 0);
        var sec:Number = DateAndTime.deriveSecondOfMinute(
            et, 2002, 1, 1, 0, 0, 0);
        //trace(yr, month, day, hr, min, sec);
        Assert.assertEquals(2002, yr);
        Assert.assertEquals(1, month);
        Assert.assertEquals(1, day);
        Assert.assertEquals(1, hr);
        Assert.assertEquals(0, min);
        Assert.assertEquals(0, sec);
        et = 23.9999999999;
        yr = DateAndTime.deriveCurrentYear(
            et, 2002, 1, 1, 0, 0, 0);
        month = DateAndTime.deriveCurrentMonth(
            et, 2002, 1, 1, 0, 0, 0);
        day = DateAndTime.deriveDayOfMonth(
            et, 2002, 1, 1, 0, 0, 0);
        hr = DateAndTime.deriveHourOfDay(
            et, 2002, 1, 1, 0, 0, 0);
        min = DateAndTime.deriveMinuteOfHour(
            et, 2002, 1, 1, 0, 0, 0);
        sec = DateAndTime.deriveSecondOfMinute(
            et, 2002, 1, 1, 0, 0, 0);
        //trace(yr, month, day, hr, min, sec);
        Assert.assertEquals(2002, yr);
        Assert.assertEquals(1, month);
        Assert.assertEquals(2, day);
        Assert.assertEquals(0, hr);
        Assert.assertEquals(0, min);
        Assert.assertEquals(0, sec);
        et = 24.0 * 365.0 - 0.0000001;
        yr = DateAndTime.deriveCurrentYear(
            et, 2002, 1, 1, 0, 0, 0);
        month = DateAndTime.deriveCurrentMonth(
            et, 2002, 1, 1, 0, 0, 0);
        day = DateAndTime.deriveDayOfMonth(
            et, 2002, 1, 1, 0, 0, 0);
        hr = DateAndTime.deriveHourOfDay(
            et, 2002, 1, 1, 0, 0, 0);
        min = DateAndTime.deriveMinuteOfHour(
            et, 2002, 1, 1, 0, 0, 0);
        sec = DateAndTime.deriveSecondOfMinute(
            et, 2002, 1, 1, 0, 0, 0);
        //trace(yr, month, day, hr, min, sec);
        Assert.assertEquals(2003, yr);
        Assert.assertEquals(1, month);
        Assert.assertEquals(1, day);
        Assert.assertEquals(0, hr);
        Assert.assertEquals(0, min);
        Assert.assertEquals(0, sec);
    }

    [Test]
    public function testMonthYear():void {
        var md:MetaData =
            DateAndTime.SERIES_META_DATA[DateAndTime.MONTH_YEAR] as MetaData;
        var hdr:Object = DateAndTime.makeHeaderFromDate(-6, 2012, 1, 1, 0);
        var pt1:Object =
            MapUtils.makeObject([DateAndTime.ELAPSED_TIME_hrs, 0]);
        var pt2:Object =
            MapUtils.makeObject([DateAndTime.ELAPSED_TIME_hrs, 4375]);
        var md1:String = md.getter(hdr, pt1);
        var md2:String = md.getter(hdr, pt2);
        Assert.assertEquals("January 2012", md1);
        Assert.assertEquals("July 2012", md2);
    }
}
}