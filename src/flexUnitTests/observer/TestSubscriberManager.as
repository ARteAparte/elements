/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package flexUnitTests.observer {
import flexunit.framework.Assert;

import observer.SubscriberManager;

public class TestSubscriberManager {
  private var _msg:Array;
  private var sm:SubscriberManager;
  [Before]
  public function setUp():void {
    _msg = [];
    sm = new SubscriberManager();
  }
  public function updateA(e:String):void {
    _msg.push(e);
  }
  public function updateB(e:String):void {
    _msg.push("B");
  }
  public function updateC(e:String):void {
    _msg.push("C");
  }
  [Test]
  public function testNotifySubscribers():void {
    sm.notifySubscribers("A");
    Assert.assertEquals(0, _msg.length);
    sm.registerSubscriberFunction(updateA);
    sm.notifySubscribers("A");
    Assert.assertEquals(1, _msg.length);
    _msg = [];
    sm.registerSubscriberFunction(updateB);
    sm.notifySubscribers("A");
    Assert.assertEquals(2, _msg.length);
    Assert.assertEquals("A", _msg[0]);
    Assert.assertEquals("B", _msg[1]);
  }
  [Test]
  public function testRemoveSubscriberFunction():void {
    sm.notifySubscribers("A");
    Assert.assertEquals(0, _msg.length);
    sm.registerSubscriberFunction(updateA);
    sm.registerSubscriberFunction(updateB);
    sm.registerSubscriberFunction(updateC);
    sm.removeSubscriberFunction(updateB);
    sm.notifySubscribers("A");
    Assert.assertEquals(2, _msg.length);
    Assert.assertEquals("A", _msg[0]);
    Assert.assertEquals("C", _msg[1]);
  }
  [Test]
  public function testIdempotency():void {
    sm.notifySubscribers("A");
    Assert.assertEquals(0, _msg.length);
    sm.registerSubscriberFunction(updateA);
    sm.registerSubscriberFunction(updateB);
    sm.registerSubscriberFunction(updateC);
    sm.removeSubscriberFunction(updateB);
    sm.removeSubscriberFunction(updateB);
    sm.removeSubscriberFunction(updateB);
    sm.notifySubscribers("A");
    Assert.assertEquals(2, _msg.length);
    Assert.assertEquals("A", _msg[0]);
    Assert.assertEquals("C", _msg[1]);
  }
}
}