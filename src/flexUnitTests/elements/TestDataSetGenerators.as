/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package flexUnitTests.elements {
import dse.DataSet;

import elements.DataSetGenerators;

import flexunit.framework.Assert;

public class TestDataSetGenerators {
  [Before]
  public function setUp():void {
  }
  [After]
  public function tearDown():void {
  }
  [Test]
  public function testGen8760():void {
    var ds:DataSet = DataSetGenerators.gen8760();
    var pt:Object = ds.pointN(0);
    //MapUtils.traceObject(pt, 'pt');
    Assert.assertEquals(8760, ds.points.length);
  }
}
}