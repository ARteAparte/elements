/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package flexUnitTests.elements {
import flash.desktop.Clipboard;
import flash.desktop.ClipboardFormats;
import flash.filesystem.File;
import flash.system.Capabilities;

import mx.collections.ArrayCollection;
import mx.collections.IList;

import spark.components.gridClasses.CellPosition;
import spark.components.gridClasses.GridColumn;

import dse.ConstraintFunctions;
import dse.DataSet;
import dse.DataSetBuilder;
import dse.MetaData;
import dse.SeriesSelection;

import elements.DataDisplayOptions;
import elements.DocHandlers;
import elements.DocState;
import elements.DocStateBuilder;
import elements.NormWizData;
import elements.commands.DocAddDataColumn;
import elements.commands.DocAddSeriesToSelection;
import elements.commands.DocCopySelected;
import elements.commands.DocExportData;
import elements.commands.DocImportData;
import elements.commands.DocOffsetSelected;
import elements.commands.DocPaste;
import elements.commands.DocRemoveDataColumn;
import elements.commands.DocSaveFile;
import elements.commands.DocScaleByMinMax;
import elements.commands.DocScaleSelected;
import elements.commands.DocScaleSolarByMonthlyAvgDailyInsolation;
import elements.commands.DocSetActiveSettingSet;
import elements.commands.DocSetDisplayUnitSys;
import elements.commands.DocSetFileModified;
import elements.commands.DocSetFileUnModified;
import elements.commands.DocSetGridSelection;
import elements.commands.DocSetSelection;
import elements.commands.DocSetSelectionToReading;
import elements.commands.DocSetSelectionToSeries;
import elements.commands.DocShiftSeriesColumnToHead;
import elements.commands.DocShiftSeriesColumnToTail;
import elements.commands.DocShiftSeriesColumnTowardsHead;
import elements.commands.DocShiftSeriesColumnTowardsTail;
import elements.commands.DocUpdateDataPoint;

import flexUnitTests.plugins.TestSolar;
import flexUnitTests.utils.MyAssert;

import flexunit.framework.Assert;

import gui.IManagedRef;

import pdt.IPMap;
import pdt.IPVec;
import pdt.PersistentArrayMap;
import pdt.PersistentVector;

import plugins.DateAndTime;
import plugins.EPWSpecific;
import plugins.Location;
import plugins.Psych;
import plugins.Solar;
import plugins.Wind;

import scaling.ScalingFactors;

import units.NoUnits;
import units.TemperatureUnits;
import units.UnitSystems;

import utils.ArrayUtils;
import utils.FormatUtils;
import utils.MapUtils;

import weatherfile.exporters.ExporterDOE2BIN;
import weatherfile.exporters.ExporterDOE2FMT;
import weatherfile.exporters.ExporterEnergyPlusEPW;
import weatherfile.exporters.IExporter;
import weatherfile.importers.IImporter;
import weatherfile.importers.ImporterDOE2BIN;
import weatherfile.importers.ImporterDOE2FMT;
import weatherfile.importers.ImporterEnergyPlusEPW;

public class TestDocState {
    public var sc:IManagedRef;
    public var Tdb_C:Number;
    public var Twb_C:Number;
    public var p_kPa:Number;
    public var NUM_POINTS:int;
    public var pts:IPVec;

    [Before]
    public function setUp():void {
        Tdb_C = 25.0;
        Twb_C = 20.0;
        p_kPa = 101.325;
        var hdr:Object = DateAndTime.makeHeaderFromDate(
            -7, 2012, 1, 1, 0, 0, 0);
        hdr[Location.LATITUDE_deg] = 39.7392;
        hdr[Location.LONGITUDE_deg] = -104.9842;
        hdr[Location.ELEVATION_m] = 300.0;
        NUM_POINTS = 8760;
        var pts_:Array = new Array(NUM_POINTS);
        var flag:Number = 0.0;
        var i:int;
        for (i=0; i<NUM_POINTS; i++) {
            if (i==12) {
              flag = 1.0;
            } else {
              flag = 0.0;
            }
            pts_[i] = MapUtils.makeObject([
                DateAndTime.ELAPSED_TIME_hrs, i,
                Psych.DRY_BULB_TEMP_C, Tdb_C,
                Psych.WET_BULB_TEMP_C, Twb_C,
                Psych.PRESSURE_kPa, p_kPa,
                Solar.DIFFUSE_HORIZONTAL_Wh__m2, 50.0 * flag,
                Solar.BEAM_NORMAL_Wh__m2, 450.0 * flag,
                Solar.EXTRATERRESTRIAL_HORIZ_Wh__m2, 0,
                Solar.EXTRATERRESTRIAL_NORMAL_Wh__m2, 0,
                Solar.IR_FROM_SKY_Wh__m2, 0,
                Wind.DIRECTION_deg, 0,
                Wind.SPEED_m__s, 0,
                EPWSpecific.BEAM_NORMAL_ILLUMINANCE_lux, i / 1000.0,
                EPWSpecific.DATA_SOURCE_AND_UNCERTAINTY_FLAGS, "???",
                EPWSpecific.DIFFUSE_HORIZONTAL_ILLUMINANCE_lux, 0,
                EPWSpecific.GLOBAL_HORIZONTAL_ILLUMINANCE_lux, 0,
                EPWSpecific.ZENITH_LUMINANCE_Cd__m2, 0
            ]);
        }
        pts = PersistentVector.create(pts_);
        var smMap:Object = MapUtils.mergeAll([
            Psych.SERIES_META_DATA,
            DateAndTime.SERIES_META_DATA,
            EPWSpecific.SERIES_META_DATA,
            Wind.SERIES_META_DATA,
            Solar.SERIES_META_DATA]);
        var hdrMap:Object = {};
        var dataSet:DataSet = (new DataSetBuilder())
          .withHeader(hdr)
          .withPoints(pts)
          .withSeriesMetaData(smMap)
          .withHeaderMetaData(hdrMap)
          .build();
        var ds:DocState = DocStateBuilder.createDefault(dataSet).build();
        ds = ds.settingSelectionsTo(PersistentVector.create([
            new SeriesSelection(pts, Psych.RELATIVE_HUMIDITY, 0, 1)]));
        sc = DocHandlers.makeControlledDocState(ds);
    }

    [Test]
    public function testCreate():void {
        var ds:DocState = (new DocStateBuilder())
          .withDataSet((new DataSetBuilder()).build())
          .withDisplayUnits(PersistentArrayMap.EMPTY)
          .withSelections(PersistentVector.EMPTY)
          .withDisplayOptionsMap(PersistentArrayMap.EMPTY)
          .withShownData(PersistentVector.EMPTY)
          .withFileName("")
          .withTitle("untitled")
          .withFile(null)
          .withFileModified(false)
          .withActiveSettingSets(PersistentArrayMap.EMPTY)
          .build();
        if (Capabilities.os.indexOf("Windows") >= 0) {
          Assert.assertEquals(ds.title, 'untitled - Elements');
        } else {
          Assert.assertEquals(ds.title, 'untitled');
        }
    }

    [Test]
    public function testCreateEmpty():void {
        trace('testCreateEmpty');
        var ds:DocState = (new DocStateBuilder()).build();
        var dataSet:DataSet = ds.dataSet;
        Assert.assertEquals(0, dataSet.points.length);
        Assert.assertEquals(0,
            MapUtils.keys(dataSet.header).length);
        Assert.assertEquals(0,
            MapUtils.keys(dataSet.seriesMetaData).length);
        Assert.assertEquals(0,
            MapUtils.keys(dataSet.headerMetaData).length);
        Assert.assertEquals(0, ds.selections.length);
        Assert.assertEquals(0, ds.shownData.length);
    }

    public function makeDataSet():DataSet {
        var sm:Object = {
            A: new MetaData("A", "The Letter A", NoUnits.NONE,
                NoUnits.QUANTITY_OF_MEASURE, MetaData.makeValueGetter('A'),
                MetaData.DATA_TYPE__NUMBER, [{B: true}, {C: true}],
                ConstraintFunctions.makeAlwaysGoodConstraintFunction(),
                {A:true, B:true}, true, NoUnits.AVAILABLE_UNITS),
            B: new MetaData("B", "The Letter B", NoUnits.NONE,
                NoUnits.QUANTITY_OF_MEASURE, MetaData.makeValueGetter('B'),
                MetaData.DATA_TYPE__NUMBER, [{A: true}, {C: true}],
                ConstraintFunctions.makeAlwaysGoodConstraintFunction(),
                {A:true, B:true}, true, NoUnits.AVAILABLE_UNITS),
            C:  new MetaData("B", "The Letter B", NoUnits.NONE,
                NoUnits.QUANTITY_OF_MEASURE, MetaData.makeValueGetter('B'),
                MetaData.DATA_TYPE__NUMBER, [{A: true}, {C: true}],
                ConstraintFunctions.makeAlwaysGoodConstraintFunction(),
                {A:true, B:true}, true, NoUnits.AVAILABLE_UNITS)};
        return (new DataSetBuilder())
          .withHeader({})
          .withPoints(PersistentVector.create([
            {A:1, B:2, C:3}, {A:4, B:5, C:6}, {A:7, B:8, C:9}]))
          .withSeriesMetaData(sm)
          .withHeaderMetaData({})
          .build();
    }

    public function makeControlledDocState():IManagedRef {
      var docState:DocState = DocStateBuilder
        .createDefault(makeDataSet())
        .build();
      return DocHandlers.makeControlledDocState(docState);
    }

    [Test]
    public function testCreateWithDefaults():void {
        var dataSet:DataSet = makeDataSet();
        var ds:DocState = DocStateBuilder.createDefault(dataSet).build();
        Assert.assertTrue(ds != null);
    }

    [Test]
    public function testMakeControlledDocState():void {
        Assert.assertTrue(sc != null);
    }

    [Test]
    public function testReadDocState():void {
        var m:DocState = DocHandlers.readDocState(sc);
        Assert.assertTrue(m != null);
    }

    [Test]
    public function testGetDataSet():void {
        var docState:DocState = DocHandlers.readDocState(sc);
        var dataSet:DataSet = docState.dataSet;
        var md:MetaData = docState.dataSet.metaDataFor(Psych.DRY_BULB_TEMP_C);
        Assert.assertTrue(dataSet != null);
        Assert.assertEquals(NUM_POINTS, dataSet.points.length);
        Assert.assertEquals(Psych.DRY_BULB_TEMP_C, md.name);
    }

    [Test]
    public function testGetPoints():void {
        var docState:DocState = DocHandlers.readDocState(sc);
        var points:IPVec = docState.dataSet.points;
        Assert.assertTrue(points != null);
        Assert.assertEquals(NUM_POINTS, points.count);
        Assert.assertEquals(Tdb_C, points.valAt(0)[Psych.DRY_BULB_TEMP_C]);
    }

    [Test]
    public function testGetPointN():void {
        var docState:DocState = DocHandlers.readDocState(sc);
        var pt:Object = docState.dataSet.pointN(0);
        Assert.assertTrue(pt != null);
        Assert.assertTrue(Tdb_C, pt[Psych.DRY_BULB_TEMP_C]);
    }

    [Test]
    public function testGetSeriesMetaData():void {
        var ds:DocState = DocHandlers.readDocState(sc);
        var smd:Object = ds.dataSet.seriesMetaData;
        Assert.assertTrue(smd.hasOwnProperty(Psych.DRY_BULB_TEMP_C));
    }

    [Test]
    public function testGetHeaderMetaData():void {
        var ds:DocState = DocHandlers.readDocState(sc);
        var hmd:Object = ds.dataSet.headerMetaData
        Assert.assertTrue(hmd != null);
    }

    [Test]
    public function testGetHeader():void {
        var ds:DocState = DocHandlers.readDocState(sc);
        var hdr:Object = ds.dataSet.header;
        Assert.assertTrue(hdr.hasOwnProperty(DateAndTime.REFERENCE_YEAR));
    }

    [Test]
    public function testGetActiveSettingSets():void {
        var ds:DocState = DocHandlers.readDocState(sc);
        var ass:IPMap = ds.activeSettingSets;
        Assert.assertEquals(0, ass.keys.length);
    }

    [Test]
    public function testGetSelections():void {
        var ds:DocState = DocHandlers.readDocState(sc);
        var ss:IPVec = ds.selections;
        Assert.assertEquals(1, ss.count);
    }

    [Test]
    public function testGetSelectionN():void {
        var ds:DocState = DocHandlers.readDocState(sc);
        var s0:SeriesSelection = ds.selectionN(0);
        Assert.assertTrue(s0 != null);
        Assert.assertEquals(Psych.RELATIVE_HUMIDITY, s0.selectedSeries);
    }

    [Test]
    public function testGetSeriesValue():void {
        var ds:DocState = getDS();
        var Igh_W__m2:Number =
          ds.dataSet.seriesValue(0, Solar.GLOBAL_HORIZ_Wh__m2);
        Assert.assertTrue(!isNaN(Igh_W__m2));
        var _Tdb_C:Number =
            ds.dataSet.seriesValue(10, Psych.DRY_BULB_TEMP_C);
        Assert.assertEquals(Tdb_C, _Tdb_C);
    }

    [Test]
    public function testSetPointN():void {
        var ds:DocState = DocHandlers.readDocState(sc);
        var ds2:DocState =
          ds.settingDataSetTo(ds.dataSet.settingNthPointTo(0, {A:1, B:2, C:3}));
        var pt0:Object = ds2.dataSet.pointN(0);
        Assert.assertTrue(pt0.hasOwnProperty('A'));
        Assert.assertTrue(pt0.hasOwnProperty('B'));
        Assert.assertTrue(pt0.hasOwnProperty('C'));
        Assert.assertEquals(1, pt0.A);
        Assert.assertEquals(2, pt0.B);
        Assert.assertEquals(3, pt0.C);
    }

    [Test]
    public function testSetPoints():void {
        var ds_A:DocState = DocHandlers.readDocState(sc);
        var ds_B:DocState = ds_A.settingDataSetTo(
          ds_A.dataSet.settingPointsTo(
            PersistentVector.create([{A:1, B:2, C:3}, {A:4, B:5, C:6}])));
        var pt0_A:Object = ds_A.dataSet.pointN(0);
        var pt0_B:Object = ds_B.dataSet.pointN(0);
        Assert.assertTrue(pt0_A.hasOwnProperty(Psych.DRY_BULB_TEMP_C));
        Assert.assertTrue(pt0_A.hasOwnProperty(Psych.WET_BULB_TEMP_C));
        Assert.assertTrue(pt0_A.hasOwnProperty(DateAndTime.ELAPSED_TIME_hrs));
        Assert.assertEquals(MapUtils.keys(pt0_A).length,
            MapUtils.keys(pt0_A).length);
        Assert.assertTrue(pt0_B.hasOwnProperty('A'));
        Assert.assertTrue(pt0_B.hasOwnProperty('B'));
        Assert.assertTrue(pt0_B.hasOwnProperty('C'));
        Assert.assertEquals(3, MapUtils.keys(pt0_B).length);
        Assert.assertEquals(3, pt0_B.C);
    }

    [Test]
    public function testGetFile():void {
        var ds:DocState = DocHandlers.readDocState(sc);
        var f:File = ds.file;
        Assert.assertEquals(null, f);
    }

    [Test]
    public function testGetFileModified():void {
        var ds:DocState = DocHandlers.readDocState(sc);
        var mod:Boolean = ds.fileModified;
        Assert.assertEquals(true, mod);
    }

    [Test]
    public function testGetFileName():void {
        var ds:DocState = DocHandlers.readDocState(sc);
        var name:String = ds.fileName;
        //trace('name',  name.slice(0, 8))
        Assert.assertEquals('Untitled', name.slice(0, 8));
    }

    [Test]
    public function testGetFileTitle():void {
        var ds:DocState = DocHandlers.readDocState(sc);
        var name:String = ds.title;
        //trace('name',  name.slice(0, 9))
        Assert.assertEquals('Untitled', name.slice(0, 8));
        if (Capabilities.os.indexOf("Windows") >= 0) {
          // on windows, title ends with " - Elements"
          Assert.assertEquals('*', name.charAt(name.length - 12));
        } else {
          Assert.assertEquals('*', name.charAt(name.length - 1));
        }
    }

    [Test]
    public function testGetDisplayUnits():void {
        var ds:DocState = DocHandlers.readDocState(sc);
        var du:IPMap = ds.displayUnits;
        Assert.assertEquals(null, du.valAt(Psych.DRY_BULB_TEMP_C));
        Assert.assertEquals(0, du.keys.length);
    }

    [Test]
    public function testSetSelections():void {
        var ds:DocState = DocHandlers.readDocState(sc);
        Assert.assertEquals(1, ds.selections.length);
        var ds2:DocState = ds.settingSelectionsTo(PersistentVector.EMPTY);
        Assert.assertEquals(0, ds2.selections.length);
    }

    [Test]
    public function testSetHeader():void {
        var ds:DocState = DocHandlers.readDocState(sc);
        var ds2:DocState =
          ds.settingDataSetTo(ds.dataSet.settingHeaderTo({A:1, B:2}));
        var hdr:Object = ds2.dataSet.header;
        Assert.assertEquals(2, MapUtils.keys(hdr).length);
        Assert.assertEquals(1, hdr.A);
        Assert.assertEquals(2, hdr.B);
    }

    [Test]
    public function testGetDataDisplayOptions():void {
        var ds:DocState = DocHandlers.readDocState(sc);
        var opts:IPMap = ds.displayOptionsMap;
        var smd:Object = ds.dataSet.seriesMetaData;
        Assert.assertEquals(MapUtils.keys(smd).length, opts.keys.length);
    }

    [Test]
    public function testGetDataDisplayOptionsFor():void {
        //trace('testGetDataDisplayOptionsFor');
        var ds:DocState = DocHandlers.readDocState(sc);
        var ddo:DataDisplayOptions = ds.displayOptionsFor(Psych.DRY_BULB_TEMP_C);
        Assert.assertTrue(ddo != null);
    }

    [Test]
    public function testGetShownData():void {
        var ds:DocState = DocHandlers.readDocState(sc);
        var sd:IPVec = ds.shownData;
        Assert.assertEquals(DocHandlers.DEFAULT_SHOWN_COLUMNS.count, sd.count);
    }

    [Test]
    public function testGetMetaData():void {
        var ds:DocState = DocHandlers.readDocState(sc);
        var md:MetaData = ds.dataSet.metaDataFor(Psych.DRY_BULB_TEMP_C);
        Assert.assertEquals(Psych.DRY_BULB_TEMP_C, md.name);
    }

    [Test]
    public function testDeriveMonthlyAverageDailyIrradiation():void {
        var dataSet:DataSet = TestSolar.makeSolarDataSet();
        //trace('keys', dataSet.keys);
        //trace('points.length', DataSet.getPointsAsIList(dataSet).length);
        //MapUtils.traceObject(DataSet.getPointN(dataSet, 0), 'p[0]');
        //MapUtils.traceObject(DataSet.getPointN(dataSet, 1), 'p[1]');
        //var p0_:Object = DataSet.getPointN(dataSet, 0);
        //MapUtils.traceObject(p0_, 'p0_');
        var docState:DocState = DocStateBuilder.createDefault(dataSet).build();
        var ds:DataSet = docState.dataSet;
        //trace('ds == dataSet', ds == dataSet);
        //trace('ds === dataSet', ds === dataSet);
        //trace('keys', ds.keys);
        //trace('points.length', DataSet.getPointsAsIList(ds).length);
        //var p0:Object = DataSet.getPointN(ds, 0);
        //trace('attributes:', MapUtils.attributes(p0));
        //trace('p[0] == null', DataSet.getPointN(ds, 0) == null);
        //MapUtils.traceObject(DataSet.getPointN(ds, 0), 'p[0]');
        //trace('back to dataSet');
        //MapUtils.traceObject(DataSet.getPointN(dataSet, 0), 'p[0]');
        //MapUtils.traceObject(DataSet.getPointN(dataSet, 1), 'p[1]');
        //MapUtils.traceObject(p0_, 'p0_');
        /////////////
        var items:ArrayCollection =
            DocHandlers.deriveMonthlyAverageDailyIrradiation(docState);
        var expected_Wh__m2_day:Number = 24.0;
        var Wh__m2_to_MJ__m2:Number = 0.0036;
        var expected_MJ__m2_day:Number = 24.0 * Wh__m2_to_MJ__m2;
        for each (var item:Object in items) {
            Assert.assertTrue(
                Math.abs(expected_MJ__m2_day - item.Hgh_MJ__m2) < 1e-3);
        }
    }

    [Test]
    public function testOnPaste():void {
        var ds:DocState = DocHandlers.readDocState(sc);
        var dsNoSel:DocState = ds.settingSelectionsTo(PersistentVector.EMPTY);
        var sc1:IManagedRef = DocHandlers.makeControlledDocState(dsNoSel);
        sc1.handleCommand(new DocPaste());
        var ds2:DocState = DocHandlers.readDocState(sc1);
        Assert.assertTrue(ds2 == dsNoSel);
        Assert.assertTrue(ds2 === dsNoSel);
        var cb:Clipboard = Clipboard.generalClipboard;
        cb.setData(ClipboardFormats.TEXT_FORMAT, "13.13");
        sc.handleCommand(new DocPaste());
        var ds3:DocState = DocHandlers.readDocState(sc);
        var p0:Object = ds3.dataSet.pointN(0);
        var md:MetaData = ds3.dataSet.metaDataFor(Psych.RELATIVE_HUMIDITY);
        Assert.assertTrue(Math.abs(0.1313 - md.getter({}, p0)) < 1e-3);
    }

    [Test]
    public function testOnCopySelected():void {
        var ds:DocState = DocHandlers.readDocState(sc);
        var rhMD:MetaData = ds.dataSet.metaDataFor(Psych.RELATIVE_HUMIDITY);
        var hdr:Object = ds.dataSet.header;
        var pt0:Object = ds.dataSet.pointN(0);
        var RH:Number = rhMD.getter(hdr, pt0);
        sc.handleCommand(new DocCopySelected());
        var cb:Clipboard = Clipboard.generalClipboard;
        var data:Number = Number(cb.getData(ClipboardFormats.TEXT_FORMAT));
        Assert.assertEquals(
          FormatUtils.numberToFormattedString(
            RH * ScalingFactors.PERCENTAGE, 2), data);
    }

    [Test]
    public function testOnSetSelectionToReading():void {
      sc.handleCommand(new DocSetSelectionToReading(Psych.DRY_BULB_TEMP_C, 0));
      var ss:IPVec = DocHandlers.readDocState(sc).selections;
      Assert.assertEquals(1, ss.count);
      var s:SeriesSelection = ss.nth(0) as SeriesSelection;
      Assert.assertEquals(Psych.DRY_BULB_TEMP_C, s.selectedSeries);
      Assert.assertEquals(0, s.startIndex);
      Assert.assertEquals(0, s.endIndex);
    }

    [Test]
    public function testOnChangeActiveSettingSet():void {
      sc.handleCommand(new DocSetSelectionToReading(Psych.DRY_BULB_TEMP_C, 0));
      sc.handleCommand(new DocSetActiveSettingSet(1));
      var ds:DocState = DocHandlers.readDocState(sc);
      var ass:IPMap = ds.activeSettingSets;
      Assert.assertEquals(1, ass.valAt(Psych.DRY_BULB_TEMP_C));
    }

    [Test]
    public function testOnScaleSolarMnthAvgDlyInsltn():void {
        var Wh__m2_to_MJ__m2:Number = 0.0036;
        var dataSet:DataSet = TestSolar.makeSolarDataSet();
        var docState:DocState = DocStateBuilder.createDefault(dataSet).build();
        var sc1:IManagedRef = DocHandlers.makeControlledDocState(docState);
        sc1.handleCommand(new DocScaleSolarByMonthlyAvgDailyInsolation(
          48.0 * Wh__m2_to_MJ__m2, {year:2012, month:1}));
        var ds:DocState = DocHandlers.readDocState(sc1);
        var pt0:Object = ds.dataSet.pointN(0);
        var hdr:Object = ds.dataSet.header;
        var IghMD:MetaData = ds.dataSet.metaDataFor(Solar.GLOBAL_HORIZ_Wh__m2);
        Assert.assertTrue(Math.abs(2.0 - IghMD.getter(hdr, pt0)) < 1e-4);
    }

    [Test]
    public function testOnAddSeriesToSelection():void {
      sc.handleCommand(new DocAddSeriesToSelection(Psych.DRY_BULB_TEMP_C));
      var ds:DocState = DocHandlers.readDocState(sc);
      var ss:IPVec = ds.selections;
      var s0:SeriesSelection = ss.nth(0) as SeriesSelection;
      var s1:SeriesSelection = ss.nth(1) as SeriesSelection;
      Assert.assertEquals(2, ss.count);
      Assert.assertEquals(Psych.RELATIVE_HUMIDITY, s0.selectedSeries);
      Assert.assertEquals(Psych.DRY_BULB_TEMP_C, s1.selectedSeries);
    }

    [Test]
    public function testOnSetSelectionToSeries():void {
      sc.handleCommand(new DocSetSelectionToSeries(Psych.DRY_BULB_TEMP_C));
      var ds:DocState = DocHandlers.readDocState(sc);
      var ss:IPVec = ds.selections;
      Assert.assertEquals(1, ss.length);
      var s0:SeriesSelection = ds.selectionN(0);
      Assert.assertEquals(Psych.DRY_BULB_TEMP_C, s0.selectedSeries);
    }

    [Test]
    public function testGetSettingSetIdx():void {
        var ds:DocState = DocHandlers.readDocState(sc);
        var idx:int = ds.settingSetIndexFor(Psych.RELATIVE_HUMIDITY);
        Assert.assertEquals(0, idx);
    }

    [Test]
    public function testGetSelectedSettingSet():void {
        var ds:DocState = DocHandlers.readDocState(sc);
        var ss:Array = ds.selectedSettingSet;
        Assert.assertTrue(ArrayUtils.contains(ss,Psych.DRY_BULB_TEMP_C));
        Assert.assertTrue(ArrayUtils.contains(ss,Psych.PRESSURE_kPa));
    }

    [Test]
    public function testUpdate():void {
        // set relative humidity to RH(Tdb=25, Twb=20.5189849, p=101.325)
        var expectedTwb_C:Number = Twb_C - 2.0;
        var RH_expected:Number = Psych.relativeHumidity(
            Tdb_C, expectedTwb_C, p_kPa);
        //trace('RH_expected', RH_expected);
        //trace('last RH', Psych.relativeHumidity(Tdb_C, Twb_C, p_kPa));
        sc.handleCommand(
          new DocUpdateDataPoint(Psych.RELATIVE_HUMIDITY, RH_expected, 0));
        var docState:DocState = DocHandlers.readDocState(sc);
        var dp:Object = docState.dataSet.pointN(0);
        // MapUtils.traceObject(dp, 'dp');
        Assert.assertTrue(Math.abs(
            dp[Psych.DRY_BULB_TEMP_C] - Tdb_C) < 1e-3);
        Assert.assertTrue(Math.abs(
            dp[Psych.WET_BULB_TEMP_C] - expectedTwb_C) < 1e-3);
        Assert.assertTrue(Math.abs(
            dp[Psych.PRESSURE_kPa] - p_kPa) < 1e-3);
    }

    [Test]
    public function testUpdate01():void {
        // set relative humidity to RH(Tdb=25, Twb=20.5189849, p=101.325)
        var RH_request:Number = 1.2;
        //trace('RH_request', RH_request);
        var RH_expected:Number = 1.0;
        sc.handleCommand(
          new DocUpdateDataPoint(Psych.RELATIVE_HUMIDITY, RH_request, 0));
        var docState:DocState = DocHandlers.readDocState(sc);
        var dp:Object = docState.dataSet.pointN(0);
        var sm:MetaData = docState.dataSet.metaDataFor(Psych.RELATIVE_HUMIDITY);
        var hdr:Object = docState.dataSet.header;
        var RH_actual:Number = sm.getter(hdr, dp);
        Assert.assertTrue(Math.abs(RH_actual - RH_expected) < 1e-3);
    }

    [Test]
    public function testUpdate02():void {
        var DP_request:Number = -60.0;
        // trace('DP_request', DP_request);
        sc.handleCommand(
          new DocUpdateDataPoint(Psych.DEW_POINT_TEMP_C, DP_request, 0));
        var docState:DocState = DocHandlers.readDocState(sc);
        var dp:Object = docState.dataSet.pointN(0);
        //MapUtils.traceObject(dp, 'dp');
        var sm:MetaData = docState.dataSet.metaDataFor(Psych.WET_BULB_TEMP_C);
        var hdr:Object = docState.dataSet.header;
        var WB_actual:Number = sm.getter(hdr, dp);
        var WB_expected:Number = 8.50097;
        //trace('WB_actual:', WB_actual);
        Assert.assertTrue(Math.abs(WB_actual - WB_expected) < 1e-3);
    }

    [Test]
    public function testVectorOfCellPositionsToArray():void {
        var xs:Array =
            DocHandlers.vectorOfCellPositionsToArray(new Vector.<CellPosition>());
        Assert.assertEquals(0, xs.length);
        var vs:Vector.<CellPosition> = new Vector.<CellPosition>();
        vs.push(new CellPosition());
        vs.push(new CellPosition());
        var ys:Array = DocHandlers.vectorOfCellPositionsToArray(vs);
        Assert.assertEquals(vs.length, ys.length);
    }

    [Test]
    public function testOnSetGridSelection():void {
        var c0:GridColumn = new GridColumn('A');
        var c1:GridColumn = new GridColumn('B');
        var c2:GridColumn = new GridColumn('C');
        var columns:IList = new ArrayCollection([c0, c1, c2]);
        var cell0:CellPosition = new CellPosition(0, 0);
        var cell1:CellPosition = new CellPosition(1, 1);
        var cell2:CellPosition = new CellPosition(2, 2);
        var cells:Vector.<CellPosition> = new Vector.<CellPosition>(2);
        cells[0] = cell0;
        cells[1] = cell1;
        cells[2] = cell2;
        var sc1:IManagedRef = makeControlledDocState();
        sc1.handleCommand(new DocSetGridSelection(cells, columns));
        var ds:DocState = DocHandlers.readDocState(sc1);
        var ss:IPVec = ds.selections;
        Assert.assertEquals(3, ss.count);
        var s0:SeriesSelection = ds.selectionN(0);
        var s1:SeriesSelection = ds.selectionN(1);
        var s2:SeriesSelection = ds.selectionN(2);
        var items:Array = [
          s0.selectedSeries, s1.selectedSeries, s2.selectedSeries];
        Assert.assertTrue(ArrayUtils.contains(items, 'A'));
        Assert.assertTrue(ArrayUtils.contains(items, 'B'));
        Assert.assertTrue(ArrayUtils.contains(items, 'C'));
    }

  public function runOnExportTest(exp:IExporter, fname:String):void {
    var f:File = File.applicationStorageDirectory.resolvePath(fname);
    trace('TestDocState.runOnExportTest :: ' + f.nativePath);
    sc.handleCommand(new DocExportData(exp, f));
    Assert.assertTrue(f.exists);
    if (f.exists) {
      f.deleteFile();
    }
  }

    [Test(async, timeout="10000")]
    public function testOnExportDataWithEnergyPlusEPWExporter():void {
        runOnExportTest(new ExporterEnergyPlusEPW(), 'tmp.epw');
    }

    [Test(async, timeout="10000")]
    public function testOnExportDataWithDOE2BINExporter():void {
      runOnExportTest(new ExporterDOE2BIN(), 'tmp.bin');
    }

    [Test(async, timeout="10000")]
    public function testOnExportDataWithDOE2FMTExporter():void {
        runOnExportTest(new ExporterDOE2FMT(), 'tmp.fmt');
    }

    public function runImportDataTest(path:String, imp:IImporter):void {
        var f:File = File.applicationDirectory.resolvePath(path);
        sc.handleCommand((new DocImportData(imp, f)));
        var docState:DocState = DocHandlers.readDocState(sc);
        var pts:IPVec = docState.dataSet.points;
        Assert.assertEquals(8760, pts.length);
    }

    [Test]
    public function testOnImportDataFromEPW():void {
        var path:String = 'resources/boulder.epw';
        runImportDataTest(path, new ImporterEnergyPlusEPW());
    }

    [Test]
    public function testOnImportDataFromDOE2BIN():void {
        var path:String = 'resources/denver.bin';
        runImportDataTest(path, new ImporterDOE2BIN());
    }

    [Test]
    public function testOnImportDataFromDOE2FMT():void {
        var path:String = 'resources/denver.fmt';
        runImportDataTest(path, new ImporterDOE2FMT());
    }

    [Test]
    public function testDefaultShownDataColumns():void {
        var smd:Object = {};
        var shown:IPVec = DocHandlers.defaultShownDataColumns(smd);
        Assert.assertEquals(0, shown.count);
        var docState:DocState = DocHandlers.readDocState(sc);
        smd = docState.dataSet.seriesMetaData;
        shown = DocHandlers.defaultShownDataColumns(smd);
        Assert.assertEquals(DocHandlers.DEFAULT_SHOWN_COLUMNS.count, shown.count);
    }

    [Test(async, timeout="10000")]
    public function testOnSaveFile():void {
        var f:File = File.applicationStorageDirectory.resolvePath(
            'junk.elements');
        if (f.exists) {
          f.deleteFile();
        }
        sc.handleCommand(new DocSaveFile(f));
        Assert.assertEquals(true, f.exists);
        var docState:DocState = DocHandlers.readDocState(sc);
        Assert.assertEquals(false, docState.fileModified);
        f.deleteFile();
    }

    [Test]
    public function testOnShiftSeriesColumnToTail():void {
        var docStateBefore:DocState = DocHandlers.readDocState(sc);
        var shwnBefore:IPVec = docStateBefore.shownData;
        var len:int = shwnBefore.count;
        var endIdx:int = len - 1;
        sc.handleCommand(new DocShiftSeriesColumnToTail(
          shwnBefore.nth(0) as String));
        var docStateAfter:DocState = DocHandlers.readDocState(sc);
        var shwnAfter:IPVec = docStateAfter.shownData;
        Assert.assertEquals(shwnBefore.count, shwnAfter.count);
        Assert.assertEquals(shwnBefore.nth(0), shwnAfter.nth(endIdx));
        sc.handleCommand(new DocShiftSeriesColumnToTail(
          shwnAfter.nth(endIdx) as String));
        var docStateAfter2:DocState = DocHandlers.readDocState(sc);
        var shwnAfter2:IPVec = docStateAfter2.shownData;
        Assert.assertEquals(shwnAfter2.count, shwnAfter.length);
        for (var i:int=0; i<len; i++) {
            Assert.assertEquals(shwnAfter2.nth(i), shwnAfter.nth(i));
        }
    }

    [Test]
    public function testOnShiftSeriesColumnToHead():void {
        var docStateBefore:DocState = DocHandlers.readDocState(sc);
        var shwnBefore:IPVec = docStateBefore.shownData;
        var len:int = shwnBefore.count;
        var endIdx:int = len - 1;
        sc.handleCommand(
          new DocShiftSeriesColumnToHead(shwnBefore.nth(endIdx) as String));
        var docStateAfter:DocState = DocHandlers.readDocState(sc);
        var shwnAfter:IPVec = docStateAfter.shownData;
        Assert.assertEquals(shwnBefore.count, shwnAfter.length);
        Assert.assertEquals(shwnBefore.nth(endIdx), shwnAfter.nth(0));
        sc.handleCommand(
          new DocShiftSeriesColumnToHead(shwnAfter.nth(0) as String));
        var docStateAfter2:DocState = DocHandlers.readDocState(sc);
        var shwnAfter2:IPVec = docStateAfter2.shownData;
        Assert.assertEquals(shwnAfter2.count, shwnAfter.length);
        for (var i:int=0; i<len; i++) {
            Assert.assertEquals(shwnAfter2.nth(i), shwnAfter.nth(i));
        }
    }

    [Test]
    public function testOnShiftSeriesColumnTowardsTail():void {
        var docStateBefore:DocState = DocHandlers.readDocState(sc);
        var shwnBefore:IPVec = docStateBefore.shownData;
        var len:int = shwnBefore.length;
        var endIdx:int = len - 1;
        sc.handleCommand(
          new DocShiftSeriesColumnTowardsTail(shwnBefore.nth(0) as String));
        var docStateAfter:DocState = DocHandlers.readDocState(sc);
        var shwnAfter:IPVec = docStateAfter.shownData;
        Assert.assertEquals(shwnBefore.count, shwnAfter.length);
        Assert.assertEquals(shwnBefore.nth(0), shwnAfter.nth(1));
        Assert.assertEquals(shwnBefore.nth(1), shwnAfter.nth(0));
        sc.handleCommand(
          new DocShiftSeriesColumnToTail(shwnAfter.nth(endIdx) as String));
        var docStateAfter2:DocState = DocHandlers.readDocState(sc);
        var shwnAfter2:IPVec = docStateAfter2.shownData;
        Assert.assertEquals(shwnAfter2.count, shwnAfter.length);
        for (var i:int=0; i<len; i++) {
            Assert.assertEquals(shwnAfter2.nth(i), shwnAfter.nth(i));
        }
    }

    [Test]
    public function testOnShiftSeriesColumnTowardsHead():void {
        var docStateBefore:DocState = DocHandlers.readDocState(sc);
        var shwnBefore:IPVec = docStateBefore.shownData;
        var len:int = shwnBefore.length;
        var endIdx:int = len - 1;
        sc.handleCommand(
          new DocShiftSeriesColumnTowardsHead(
            shwnBefore.nth(endIdx) as String));
        var docStateAfter:DocState = DocHandlers.readDocState(sc);
        var shwnAfter:IPVec = docStateAfter.shownData;
        Assert.assertEquals(shwnBefore.length, shwnAfter.length);
        Assert.assertEquals(shwnBefore.nth(endIdx), shwnAfter.nth(endIdx-1));
        Assert.assertEquals(shwnBefore.nth(endIdx-1), shwnAfter.nth(endIdx));
        sc.handleCommand(
          new DocShiftSeriesColumnTowardsHead(shwnAfter.nth(0) as String));
        var docStateAfter2:DocState = DocHandlers.readDocState(sc);
        var shwnAfter2:IPVec = docStateAfter2.shownData;
        Assert.assertEquals(shwnAfter2.count, shwnAfter.length);
        for (var i:int=0; i<len; i++) {
            Assert.assertEquals(shwnAfter2.nth(i), shwnAfter.nth(i));
        }
    }

    [Test]
    public function testShiftForwardShiftBackIsNoChange():void {
        var docStateBefore:DocState = DocHandlers.readDocState(sc);
        var shwnBefore:IPVec = docStateBefore.shownData;
        sc.handleCommand(
          new DocShiftSeriesColumnTowardsHead(shwnBefore.nth(3) as String));
        sc.handleCommand(
          new DocShiftSeriesColumnTowardsTail(shwnBefore.nth(3) as String));
        var docStateAfter:DocState = DocHandlers.readDocState(sc);
        var shwnAfter:IPVec = docStateAfter.shownData;
        Assert.assertEquals(shwnAfter.length, shwnBefore.length);
        for (var i:int=0; i<shwnBefore.length; i++) {
            Assert.assertEquals(shwnBefore.nth(i), shwnAfter.nth(i));
        }
    }

    public function getDS():DocState {
        return DocHandlers.readDocState(sc);
    }

    [Test]
    public function testOnRemoveDataColumn():void {
        var docStateBefore:DocState = getDS();
        var shwnBefore:IPVec = docStateBefore.shownData;
        var len:int = shwnBefore.length;
        var endIdx:int = len - 1;
        sc.handleCommand(new DocRemoveDataColumn('A'));
        var docStateAfter:DocState = getDS();
        var shwnAfter:IPVec = docStateAfter.shownData;
        Assert.assertEquals(shwnBefore.length, shwnAfter.length);
        for (var i:int=0; i<len; i++) {
            Assert.assertEquals(shwnBefore.nth(i), shwnAfter.nth(i));
        }
        sc.handleCommand(new DocRemoveDataColumn(shwnAfter.nth(0) as String));
        var docStateAfter2:DocState = getDS();
        var shwnAfter2:IPVec = docStateAfter2.shownData;
        Assert.assertEquals(shwnAfter.length - 1, shwnAfter2.length);
        for (i=0; i<(len - 1); i++) {
            Assert.assertEquals(shwnAfter2.nth(i), shwnAfter.nth(i+1));
        }
    }

    [Test]
    public function testOnAddDataColumn():void {
        var ds0:DocState = getDS();
        var shwn0:IPVec = ds0.shownData;
        sc.handleCommand(new DocAddDataColumn('A'));
        var ds1:DocState = getDS();
        var shwn1:IPVec = ds1.shownData;
        Assert.assertEquals(shwn0.length + 1, shwn1.length);
        Assert.assertEquals('A', shwn1.nth(shwn1.count - 1));
        sc.handleCommand(new DocAddDataColumn(shwn1.nth(0) as String));
        var ds2:DocState = getDS();
        var shwn2:IPVec = ds2.shownData;
        Assert.assertEquals(shwn1.length, shwn2.length);
        for (var i:int=0; i<shwn1.length; i++) {
            Assert.assertEquals(shwn2.nth(i), shwn1.nth(i));
        }
    }

    [Test]
    public function testIsProperToolCall():void {
        Assert.assertTrue(DocHandlers.isProperToolCall(getDS(), {offset: "10"},
            "offset"));
        Assert.assertEquals(false, DocHandlers.isProperToolCall(getDS(),
            {a: "abba"}, "a"));
        Assert.assertEquals(false, DocHandlers.isProperToolCall(getDS(),
            {bob:10}, 'john'));
        sc.handleCommand(new DocAddSeriesToSelection(Psych.DRY_BULB_TEMP_C));
        Assert.assertEquals(false,
            DocHandlers.isProperToolCall(getDS(), {offset: "10"}, "offset"));
    }

    [Test]
    public function testOnOffsetSelected():void {
        trace('testOnOffsetSelected');
        sc.handleCommand(new DocSetSelectionToSeries(Wind.SPEED_m__s));
        var start:Date = new Date();
        sc.handleCommand(new DocOffsetSelected(10.0));
        var end:Date = new Date();
        trace('(1) time to offset 8760 points (ms):', end.time - start.time);
        var ds:DocState = getDS();
        Assert.assertEquals(10.0, ds.dataSet.seriesValue(0, Wind.SPEED_m__s));
        Assert.assertEquals(10.0, ds.dataSet.seriesValue(10, Wind.SPEED_m__s));
        sc.handleCommand(new DocOffsetSelected(-19.0));
        ds = getDS();
        var val:Number = ds.dataSet.seriesValue(0, Wind.SPEED_m__s);
        trace('wind speed after request to set negative (m/s) =', val);
        MyAssert.almostEquals(0.0, val, 1e-2);
    }

    [Test]
    public function testOnOffsetSelected2():void {
        trace('testOnOffsetSelected2');
        sc.handleCommand(new DocSetSelectionToSeries(Psych.DRY_BULB_TEMP_C));
        var start:Date = new Date();
        sc.handleCommand(new DocOffsetSelected(1.0));
        var end:Date = new Date();
        trace('(2) time to offset 8760 points (ms):', end.time - start.time);
        var ds:DocState = getDS();
        Assert.assertEquals(Tdb_C + 1,
          ds.dataSet.seriesValue(0, Psych.DRY_BULB_TEMP_C));
        Assert.assertEquals(Tdb_C + 1,
          ds.dataSet.seriesValue(10, Psych.DRY_BULB_TEMP_C));
    }

    [Test]
    public function testOnScaleSelected():void {
      trace('testOnScaleSelected');
      var idx:int = 12;
      sc.handleCommand(
        new DocSetSelectionToReading(Solar.GLOBAL_HORIZ_Wh__m2, idx));
      var ds0:DocState = getDS();
      var sg0:Number = ds0.dataSet.seriesValue(idx, Solar.GLOBAL_HORIZ_Wh__m2);
      trace('sg0 (W/m2) =', sg0);
      sc.handleCommand(new DocOffsetSelected(1.0));
      var ds1:DocState = getDS();
      var sg1:Number = ds1.dataSet.seriesValue(idx, Solar.GLOBAL_HORIZ_Wh__m2);
      MyAssert.almostEquals(sg0 + 1, sg1);
      sc.handleCommand(new DocScaleSelected(2.0));
      var ds2:DocState = getDS();
      var sg2:Number = ds2.dataSet.seriesValue(idx, Solar.GLOBAL_HORIZ_Wh__m2);
      MyAssert.almostEquals(2.0 * sg1, sg2);
    }

    /* TODO: Rework this test. Perhaps apply it to another value not so
    sensitive like solar (e.g., wind speed)
    
    [Test]
    public function testOnNormalizeSelected():void {
        trace('testOnNormalizeSelected');
        var ds0:DocState = getDS();
        var idx:int = 12;
        var Idh0_W__m2:Number =
          ds0.dataSet.seriesValue(idx, Solar.DIFFUSE_HORIZONTAL_Wh__m2);
        trace('Idh0_W__m2', Idh0_W__m2);
        sc.handleCommand(
          new DocSetSelectionToSeries(Solar.DIFFUSE_HORIZONTAL_Wh__m2));
        sc.handleCommand(new DocNormalizeSelected(Idh0_W__m2 * 2.0));
        var ds:DocState = getDS();
        var Idh_W__m2:Number =
          ds.dataSet.seriesValue(idx, Solar.DIFFUSE_HORIZONTAL_Wh__m2);
        trace('Idh_W__m2', Idh_W__m2);
        MyAssert.almostEquals(Idh0_W__m2 * 2.0, Idh_W__m2);
    }
    */

    [Test]
    public function testChangeActiveSettingSet():void {
      sc.handleCommand(
        new DocSetSelectionToReading(Psych.DEW_POINT_TEMP_C, 0));
      var ds:DocState = DocHandlers.readDocState(sc);
      var pts:IPVec = ds.dataSet.points;
      var hdr:Object = ds.dataSet.header;
      var Tdew_md:MetaData = ds.dataSet.metaDataFor(Psych.DEW_POINT_TEMP_C);
      var Tdew_before:Number = Tdew_md.getter(hdr, pts.nth(0));
      var docState:DocState = DocHandlers.readDocState(sc);
      var actSS:IPMap = docState.activeSettingSets;
      Assert.assertTrue(
        !actSS.containsKey(Psych.DEW_POINT_TEMP_C));
      sc.handleCommand(new DocSetActiveSettingSet(1));
      var newDS:DocState = DocHandlers.readDocState(sc);
      var newActSS:IPMap = newDS.activeSettingSets;
      Assert.assertTrue(newActSS.containsKey(Psych.DEW_POINT_TEMP_C));
      Assert.assertEquals(1, newActSS.valAt(Psych.DEW_POINT_TEMP_C));
      // change dew point while holding wet bulb and pressure
      //trace('calling update holding dew point and wet bulb constant');
      sc.handleCommand(
        new DocUpdateDataPoint(Psych.DEW_POINT_TEMP_C, Twb_C, 0));
      var newNewDs:DocState = DocHandlers.readDocState(sc);
      hdr = newNewDs.dataSet.header;
      pts = newNewDs.dataSet.points;
      var smd:Object = newNewDs.dataSet.seriesMetaData;
      var Twb_md:MetaData = smd[Psych.WET_BULB_TEMP_C] as MetaData;
      Tdew_md = smd[Psych.DEW_POINT_TEMP_C] as MetaData;
      var Tdb_md:MetaData = smd[Psych.DRY_BULB_TEMP_C] as MetaData;
      var p_md:MetaData = smd[Psych.PRESSURE_kPa] as MetaData;
      Assert.assertTrue(Math.abs(Twb_C -
        Tdew_md.getter(hdr, pts.nth(0))) < 1e-3);
      Assert.assertTrue(Math.abs(p_kPa -
        p_md.getter(hdr, pts.nth(0))) < 1e-3);
      // trace('Twb =', Twb_md.getter(hdr, pts.nth(0)));
      // trace('Tdb =', Tdb_md.getter(hdr, pts.nth(0)));
      // trace('p =', p_md.getter(hdr, pts.nth(0)));
      Assert.assertTrue(Math.abs(Twb_C -
        Twb_md.getter(hdr, pts.nth(0))) < 1e-3);
      Assert.assertTrue(Math.abs(Twb_C -
        Tdb_md.getter(hdr, pts.nth(0))) < 1e-1);
    }

    [Test]
    public function testChangeActiveSettingSet2():void {
      sc.handleCommand(new DocSetSelectionToReading(Psych.DRY_BULB_TEMP_C, 0));
      sc.handleCommand(new DocSetActiveSettingSet(1));
      var ds:DocState = DocHandlers.readDocState(sc);
      var actSS:IPMap = ds.activeSettingSets;
      Assert.assertEquals(1, actSS.valAt(Psych.DRY_BULB_TEMP_C));
    }

    [Test]
    public function testCopySelected():void {
      sc.handleCommand(new DocSetSelectionToSeries(Psych.DRY_BULB_TEMP_C));
      sc.handleCommand(new DocAddSeriesToSelection(Psych.WET_BULB_TEMP_C));
      sc.handleCommand(new DocAddSeriesToSelection(Psych.PRESSURE_kPa));
    }

    [Test]
    public function testOnFileModified():void {
      var ds:DocState = DocHandlers.readDocState(sc);
      sc.handleCommand(new DocSetFileModified());
      var newDS:DocState = DocHandlers.readDocState(sc);
      Assert.assertEquals(true, newDS.fileModified);
      var title:String = newDS.title;
      if (Capabilities.os.indexOf("Windows") >= 0) {
        Assert.assertTrue('*' == title.charAt(
          title.length - 12));
      } else {
        Assert.assertTrue('*' == title.charAt(title.length - 1));
      }
    }

    [Test]
    public function testOnFileUnModified():void {
      var ds:DocState = DocHandlers.readDocState(sc);
      sc.handleCommand(new DocSetFileModified());
      sc.handleCommand(new DocSetFileUnModified());
      var newDS:DocState = DocHandlers.readDocState(sc);
      Assert.assertEquals(true, ds.fileModified);
      var title:String = newDS.title;
      Assert.assertTrue('*' != title.charAt(title.length - 1));
    }

    [Test]
    public function testHasData():void {
        trace('testHasData');
        var empty:DocState = (new DocStateBuilder()).build();
        Assert.assertTrue(
            DocHandlers.hasData(DocHandlers.readDocState(sc)));
        Assert.assertTrue(!DocHandlers.hasData(empty));
    }

    [Test]
    public function testOnSetDisplayUnitSystem():void {
        trace('testOnSetDisplayUnitSystem');
        sc.handleCommand(new DocSetDisplayUnitSys(UnitSystems.IP));
        var ds1:DocState = getDS();
        var units1:String = ds1.displayUnitsFor(Psych.DRY_BULB_TEMP_C);
        Assert.assertEquals(TemperatureUnits.F, units1);
    }

    [Test]
    public function testGetDisplayUnitsFor():void {
        trace('testGetDisplayUnitsFor');
        var ds0:DocState = getDS();
        var units0:String = ds0.displayUnitsFor(Psych.DRY_BULB_TEMP_C);
        Assert.assertEquals(TemperatureUnits.C, units0);
    }

    [Test]
    public function testGetDisplayUnitSystem():void {
        trace('testGetDisplayUnitSystem');
        var ds:DocState = getDS();
        var us:String = ds.displayUnitSystem;
        var usExp:String = UnitSystems.SI;
        Assert.assertEquals(usExp, us);
        for each (usExp in UnitSystems.UNIT_SYSTEMS) {
          sc.handleCommand(new DocSetDisplayUnitSys(usExp));
          ds = getDS();
          us = ds.displayUnitSystem;
          Assert.assertEquals(usExp, us);
        }
    }

    [Test]
    public function testGetSeriesMetricNames():void {
        trace('testGetSeriesMetricNames');
        var ds:DocState = getDS();
        var ns:Array = ds.dataSet.seriesMetricNames;
        Assert.assertTrue(ns.length > 0);
    }

    [Test]
    public function testGetSelectionsByMonth():void {
        var docState:DocState = DocHandlers.readDocState(sc);
        var ss:Array = DocHandlers.getSelectionsByMonth(docState);
        Assert.assertTrue(ss.length > 0);
        Assert.assertEquals(12, ss.length);
        var s:SeriesSelection = ss[0];
        var getMonth:Function =
          docState.dataSet.metaDataFor(DateAndTime.MONTH).getter;
        var selectedRows:Array = s.selectedRows;
        var hdr:Object = docState.dataSet.header;
        var pts:IPVec = docState.dataSet.points;
        Assert.assertEquals(31 * 24, selectedRows.length);
        for (var i:int=0; i<selectedRows.length; i++) {
            var pt:Object = pts.valAt(selectedRows[i]);
            var m:int = getMonth(hdr, pt);
            Assert.assertEquals(1, m);
        }
        s = ss[11];
        selectedRows = s.selectedRows;
        Assert.assertEquals(31 * 24, selectedRows.length);
        for (i=0; i<selectedRows.length; i++) {
            pt = pts.valAt(selectedRows[i]);
            m = getMonth(hdr, pt);
            Assert.assertEquals(12, m);
        }
    }

    [Test]
    public function testGetScaleByMonthPanelData():void {
        var docState:DocState = DocHandlers.readDocState(sc);
        var pd:NormWizData = DocHandlers.getScaleByMonthPanelData(docState);
        Assert.assertTrue(pd.metrics != null);
        Assert.assertTrue(pd.settingSets != null);
        Assert.assertTrue(pd.selections != null);
    }

    [Test]
    public function testGetNumericMetricNames():void {
        var docState:DocState = DocHandlers.readDocState(sc);
        var ns:Array = docState.dataSet.numericMetricNames;
        Assert.assertEquals(true, ns.length > 0);
    }

    [Test]
    public function testGetSettingSetMap():void {
        var docState:DocState = DocHandlers.readDocState(sc);
        var ssm:Object = docState.dataSet.settingSetMap;
        Assert.assertTrue(ssm != null);
        Assert.assertTrue(ssm.hasOwnProperty(Psych.DRY_BULB_TEMP_C));
        Assert.assertTrue((ssm[Psych.DRY_BULB_TEMP_C] as Array).length > 0);
    }

    [Test]
    public function testGetSettingSetStringMap():void {
        var docState:DocState = DocHandlers.readDocState(sc);
        var sssm:Object = docState.dataSet.settingSetStringMap;
        Assert.assertTrue(sssm != null);
        Assert.assertTrue(sssm.hasOwnProperty(Psych.DRY_BULB_TEMP_C));
    }

    [Test]
    public function testOnSetSelection():void {
        var points:IPVec = this.pts;
        var seriesName:String = EPWSpecific.BEAM_NORMAL_ILLUMINANCE_lux;
        var startIndex:int = 5000;
        var numberOfValues:int = 24;
        var s:SeriesSelection =
            new SeriesSelection(
                points, seriesName, startIndex, numberOfValues);
        sc.handleCommand(new DocSetSelection(s));
        var ds:DocState = DocHandlers.readDocState(sc);
        var ss:IPVec = ds.selections;
        Assert.assertEquals(1, ss.length);
        var s0:SeriesSelection = ds.selectionN(0);
        Assert.assertEquals(seriesName, s0.selectedSeries);
        Assert.assertEquals(startIndex, s0.startIndex);
        Assert.assertEquals(numberOfValues, s0.numberOfValues);
    }

    [Test]
    public function testOnScaleByMinMax():void {
      sc.handleCommand(
        new DocSetSelection(
          new SeriesSelection(
            pts, EPWSpecific.BEAM_NORMAL_ILLUMINANCE_lux, 0, 100)));
      sc.handleCommand(
        new DocScaleByMinMax(0, 0.099, 1, 100));
      var ds:DocState = DocHandlers.readDocState(sc);
      var newPts:IPVec = ds.dataSet.points;
      var hdr:Object = ds.dataSet.header;
      var getter:Function = ds.dataSet.metaDataFor(
        EPWSpecific.BEAM_NORMAL_ILLUMINANCE_lux).getter;
      Assert.assertEquals(1, getter(hdr, newPts.valAt(0)));
      Assert.assertEquals(100, getter(hdr, newPts.valAt(99)));
      Assert.assertEquals(51, getter(hdr, newPts.valAt(50)));
    }

    [Test]
    public function testOnScaleSelected2():void {
        //trace('testOnScaleSelected2');
      sc.handleCommand(new DocSetSelection(
        new SeriesSelection(
          pts,EPWSpecific.BEAM_NORMAL_ILLUMINANCE_lux, 0, 100)));
      sc.handleCommand(new DocScaleSelected(2.0));
      var ds:DocState = DocHandlers.readDocState(sc);
      var newPts:IPVec = ds.dataSet.points;
      var hdr:Object = ds.dataSet.header;
      var getter:Function = ds.dataSet.metaDataFor(
        EPWSpecific.BEAM_NORMAL_ILLUMINANCE_lux).getter;
      Assert.assertEquals(10, getter({},
        MapUtils.makeObject([
          EPWSpecific.BEAM_NORMAL_ILLUMINANCE_lux, 10])));
      MapUtils.traceObject(newPts.valAt(0), 'newPts');
      Assert.assertEquals(0, getter(hdr, newPts.valAt(0)));
      Assert.assertEquals(0.198, getter(hdr, newPts.valAt(99)));
      Assert.assertEquals(0.1, getter(hdr, newPts.valAt(50)));
    }
}
}