/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package flexUnitTests.elements.helpers {
import mx.collections.ArrayCollection;
import mx.collections.IList;
import mx.utils.ObjectUtil;

import elements.helpers.FnBasedDataProvider;
import elements.helpers.IProvideData;

import flexunit.framework.Assert;

import utils.MapUtils;

public class TestFnBasedDataProvider {
  public var dataProvider:IProvideData;

  [Before]
  public function setUp():void {
    var f:Function = function():IList {
      var xs:Array = [{a:1,b:1,c:1},{a:2,b:2,c:2},{a:3,b:3,c:3}];
      return new ArrayCollection(xs);
    };
    var g:Function = function(items:Array):IList {
      var xs:Array = [];
      for (var n:int=0; n<3; n++) {
        var obj:Object = {a:n+1,b:n+1,c:n+1};
        var modObj:Object = MapUtils.onlyTheseKeys(obj, items);
        xs.push(modObj);
      }
      return new ArrayCollection(xs);
    };
    dataProvider = new FnBasedDataProvider(f, g);
  }
  [Test]
  public function testProvideData():void {
    var xs:ArrayCollection = dataProvider.provideData() as ArrayCollection;
    var ys:Array = [{a:1,b:1,c:1},{a:2,b:2,c:2},{a:3,b:3,c:3}];
    Assert.assertEquals(0, ObjectUtil.compare(ys[0], xs[0]));
    Assert.assertEquals(3, xs.length);
  }
  [Test]
  public function testProvideGivenData():void {
    var items:Array = ["a","b"];
    var xs:ArrayCollection =
      dataProvider.provideGivenData(items) as ArrayCollection;
    var ys:Array = [{a:1,b:1},{a:2,b:2},{a:3,b:3}];
    Assert.assertEquals(0, ObjectUtil.compare(ys[0], xs[0]));
    Assert.assertEquals(3, xs.length);
  }
}
}