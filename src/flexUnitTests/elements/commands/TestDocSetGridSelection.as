/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package flexUnitTests.elements.commands {
import mx.collections.ArrayCollection;
import mx.collections.IList;

import spark.components.gridClasses.CellPosition;
import spark.components.gridClasses.GridColumn;

import dse.DataSet;
import dse.DataSetBuilder;
import dse.MetaData;
import dse.SeriesSelection;

import elements.DocState;
import elements.DocStateBuilder;
import elements.commands.DocSetGridSelection;

import flexunit.framework.Assert;

import gui.AcmeManagedRef;
import gui.IManagedRef;

import pdt.PersistentVector;

import utils.ArrayUtils;

public class TestDocSetGridSelection {
  public var columns:IList;
  public var oneCell:Vector.<CellPosition>;
  public var columnSelectA:Vector.<CellPosition>;
  public var rowSelectInBCD:Vector.<CellPosition>;
  public var selectAll:Vector.<CellPosition>;
  public var docState:DocState;
  public var dsRef:IManagedRef;
  [Before]
  public function setUp():void {
    var colA:GridColumn = new GridColumn("A");
    var colB:GridColumn = new GridColumn("B");
    var colC:GridColumn = new GridColumn("C");
    var colD:GridColumn = new GridColumn("D");
    columns = new ArrayCollection([colA,colB,colC,colD]);
    oneCell = Vector.<CellPosition>([new CellPosition(0,0)]);
    columnSelectA = Vector.<CellPosition>([
      new CellPosition(0,0),
      new CellPosition(1,0),
      new CellPosition(2,0),
      new CellPosition(3,0)]);
    rowSelectInBCD = Vector.<CellPosition>([
      new CellPosition(2,1),
      new CellPosition(2,2),
      new CellPosition(2,3)]);
    selectAll = Vector.<CellPosition>([
      new CellPosition(0,0),
      new CellPosition(1,0),
      new CellPosition(2,0),
      new CellPosition(3,0),
      new CellPosition(0,1),
      new CellPosition(1,1),
      new CellPosition(2,1),
      new CellPosition(3,1),
      new CellPosition(0,2),
      new CellPosition(1,2),
      new CellPosition(2,2),
      new CellPosition(3,2),
      new CellPosition(0,3),
      new CellPosition(1,3),
      new CellPosition(2,3),
      new CellPosition(3,3)]);

    var dataSet:DataSet = (new DataSetBuilder())
      .withHeader({})
      .withPoints(PersistentVector.create([
        {A:1,B:10,C:100,D:1000},
        {A:2,B:20,C:200,D:2000},
        {A:3,B:30,C:300,D:3000},
        {A:4,B:40,C:400,D:4000}]))
      .withSeriesMetaData(
        { A:new MetaData("A"),
          B:new MetaData("B"),
          C:new MetaData("C"),
          D:new MetaData("D")})
      .withHeaderMetaData({})
      .build();
    docState = DocStateBuilder.createDefault(dataSet).build();
    dsRef = new AcmeManagedRef(docState);
  }
  [Test]
  public function testRunOneCell():void {
    var ds01:DocState = dsRef.read();
    Assert.assertEquals(0, ds01.selections.length);
    dsRef.handleCommand(new DocSetGridSelection(oneCell, columns));
    var docState:DocState = dsRef.read() as DocState;
    Assert.assertEquals(1, docState.selections.length);
    var selection:SeriesSelection =
      docState.selections.getItemAt(0) as SeriesSelection;
    Assert.assertEquals(0, selection.startIndex);
    Assert.assertEquals(0, selection.endIndex);
    Assert.assertEquals("A", selection.selectedSeries);
  }
  [Test]
  public function testUnderstandingOfGridColumns():void {
    Assert.assertEquals("A", (columns[0] as GridColumn).dataField);
  }
  [Test]
  public function testRunColumnSelect():void {
    var ds01:DocState = dsRef.read();
    Assert.assertEquals(0, ds01.selections.length);
    dsRef.handleCommand(new DocSetGridSelection(columnSelectA, columns));
    var docState:DocState = dsRef.read() as DocState;
    Assert.assertEquals(1, docState.selections.length);
    var selection:SeriesSelection =
      docState.selections.getItemAt(0) as SeriesSelection;
    Assert.assertEquals(0, selection.startIndex);
    Assert.assertEquals(3, selection.endIndex);
    Assert.assertEquals("A", selection.selectedSeries);
  }
  [Test]
  public function testRunRowSelect():void {
    var ds01:DocState = dsRef.read();
    Assert.assertEquals(0, ds01.selections.length);
    dsRef.handleCommand(new DocSetGridSelection(rowSelectInBCD, columns));
    var docState:DocState = dsRef.read() as DocState;
    Assert.assertEquals(3, docState.selections.length);
    var selection:SeriesSelection =
      docState.selections.getItemAt(0) as SeriesSelection;
    var items:Array = [
      (docState.selections.getItemAt(0) as SeriesSelection).selectedSeries,
      (docState.selections.getItemAt(1) as SeriesSelection).selectedSeries,
      (docState.selections.getItemAt(2) as SeriesSelection).selectedSeries];
    Assert.assertEquals(2, selection.startIndex);
    Assert.assertEquals(2, selection.endIndex);
    Assert.assertTrue(ArrayUtils.contains(items, "B"));
    selection = docState.selections.getItemAt(1) as SeriesSelection;
    Assert.assertEquals(2, selection.startIndex);
    Assert.assertEquals(2, selection.endIndex);
    Assert.assertTrue(ArrayUtils.contains(items, "C"));
    selection = docState.selections.getItemAt(2) as SeriesSelection;
    Assert.assertEquals(2, selection.startIndex);
    Assert.assertEquals(2, selection.endIndex);
    Assert.assertTrue(ArrayUtils.contains(items, "D"));
  }
  [Test]
  public function testRunSelectAll():void {
    var ds01:DocState = dsRef.read();
    Assert.assertEquals(0, ds01.selections.length);
    dsRef.handleCommand(new DocSetGridSelection(selectAll, columns));
    var docState:DocState = dsRef.read() as DocState;
    Assert.assertEquals(4, docState.selections.length);
    var selection:SeriesSelection =
      docState.selections.getItemAt(0) as SeriesSelection;
    var items:Array = [
      (docState.selections.getItemAt(0) as SeriesSelection).selectedSeries,
      (docState.selections.getItemAt(1) as SeriesSelection).selectedSeries,
      (docState.selections.getItemAt(2) as SeriesSelection).selectedSeries,
      (docState.selections.getItemAt(3) as SeriesSelection).selectedSeries];
    Assert.assertEquals(0, selection.startIndex);
    Assert.assertEquals(3, selection.endIndex);
    Assert.assertTrue(ArrayUtils.contains(items, "A"));
    selection = docState.selections.getItemAt(1) as SeriesSelection;
    Assert.assertEquals(0, selection.startIndex);
    Assert.assertEquals(3, selection.endIndex);
    Assert.assertTrue(ArrayUtils.contains(items, "B"));
    selection = docState.selections.getItemAt(2) as SeriesSelection;
    Assert.assertEquals(0, selection.startIndex);
    Assert.assertEquals(3, selection.endIndex);
    Assert.assertTrue(ArrayUtils.contains(items, "C"));
    selection = docState.selections.getItemAt(3) as SeriesSelection;
    Assert.assertEquals(0, selection.startIndex);
    Assert.assertEquals(3, selection.endIndex);
    Assert.assertTrue(ArrayUtils.contains(items, "D"));
  }
}
}