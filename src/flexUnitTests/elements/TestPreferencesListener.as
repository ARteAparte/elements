package flexUnitTests.elements {
  import flash.filesystem.File;

  import elements.DocEvents;
  import elements.DocState;
  import elements.DocStateBuilder;
  import elements.PreferencesListener;

  import gui.AcmeManagedRef;

  import org.flexunit.Assert;

public class TestPreferencesListener {
  public var pl:PreferencesListener;
  public var f:File;
  [Before]
  public function setUp():void {
    var ds:DocState = (new DocStateBuilder()).build();
    pl = new PreferencesListener(new AcmeManagedRef(ds), "test.xml");
    f = File.applicationStorageDirectory.resolvePath("test.xml");
  }
  [After]
  public function tearDown():void {
    var f:File = File.applicationStorageDirectory.resolvePath("test.xml");
    f.deleteFile();
  }
  [Test]
  public function testUpdate():void {
    pl.update(DocEvents.EVENT_DISPLAY_UNITS_CHANGED);
    Assert.assertTrue(f.exists);
  }
}
}