/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package flexUnitTests.elements {
import dse.DataSet;
import dse.DataSetBuilder;
import dse.MetaData;

import elements.DataGridHelpers;
import elements.DocState;
import elements.DocStateBuilder;

import flexunit.framework.Assert;

import pdt.IPVec;
import pdt.PersistentVector;

import scaling.ScalingFactors;

import units.IrradianceUnits;

public class TestDataGridHelpers {
    public var docState:DocState;
    [Before]
    public function setUp():void {
        var hdr:Object = {};
        var pts:IPVec = PersistentVector.create([
            {},
            {},
            {}]);
        var smd:Object = {
            A: new MetaData('A', 'The Letter A',
                IrradianceUnits.W__M2, IrradianceUnits.QUANTITY_OF_MEASURE,
                function(hdr:Object, pt:Object):Number {
                    return pt.A;
                }),
            B: new MetaData('B', 'The Letter B',
                IrradianceUnits.W__M2, IrradianceUnits.QUANTITY_OF_MEASURE,
                function(hdr:Object, pt:Object):Number {
                    return pt.B;
                }),
            C: new MetaData('C', 'The Letter C',
                IrradianceUnits.W__M2, IrradianceUnits.QUANTITY_OF_MEASURE,
                function(hdr:Object, pt:Object):Number {
                    return pt.A + pt.B;
                })
        };
        (smd['A'] as MetaData).defaultScale = ScalingFactors.x1e3;
        (smd['C'] as MetaData).defaultScale = ScalingFactors.PERCENTAGE;
        var hmd:Object = {};
        var dataSet:DataSet = (new DataSetBuilder())
          .withHeader(hdr)
          .withPoints(pts)
          .withSeriesMetaData(smd)
          .withHeaderMetaData(hmd)
          .build();
        docState = DocStateBuilder.createDefault(dataSet).build();
    }

    [Test]
    public function testMakeHeaderText():void {
        var expected:String = "The Letter A [W/m2] x 1000";
        var actual:String = DataGridHelpers.makeHeaderText(docState, 'A');
        trace('actual', actual);
        Assert.assertEquals(expected, actual);
    }

    [Test]
    public function testMakeHeaderText2():void {
        var expected:String = "The Letter B [W/m2]";
        var actual:String = DataGridHelpers.makeHeaderText(docState, 'B');
        trace('actual', actual);
        Assert.assertEquals(expected, actual);
    }

    [Test]
    public function testMakeHeaderText3():void {
        var expected:String = "The Letter C [W/m2] %";
        var actual:String = DataGridHelpers.makeHeaderText(docState, 'C');
        trace('actual', actual);
        Assert.assertEquals(expected, actual);
    }
}
}