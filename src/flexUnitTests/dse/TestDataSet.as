/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package flexUnitTests.dse {
import dse.DataSet;
import dse.DataSetBuilder;
import dse.MetaData;

import flexUnitTests.utils.MyAssert;

import flexunit.framework.Assert;

import pdt.IPVec;
import pdt.PersistentVector;

import units.NoUnits;

import utils.MapUtils;
import utils.Set;

public class TestDataSet {
    public var ds:DataSet;

    [Before]
    public function setUp():void {
        var pts:IPVec = PersistentVector.create([
            {A:1, B:2},
            {A:4, B:5},
            {A:7, B:8}]);
        var cf:Function = function(hdr:Object, vals:Object):Number {
            var err:Number = 0.0;
            if (vals.A < 0.0) {
                err += 0.0 - vals.A;
            }
            if (vals.B < 0.0) {
                err += 0.0 - vals.B;
            }
            return err;
        };
        var deriveC:Function = function(hdr:Object, point:Object):Number {
            return point.A + point.B;
        };
        var smd:Object = MapUtils.makeObject([
            'A', new MetaData('A', 'The Letter A',
                NoUnits.NONE,
                NoUnits.QUANTITY_OF_MEASURE,
                MetaData.makeValueGetter('A'),
                MetaData.DATA_TYPE__NUMBER,
                [['B'], ['C']],
                cf,
                Set.make(['A', 'B']),
                true,
                NoUnits.AVAILABLE_UNITS),
            'B', new MetaData('B', 'The Letter B',
                NoUnits.NONE, NoUnits.QUANTITY_OF_MEASURE,
                MetaData.makeValueGetter('B'), MetaData.DATA_TYPE__NUMBER,
                [['A'], ['C']],
                cf,
                Set.make(['A', 'B']),
                true,
                NoUnits.AVAILABLE_UNITS),
            'C', new MetaData('C', 'The Letter C',
                NoUnits.NONE, NoUnits.QUANTITY_OF_MEASURE,
                deriveC, MetaData.DATA_TYPE__NUMBER,
                [['A'], ['B']],
                cf,
                Set.make(['A', 'B']),
                true,
                NoUnits.AVAILABLE_UNITS)]);
        var hmd:Object = {};
        ds = (new DataSetBuilder())
          .withHeader({})
          .withPoints(pts)
          .withSeriesMetaData(smd)
          .withHeaderMetaData(hmd)
          .build();
    }

    [Test]
    public function testCreate():void {
        Assert.assertTrue(ds != null);
    }

    [Test]
    public function testCreateEmpty():void {
        var ds:DataSet = new DataSetBuilder().build();
        Assert.assertEquals(0,
            MapUtils.keys(ds.header).length);
        Assert.assertEquals(0, ds.points.length);
        Assert.assertEquals(0,
            MapUtils.keys(ds.seriesMetaData));
        Assert.assertEquals(0,
            MapUtils.keys(ds.headerMetaData));
    }

    [Test]
    public function testGetMetaData():void {
        var mdA:MetaData = ds.metaDataFor('A');
        Assert.assertEquals('A', mdA.name);
        MyAssert.assertThrows(function ():void {
            var mdE:MetaData = ds.metaDataFor('E');
        }, DataSet.ERROR_UNKNOWN_META_DATA_KEY);
    }

    [Test]
    public function testGetSeriesValue():void {
        var a:Number = ds.seriesValue(0, 'A');
        var c:Number = ds.seriesValue(0, 'C');
        Assert.assertEquals(1, a);
        Assert.assertEquals(3, c);
    }

    [Test]
    public function testGetSeriesMetricNames():void {
        var ns:Array = ds.seriesMetricNames;
        Assert.assertEquals('A', ns[0]);
        Assert.assertEquals('B', ns[1]);
        Assert.assertEquals('C', ns[2]);
    }

    [Test]
    public function testGetNumericMetricNames():void {
        var ns:Array = ds.numericMetricNames;
        Assert.assertTrue(ns.length > 0);
        Assert.assertEquals(3, ns.length);
    }

    [Test]
    public function testGetNumericMetricFullNames():void {
        var ns:Array = ds.numericMetricFullNames;
        Assert.assertTrue(ns.length > 0);
        Assert.assertEquals(3, ns.length);
        Assert.assertEquals('The Letter A', ns[0]);
    }

    [Test]
    public function testGetSettingSetMap():void {
        var ssm:Object = ds.settingSetMap;
        Assert.assertTrue(ssm != null);
        Assert.assertTrue(ssm.hasOwnProperty('A'));
        var ss:Array = ssm.A as Array;
        Assert.assertEquals(2, ss.length);
        Assert.assertTrue(ss[0][0] == 'B');
    }

    [Test]
    public function testGetSettingSetStringMap():void {
        var sssm:Object = ds.settingSetStringMap;
        Assert.assertTrue(sssm != null);
        Assert.assertEquals(true, sssm.hasOwnProperty('A'));
        Assert.assertTrue(sssm.A is Array);
        Assert.assertTrue(sssm.A[0] is String);
        Assert.assertEquals('The Letter B', sssm.A[0]);
    }
}
}