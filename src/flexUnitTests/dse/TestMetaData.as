/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package flexUnitTests.dse {
import dse.MetaData;

import flexunit.framework.Assert;

import units.NoUnits;

public class TestMetaData {
    public var md:MetaData;
    public var header:Object;
    [Before]
    public function setUp():void {
        header = new Object();
        md = new MetaData('A', 'The Letter A',
            NoUnits.NONE, NoUnits.QUANTITY_OF_MEASURE,
            MetaData.makeValueGetter('A'));
    }

    [Test]
    public function testMakeValueGetter():void {
        var f:Function = MetaData.makeValueGetter('A');
        Assert.assertEquals(10, f(header, {A:10}));
    }

    [Test]
    public function testMakeValueGetterWithDefault():void {
        var g:Function = MetaData.makeValueGetterWithDefault('A', 10.0);
        Assert.assertEquals(10.0, g(header, {}));
        Assert.assertEquals(2, g(header, {A:2}));
    }

    [Test]
    public function testMetaData():void {
        Assert.assertEquals(true, md.stateNames.hasOwnProperty('A'));
        Assert.assertEquals(0, md.settingSets.length);
        Assert.assertEquals(0.0, md.constraintFunc({}, {}));
    }

    [Test]
    public function testToString():void {
        Assert.assertEquals(md.fullName, md.toString());
    }
}
}