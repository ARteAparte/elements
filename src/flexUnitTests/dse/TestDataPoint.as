/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package flexUnitTests.dse {
import dse.DataPoint;
import dse.DataSet;
import dse.DataSetBuilder;
import dse.MetaData;

import elements.DocState;
import elements.DocStateBuilder;

import flexUnitTests.utils.MyAssert;

import flexunit.framework.Assert;

import pdt.IPVec;
import pdt.PersistentVector;

import plugins.DateAndTime;
import plugins.Location;
import plugins.Psych;
import plugins.Solar;
import plugins.Wind;

import utils.MapUtils;

public class TestDataPoint {
    private var dp:DataPoint;
    private var Tdb_C:Number;
    private var Twb_C:Number;
    private var p_kPa:Number;
    private var hdr:Object;
    private var pt:Object;

    [Before]
    public function setUp():void {
        hdr = MapUtils.makeObject([
                Location.ELEVATION_m, 1000.0,
                Location.LATITUDE_deg, 34.0,
                Location.LONGITUDE_deg, -104.0,
                DateAndTime.REFERENCE_YEAR, 2012,
                DateAndTime.REFERENCE_MONTH, 6,
                DateAndTime.REFERENCE_DAY, 1,
                DateAndTime.REFERENCE_HOUR, 12,
                DateAndTime.REFERENCE_MINUTE, 58,
                DateAndTime.REFERENCE_SECOND, 0,
                DateAndTime.TIME_ZONE, -6.0]);
        Tdb_C = 25.0;
        Twb_C = 20.0;
        p_kPa = 101.325;
        /* TODO: You can get some tricky and confusing errors if you forget
        to populate a data point with the correct state-fields. We need to
        do better error handling / validation for this. That is what the
        STATE_NAME Set in each plug-in is for -- we should check that we
        have those state-names in -- it's passed in with the update call so
        we should make more use of it... */
        pt = MapUtils.makeObject([
            Solar.BEAM_NORMAL_Wh__m2, 500.0,
            Solar.DIFFUSE_HORIZONTAL_Wh__m2, 50.0,
            DateAndTime.ELAPSED_TIME_hrs, 0.0,
            Wind.SPEED_m__s, 1.0,
            Wind.DIRECTION_deg, 1.0,
            Psych.DRY_BULB_TEMP_C, Tdb_C,
            Psych.WET_BULB_TEMP_C, Twb_C,
            Psych.PRESSURE_kPa, p_kPa]);
    }

    [Test]
    public function testValid():void {
        Assert.assertEquals(true,
            DataPoint.valid(hdr, pt, Solar.CONSTRAINT_FUNC));
        Assert.assertEquals(true,
            DataPoint.valid(hdr, pt, Psych.CONSTRAINT_FUNC));
    }

    [Test]
    public function testSettingError():void {
        var err:Number = DataPoint.settingError(
            hdr, pt, Psych.DERIVATION_FUNCS,
            MapUtils.makeObject([
                Psych.DRY_BULB_TEMP_C, Tdb_C + 2,
                Psych.WET_BULB_TEMP_C, Twb_C + 2,
                Psych.PRESSURE_kPa, p_kPa
            ]), Psych.SCALE_FACTORS);
        var expected:Number = 2 * MapUtils.atKeyOrAlt(
            Psych.SCALE_FACTORS, Psych.DRY_BULB_TEMP_C, 1.0) +
            2.0 * MapUtils.atKeyOrAlt(
                Psych.SCALE_FACTORS, Psych.WET_BULB_TEMP_C, 1.0);
        Assert.assertEquals(expected, err);
    }

    [Test]
    public function
    testUpdate():void {
        var justGet:Function = function(hdr:Object, theVals:Object,
                                        key:String):Number {
            return (Psych.SERIES_META_DATA[key]
                    as MetaData).getter(hdr, theVals);
        };
        var settings01:Object = MapUtils.makeObject([
            Psych.DRY_BULB_TEMP_C, 26.0]);
        var newVals:Object = DataPoint.update(
            hdr, pt, Psych.CONSTRAINT_FUNC, Psych.DERIVATION_FUNCS,
            settings01, Psych.MINS, Psych.MAXS, Psych.STATE_NAMES);
        Assert.assertEquals(26.0, newVals[Psych.DRY_BULB_TEMP_C]);
        Assert.assertEquals(Twb_C, newVals[Psych.WET_BULB_TEMP_C]);
        Assert.assertEquals(p_kPa, newVals[Psych.PRESSURE_kPa]);
        //ObjectUtils.traceObject(newVals, 'newVals');
        Assert.assertEquals(26.0,
            justGet(hdr, newVals, Psych.DRY_BULB_TEMP_C));
        Assert.assertEquals(Twb_C,
            justGet(hdr, newVals, Psych.WET_BULB_TEMP_C));
        Assert.assertEquals(p_kPa,
            justGet(hdr, newVals, Psych.PRESSURE_kPa));
        Assert.assertEquals(Psych.relativeHumidity(26.0, Twb_C, p_kPa),
            justGet(hdr, newVals, Psych.RELATIVE_HUMIDITY));
        Assert.assertEquals(Psych.specVol_m3__kg(26.0, Twb_C, p_kPa),
            justGet(hdr, newVals, Psych.SPECIFIC_VOLUME_m3__kg));
        Assert.assertEquals(Psych.density_kg__m3(26.0, Twb_C, p_kPa),
            justGet(hdr, newVals, Psych.DENSITY_kg__m3));
        Assert.assertEquals(Psych.dewPointTemp_C(26.0, Twb_C, p_kPa),
            justGet(hdr, newVals, Psych.DEW_POINT_TEMP_C));
        Assert.assertEquals(Psych.enthalpy_kJ__kg(26.0, Twb_C, p_kPa),
            justGet(hdr, newVals, Psych.ENTHALPY_kJ__kg));
        Assert.assertEquals(Psych.humidityRatio(26.0, Twb_C, p_kPa),
            justGet(hdr, newVals, Psych.HUMIDITY_RATIO));
//        Assert.assertEquals(Psych.degreeOfSaturation(26.0, Twb_C, p_kPa),
//            justGet(hdr, newVals, Psych.DEGREE_OF_SATURATION));
        Assert.assertTrue(
            newVals.hasOwnProperty(Solar.BEAM_NORMAL_Wh__m2));
        Assert.assertTrue(
            newVals.hasOwnProperty(Solar.DIFFUSE_HORIZONTAL_Wh__m2));
        Assert.assertTrue(
            newVals.hasOwnProperty(DateAndTime.ELAPSED_TIME_hrs));
        Assert.assertTrue(
            newVals.hasOwnProperty(Psych.DRY_BULB_TEMP_C));
        Assert.assertTrue(
            newVals.hasOwnProperty(Psych.WET_BULB_TEMP_C));
        Assert.assertTrue(
            newVals.hasOwnProperty(Psych.PRESSURE_kPa));
    }

    [Test]
    public function
    testUpdate02():void {
        var justGet:Function = function(hdr:Object, theVals:Object,
                                        key:String):Number {
            return (Psych.SERIES_META_DATA[key]
                    as MetaData).getter(hdr, theVals);
        };
        var settings01:Object = MapUtils.makeObject([
            Psych.WET_BULB_TEMP_C, 26.0]);
        var newVals:Object = DataPoint.update(
            hdr, pt, Psych.CONSTRAINT_FUNC, Psych.DERIVATION_FUNCS,
            settings01, Psych.MINS, Psych.MAXS, Psych.STATE_NAMES);
        //ObjectUtils.traceObject(newVals, 'newVals');
        MyAssert.almostEquals(25.0, newVals[Psych.WET_BULB_TEMP_C], 1e-3);
        Assert.assertTrue(
            newVals.hasOwnProperty(Solar.BEAM_NORMAL_Wh__m2));
        Assert.assertTrue(
            newVals.hasOwnProperty(Solar.DIFFUSE_HORIZONTAL_Wh__m2));
        Assert.assertTrue(
            newVals.hasOwnProperty(DateAndTime.ELAPSED_TIME_hrs));
        Assert.assertTrue(
            newVals.hasOwnProperty(Psych.DRY_BULB_TEMP_C));
        Assert.assertTrue(
            newVals.hasOwnProperty(Psych.WET_BULB_TEMP_C));
        Assert.assertTrue(
            newVals.hasOwnProperty(Psych.PRESSURE_kPa));
    }

    [Test]
    public function
    testUpdate03():void {
        var justGet:Function = function(hdr:Object, theVals:Object,
                                        key:String):Number {
            return (Psych.SERIES_META_DATA[key]
                    as MetaData).getter(hdr, theVals);
        };
        var settings01:Object = MapUtils.makeObject([
            Psych.PRESSURE_kPa, p_kPa,
            Psych.DRY_BULB_TEMP_C, 30.0,
            Psych.RELATIVE_HUMIDITY, 0.5]);
        var newVals:Object = DataPoint.update(
            hdr, pt, Psych.CONSTRAINT_FUNC, Psych.DERIVATION_FUNCS,
            settings01, Psych.MINS, Psych.MAXS, Psych.STATE_NAMES);
        //ObjectUtils.traceObject(newVals, 'newVals');
        MyAssert.almostEquals(0.5,
            justGet(hdr, newVals, Psych.RELATIVE_HUMIDITY), 1e-3);
        MyAssert.almostEquals(30.0, newVals[Psych.DRY_BULB_TEMP_C], 1e-3);
        Assert.assertTrue(
            newVals.hasOwnProperty(Solar.BEAM_NORMAL_Wh__m2));
        Assert.assertTrue(
            newVals.hasOwnProperty(Solar.DIFFUSE_HORIZONTAL_Wh__m2));
        Assert.assertTrue(
            newVals.hasOwnProperty(DateAndTime.ELAPSED_TIME_hrs));
        Assert.assertTrue(
            newVals.hasOwnProperty(Psych.DRY_BULB_TEMP_C));
        Assert.assertTrue(
            newVals.hasOwnProperty(Psych.WET_BULB_TEMP_C));
        Assert.assertTrue(
            newVals.hasOwnProperty(Psych.PRESSURE_kPa));
    }

    [Test]
    public function
    testUpdate04():void {
        var justGet:Function = function(hdr:Object, theVals:Object,
                                        key:String):Number {
            return (Psych.SERIES_META_DATA[key]
                    as MetaData).getter(hdr, theVals);
        };
        var settings01:Object = MapUtils.makeObject([
            Psych.PRESSURE_kPa, p_kPa,
            Psych.DEW_POINT_TEMP_C, 26.0,
            Psych.RELATIVE_HUMIDITY, 0.5]);
        var newVals:Object = DataPoint.update(
            hdr, pt, Psych.CONSTRAINT_FUNC, Psych.DERIVATION_FUNCS,
            settings01, Psych.MINS, Psych.MAXS, Psych.STATE_NAMES);
        //ObjectUtils.traceObject(newVals, 'newVals');
        var RH:Number = justGet(hdr, newVals, Psych.RELATIVE_HUMIDITY);
        var Tdew:Number = justGet(hdr, newVals, Psych.DEW_POINT_TEMP_C);
        //trace('RH', RH);
        //trace('Tdew', Tdew);
        MyAssert.almostEquals(0.5, RH, 1e-2);
        MyAssert.almostEquals(26.0, Tdew, 1e-1);
        Assert.assertEquals(p_kPa, newVals[Psych.PRESSURE_kPa]);
        Assert.assertTrue(
            newVals.hasOwnProperty(Solar.BEAM_NORMAL_Wh__m2));
        Assert.assertTrue(
            newVals.hasOwnProperty(Solar.DIFFUSE_HORIZONTAL_Wh__m2));
        Assert.assertTrue(
            newVals.hasOwnProperty(DateAndTime.ELAPSED_TIME_hrs));
        Assert.assertTrue(
            newVals.hasOwnProperty(Psych.DRY_BULB_TEMP_C));
        Assert.assertTrue(
            newVals.hasOwnProperty(Psych.WET_BULB_TEMP_C));
        Assert.assertTrue(
            newVals.hasOwnProperty(Psych.PRESSURE_kPa));
    }

    [Test]
    public function
    testUpdate05():void {
        var justGet:Function = function(hdr:Object, theVals:Object,
                                        key:String):Number {
            //trace('getting ', key);
            //trace('has key?', Solar.SERIES_META_DATA.hasOwnProperty(key));
            var sm:MetaData = Solar.SERIES_META_DATA[key] as MetaData;
            //trace('sm', sm);
            return sm.getter(hdr, theVals);
        };
        var settings01:Object = MapUtils.makeObject([
            DateAndTime.ELAPSED_TIME_hrs, 0.0,
            Solar.GLOBAL_HORIZ_Wh__m2, 500.0,
            Solar.BEAM_NORMAL_Wh__m2, 0.0]);
        var newVals:Object = DataPoint.update(
            hdr, pt, Solar.CONSTRAINT_FUNC, Solar.DERIVATION_FUNCS,
            settings01, Solar.MINS, Solar.MAXS, Solar.STATE_NAMES);
        Assert.assertTrue(Math.abs(
            500.0 - justGet(hdr, newVals, Solar.DIFFUSE_HORIZONTAL_Wh__m2))
            < 1e-2);
        Assert.assertTrue(
            newVals.hasOwnProperty(Solar.BEAM_NORMAL_Wh__m2));
        Assert.assertTrue(
            newVals.hasOwnProperty(Solar.DIFFUSE_HORIZONTAL_Wh__m2));
        Assert.assertTrue(
            newVals.hasOwnProperty(DateAndTime.ELAPSED_TIME_hrs));
        Assert.assertTrue(
            newVals.hasOwnProperty(Psych.DRY_BULB_TEMP_C));
        Assert.assertTrue(
            newVals.hasOwnProperty(Psych.WET_BULB_TEMP_C));
        Assert.assertTrue(
            newVals.hasOwnProperty(Psych.PRESSURE_kPa));
    }

    [Test]
    public function
    testUpdate06():void {
        var settings01:Object = MapUtils.makeObject([
            DateAndTime.ELAPSED_TIME_hrs, 0.0,
            Solar.GLOBAL_HORIZ_Wh__m2, 0.0,
            Solar.DIFFUSE_HORIZONTAL_Wh__m2, 0.0]);
        var newVals:Object = DataPoint.update(
            hdr, pt, Solar.CONSTRAINT_FUNC, Solar.DERIVATION_FUNCS,
            settings01, Solar.MINS, Solar.MAXS, Solar.STATE_NAMES);
        var _get:Function = DataPoint.makeGetter(
            Solar.SERIES_META_DATA, newVals, hdr);
        Assert.assertTrue(Math.abs(
            0.0 - _get(Solar.BEAM_NORMAL_Wh__m2)) < 1e-2);
        Assert.assertTrue(Math.abs(
            0.0 - _get(Solar.GLOBAL_HORIZ_Wh__m2)) < 1e-2);
        Assert.assertTrue(Math.abs(
            0.0 - _get(Solar.DIFFUSE_HORIZONTAL_Wh__m2)) < 1e-2);
        Assert.assertTrue(
            newVals.hasOwnProperty(Solar.BEAM_NORMAL_Wh__m2));
        Assert.assertTrue(
            newVals.hasOwnProperty(Solar.DIFFUSE_HORIZONTAL_Wh__m2));
        Assert.assertTrue(
            newVals.hasOwnProperty(DateAndTime.ELAPSED_TIME_hrs));
        Assert.assertTrue(
            newVals.hasOwnProperty(Psych.DRY_BULB_TEMP_C));
        Assert.assertTrue(
            newVals.hasOwnProperty(Psych.WET_BULB_TEMP_C));
        Assert.assertTrue(
            newVals.hasOwnProperty(Psych.PRESSURE_kPa));
    }

    [Test]
    public function
    testUpdate06a():void {
        var settings01:Object = MapUtils.makeObject([
            DateAndTime.ELAPSED_TIME_hrs, 0.0,
            Solar.BEAM_NORMAL_Wh__m2, 500.0,
            Solar.DIFFUSE_HORIZONTAL_Wh__m2, 100.0]);
        var newVals:Object = DataPoint.update(
            hdr, pt, Solar.CONSTRAINT_FUNC, Solar.DERIVATION_FUNCS,
            settings01, Solar.MINS, Solar.MAXS, Solar.STATE_NAMES);
        var _get:Function = DataPoint.makeGetter(
            Solar.SERIES_META_DATA, newVals, hdr);
        Assert.assertTrue(Math.abs(
            500.0 - _get(Solar.BEAM_NORMAL_Wh__m2)) < 1e-2);
        Assert.assertTrue(Math.abs(
            100.0 - _get(Solar.DIFFUSE_HORIZONTAL_Wh__m2)) < 1e-2);
        Assert.assertTrue(
            newVals.hasOwnProperty(Solar.BEAM_NORMAL_Wh__m2));
        Assert.assertTrue(
            newVals.hasOwnProperty(Solar.DIFFUSE_HORIZONTAL_Wh__m2));
        Assert.assertTrue(
            newVals.hasOwnProperty(DateAndTime.ELAPSED_TIME_hrs));
        Assert.assertTrue(
            newVals.hasOwnProperty(Psych.DRY_BULB_TEMP_C));
        Assert.assertTrue(
            newVals.hasOwnProperty(Psych.WET_BULB_TEMP_C));
        Assert.assertTrue(
            newVals.hasOwnProperty(Psych.PRESSURE_kPa));
    }

    [Test]
    public function
    testUpdate06b():void {
        var settings01:Object = MapUtils.makeObject([
            DateAndTime.ELAPSED_TIME_hrs, 0.0,
            Solar.BEAM_NORMAL_Wh__m2, 500.0,
            Solar.DIFFUSE_HORIZONTAL_Wh__m2, 100.0]);
        var newVals:Object = DataPoint.update(
            hdr, pt, Solar.CONSTRAINT_FUNC, Solar.DERIVATION_FUNCS,
            settings01, Solar.MINS, Solar.MAXS, Solar.STATE_NAMES);
        var _get:Function = DataPoint.makeGetter(
            Solar.SERIES_META_DATA, newVals, hdr);
        var cosz:Number = _get(Solar.HOURLY_AVERAGED_COSZ);
        var globalSolar:Number = 100.0 + cosz * 500.0;
        //trace('cosz is:', cosz);
        //trace('globalSolar is:', globalSolar);
        //trace('computed globalSolar is:', _get(Solar.GLOBAL_HORIZ_Wh__m2));
        var calculatedGlobalSolar:Number = _get(Solar.GLOBAL_HORIZ_Wh__m2);
        Assert.assertTrue(globalSolar > 100.0);
        Assert.assertTrue(calculatedGlobalSolar > 100.0);
        Assert.assertTrue(Math.abs(
          globalSolar - _get(Solar.GLOBAL_HORIZ_Wh__m2)) < 1e-2);
    }

    [Test]
    public function testUpdate07():void {
        var settings:Object = MapUtils.makeObject([
            Psych.DRY_BULB_TEMP_C, Tdb_C + 10,
            Psych.WET_BULB_TEMP_C, Twb_C - 1,
            Psych.PRESSURE_kPa, p_kPa - 1]);
        var updatedPoint:Object = DataPoint.update(hdr, pt,
            Psych.CONSTRAINT_FUNC, Psych.DERIVATION_FUNCS,
            settings, Psych.MINS, Psych.MAXS, Psych.STATE_NAMES);
        Assert.assertTrue(
            updatedPoint.hasOwnProperty(DateAndTime.ELAPSED_TIME_hrs));
        Assert.assertTrue(
            updatedPoint.hasOwnProperty(Solar.BEAM_NORMAL_Wh__m2));
        Assert.assertTrue(
            updatedPoint.hasOwnProperty(Solar.DIFFUSE_HORIZONTAL_Wh__m2));
        Assert.assertEquals(Tdb_C + 10, updatedPoint[Psych.DRY_BULB_TEMP_C]);
        Assert.assertEquals(Twb_C - 1, updatedPoint[Psych.WET_BULB_TEMP_C]);
        Assert.assertEquals(p_kPa - 1, updatedPoint[Psych.PRESSURE_kPa]);
    }

    [Test]
    public function testUpdate08():void {
      hdr = MapUtils.makeObject([
        Location.ELEVATION_m, 1650.0,
        Location.LATITUDE_deg, 39.83,
        Location.LONGITUDE_deg, 104.65,
        DateAndTime.REFERENCE_YEAR, 1995,
        DateAndTime.REFERENCE_MONTH, 1,
        DateAndTime.REFERENCE_DAY, 1,
        DateAndTime.REFERENCE_HOUR, 0,
        DateAndTime.REFERENCE_MINUTE, 0,
        DateAndTime.REFERENCE_SECOND, 0,
        DateAndTime.TIME_ZONE, -7.0]);
      Tdb_C = 2.2;
      Twb_C = -2.41;
      p_kPa = 82.1;
      pt = MapUtils.makeObject([
        Solar.BEAM_NORMAL_Wh__m2, 369.0,
        Solar.DIFFUSE_HORIZONTAL_Wh__m2, 62.0,
        DateAndTime.ELAPSED_TIME_hrs, 15.0,
        Wind.SPEED_m__s, 6.2,
        Wind.DIRECTION_deg, 10.0,
        Psych.DRY_BULB_TEMP_C, Tdb_C,
        Psych.WET_BULB_TEMP_C, Twb_C,
        Psych.PRESSURE_kPa, p_kPa]);
      var settings:Object = MapUtils.makeObject([
        Psych.DRY_BULB_TEMP_C, -11.1,
        Psych.RELATIVE_HUMIDITY, 0.36,
        Psych.PRESSURE_kPa, p_kPa]);
      var updatedPoint:Object = DataPoint.update(
        hdr,
        pt,
        Psych.CONSTRAINT_FUNC,
        Psych.DERIVATION_FUNCS,
        settings,
        Psych.MINS,
        Psych.MAXS,
        Psych.STATE_NAMES);
      var _get:Function = DataPoint.makeGetter(
        Psych.SERIES_META_DATA, updatedPoint, hdr);
      //MapUtils.traceObject(updatedPoint, 'updatedPoint');
      Assert.assertTrue(
        Math.abs(_get(Psych.PRESSURE_kPa) - 82.1) < 0.1);
      Assert.assertTrue(
        Math.abs(_get(Psych.DRY_BULB_TEMP_C) - -11.1) < 0.1);
      Assert.assertTrue(
        Math.abs(_get(Psych.RELATIVE_HUMIDITY) - 0.36) < 0.001);
      Assert.assertTrue(
        Math.abs(_get(Psych.WET_BULB_TEMP_C) - -13.3849) < 0.1);
    }

    [Test]
    public function testUpdateWindSpeed():void {
        //trace('testUpdateWindSpeed');
        var expected:Number = 10.0;
        var md:MetaData = Wind.SERIES_META_DATA[Wind.SPEED_m__s] as MetaData;
        var f:Function = md.getter;
        var settings:Object = MapUtils.makeObject([
            Wind.SPEED_m__s, expected]);
        var updatedPt:Object = DataPoint.update(hdr, pt,
            Wind.CONSTRAINT_FUNCTION, Wind.DERIVATION_FUNCS,
            settings, Wind.MINS, Wind.MAXS, md.stateNames);
        Assert.assertTrue(expected, f(hdr, updatedPt));
    }

    [Test]
    public function testUpdateGlobalHorizontalSolar():void {
        //trace('testUpdateGlobalHorizontalSolar');
        var expected:Number = 600.0;
        var md:MetaData =
            Solar.SERIES_META_DATA[Solar.GLOBAL_HORIZ_Wh__m2] as MetaData;
        var settings:Object = MapUtils.makeObject([
            Solar.GLOBAL_HORIZ_Wh__m2, expected,
            DateAndTime.ELAPSED_TIME_hrs, 0.0,
            Solar.BEAM_NORMAL_Wh__m2, 500.0]);
        var newPt:Object = DataPoint.update(hdr, pt,
            Solar.CONSTRAINT_FUNC, Solar.DERIVATION_FUNCS,
            settings, Solar.MINS, Solar.MAXS, md.stateNames);
        Assert.assertTrue(expected, md.getter(hdr, newPt));
    }

    [Test]
    public function testUpdateWindWithNoStateNames():void {
        //trace('testUpdateWindWithNoStateNames');
        var expected:Number = 10.0;
        var md:MetaData = Wind.SERIES_META_DATA[Wind.SPEED_m__s] as MetaData;
        var f:Function = md.getter;
        var settings:Object = MapUtils.makeObject([
            Wind.SPEED_m__s, expected]);
        var run:Function = function():void {
            var updatedPt:Object = DataPoint.update(hdr, pt,
                Wind.CONSTRAINT_FUNCTION, Wind.DERIVATION_FUNCS,
                settings, Wind.MINS, Wind.MAXS, {});
        };
        MyAssert.assertThrows(run,
            DataPoint.ERROR_STATE_NAMES_MUST_CONTAINT_GTE_ONE_VALUE);
    }

    private function makeDocState():DocState {
      var dataSet:DataSet = new DataSetBuilder()
        .withHeader({})
        .withPoints(PersistentVector.create([pt]))
        .withSeriesMetaData(
          MapUtils.mergeAll([Psych.SERIES_META_DATA, Solar.SERIES_META_DATA]))
        .withHeaderMetaData({})
        .build();
      return DocStateBuilder.createDefault(dataSet).build();
    }

    [Test]
    public function testUpdateWithSettingsAndDocState():void {
        //trace('testUpdateWithSettingsAndDocState');
        var ds:DocState = makeDocState();
        var dataField:String = Psych.DRY_BULB_TEMP_C;
        var dataValue:Number = Tdb_C + 2;
        var index:int = 0;
        var settingSetIdx:int = 2;
        var newDs:DocState = DataPoint.updateDocStateWithSettingsAndDocState(
            dataField, dataValue, ds, index, settingSetIdx);
        var pt:Object = newDs.dataSet.pointN(0);
        Assert.assertEquals(Tdb_C + 2, pt[Psych.DRY_BULB_TEMP_C]);
        Assert.assertEquals(Twb_C, pt[Psych.WET_BULB_TEMP_C]);
        Assert.assertEquals(p_kPa, pt[Psych.PRESSURE_kPa]);
        Assert.assertTrue(
            pt.hasOwnProperty(DateAndTime.ELAPSED_TIME_hrs));
        Assert.assertTrue(
            pt.hasOwnProperty(Solar.BEAM_NORMAL_Wh__m2));
        Assert.assertTrue(
            pt.hasOwnProperty(Solar.DIFFUSE_HORIZONTAL_Wh__m2));
    }

    [Test]
    public function testMakeGetter():void {
        //trace('testMakeGetter');
        var ds:DocState = makeDocState();
        var smd:Object = ds.dataSet.seriesMetaData;
        var points:IPVec = ds.dataSet.points;
        var pt:Object = points.getItemAt(0);
        var hdr:Object = ds.dataSet.header;
        var f:Function = DataPoint.makeGetter(smd, pt, hdr);
        Assert.assertEquals(Tdb_C, f(Psych.DRY_BULB_TEMP_C));
        Assert.assertEquals(Twb_C, f(Psych.WET_BULB_TEMP_C));
        Assert.assertEquals(p_kPa, f(Psych.PRESSURE_kPa));
    }

    [Test]
    public function testMakeGetter2():void {
        //trace('testMakeGetter2');
        var ds:DocState = makeDocState();
        var smd:Object = ds.dataSet.seriesMetaData;
        var points:IPVec = ds.dataSet.points;
        var pt:Object = points.getItemAt(0);
        var hdr:Object = ds.dataSet.header;
        var f:Function = DataPoint.makeGetter(smd, pt, hdr);
        Assert.assertEquals(3, f("junk", 3));
        Assert.assertEquals("hello", f("more junk", "hello"));
    }

    [Test]
    public function testUpdatePointWithSettingsAndDocStateWithNoChange():void {
        var ds:DocState = makeDocState();
        var dataField:String = Psych.DRY_BULB_TEMP_C;
        var dataValue:Number = Tdb_C;
        var index:int = 0;
        var settingSetIdx:int = 0;
        var newDs:DocState = DataPoint.updateDocStateWithSettingsAndDocState(
            dataField, dataValue, ds, index, settingSetIdx);
        var pt:Object = newDs.dataSet.pointN(0);
        Assert.assertEquals(Tdb_C, pt[Psych.DRY_BULB_TEMP_C]);
        Assert.assertEquals(Twb_C, pt[Psych.WET_BULB_TEMP_C]);
        Assert.assertEquals(p_kPa, pt[Psych.PRESSURE_kPa]);
        Assert.assertTrue(
            pt.hasOwnProperty(DateAndTime.ELAPSED_TIME_hrs));
        Assert.assertTrue(
            pt.hasOwnProperty(Solar.BEAM_NORMAL_Wh__m2));
        Assert.assertTrue(
            pt.hasOwnProperty(Solar.DIFFUSE_HORIZONTAL_Wh__m2));
    }
}
}