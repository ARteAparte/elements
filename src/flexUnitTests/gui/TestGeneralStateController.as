/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package flexUnitTests.gui {
import flexunit.framework.Assert;

import gui.AcmeManagedRef;
import gui.IManagedRef;

import pdt.IPMap;
import pdt.PersistentArrayMap;

public class TestGeneralStateController {
    public var origS:IPMap;
    public var gcs:AcmeManagedRef;
    [Before]
    public function setUp():void {
        var obj:Object = {
            A: new PersistentArrayMap(['A_1', 1, 'A_2', 2]),
            B: new PersistentArrayMap(['B_1', 1, 'B_2', 2]),
            C: new PersistentArrayMap(['C_1', 1, 'C_2',
                new PersistentArrayMap(['C_2_1', 1, 'C_2_2', 2])])};
        origS = PersistentArrayMap.createFromObj(obj);
        gcs = new AcmeManagedRef(origS);
    }

    [Test]
    public function testGeneralStateController():void {
        var test:AcmeManagedRef = new AcmeManagedRef(
            {   A: {A_1:1, A_2:2},
                B: {B_1:1, B_2:2},
                C: {C_1:1, C_2: {C_2_1:1, C_2_2:2}}});
    }

    [Test]
    public function testHandleEvent():void {
        var f:Function = function(state:IPMap, args:Object):IPMap {
            var A_1:Number = state.getIn(['A', 'A_1']) as Number;
            var B_1:Number = state.getIn(['B', 'B_1']) as Number;
            var C_1:Number = state.getIn(['C', 'C_1']) as Number;
            var num:Number = args['num'] as Number;
            var A_2:Number = A_1 + B_1 + C_1 + num;
            return state.assocIn(['A', 'A_2'], A_2);
        };
        var flag:Object = {value:false};
        var onA2Changed:Function = function(evt:String):void {
            if (evt == 'A2Changed') {
                flag.value = true;
            }
        };
        gcs.registerSubscriberFunction(onA2Changed);
        gcs.onRequest('addToA2', f, ['A2Changed']);
        gcs.handleRequest('addToA2', {num: 10});
        var s:IPMap = gcs.read() as IPMap;
        Assert.assertEquals(1,  s.getIn(['A', 'A_1']) as Number);
        Assert.assertEquals(13, s.getIn(['A', 'A_2']) as Number);
        Assert.assertEquals(2,  origS.getIn(['A', 'A_2']) as Number);
        Assert.assertEquals(1,  s.getIn(['B', 'B_1']) as Number);
        Assert.assertEquals(2,  s.getIn(['B', 'B_2']) as Number);
        Assert.assertEquals(1,  s.getIn(['C', 'C_1']) as Number);
        Assert.assertEquals(1,  s.getIn(['C', 'C_2', 'C_2_1']) as Number);
        Assert.assertEquals(2,  s.getIn(['C', 'C_2', 'C_2_2']) as Number);
        Assert.assertTrue(flag.value);
    }

    [Test]
    public function testRead():void {
        var state:IPMap = gcs.read() as IPMap;
        Assert.assertTrue(state.containsKey('A'));
    }

    [Test]
    public function testRegisterUnRegisterSubscriberFunction():void {
        var state:IPMap = new PersistentArrayMap([
            'A', 1, 'B', 2, 'C', 3]);
        var f:Function = function(state:IPMap, args:Object):IPMap {
            return state.assoc('A', args.val);
        };
        var g:Function = function(state:IPMap, args:Object):IPMap {
            return state.assoc('B', args.val);
        };
        var h:Function = function(state:IPMap, args:Object):IPMap {
            return state.assoc('C', args.val);
        };
        var sc:IManagedRef = new AcmeManagedRef(state);
        var flags:Object = {Aflag:false, Bflag:false, Cflag:false};
        var a:Function = function(e:String):void {
            if (e == 'A changed') {
                flags.Aflag = true;
            }
        };
        var b:Function = function(e:String):void {
            if (e == 'B changed') {
                flags.Bflag = true;
            }
        };
        var c:Function = function(e:String):void {
            if (e == 'C changed') {
                flags.Cflag = true;
            }
        };
        sc.registerSubscriberFunction(a);
        sc.registerSubscriberFunction(b);
        sc.registerSubscriberFunction(c);
        sc.onRequest('set A', f, ['A changed']);
        sc.onRequest('set B', g, ['B changed']);
        sc.onRequest('set C', h, ['C changed']);
        sc.handleRequest('set A', {val: 10});
        Assert.assertEquals(true, flags.Aflag);
        Assert.assertEquals(false, flags.Bflag);
        Assert.assertEquals(false, flags.Cflag);
        sc.handleRequest('set B', {val: 10});
        Assert.assertEquals(true, flags.Aflag);
        Assert.assertEquals(true, flags.Bflag);
        Assert.assertEquals(false, flags.Cflag);
        sc.handleRequest('set C', {val: 100});
        Assert.assertEquals(true, flags.Aflag);
        Assert.assertEquals(true, flags.Bflag);
        Assert.assertEquals(true, flags.Cflag);
        flags.Aflag = false;
        flags.Bflag = false;
        flags.Cflag = false;
        sc.removeSubscriberFunction(a);
        sc.removeSubscriberFunction(b);
        sc.handleRequest('set A', {val: 20});
        Assert.assertEquals(false, flags.Aflag);
        Assert.assertEquals(false, flags.Bflag);
        Assert.assertEquals(false, flags.Cflag);
        sc.handleRequest('set B', {val: 20});
        Assert.assertEquals(false, flags.Aflag);
        Assert.assertEquals(false, flags.Bflag);
        Assert.assertEquals(false, flags.Cflag);
        sc.handleRequest('set C', {val: 200});
        Assert.assertEquals(false, flags.Aflag);
        Assert.assertEquals(false, flags.Bflag);
        Assert.assertEquals(true, flags.Cflag);
    }

    [Test]
    public function testUndoRedo():void {
        var state:IPMap = new PersistentArrayMap([
            'A', 1, 'B', 2, 'C', 3]);
        var f:Function = function(state:IPMap, args:Object):IPMap {
            var A:Number = state.valAt('A') as Number;
            var B:Number = state.valAt('B') as Number;
            return state.assoc('C', A + B);
        };
        var g:Function = function(state:IPMap, args:Object):IPMap {
            return state.assoc('A', args.val);
        };
        var sc:IManagedRef = new AcmeManagedRef(state);
        sc.onRequest('C = A + B', f, ['C Changed']);
        sc.onRequest('set A', g, ['A Changed']);
        var s0:IPMap = sc.read() as IPMap;
        Assert.assertEquals(1, s0.valAt('A'));
        Assert.assertEquals(3, s0.valAt('C'));
        sc.handleRequest('set A', {val: 10});
        var s1:IPMap = sc.read() as IPMap;
        Assert.assertEquals(10, s1.valAt('A'));
        Assert.assertEquals(3, s1.valAt('C'));
        Assert.assertEquals(1, s0.valAt('A'));
        Assert.assertEquals(3, s0.valAt('C'));
        sc.handleRequest('C = A + B', {});
        var s2:IPMap = sc.read() as IPMap;
        Assert.assertEquals(10, s2.valAt('A'));
        Assert.assertEquals(12, s2.valAt('C'));
        Assert.assertEquals(10, s1.valAt('A'));
        Assert.assertEquals(3, s1.valAt('C'));
        Assert.assertEquals(1, s0.valAt('A'));
        Assert.assertEquals(3, s0.valAt('C'));
        sc.undo();
        var s3:IPMap = sc.read() as IPMap;
        Assert.assertEquals(10, s3.valAt('A'));
        Assert.assertEquals(3, s3.valAt('C'));
        Assert.assertEquals(10, s2.valAt('A'));
        Assert.assertEquals(12, s2.valAt('C'));
        Assert.assertEquals(10, s1.valAt('A'));
        Assert.assertEquals(3, s1.valAt('C'));
        Assert.assertEquals(1, s0.valAt('A'));
        Assert.assertEquals(3, s0.valAt('C'));
        sc.undo();
        var s4:IPMap = sc.read() as IPMap;
        Assert.assertEquals(1, s4.valAt('A'));
        Assert.assertEquals(3, s4.valAt('C'));
        Assert.assertEquals(10, s3.valAt('A'));
        Assert.assertEquals(3, s3.valAt('C'));
        Assert.assertEquals(10, s2.valAt('A'));
        Assert.assertEquals(12, s2.valAt('C'));
        Assert.assertEquals(10, s1.valAt('A'));
        Assert.assertEquals(3, s1.valAt('C'));
        Assert.assertEquals(1, s0.valAt('A'));
        Assert.assertEquals(3, s0.valAt('C'));
        sc.undo();
        var s5:IPMap = sc.read() as IPMap;
        Assert.assertEquals(1, s5.valAt('A'));
        Assert.assertEquals(3, s5.valAt('C'));
        Assert.assertEquals(1, s4.valAt('A'));
        Assert.assertEquals(3, s4.valAt('C'));
        Assert.assertEquals(10, s3.valAt('A'));
        Assert.assertEquals(3, s3.valAt('C'));
        Assert.assertEquals(10, s2.valAt('A'));
        Assert.assertEquals(12, s2.valAt('C'));
        Assert.assertEquals(10, s1.valAt('A'));
        Assert.assertEquals(3, s1.valAt('C'));
        Assert.assertEquals(1, s0.valAt('A'));
        Assert.assertEquals(3, s0.valAt('C'));
        sc.redo();
        var s6:IPMap = sc.read() as IPMap;
        Assert.assertEquals(10, s6.valAt('A'));
        Assert.assertEquals(3, s6.valAt('C'));
        sc.redo();
        var s7:IPMap = sc.read() as IPMap;
        Assert.assertEquals(10, s7.valAt('A'));
        Assert.assertEquals(12, s7.valAt('C'));
        sc.undo();
        var s8:IPMap = sc.read() as IPMap;
        Assert.assertEquals(10, s8.valAt('A'));
        Assert.assertEquals( 3, s8.valAt('C'));
        sc.redo();
        var s9:IPMap = sc.read() as IPMap;
        Assert.assertEquals(10, s9.valAt('A'));
        Assert.assertEquals(12, s9.valAt('C'));
    }
}
}