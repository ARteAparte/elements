/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package flexUnitTests.utils {
import flexunit.framework.Assert;

import utils.MapUtils;

public class TestMapUtils {
    [Before]
    public function setUp():void {
    }

    [Test]
    public function testKeys():void {
      var obj:Object = {a:1, b:2, c:3};
      var keys:Array = MapUtils.keys(obj).sort();
      var expd:Array = ["a", "b", "c"];
      Assert.assertEquals(3, keys.length);
      for (var idx:int=0; idx<expd.length; idx++) {
        Assert.assertEquals(expd[idx], keys[idx]);
      }
    }

    [Test]
    public function testOnlyTheseKeys():void {
        var objA:Object = {A:1, B:2, C:3};
        var objB:Object = MapUtils.onlyTheseKeys(objA, ["B", "C"]);
        Assert.assertTrue(objB.hasOwnProperty("A") == false);
        Assert.assertTrue(objB.hasOwnProperty("B"));
        Assert.assertTrue(objB.hasOwnProperty("C"));
    }

    [Test]
    public function testMergeWith():void {
        var a:Object = {A:1, B:2, C:3};
        var b:Object = {A:0, B:1, C:2};
        var c:Object = MapUtils.mergeWith(a, b,
            function(x:Number,y:Number):Number {
                return x - y;
            });
        Assert.assertEquals(3, MapUtils.keys(c).length);
        Assert.assertEquals(1, c.A);
        Assert.assertEquals(1, c.B);
        Assert.assertEquals(1, c.C);
    }

    [Test]
    public function testAssocIn():void {
        var obj:Object = {A:{B:{C:1, D:2}, E:3}, F:4};
        var newObj:Object = MapUtils.assocIn(obj,
            ['A', 'B', 'D'], 3);
        Assert.assertEquals(4, obj.F);
        Assert.assertEquals(4, newObj.F);
        Assert.assertEquals(3, obj.A.E);
        Assert.assertEquals(3, newObj.A.E);
        Assert.assertEquals(1, obj.A.B.C);
        Assert.assertEquals(1, newObj.A.B.C);
        Assert.assertEquals(3, newObj.A.B.D);
        Assert.assertEquals(2, obj.A.B.D);
    }

    [Test]
    public function testCopyObject():void {
        var obj1:Object = {A:1, B:2};
        var obj2:Object = MapUtils.copyObject(obj1);
        obj2.A = 10;
        Assert.assertEquals(1,  obj1.A);
        Assert.assertEquals(10, obj2.A);
        Assert.assertEquals(2,  obj1.B);
        Assert.assertEquals(2,  obj2.B);
    }

    [Test]
    public function testStringRepresentation():void {
        var exp:String = "{a: 10, b: 20, c: 30}";
        var act:String = MapUtils.stringRepresentation({c: 30, b: 20, a: 10});
        Assert.assertEquals(exp, act);
    }
}
}