/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package flexUnitTests.utils {
  import flexunit.framework.Assert;

  import utils.FormatUtils;

  public class TestFormatUtils {
    [Before]
    public function setUp():void {
    }

    [After]
    public function tearDown():void {
    }

    [Test]
    public function testFixStringLength():void {
      var s1:String = FormatUtils.fixStringLength(
        10, 'how long is this?', '*', FormatUtils.PAD_RIGHT);
      var s2:String = FormatUtils.fixStringLength(
        10, '---', '*', FormatUtils.PAD_RIGHT);
      var s3:String = FormatUtils.fixStringLength(
        10, '---', '*', FormatUtils.PAD_LEFT);
      Assert.assertEquals('how long i', s1);
      Assert.assertEquals('---*******', s2);
      Assert.assertEquals('*******---', s3);
    }

    [Test]
    public function testFloatDrop0():void {
      var s1:String = FormatUtils.floatDrop0(3.56, 1);
      var s2:String = FormatUtils.floatDrop0(0.56, 1);
      var s3:String = FormatUtils.floatDrop0(12.56, 1);
      var s4:String = FormatUtils.floatDrop0(12.0, 1);
      var s5:String = FormatUtils.floatDrop0(-7.0, 1);
      Assert.assertEquals('3.6', s1);
      Assert.assertEquals('0.6', s2);
      Assert.assertEquals('12.6', s3);
      Assert.assertEquals('12', s4);
      Assert.assertEquals('-7', s5);
    }

    [Test]
    public function testFormatFloat():void {
      var s1:String = FormatUtils.formatFloat(3.56, 1);
      var s2:String = FormatUtils.formatFloat(0.56, 1);
      var s3:String = FormatUtils.formatFloat(12.56, 1);
      var s4:String = FormatUtils.formatFloat(12.0, 1);
      Assert.assertEquals('3.6', s1);
      Assert.assertEquals('0.6', s2);
      Assert.assertEquals('12.6', s3);
      Assert.assertEquals('12.0', s4);
    }

    [Test]
    public function testFormatInt():void {
      var s1:String = FormatUtils.formatInt(3.56);
      var s2:String = FormatUtils.formatInt(0.56);
      var s3:String = FormatUtils.formatInt(12.56);
      var s4:String = FormatUtils.formatInt(12.0);
      Assert.assertEquals('4', s1);
      Assert.assertEquals('1', s2);
      Assert.assertEquals('13', s3);
      Assert.assertEquals('12', s4);
    }

    [Test]
    public function testNumberToFormattedString():void {
      var s1:String = FormatUtils.numberToFormattedString(3.56, 1);
      var s2:String = FormatUtils.numberToFormattedString(0.56, 1);
      var s3:String = FormatUtils.numberToFormattedString(12.56, 1);
      var s4:String = FormatUtils.numberToFormattedString(12.0, 1);
      Assert.assertEquals('3.6', s1);
      Assert.assertEquals('0.6', s2);
      Assert.assertEquals('12.6', s3);
      Assert.assertEquals('12', s4);
    }

    [Test]
    public function testPad02():void {
      var s1:String = FormatUtils.pad02('34');
      var s2:String = FormatUtils.pad02('3');
      var s3:String = FormatUtils.pad02('.');
      Assert.assertEquals('34', s1);
      Assert.assertEquals('03', s2);
      Assert.assertEquals('0.', s3);
    }

    [Test]
    public function testPadString():void {
      var s:String = FormatUtils.padString(10, 'abc', '-');
      Assert.assertEquals('abc-------', s);
    }
  }
}