/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package flexUnitTests.utils {
import flexunit.framework.Assert;

import utils.ArrayUtils;

public class TestArrayUtils {
    private var xs:Array;
    private var es:Array;

    [Before]
    public function setUp():void {
        xs = [3,4,5,6];
        es = new Array(32);
    }

    [Test]
    public function testCopyArray():void {
        var ys:Array = ArrayUtils.copyArray(xs);
        Assert.assertEquals(4, ys.length);
        Assert.assertEquals(3, ys[0]);
        Assert.assertEquals(4, ys[1]);
        Assert.assertEquals(5, ys[2]);
        Assert.assertEquals(6, ys[3]);
    }

    [Test]
    public function testCopyArray2():void {
        var ys:Array = ArrayUtils.copyArray(es);
        Assert.assertEquals(32, es.length);
        Assert.assertEquals(32, ys.length);
    }

    [Test]
    public function testCopyIntoArray():void {
        var ys:Array = new Array(xs.length + 2);
        ArrayUtils.copyIntoArray(xs, 0, ys, 2, xs.length);
        ys[0] = 1;
        ys[1] = 2;
        Assert.assertEquals(6, ys.length);
        Assert.assertEquals(1, ys[0]);
        Assert.assertEquals(2, ys[1]);
        Assert.assertEquals(3, ys[2]);
        Assert.assertEquals(4, ys[3]);
        Assert.assertEquals(5, ys[4]);
        Assert.assertEquals(6, ys[5]);
    }

    [Test]
    public function testCopyIntoArrayXsTooShort():void {
        var ys:Array = new Array(10);
        ArrayUtils.copyIntoArray(xs, 0, ys, 0, ys.length);
        Assert.assertEquals(10, ys.length);
        Assert.assertEquals(3, ys[0]);
        Assert.assertEquals(4, ys[1]);
        Assert.assertEquals(5, ys[2]);
        Assert.assertEquals(6, ys[3]);
        Assert.assertEquals(null, ys[4]);
        Assert.assertEquals(null, ys[5]);
        Assert.assertEquals(null, ys[6]);
        Assert.assertEquals(null, ys[7]);
        Assert.assertEquals(null, ys[8]);
        Assert.assertEquals(null, ys[9]);
    }

    [Test]
    public function testCopyIntoArrayYsTooShort():void {
        var ys:Array = new Array(2);
        ArrayUtils.copyIntoArray(xs, 0, ys, 0, xs.length);
        Assert.assertEquals(2, ys.length);
        Assert.assertEquals(3, ys[0]);
        Assert.assertEquals(4, ys[1]);
    }

    [Test]
    public function testEquals():void {
        var xs:Array = [1,2,3,4];
        var ys:Array = [1,2,3,4];
        Assert.assertEquals(true, ArrayUtils.equals(xs, ys));
        var zs:Array = [1,2,3,4,5];
        Assert.assertEquals(false, ArrayUtils.equals(xs, zs));
    }
    
    [Test]
    public function testFirstIndexOf():void {
      var xs:Array = [10,20,30,40];
      var x:int = 30;
      Assert.assertEquals(2, ArrayUtils.firstIndexOf(x, xs));
      var ys:Array = [1,2,3,4];
      Assert.assertEquals(-1, ArrayUtils.firstIndexOf(x, ys));
    }
}
}