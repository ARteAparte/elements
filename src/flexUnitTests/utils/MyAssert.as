/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package flexUnitTests.utils {
import flexunit.framework.Assert;

public class MyAssert extends Assert {
    public static function
    almostEquals(x:Number, y:Number, tol:Number=1e-6):void {
        if (!(Math.abs(x - y) < tol)) {
            Assert.fail('expected | x - y | < tol for x = ' +
                x.toString() + ' and y = ' + y.toString() + ' but failed');
        }
    }

    public static function assertThrows(f:Function, err:String):void {
        try {
            f();
            Assert.fail("assertThrows: did not throw an error");
        } catch (e:Error) {
            Assert.assertEquals(err, e.message);
        }
    }
}
}