/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package flexUnitTests.utils {
import flexunit.framework.Assert;

import utils.AdvMath;

public class TestAdvMath {
  public var f:Function;
  public var g:Function;
  public var h:Function;
  public var inc:Function;
  public var dec:Function;
  [Before]
  public function setUp():void {
    f = function(x:Number):Number {
      if (x < 2.0) {
        return NaN;
      }
      return x;
    };
    inc = function(x:Number,dx:Number):Number {
      return x + dx;
    };
    g = function(x:Number):Number {
      if (x < 1.5) {
        return NaN;
      } else if (x > 4.0) {
        return NaN;
      }
      return x * x - 3.0;
    };
    h = function(x:Number):Number {
      return x;
    };
    dec = function(x:Number,dx:Number):Number {
      return x - dx;
    }
  }
  [After]
  public function tearDown():void {
  }
  [Test]
  public function test_find_good_bound01():void {
    var out:Vector.<Number> = AdvMath.find_good_bound(f, 0.0, 0.1, inc,
                                                      100, 0.01);
    Assert.assertTrue(Math.abs(out[0] - 2.0) < 0.02);
  }
  [Test]
  public function test_find_good_bound02():void {
    var out:Vector.<Number> = AdvMath.find_good_bound(g, 10.0, 0.1, dec,
                                                      100, 0.01);
    Assert.assertTrue(Math.abs(out[0] - 4.0) < 0.02);
  }
  [Test]
  public function test_find_good_bound03():void {
    var out:Vector.<Number> = AdvMath.find_good_bound(g, -3.0, 1.0, inc,
                                                      100, 0.01);
    Assert.assertTrue(Math.abs(out[0] - 1.5) < 0.02);
  }
  [Test]
  public function test_find_valid_bounds01():void {
    var out:Vector.<Number> = AdvMath.find_valid_bounds(g, 0.0, 10.0, 0.01, 100);
    Assert.assertTrue(Math.abs(out[0] - 1.5) < 0.02);
    Assert.assertTrue(Math.abs(out[2] - 4.0) < 0.02);
  }
  [Test]
  public function test_find_valid_bounds02():void {
    var out:Vector.<Number> = AdvMath.find_valid_bounds(h, 0.0, 10.0, 0.01, 99);
    Assert.assertTrue(Math.abs(out[0] - 0.0) < 0.02);
    Assert.assertTrue(Math.abs(out[2] - 10.0) < 0.02);
  }
  [Test]
  public function test_wrapToPlusMinusN():void
  {
    Assert.assertTrue(8.5 == AdvMath.wrapToPlusMinusN(368.5, 360));
    Assert.assertTrue(-8.5 == AdvMath.wrapToPlusMinusN(-368.5, 360.0));
    Assert.assertTrue(8.5 == AdvMath.wrapToPlusMinusN(8.5, 360.0));
    Assert.assertTrue(-8.5 == AdvMath.wrapToPlusMinusN(-8.5, 360.0));
  }
}
}