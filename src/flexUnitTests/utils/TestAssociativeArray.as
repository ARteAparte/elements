/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package flexUnitTests.utils {
import flexunit.framework.Assert;

import utils.AssociativeArray;

public class TestAssociativeArray {
  private var _aa:AssociativeArray;
  private var _bb:AssociativeArray;
  [Before]
  public function setUp():void {
    _aa = new AssociativeArray(["a","b","c","d"], [1,2,3,4]);
    _bb = new AssociativeArray(["a","b","b","d"], [1,2,3,4]);
  }
  [After]
  public function tearDown():void {
  }
  [Test]
  public function testFromItems():void {
    var aa:AssociativeArray = AssociativeArray.fromItems([1,2,2,3,4,5],
      function (x:int):int {
        return x;
      },
      function (x:int):int {
        return x * 10;
      });
    var expd:Array = [1,2,3,4,5];
    var keys:Array = aa.keys();
    Assert.assertEquals(expd.length, keys.length);
    for (var idx:int = 0; idx<expd.length; idx++) {
      Assert.assertEquals(expd[idx], keys[idx]);
    }
    Assert.assertEquals(2, aa.getValByKey(2).length);
    Assert.assertEquals(20, aa.getValByKey(2)[0]);
    Assert.assertEquals(20, aa.getValByKey(2)[1]);
  }
  [Test]
  public function testGetValByKey():void {
    Assert.assertEquals(1, _aa.getValByKey("a"));
    Assert.assertEquals(3, _bb.getValByKey("b"));
  }
  [Test]
  public function testHasKey():void {
    Assert.assertTrue(_aa.hasKey("d"));
  }
  [Test]
  public function testKeys():void {
    var keys:Array = _aa.keys();
    var expd:Array = ["a", "b", "c", "d"];
    Assert.assertEquals(expd.length, keys.length);
    for (var idx:int=0; idx<keys.length; idx++) {
      Assert.assertEquals(expd[idx], keys[idx]);
    }
  }
  [Test]
  public function testNumItems():void {
    Assert.assertEquals(4, _aa.numItems());
  }
  [Test]
  public function testSetKeyVal():void {
    _aa.setKeyVal("e", 5);
    _aa.setKeyVal("a", 10);
    Assert.assertEquals([5], _aa.getValByKey("e"));
    Assert.assertEquals([10], _aa.getValByKey("a"));
  }
  [Test]
  public function testToString():void {
    Assert.assertEquals("{a:1, b:3, d:4}", _bb.toString());
  }
  [Test]
  public function testVals():void {
    var expd:Array = [1,2,3,4];
    var vals:Array = _aa.vals();
    Assert.assertEquals(4, vals.length);
    for (var idx:int=0; idx<vals.length; idx++) {
      Assert.assertEquals(expd[idx], vals[idx]);
    }
  }
  [Test]
  public function testVals2():void {
    var expd:Array = [1,3,4];
    var vals:Array = _bb.vals();
    Assert.assertEquals(3, vals.length);
    for (var idx:int=0; idx<vals.length; idx++) {
      Assert.assertEquals(expd[idx], vals[idx]);
    }
  }
}
}