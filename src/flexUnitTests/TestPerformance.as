/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package flexUnitTests {
import flexunit.framework.Assert;

import pdt.IPVec;
import pdt.PersistentVector;

import utils.ArrayUtils;

public class TestPerformance {
    public var N:int;
    public var XS:Array;

    [Before]
    public function setUp():void {
        N = 100000;
        XS = new Array(N);
        var i:int;
        for (i=0; i<N; i++) {
            XS[i] = i;
        }
    }

    [Test]
    public function testInstrumentedPVec():void {
        var pvs:IPVec = PersistentVector.create(XS);
    }

    [Test]
    public function testCheckVec():void {
        trace('creating multiple transients -> persistent vectors');
        var pvs:IPVec;
        var i:int;
        var x:int;
        var start:Date = new Date();
        for (i=0; i<10; i++) {
            pvs = PersistentVector.create(XS);
        }
        var end:Date = new Date();
        trace('time taken for 10 persistent vectors (ms) = ',
            end.time - start.time);
        trace('-------------');
        trace('lookup time trials...');
        start = new Date();
        for (i=0; i<N; i+=1000) {
            x = XS[i];
        }
        end = new Date();
        trace('time to address array (ms)', end.time - start.time);
        start = new Date();
        for (i=0; i<N; i+=1000) {
            x = pvs.nth(i) as int;
        }
        end = new Date();
        trace('time to address persistent vector (ms)', end.time - start.time);
        var ys:Array;
        start = new Date();
        ys = pvs.toArray();
        end = new Date();
        trace('time to convert persistent vector to array (ms)',
            end.time - start.time);
        for (i=0; i<N; i++) {
            Assert.assertEquals(i, ys[i]);
        }
    }

    [Test]
    public function testPerformance():void {
        // in a separate test, it was discovered that
        // for each (x:Object in xs) { ... x ... }
        // is faster than
        // for (var i:int=0; i<xs.length; i++) { ... xs[i] ... }
        // I suspect this is due to the lookup time for xs[i]???
        var i:int;
        var start:Date = new Date();
        var xs:Array = new Array(N);
        for (i=0; i<N; i++) {
            xs[i] = i;
        }
        var end:Date = new Date();
        trace('create an array via pre-allocation');
        trace('elapsed time (ms):', end.time - start.time);
        start = new Date();
        var ys:Array = new Array();
        for (i=0; i<N; i++) {
            ys.push(i);
        }
        end = new Date();
        trace('create an array via push');
        trace('elapsed time (ms):', end.time - start.time);
        start = new Date();
        var zs:Vector.<int> = new Vector.<int>(N);
        for (i=0; i<N; i++) {
            zs[i] = i;
        }
        end = new Date();
        trace('create a vector via pre-allocation');
        trace('elapsed time (ms):', end.time - start.time);
        start = new Date();
        zs = new Vector.<int>();
        for (i=0; i<N; i++) {
            zs.push(i);
        }
        end = new Date();
        trace('create a vector via push');
        trace('elapsed time (ms):', end.time - start.time);
        start = new Date();
        var pvs:IPVec = PersistentVector.create(xs);
        end = new Date();
        trace('create a persistent vector from array');
        trace('elapsed time (ms):', end.time - start.time);
        var xs2:Array;
        start = new Date();
        xs2 = ArrayUtils.copyArray(xs);
        end = new Date();
        trace('copy an array using ArrayUtils.copyArray');
        trace('elapsed time (ms):', end.time - start.time);
        start = new Date();
        var xs3:Array = new Array(N);
        ArrayUtils.copyIntoArray(xs, 0, xs3, 0, N);
        end = new Date();
        trace('copy an array using ArrayUtils.copyIntoArray');
        trace('elapsed time (ms):', end.time - start.time);
    }
}
}