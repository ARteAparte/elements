/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package flexUnitTests.pdt {
import flash.events.Event;

import flexUnitTests.utils.MyAssert;

import flexunit.framework.Assert;

import pdt.IPVec;
import pdt.Node;
import pdt.PersistentList;
import pdt.PersistentVector;
import pdt.TransientVector;

public class TestPersistentVector {
    public var pv:PersistentVector;

    [Before]
    public function setUp():void {
        pv = PersistentVector.create([10,20,30]);
    }

    [Test]
    public function testAddEventListener():void {
        pv.addEventListener(Event.CHANGE, function(e:Event):void {});
    }

    [Test]
    public function testAddItem():void {
        MyAssert.assertThrows(function():void {
            pv.addItem(100);
        }, PersistentList.ERROR_MUTATION_ATTEMPTED);
    }

    [Test]
    public function testAddItemAt():void {
        MyAssert.assertThrows(function():void {
            pv.addItemAt(100, 0);
        }, PersistentList.ERROR_MUTATION_ATTEMPTED);
    }

    [Test]
    public function testAssocN():void {
        var pv2:PersistentVector = pv.assocN(0, "Hello") as PersistentVector;
        Assert.assertEquals(10, pv.nth(0));
        Assert.assertEquals("Hello", pv2.nth(0));
    }

    [Test]
    public function testCons():void {
        var pv2:PersistentVector = pv.cons(100) as PersistentVector;
        Assert.assertEquals(100, pv2.nth(3));
    }

    [Test]
    public function testContains():void {
        Assert.assertTrue(pv.containsKey(10));
        Assert.assertEquals(false, pv.containsKey("bob"));
    }

    [Test]
    public function testGet_count():void {
        Assert.assertEquals(3, pv.count);
    }

    [Test]
    public function testDispatchEvent():void {
        pv.dispatchEvent(new Event(Event.CHANGE));
    }

    [Test]
    public function testEmpty():void {
        var e:PersistentVector = pv.empty() as PersistentVector;
        Assert.assertEquals(0, e.count);
    }

    [Test]
    public function testEquiv():void {
        var pv2:PersistentVector = PersistentVector.create([10,20,30]);
        Assert.assertTrue(pv.equiv(pv));
        Assert.assertTrue(pv2.equiv(pv));
        Assert.assertTrue(pv.equiv(pv2));
    }

    [Test]
    public function testGetItemAt():void {
        Assert.assertEquals(10, pv.getItemAt(0));
        Assert.assertEquals(20, pv.getItemAt(1));
        Assert.assertEquals(30, pv.getItemAt(2));
        MyAssert.assertThrows(function():void {
            pv.getItemAt(3);
        }, TransientVector.ERROR_INDEX_OUT_OF_BOUNDS);
    }

    [Test]
    public function testGetItemIndex():void {
        Assert.assertEquals(0, pv.getItemIndex(10));
        Assert.assertEquals(1, pv.getItemIndex(20));
        Assert.assertEquals(2, pv.getItemIndex(30));
        Assert.assertEquals(-1, pv.getItemIndex(100));
    }

    [Test]
    public function testHasEventListener():void {
        Assert.assertEquals(false, pv.hasEventListener(Event.CHANGE));
    }

    [Test]
    public function testItemUpdated():void {
        pv.itemUpdated(10);
    }

    [Test]
    public function testGet_length():void {
        Assert.assertEquals(3, pv.length);
    }

    [Test]
    public function testNth():void {
        Assert.assertEquals(10, pv.nth(0));
        Assert.assertEquals(20, pv.nth(1));
        Assert.assertEquals(30, pv.nth(2));
    }

    [Test]
    public function testPeek():void {
        Assert.assertEquals(30, pv.peek());
    }

    [Test]
    public function testPersistentVector():void {
        var pv:IPVec = PersistentVector.create([]);
        Assert.assertEquals(0, pv.count);
    }

    [Test]
    public function testPersistentVector2():void {
        var pv:IPVec = PersistentVector.create([1, 2, 3]);
        Assert.assertEquals(3, pv.count);
    }

    [Test]
    public function testPop():void {
        var pv2:PersistentVector = pv.pop() as PersistentVector;
        Assert.assertEquals(2, pv2.length);
        Assert.assertEquals(3, pv.length);
        Assert.assertEquals(10, pv2.nth(0));
        Assert.assertEquals(20, pv2.nth(1));
    }

    [Test]
    public function testRemoveAll():void {
        MyAssert.assertThrows(function():void {
            pv.removeAll();
        }, PersistentList.ERROR_MUTATION_ATTEMPTED);
    }

    [Test]
    public function testRemoveEventListener():void {
        pv.removeEventListener(Event.CHANGE, function(e:Event):void {});
    }

    [Test]
    public function testRemoveItemAt():void {
        MyAssert.assertThrows(function():void {
            pv.removeItemAt(0);
        }, PersistentList.ERROR_MUTATION_ATTEMPTED);
    }

    [Test]
    public function testRseq():void {
        Assert.assertEquals(null, pv.rseq());
    }

    [Test]
    public function testSeq():void {
        Assert.assertEquals(10, pv.seq().first);
        Assert.assertEquals(20, pv.seq().next().first);
        Assert.assertEquals(30, pv.seq().next().next().first);
        Assert.assertEquals(null, pv.seq().next().next().next());
    }

    [Test]
    public function testSetItemAt():void {
        MyAssert.assertThrows(function():void {
            pv.setItemAt("hello", 0);
        }, PersistentList.ERROR_MUTATION_ATTEMPTED);
    }

    [Test]
    public function testToArray():void {
        var xs:Array = pv.toArray();
        Assert.assertEquals(10, xs[0]);
        Assert.assertEquals(20, xs[1]);
        Assert.assertEquals(30, xs[2]);
    }

    [Test]
    public function testWillTrigger():void {
        Assert.assertEquals(false, pv.willTrigger(Event.CHANGE));
    }

    [Test]
    public function testAsTransient():void {
        var tv:TransientVector = pv.asTransient();
        Assert.assertEquals(3, tv.count);
        Assert.assertEquals(10, tv.nth(0));
        Assert.assertEquals(20, tv.nth(1));
        Assert.assertEquals(30, tv.nth(2));
    }

    [Test]
    public function testToString():void {
        var exp:String = "PersistentVector.create([10, 20, 30])";
        var act:String = pv.toString();
        Assert.assertEquals(exp, act);
    }

    [Test]
    public function testAdd10kElements():void {
        var NUM_ELEMS:int = 10000;
        var xs:Array = new Array(NUM_ELEMS);
        for (var i:int=0; i < xs.length; i++) {
            xs[i] = {A:i, B:i*10, C:i*100};
        }
        var pv:PersistentVector = PersistentVector.create(xs);
    }

    [Test]
    public function testDetailedAttrs():void {
        var pv1:PersistentVector = PersistentVector.create([1, 2, 3]);
        Assert.assertEquals(3, pv1._cnt);
        Assert.assertEquals(5, pv1._shift);
        Assert.assertEquals(1, pv1._tail[0]);
        Assert.assertEquals(2, pv1._tail[1]);
        Assert.assertEquals(3, pv1._tail[2]);
    }

    public function runEmptyAndEmptyNodeAssertions():void {
        var cnt:int = PersistentVector.EMPTY._cnt;
        var shift:int = PersistentVector.EMPTY._shift;
        var nd:Node = PersistentVector.EMPTY._root;
        var tl:Array = PersistentVector.EMPTY._tail;
        Assert.assertTrue(nd == PersistentVector.EMPTY_NODE);
        Assert.assertTrue(nd === PersistentVector.EMPTY_NODE);
        Assert.assertEquals(0, cnt);
        Assert.assertEquals(5, shift);
        Assert.assertEquals(0, tl.length);
        var xs:Array = PersistentVector.EMPTY_NODE.array;
        Assert.assertEquals(32, xs.length);
        for (var i:int=0; i<xs.length; i++) {
            Assert.assertTrue(xs[i] == null);
        }
    }

    [Test]
    public function testEmptyAndEmptyNodeDoNotChange():void {
        runEmptyAndEmptyNodeAssertions();
        var xs:Array = [{A:1, B:2, C:3}, {A:4, B:5, C:6}];
        var pv1:IPVec = PersistentVector.create(xs);
        Assert.assertEquals(true, pv1.nth(0).hasOwnProperty('A'));
        Assert.assertEquals(1, pv1.nth(0).A);
        runEmptyAndEmptyNodeAssertions();
        var ys:Array = [{D:10, E:100, F:1000}, {D:20, E:200, F:2000}];
        var pv2:IPVec = PersistentVector.create(ys);
        Assert.assertEquals(true, pv1.nth(0).hasOwnProperty('A'));
        Assert.assertEquals(1, pv1.nth(0).A);
        Assert.assertEquals(true, pv2.nth(0).hasOwnProperty('D'));
        Assert.assertEquals(10, pv2.nth(0).D);
        runEmptyAndEmptyNodeAssertions();
        Assert.assertEquals(true, pv1.nth(0).hasOwnProperty('A'));
        Assert.assertEquals(1, pv1.nth(0).A);
        Assert.assertEquals(true, pv2.nth(0).hasOwnProperty('D'));
        Assert.assertEquals(10, pv2.nth(0).D);
    }

    [Test]
    public function testToArrayIsCorrect():void {
        var N:int = 100010;
        var pv:IPVec;
        var ys:Array;
        var xs:Array = new Array(N);
        var i:int;
        for (i=0; i<N; i++) {
            xs[i] = i;
        }
        pv = PersistentVector.create(xs);
        ys = pv.toArray();
        for (i=0; i<N; i++) {
            Assert.assertEquals(i, ys[i]);
        }
        N=10;
        for (i=0; i<N; i++) {
            xs[i] = i;
        }
        pv = PersistentVector.create(xs);
        ys = pv.toArray();
        for (i=0; i<N; i++) {
            Assert.assertEquals(i, ys[i]);
        }
    }
}
}