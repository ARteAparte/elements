/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package flexUnitTests.pdt {
import flash.events.Event;

import flexUnitTests.utils.MyAssert;

import flexunit.framework.Assert;

import mx.utils.ObjectUtil;

import pdt.ISeq;
import pdt.PersistentList;

public class TestPersistentList {
    private var _lst:PersistentList;

    public var assertThrows:Function = MyAssert.assertThrows;

    [Before]
    public function setUp():void {
        _lst =
            new PersistentList(1,
                new PersistentList(2,
                    new PersistentList(3,
                        new PersistentList(4))));
    }

    [Test]
    public function testFirst():void {
        var lst:PersistentList = new PersistentList(1);
        Assert.assertEquals(1, lst.first);
    }

    [Test]
    public function testRest():void {
        var lst:PersistentList = new PersistentList(1);
        Assert.assertTrue(lst.rest == PersistentList.EMPTY);
    }

    [Test]
    public function testPersistentList():void {
        var lst1:PersistentList = new PersistentList(3);
        var lst2:PersistentList = new PersistentList(2, lst1);
        var lst3:PersistentList = new PersistentList(1, lst2);
        Assert.assertTrue(lst3.rest === lst2);
        Assert.assertTrue(lst3.rest == lst2);
        Assert.assertTrue(lst3.first == 1);
        Assert.assertTrue(lst3.rest.first == 2);
        Assert.assertTrue(lst3.rest.rest.first == 3);
        Assert.assertTrue(lst3.rest.rest.rest == PersistentList.EMPTY);
    }

    [Test]
    public function testCons():void {
        var lst:ISeq = _lst.cons(0);
        Assert.assertEquals(0, lst.first);
    }

    [Test]
    public function testCount():void {
        Assert.assertEquals(4, _lst.count);
    }

    [Test]
    public function testPeek():void {
        Assert.assertEquals(1, _lst.peek());
    }

    [Test]
    public function testPop():void {
        Assert.assertEquals(2, _lst.pop().peek());
    }

    [Test]
    public function testLength():void {
        Assert.assertEquals(4, _lst.length);
    }

    [Test]
    public function testDoNothingInterfaceFuncs():void {
        _lst.addEventListener("", function(x:int):int { return x+1; });
        _lst.removeEventListener(Event.CHANGE, function():void {});
    }

    [Test]
    public function testErrorInterfaceFuncs():void {
        assertThrows(function():void {
            _lst.addItem({});
        }, PersistentList.ERROR_MUTATION_ATTEMPTED);
        assertThrows(function():void {
            _lst.addItemAt({}, 0);
        }, PersistentList.ERROR_MUTATION_ATTEMPTED);
        assertThrows(function():void {
            _lst.removeAll();
        }, PersistentList.ERROR_MUTATION_ATTEMPTED);
        assertThrows(function():void {
            _lst.removeItemAt(0);
        }, PersistentList.ERROR_MUTATION_ATTEMPTED);
        assertThrows(function():void {
            _lst.setItemAt(0, 0);
        }, PersistentList.ERROR_MUTATION_ATTEMPTED);
    }

    [Test]
    public function testDispatchEvent():void {
        Assert.assertTrue(_lst.dispatchEvent(new Event("Junk")) == false);
    }

    [Test]
    public function testGetItemAt():void {
        Assert.assertEquals(null, _lst.getItemAt(4));
        Assert.assertEquals(4, _lst.getItemAt(3));
        Assert.assertEquals(3, _lst.getItemAt(2));
        Assert.assertEquals(2, _lst.getItemAt(1));
        Assert.assertEquals(1, _lst.getItemAt(0));
    }

    [Test]
    public function getItemIndex():void {
        Assert.assertEquals(0, _lst.getItemIndex(1));
        Assert.assertEquals(1, _lst.getItemIndex(2));
        Assert.assertEquals(2, _lst.getItemIndex(3));
        Assert.assertEquals(3, _lst.getItemIndex(4));
        Assert.assertEquals(-1, _lst.getItemIndex(10));
    }

    [Test]
    public function testHasEventListener():void {
        Assert.assertTrue(_lst.hasEventListener(Event.CHANGE) == false);
    }

    [Test]
    public function testToArray():void {
        var xs:Array = _lst.toArray();
        Assert.assertTrue(ObjectUtil.compare(xs, [1,2,3,4]));
    }

    [Test]
    public function testWillTrigger():void {
        Assert.assertTrue(_lst.willTrigger(Event.CHANGE) == false);
    }

    [Test]
    public function testItemUpdated():void {
        _lst.itemUpdated(1);
    }

    [Test]
    public function testCreate():void {
        var lst:PersistentList = PersistentList.create([1,2,3]);
        Assert.assertEquals(1, lst.getItemAt(0));
        Assert.assertEquals(2, lst.getItemAt(1));
        Assert.assertEquals(3, lst.getItemAt(2));
        Assert.assertEquals(null, lst.getItemAt(3));
    }
}
}