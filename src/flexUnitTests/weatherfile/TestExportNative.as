/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package flexUnitTests.weatherfile {

import flash.filesystem.File;

import dse.DataSet;
import dse.DataSetBuilder;

import pdt.IPVec;
import pdt.PersistentVector;

import weatherfile.exporters.ExporterNativeFormat;
import weatherfile.exporters.IExporter;

public class TestExportNative {
    public var ds:DataSet;
    public var f:File;

    [Before]
    public function setUp():void {
        var hdr:Object = {A:10, B:20, C:30};
        var pts:IPVec = PersistentVector.create([
            {a:10, b:"The number 20", c:30},
            {a:40, b:"The number 50", c:60},
            {a:70, b:"The number 80", c:90}]);
        var smd:Object = {};
        var hmd:Object = {};
        ds = (new DataSetBuilder())
          .withHeader(hdr)
          .withPoints(pts)
          .withSeriesMetaData(smd)
          .withHeaderMetaData(hmd)
          .build();
        f = File.applicationStorageDirectory.resolvePath("temp.elements");
        //trace('file path:', f.nativePath);
    }

    [After]
    public function tearDown():void {
        f.deleteFile();
    }

    [Test]
    public function testWrite():void {
        var exp:IExporter = new ExporterNativeFormat();
        exp.write(f, ds);
    }
}
}