/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package flexUnitTests.weatherfile {
[Suite]
[RunWith("org.flexunit.runners.Suite")]
public class TestSuiteForWeatherfile {
    public var test01:TestDOE2BinRoundTrip;
    public var test02:TestDOE2FmtRoundTrip;
    public var test03:TestEPWRoundTrip;
    public var test04:TestLoadEPW;
    public var test05:TestImportNative;
    public var test06:TestExportNative;
    public var test07:TestRoundTripNative;
}
}