/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package flexUnitTests.weatherfile {

import flash.filesystem.File;

import dse.DataSet;

import flexunit.framework.Assert;

import pdt.IPVec;

import weatherfile.importers.IImporter;
import weatherfile.importers.ImporterEnergyPlusEPW;

public class TestLoadEPW {
    private var imp:IImporter;
    private var epwTMY2:File;
    private var epwTMY3:File;

    [Before]
    public function setUp():void {
        epwTMY2 =
            File.applicationDirectory.resolvePath("resources/boulder.epw");
        epwTMY3 =
            File.applicationDirectory.resolvePath("resources/tucson-big.epw");
        imp = new ImporterEnergyPlusEPW();
    }

    [Test]
    public function testRead():void {
        var ds:DataSet = imp.read(epwTMY2);
        var points:IPVec = ds.points;
        Assert.assertEquals(points.length, 8760);
    }

    [Test]
    public function testRead2():void {
        var ds:DataSet = imp.read(epwTMY3);
        var points:IPVec = ds.points;
        Assert.assertEquals(points.length, 8760);
    }
}
}