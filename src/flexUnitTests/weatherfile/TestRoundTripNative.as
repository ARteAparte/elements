/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package flexUnitTests.weatherfile {
import flash.filesystem.File;

import dse.DataSet;
import dse.DataSetBuilder;

import flexunit.framework.Assert;

import pdt.IPVec;
import pdt.PersistentVector;

import weatherfile.exporters.ExporterNativeFormat;
import weatherfile.exporters.IExporter;
import weatherfile.importers.IImporter;
import weatherfile.importers.ImporterNativeFormat;

public class TestRoundTripNative {
    public var ds:DataSet;
    public var f:File;

    [Before]
    public function setUp():void {
        var hdr:Object = {A:10, B:" Eats, shoots and leaves ", C:30};
        var pts:IPVec = PersistentVector.create([
            {a:10, b:"John, Jacob", c:30},
            {a:40, b:"  Jingleheimer Schmidt ", c:60},
            {a:70, b:"His name was My Name, too", c:90}]);
        var smd:Object = {};
        var hmd:Object = {};
        ds = (new DataSetBuilder())
          .withHeader(hdr)
          .withPoints(pts)
          .withSeriesMetaData(smd)
          .withHeaderMetaData(hmd)
          .build();
        f = File.applicationStorageDirectory.resolvePath("temp.elements");
    }

    [After]
    public function tearDown():void {
        f.deleteFile();
    }

    [Test]
    public function testRoundTrip():void {
        var imp:IImporter = new ImporterNativeFormat();
        var exp:IExporter = new ExporterNativeFormat();
        exp.write(f, ds);
        Assert.assertEquals(true, f.exists);
        var ds2:DataSet = imp.read(f);
        var pts:IPVec = ds2.points;
        Assert.assertEquals(3, pts.length);
        Assert.assertEquals(10, pts.valAt(0).a);
        Assert.assertEquals(40, pts.valAt(1).a);
        Assert.assertEquals(70, pts.valAt(2).a);
        Assert.assertEquals('John, Jacob', pts.valAt(0).b);
        Assert.assertEquals('  Jingleheimer Schmidt ', pts.valAt(1).b);
        Assert.assertEquals('His name was My Name, too', pts.valAt(2).b);
        Assert.assertEquals(30, pts.valAt(0).c);
        Assert.assertEquals(60, pts.valAt(1).c);
        Assert.assertEquals(90, pts.valAt(2).c);
        var hdr:Object = ds2.header;
        Assert.assertEquals(10, hdr.A);
        Assert.assertEquals(" Eats, shoots and leaves ", hdr.B);
        Assert.assertEquals(30, hdr.C);
    }
}
}