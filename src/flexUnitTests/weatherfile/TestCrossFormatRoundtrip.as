/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package flexUnitTests.weatherfile {
  import flash.events.Event;
  import flash.events.TimerEvent;
  import flash.filesystem.File;
  import flash.utils.Timer;

  import dse.DataSet;

  import flexunit.framework.Assert;

  import pdt.IPVec;

  import weatherfile.exporters.ExporterDOE2BIN;
  import weatherfile.exporters.ExporterDOE2FMT;
  import weatherfile.exporters.ExporterEnergyPlusEPW;
  import weatherfile.exporters.IExporter;
  import weatherfile.importers.IImporter;
  import weatherfile.importers.ImporterDOE2BIN;
  import weatherfile.importers.ImporterDOE2FMT;
  import weatherfile.importers.ImporterEnergyPlusEPW;

public class TestCrossFormatRoundtrip {
  private var srcFile:File;
  private var tmpFile:File;
  private var dstFile:File;
  private var ds01:DataSet;
  private var ds02:DataSet;
  private var impA:IImporter;
  private var expA:IExporter;
  private var impB:IImporter;
  private var expB:IExporter;
  private var impC:IImporter;
  private var expC:IExporter;
  [Before]
  public function setUp():void {
    tmpFile = File.applicationStorageDirectory.resolvePath("temp2.bin");
    impA = new ImporterDOE2BIN();
    expA = new ExporterDOE2BIN();
    impB = new ImporterEnergyPlusEPW();
    expB = new ExporterEnergyPlusEPW();
    impC = new ImporterDOE2FMT();
    expC = new ExporterDOE2FMT();
  }
  [After]
  public function tearDown():void {
    var timer:Timer = new Timer(100, 1);
    timer.addEventListener(TimerEvent.TIMER_COMPLETE,
      function(e:Event):void {
        if (tmpFile.exists) {
          tmpFile.deleteFile();
        }
        if (dstFile.exists) {
          dstFile.deleteFile();
        }
      });
    timer.start();
  }
  [Test]
  public function testDoe2BinToEpw():void {
    srcFile = File.applicationDirectory.resolvePath("resources/tucson.bin");
    dstFile = File.applicationStorageDirectory.resolvePath("dstFile.epw");
    ds01 = impA.read(srcFile);
    var pts:IPVec = ds01.points;
    Assert.assertEquals(8760, pts.length);
    expB.write(dstFile, ds01);
    ds02 = impB.read(dstFile);
    Assert.assertEquals(8760, pts.length);
  }
  [Test]
  public function testEpwToDoe2Bin():void {
    srcFile = File.applicationDirectory.resolvePath("resources/boulder.epw");
    dstFile = File.applicationStorageDirectory.resolvePath("dstFile.bin");
    ds01 = impB.read(srcFile);
    var pts:IPVec = ds01.points;
    Assert.assertEquals(8760, pts.length);
    expA.write(dstFile, ds01);
    ds02 = impA.read(dstFile);
    Assert.assertEquals(8760, pts.length);
  }
  [Test]
  public function testDoe2FmtToEpw():void {
    srcFile = File.applicationDirectory.resolvePath("resources/denver.fmt");
    dstFile = File.applicationStorageDirectory.resolvePath("dstFile.epw");
    ds01 = impC.read(srcFile);
    var pts:IPVec = ds01.points;
    Assert.assertEquals(8760, pts.length);
    expB.write(dstFile, ds01);
    ds02 = impB.read(dstFile);
    Assert.assertEquals(8760, pts.length);
  }
  [Test]
  public function testEpwToDoe2Fmt():void {
    srcFile = File.applicationDirectory.resolvePath("resources/boulder.epw");
    dstFile = File.applicationStorageDirectory.resolvePath("dstFile.fmt");
    ds01 = impB.read(srcFile);
    var pts:IPVec = ds01.points;
    Assert.assertEquals(8760, pts.length);
    expC.write(dstFile, ds01);
    ds02 = impC.read(dstFile);
    Assert.assertEquals(8760, pts.length);
  }
  [Test]
  public function testDoe2FmtToDoe2Bin():void {
    srcFile = File.applicationDirectory.resolvePath("resources/denver.fmt");
    dstFile = File.applicationStorageDirectory.resolvePath("dstFile.bin");
    ds01 = impC.read(srcFile);
    var pts:IPVec = ds01.points;
    Assert.assertEquals(8760, pts.length);
    expA.write(dstFile, ds01);
    ds02 = impA.read(dstFile);
    Assert.assertEquals(8760, pts.length);
  }
  [Test]
  public function testDoe2BinToDoe2Fmt():void {
    srcFile = File.applicationDirectory.resolvePath("resources/tucson.bin");
    dstFile = File.applicationStorageDirectory.resolvePath("dstFile.fmt");
    ds01 = impA.read(srcFile);
    var pts:IPVec = ds01.points;
    Assert.assertEquals(8760, pts.length);
    expC.write(dstFile, ds01);
    ds02 = impC.read(dstFile);
    Assert.assertEquals(8760, pts.length);
  }
}
}