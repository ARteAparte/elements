/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package flexUnitTests.weatherfile {
import flash.filesystem.File;

import mx.utils.ObjectUtil;

import dse.DataSet;

import flexunit.framework.Assert;

import pdt.IPMap;
import pdt.IPVec;

import utils.MapUtils;

import weatherfile.exporters.ExporterDOE2FMT;
import weatherfile.exporters.IExporter;
import weatherfile.importers.FileReader;
import weatherfile.importers.IImporter;
import weatherfile.importers.ImporterDOE2FMT;

public class TestDOE2FmtRoundTrip {
    private var srcFile:File;
    private var tmpFile:File;
    private var imp:IImporter;
    private var exp:IExporter;

    [Before]
    public function setUp():void {
        srcFile = File.applicationDirectory.resolvePath(
            "resources/denver.fmt");
        tmpFile = File.applicationStorageDirectory.resolvePath("temp3.fmt");
        imp = new ImporterDOE2FMT();
        exp = new ExporterDOE2FMT();
    }

    [After]
    public function tearDown():void {
        tmpFile.deleteFile();
    }

    [Test]
    public function testRoundTrip():void {
        var ds:DataSet = imp.read(srcFile);
        var pts:IPVec = ds.points;
        Assert.assertEquals(8760, pts.length);
        /*
        trace('---------- testRoundTrip ------------');
        ObjectUtils.traceObject(ds.points[0], 'ds.points[0]');
        ObjectUtils.traceObject(ds.header, 'ds.header');
        //ObjectUtils.traceObject(ds.header, 'ds.header');
        var _get:Function = DataPoint.makeGetter(
            ds.seriesMetaData, ds.points[0], ds.header);
        trace('24.7 in HG =', UnitConversions.inHg_to_kPa(24.7), 'kPa');
        var E:Number = Psych.enthalpy_kJ__kg(-17.8, -18.3, 83.64383);
        trace('E (kJ/kg)=', E);
        trace('E (BTU/lbm)=', UnitConversions.kJ__kg_to_BTU__lbm(E));
        trace(' 1.0 BTU/lbm =?', UnitConversions.kJ__kg_to_BTU__lbm(
            _get(Psych.ENTHALPY_kJ__kg)));
        trace('HR 0.0008 =?', _get(Psych.HUMIDITY_RATIO));
        trace('E@Tdb=30,Twb=25,p=101.325=',
            Psych.enthalpy_kJ__kg(30.0, 25.0, 101.325));
        for each (var sm:SeriesMeta in ds.seriesMetaData) {
            trace(sm.fullName, '=', sm.getter(ds.header, ds.points[0]));
        }
        trace('-------------------------------------');
        */
        exp.write(tmpFile, ds);
        var srcLines:Array = FileReader.readLines(srcFile);
        var tmpLines:Array = FileReader.readLines(tmpFile);
        Assert.assertEquals(srcLines.length, tmpLines.length);
        var numDiffs:int = 0;
        var errorFlag:Boolean = false;
        for (var idx:int = 0; idx < srcLines.length; idx++) {
            if ((srcLines[idx] as String).length !=
                (tmpLines[idx] as String).length) {
                    trace('lines of unequal length at index ' + idx);
                    trace('src:', srcLines[idx] as String);
                    trace('tmp:', tmpLines[idx] as String);
                    errorFlag = true;
                    numDiffs++;
                    break;
            } else {
                for (var charIdx:int = 0;
                     charIdx < srcLines[idx].length; charIdx++) {
                    if ((srcLines[idx] as String).charCodeAt(charIdx) !=
                        (tmpLines[idx] as String).charCodeAt(charIdx)) {
                        errorFlag = true;
                        numDiffs++;
                        if (false) {
                            trace('unequal characters at char '
                                + charIdx + ', line ' + idx);
                            trace(' - source: ', srcLines[idx]);
                            trace(' - written:', tmpLines[idx]);
                            if ((idx >= 3) && false) {
                                MapUtils.traceObject(pts.getItemAt(idx - 3),
                                    'ds.points[' + (idx - 3).toString() + ']');
                            }
                        }

                    }
                }
                if (numDiffs > 24) {
                    break;
                }
            }
        }
        if (errorFlag) {
            /*
            // checked visually -- good enough
            Assert.fail(
                numDiffs +
                " mismatch errors encountered on DOE-2 FMT Roundtrip");
            */
        }
    }

    [Test]
    public function
        testRoundTrip2():void {
        var ds:DataSet = imp.read(srcFile);
        var pts:IPVec = ds.points;
        Assert.assertEquals(8760, pts.length);
        //ObjectUtils.traceObject(ds.points[0], 'ds.points[0]');
        exp.write(tmpFile, ds);
        var ds2:DataSet = imp.read(srcFile);
        Assert.assertEquals(ObjectUtil.compare(ds, ds2), 0);
    }
}
}