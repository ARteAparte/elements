/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package weatherfile.exporters {
import flash.filesystem.File;
import flash.filesystem.FileStream;

import dse.DataPoint;
import dse.DataSet;
import dse.DateTime;

import pdt.IPVec;

import plugins.Clouds;
import plugins.Conditions;
import plugins.DateAndTime;
import plugins.EPWSpecific;
import plugins.GroundTemperatures;
import plugins.Location;
import plugins.Psych;
import plugins.Solar;
import plugins.Wind;
import plugins.helpers.LuminousEfficacy;

import units.UnitConversions;

import utils.FormatUtils;
import utils.MapUtils;

import weatherfile.AbstractHasFileFormat;
import weatherfile.EnergyPlusEPWFormat;

public class
ExporterEnergyPlusEPW extends AbstractHasFileFormat implements IExporter {
    public function
    ExporterEnergyPlusEPW() {
        super(EnergyPlusEPWFormat.FORMAT);
    }

    public function write(file:File, ds:DataSet):void {
      FileWriter.withFileStream(file, doWrite(ds));
    }

    private function doWrite(ds:DataSet):Function {
      return function(fs:FileStream):void {
        var NA:String = '<not available>';
        var getter:Function = MapUtils.atKeyOrAlt;
        var header:Object = ds.header;
        var points:IPVec = ds.points;
        var seriesMetaData:Object = ds.seriesMetaData;
        var lat_deg:Number = getter(header, Location.LATITUDE_deg, 39.74);
        var long_deg:Number =
          getter(header, Location.LONGITUDE_deg, -104.98);
        var tz_hrs:Number = getter(header, DateAndTime.TIME_ZONE, -7);
        var elev_m:Number = getter(header, Location.ELEVATION_m, 1609.344);
        var locationInfo:String = [
            "LOCATION",
            getter(header, Location.CITY, NA),
            getter(header, Location.STATE_PROVINCE_OR_REGION, NA),
            getter(header, Location.COUNTRY, NA),
            getter(header, Location.DATA_SOURCE, NA),
            getter(header, Location.WMO_STATION_NUMBER, NA),
            FormatUtils.floatDrop0(lat_deg, 2),
            FormatUtils.floatDrop0(long_deg, 2),
            FormatUtils.floatDrop0(tz_hrs, 1),
            FormatUtils.floatDrop0(elev_m, 2)
        ].join(",");
        fs.writeUTFBytes(locationInfo + "\n");
        var designConditions:String = getter(header, 'designConditions',
            'DESIGN CONDITIONS,0');
        fs.writeUTFBytes(designConditions + "\n");
        var typicalExtremePeriods:String = getter(header,
            'typicalExtremePeriods', 'TYPICAL/EXTREME PERIODS,0');
        fs.writeUTFBytes(typicalExtremePeriods + "\n");
        var groundTemperatures:String = getter(
            header, GroundTemperatures.GROUND_TEMPERATURES_C,
            'GROUND TEMPERATURES,0').toString();
        if ((groundTemperatures.toLowerCase()).indexOf('ground temperatures') != 0) {
          groundTemperatures = 'GROUND TEMPERATURES,0';
        }
        fs.writeUTFBytes(groundTemperatures + '\n');
        var holidaysAndDaylightSavings:String = getter(header,
            'holidaysAndDaylightSavings',
            'HOLIDAYS/DAYLIGHT SAVINGS,No,0,0,0');
        fs.writeUTFBytes(holidaysAndDaylightSavings + '\n');
        var comments01:String = getter(header, 'comments01',
          'COMMENTS 1, NA');
        fs.writeUTFBytes(comments01 + '\n');
        var comments02:String = getter(header, 'comments02',
          'COMMENTS 2, NA');
        fs.writeUTFBytes(comments02 + '\n');
        var dataPeriods:String = getter(header, 'dataPeriods',
            'DATA PERIODS,1,1,Data,Sunday,1/1,12/31');
        fs.writeUTFBytes(dataPeriods + '\n');
        for (var i:int=0; i<points.length; i++) {
            var point:Object = points.getItemAt(i);
            var _get:Function = DataPoint.makeGetter(
                seriesMetaData, point, header);
            /* TODO: The below attribute accessors need to be re-written as
            ObjectUtils.getWithDefault(...) to allow exporting from a
            non-compliant EP dataset. Defaults can be derived where possible
            but will otherwise need to be guessed.
            TODO: We need to decide what precision the file should be written
            in. I don't believe this is in the 'spec' for EPW but it seems to
            be 1 place past the decimal from what we see in actual files
            (at least for temperature)... */
            var ir:Number = Solar.skyIRFromCloudCover_W__m2(
              _get(Psych.DRY_BULB_TEMP_C),
              _get(Psych.WET_BULB_TEMP_C),
              _get(Psych.PRESSURE_kPa),
              _get(Clouds.CLOUD_COVER, 0));
            // Derive Illuminance from Irradiance
            var year:int = _get(DateAndTime.YEAR);
            var month:int = _get(DateAndTime.MONTH);
            var day:int = _get(DateAndTime.DAY);
            var hour:int = _get(DateAndTime.HOUR);
            var minute:int = _get(DateAndTime.MINUTE, 0);
            var dt:DateTime = new DateTime(year, month, day, hour, minute);
            var avg_cos_zen_ang:Number = Solar.avgCosZHourCentered(
              dt,
              tz_hrs,
              lat_deg,
              Solar.longToLong360W(long_deg));
            var zen_ang:Number = Math.acos(avg_cos_zen_ang);
            var sun_is_up:Boolean = (zen_ang <= (Math.PI/2.0));
            var dif_sol_rad:Number = _get(Solar.DIFFUSE_HORIZONTAL_Wh__m2, 0.0);
            var dir_sol_rad:Number = _get(Solar.BEAM_NORMAL_Wh__m2, 0.0);
            var Tdp_C:Number = _get(Psych.DEW_POINT_TEMP_C);
            var le:LuminousEfficacy = LuminousEfficacy.calc(
              avg_cos_zen_ang,
              dif_sol_rad,
              dir_sol_rad,
              elev_m,
              Tdp_C,
              month);
            var dif_illum:Number = le.diffuse * dif_sol_rad;
            var dir_illum:Number = le.direct * dir_sol_rad;
            var glob_illum:Number = dif_illum + dir_illum * avg_cos_zen_ang;
            if (!sun_is_up) {
              glob_illum = 0.0;
              dir_illum = 0.0;
              dif_illum = 0.0;
            }
            var line:String = [
                year.toString(),
                month.toString(),
                day.toString(),
                (hour + 1).toString(),
                minute.toString(),
                _get(EPWSpecific.DATA_SOURCE_AND_UNCERTAINTY_FLAGS, ""),
                FormatUtils.floatDrop0(_get(Psych.DRY_BULB_TEMP_C), 1),
                FormatUtils.floatDrop0(Tdp_C, 1),
                FormatUtils.formatInt(
                    _get(Psych.RELATIVE_HUMIDITY) * 100).toString(),
                FormatUtils.formatInt(UnitConversions.kPa_to_Pa(
                    _get(Psych.PRESSURE_kPa, 101.325))),
                FormatUtils.formatInt(
                  _get(Solar.EXTRATERRESTRIAL_HORIZ_Wh__m2, 0.0)),
                FormatUtils.formatInt(
                  _get(Solar.EXTRATERRESTRIAL_NORMAL_Wh__m2, 0.0)),
                FormatUtils.formatInt(
                  _get(Solar.IR_FROM_SKY_Wh__m2, ir)),
                FormatUtils.formatInt(
                  _get(Solar.GLOBAL_HORIZ_Wh__m2, 0.0)),
                FormatUtils.formatInt(dir_sol_rad),
                FormatUtils.formatInt(dif_sol_rad),
                FormatUtils.formatInt(glob_illum),
                FormatUtils.formatInt(dir_illum),
                FormatUtils.formatInt(dif_illum),
                FormatUtils.formatInt(
                  _get(EPWSpecific.ZENITH_LUMINANCE_Cd__m2, 0.0)),
                FormatUtils.formatInt(
                  _get(Wind.DIRECTION_deg, 0.0)),
                FormatUtils.floatDrop0(
                  _get(Wind.SPEED_m__s, 0.0), 1),
                // Clouds.CLOUD_COVER == Conditions.TOTAL_SKY_COVER
                // We write out Clouds.CLOUD_COVER because it is available
                // in the GUI and might have been modified whereas
                // TOTAL_SKY_COVER can not be directly modified.
                FormatUtils.formatInt(
                  _get(Clouds.CLOUD_COVER, 0.0)),
                FormatUtils.formatInt(
                  _get(Conditions.OPAQUE_SKY_COVER,
                    _get(Clouds.CLOUD_COVER, 0.0))),
                FormatUtils.floatDrop0(
                  _get(Conditions.VISIBILITY_km, 1.0), 1),
                FormatUtils.formatInt(
                  _get(Conditions.CEILING_HEIGHT_m, 1000.0)),
                FormatUtils.formatInt(
                  _get(Conditions.PRESENT_WEATHER_OBSERVATION, "")),
                FormatUtils.formatInt(
                  _get(Conditions.PRESENT_WEATHER_CODES, "")),
                FormatUtils.formatInt(
                  _get(Conditions.PRECIPITABLE_WATER_mm, 0.0)),
                FormatUtils.floatDrop0(
                  _get(Conditions.AEROSOL_OPTICAL_DEPTH_thousandths, 0.0), 4),
                FormatUtils.formatInt(
                  _get(Conditions.SNOW_DEPTH_cm, 0.0)),
                FormatUtils.formatInt(
                  _get(Conditions.DAYS_SINCE_LAST_SNOWFALL, 0.0)),
                FormatUtils.floatDrop0(
                  _get(Conditions.ALBEDO, 0.0), 3),
                FormatUtils.floatDrop0(
                  _get(Conditions.LIQUID_PRECIPITATION_DEPTH_mm, 0.0), 1),
                FormatUtils.floatDrop0(
                  _get(Conditions.LIQUID_PRECIPITATION_QUANTITY_hr,
                    0.0), 1)
            ].join(",");
            fs.writeUTFBytes(line + '\n');
        }
        fs.close();
    };
  }
}
}
