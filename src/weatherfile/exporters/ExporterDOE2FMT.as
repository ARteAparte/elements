/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package weatherfile.exporters {
import flash.filesystem.File;
import flash.filesystem.FileStream;

import dse.DataPoint;
import dse.DataSet;

import pdt.IPVec;

import plugins.Clouds;
import plugins.Conditions;
import plugins.DOE2Specific;
import plugins.DateAndTime;
import plugins.GroundTemperatures;
import plugins.Location;
import plugins.Psych;
import plugins.Solar;
import plugins.Wind;

import units.AngleUnits;
import units.DensityUnits;
import units.IrradianceUnits;
import units.PressureUnits;
import units.SpecificEnergyUnits;
import units.SpeedUnits;
import units.TemperatureUnits;
import units.UnitConversions;

import utils.AdvMath;
import utils.FP;
import utils.FormatUtils;
import utils.MapUtils;

import weatherfile.AbstractHasFileFormat;
import weatherfile.DOE2FmtFormat;

public class
ExporterDOE2FMT extends AbstractHasFileFormat implements IExporter {
  public function
  ExporterDOE2FMT() {
      super(DOE2FmtFormat.FORMAT);
  }

  public function
  write(file:File, ds:DataSet):void {
    //var fs:FileStream = FileWriter.getFileStream(file);
    FileWriter.withFileStream(file, doWrite(ds));
  }

  private function doWrite(ds:DataSet):Function {
    return function(fs:FileStream):void {
      var NA:String = '<NOT AVAILABLE>     ';
      var R:String = FormatUtils.PAD_RIGHT;
      var L:String = FormatUtils.PAD_LEFT;
      var header:Object = ds.header;
      var seriesMetaData:Object = ds.seriesMetaData;
      var points:IPVec = ds.points;
      FileWriter.writeFormatted(fs, MapUtils.atKeyOrAlt(
        header, Location.CITY, NA), 20, R);
      var _get:Function = DataPoint.makeGetter(
        seriesMetaData, points.getItemAt(0), header);
      var year:int = _get(DateAndTime.YEAR);
      FileWriter.writeFormatted(fs, year.toString(), 5, L);
      var lat:Number = MapUtils.atKeyOrAlt(
        header, Location.LATITUDE_deg, 0.0) as Number;
      var latStr:String = FormatUtils.formatFloat(lat, 2);
      FileWriter.writeFormatted(fs, latStr, 8, L);
      // Correct for sign convention of Elements internal (also EPW) versus
      // DOE2 format. DOE2 uses + as West and - as East longitude while EPW
      // and Elements internal use - as West and + as East.
      var lon:Number = -1.0 * (MapUtils.atKeyOrAlt(
        header, Location.LONGITUDE_deg, 0.0) as Number);
      var lonStr:String = FormatUtils.formatFloat(lon, 2);
      FileWriter.writeFormatted(fs, lonStr, 8, L);
      // DOE2 format does not use negative time-zones
      // TODO: check into the timezone format for DOE2 to make sure this is
      // correct...
      var tz:int = -1 * MapUtils.atKeyOrAlt(
        header, DateAndTime.TIME_ZONE, -7) as int;
      FileWriter.writeFormatted(fs, tz.toString(), 5, L);
      var solarFlag:int = MapUtils.atKeyOrAlt(
        header, DOE2Specific.SOLAR_FLAG, 5);
      FileWriter.writeFormatted(fs, solarFlag.toString(), 5, L);
      fs.writeUTFBytes('\n');
      // Monthly Clearness Indexes
      var clrIdxs:Array = MapUtils.atKeyOrAlt(header,
        Conditions.MONTHLY_CLEARNESS_INDEXES,
        FP.cycleUntilLength([1.0], 12));
      for each (var clrIdx:Number in clrIdxs) {
        var clrIdxStr:String = FormatUtils.formatFloat(clrIdx, 2);
        FileWriter.writeFormatted(fs, clrIdxStr, 6, L);
      }
      fs.writeUTFBytes('\n');
      // Grab any unit conversion functions we'll need
      var C_to_F:Function = UnitConversions.get_(
        TemperatureUnits.C, TemperatureUnits.F);
      var C_to_R:Function = UnitConversions.get_(
        TemperatureUnits.C, TemperatureUnits.R);
      var kPa_to_inHg:Function = UnitConversions.get_(
        PressureUnits.KPA, PressureUnits.IN_HG);
      var deg_to_compDir:Function = UnitConversions.get_(
        AngleUnits.DEGREES, AngleUnits.COMPASS_DIRECTION);
      var kg__m3_to_lbs__ft3:Function = UnitConversions.get_(
        DensityUnits.KG__M3, DensityUnits.LBM__FT3);
      var m__s_to_knots:Function = UnitConversions.get_(
        SpeedUnits.M__S, SpeedUnits.KNOTS);
      var W__m2_to_BTU__hr_ft2:Function = UnitConversions.get_(
        IrradianceUnits.W__M2, IrradianceUnits.BTU__HR_FT2);
      var kJ__kg_to_BTU__lbm:Function = UnitConversions.get_(
        SpecificEnergyUnits.KJ__KG, SpecificEnergyUnits.BTU__LBM);
      // Monthly Ground Temperatures in Rankine
      var grndTemps:* = MapUtils.atKeyOrAlt(header,
        GroundTemperatures.GROUND_TEMPERATURES_C,
        FP.cycleUntilLength([20.0], 12));
      var grTmps_C:Array;
      if (grndTemps is Array) {
        grTmps_C = grndTemps as Array;
      } else {
        grTmps_C = FP.cycleUntilLength([20.0], 12);
      }
      var grTmps_R:Array = AdvMath.map(C_to_R, grTmps_C);
      for each (var grTmp_R:Number in grTmps_R) {
        var grTmpStr_R:String = FormatUtils.formatFloat(grTmp_R, 1);
        FileWriter.writeFormatted(fs, grTmpStr_R, 6, L);
      }
      fs.writeUTFBytes('\n');
      // write each point out
      for (var i:int=0; i<points.length; i++) {
        var dp:Object = points.getItemAt(i);
        _get = DataPoint.makeGetter(seriesMetaData, dp, header);
        FileWriter.writeFormatted(fs,
          _get(DateAndTime.MONTH).toString(), 2, L);
        FileWriter.writeFormatted(fs,
          _get(DateAndTime.DAY).toString(), 2, L);
        FileWriter.writeFormatted(fs,
          (_get(DateAndTime.HOUR) + 1).toString(), 2, L);
        var Twb_C:Number = _get(Psych.WET_BULB_TEMP_C);
        var Twb_F:Number = C_to_F(Twb_C);
        FileWriter.writeFormatted(fs,
          FormatUtils.formatFloat(Twb_F, 0), 5, L);
        var Tdb_C:Number = _get(Psych.DRY_BULB_TEMP_C);
        var Tdb_F:Number = C_to_F(Tdb_C);
        FileWriter.writeFormatted(fs,
          FormatUtils.formatFloat(Tdb_F, 0), 5, L);
        var p_kPa:Number = _get(Psych.PRESSURE_kPa);
        var p_inHg:Number = kPa_to_inHg(p_kPa);
        FileWriter.writeFormatted(fs,
          FormatUtils.formatFloat(p_inHg, 1), 6, L);
        var cld:Number = _get(Clouds.CLOUD_COVER, 0.0);
        FileWriter.writeFormatted(fs,
          FormatUtils.formatFloat(cld, 0), 5, L);
        var snowFlag:int = _get(Conditions.SNOW_FLAG, 0);
        FileWriter.writeFormatted(fs, snowFlag.toString(), 3, L);
        var rainFlag:int = _get(Conditions.RAIN_FLAG, 0);
        FileWriter.writeFormatted(fs, rainFlag.toString(), 3, L);
        var windDir:int = int(deg_to_compDir(
          _get(Wind.DIRECTION_deg, 0)));
        FileWriter.writeFormatted(fs, windDir.toString(), 4, L);
        var HR:Number = _get(Psych.HUMIDITY_RATIO);
        FileWriter.writeFormatted(fs,
          FormatUtils.formatFloat(HR, 4), 7, L);
        var airDensity_kg__m3:Number = _get(Psych.DENSITY_kg__m3);
        var airDensity_lbs__ft3:Number =
          kg__m3_to_lbs__ft3(airDensity_kg__m3);
        FileWriter.writeFormatted(fs,
          FormatUtils.formatFloat(airDensity_lbs__ft3, 3), 6, L);
        var enthalpy_kJ__kg:Number = _get(Psych.ENTHALPY_kJ__kg);
        //var enthalpy_BTU__lbm:Number =
        //    kJ__kg_to_BTU__lbm(enthalpy_kJ__kg);
        // this equation from DOE-2.1E p VIII.14
        // apparently the DOE2 enthalpy is not on the same
        // basis as present-day ASHRAE calculations.
        /* TODO: Track down what mathematically is going on
        // here to make sure we agree with it...
        // See equation E.5 of "The Intertube Falling-Film Modes:
        // Transition, Hysteresis, and Effects on Heat Transfer" by
        // C. T. Siambekos, R. R. Crawford, October 1991
        see also:
        http://ntrs.nasa.gov/archive/nasa/casi.ntrs.nasa.gov/
        19820025949_1982025949.pdf
        page 3-53, referenced to 0 F?

        see http://www.alsafeenah.com/Uploads/CategoriesDetail/
        d0add006-020a-4a49-9dc4-f3843e63a56b.pdf

        The explenation there on page 9-12 is very clear
        Equation 9.2.8 */
        var HR_:Number = Math.round(HR * 10000.0) / 10000.0;
        var enthalpy_BTU__lbm:Number =
          0.24 * Tdb_F + (1061.0 + 0.444 * Tdb_F) * HR;
        //    0.24 * Tdb_F + (1061.0 + 0.444 * Tdb_F) * HR;
        FileWriter.writeFormatted(fs,
          FormatUtils.formatFloat(enthalpy_BTU__lbm, 1), 6, L);
        var totalHorizSolar_W__m2:Number =
          _get(Solar.GLOBAL_HORIZ_Wh__m2, 0);
        var totalHorizSolar_BTU__hr_ft2:Number =
          W__m2_to_BTU__hr_ft2(totalHorizSolar_W__m2);
        FileWriter.writeFormatted(fs,
          FormatUtils.formatFloat(totalHorizSolar_BTU__hr_ft2, 1), 7, L);
        var totalNormalSolar_Wh__m2:Number =
          _get(Solar.BEAM_NORMAL_Wh__m2, 0);
        var totalNormalSolar_BTU__hr_ft2:Number =
          W__m2_to_BTU__hr_ft2(totalNormalSolar_Wh__m2);
        FileWriter.writeFormatted(fs,
          FormatUtils.formatFloat(totalNormalSolar_BTU__hr_ft2, 1), 7, L);
        var cloudType:int = _get(Conditions.CLOUD_TYPE, 0);
        FileWriter.writeFormatted(fs, cloudType.toString(), 3, L);
        var windSpeed_m__s:Number = _get(Wind.SPEED_m__s, 0);
        var windSpeed_knots:Number = m__s_to_knots(windSpeed_m__s);
        FileWriter.writeFormatted(fs,
          FormatUtils.formatFloat(windSpeed_knots, 0), 5, L);
        fs.writeUTFBytes('\n');
      }
      fs.close();
    };
  }
}
}