/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package weatherfile.exporters {
/* Notes: In fortran (at least the fortran written for
FMTWTH.F/TXT2BIN.F/BIN2TXT.F), an integer is used to hold four ascii
characters. A character is 1 byte and an integer is 4 bytes so that's why,
for example, the location ID can have up to 20 characters because it uses
5 integers (5 integers x 4 chars/integer = 20) to store 20 bytes of character
information. */
import flash.filesystem.File;
import flash.utils.ByteArray;
import flash.utils.Endian;

import dse.DataPoint;
import dse.DataSet;

import pdt.IPVec;

import plugins.Clouds;
import plugins.Conditions;
import plugins.DOE2Specific;
import plugins.DateAndTime;
import plugins.GroundTemperatures;
import plugins.Location;
import plugins.Psych;
import plugins.Solar;
import plugins.Wind;

import units.AngleUnits;
import units.DensityUnits;
import units.IrradianceUnits;
import units.PressureUnits;
import units.SpecificEnergyUnits;
import units.SpeedUnits;
import units.TemperatureUnits;
import units.UnitConversions;

import utils.MapUtils;

import weatherfile.AbstractHasFileFormat;
import weatherfile.DOE2BinFormat;

public class
ExporterDOE2BIN extends AbstractHasFileFormat implements IExporter {
public function
  ExporterDOE2BIN() {
  super(DOE2BinFormat.FORMAT);
}

public function
  write(file:File, dataSet:DataSet):void {
  var ba:ByteArray = new ByteArray();
  ba.endian = Endian.LITTLE_ENDIAN;
  var C_to_R:Function =
    UnitConversions.get_(TemperatureUnits.C, TemperatureUnits.R);
  // DOE-2 converters say 6200, but has 4-byte header and tail
  var recordLength:uint = 6200 + 8;
  var numDays:Array = [31,28,31,30,31,30,31,31,30,31,30,31];
  var ptIdx:uint = 0;
  var seriesMetaData:Object = dataSet.seriesMetaData;
  var points:IPVec = dataSet.points;
  var header:Object = dataSet.header;
  var _get:Function = DataPoint.makeGetter(
    seriesMetaData, points.getItemAt(0), header);
  /* TODO:
  need to do some checks to make sure data is 365 days (non-leap year)
  from Jan 1 to Dec 31 and one point per hour, otherwise, the way this
  data gets written may not be non-optimal at best and completely
  incorrect in the worst case... */
  if (points.length != 8760) {
    throw new Error(
      "DOE-2 BIN format error: length of data points != 8760");
  }
  var DEFAULT_MONTHLY_CLEARNESS_IDXS:Array = [0,0,0,0,0,0,0,0,0,0,0,0];
  var DEFAULT_GROUND_TEMPERATURES_C:Array =
    [25,25,25,25,25,25,25,25,25,25,25,25];
  var W__m2_to_BTU__ft2:Function = UnitConversions.get_(
    IrradianceUnits.W__M2, IrradianceUnits.BTU__HR_FT2);
  var C_to_F:Function = UnitConversions.get_(
    TemperatureUnits.C, TemperatureUnits.F);
  var kPa_to_inHg:Function = UnitConversions.get_(
    PressureUnits.KPA, PressureUnits.IN_HG);
  var m__s_to_knots:Function = UnitConversions.get_(
    SpeedUnits.M__S, SpeedUnits.KNOTS);
  var kg__m3_to_lbm__ft3:Function = UnitConversions.get_(
    DensityUnits.KG__M3, DensityUnits.LBM__FT3);
  var kJ__kg_to_BTU__lbm:Function = UnitConversions.get_(
    SpecificEnergyUnits.KJ__KG, SpecificEnergyUnits.BTU__LBM);
  var deg_to_compDir:Function = UnitConversions.get_(
    AngleUnits.DEGREES, AngleUnits.COMPASS_DIRECTION);
  for (var recordIndex:uint = 0; recordIndex < 24; recordIndex++) {
    // The "FORTRAN 4-byte header" -- see:
    // http://paulbourke.net/dataformats/fortran/
    ba.writeInt(6200);
    // must write 20 bytes for the Location.CITY property
    var location:String = MapUtils.atKeyOrAlt(
      header, Location.CITY, "unknown location    ");
    if (location.length < 20) {
      ba.writeUTFBytes(location);
      FileWriter.writeByteNumTimes(ba, " ", 20 - location.length);
    } else if (location.length > 20) {
      // chop the name if it's over 20 characters
      ba.writeUTFBytes(location.substr(0, 20));
    } else {
      ba.writeUTFBytes(location);
    }
    ba.writeInt(_get(DateAndTime.YEAR));
    ba.writeFloat(MapUtils.atKeyOrAlt(
      header, Location.LATITUDE_deg, 0.0));
    // Correct for longitude sign convention so that + is West and - is East.
    // Elements internally uses EPW convention of + is East and - is West.
    ba.writeFloat(-1 * MapUtils.atKeyOrAlt(
      header, Location.LONGITUDE_deg, 0.0));
    // DOE2 format does not use negative time-zones
    // TODO: check into the time-zone format for DOE2 to make sure
    // this is correct
    ba.writeInt(int(-1 * MapUtils.atKeyOrAlt(
      header, DateAndTime.TIME_ZONE, 0)));
    ba.writeInt(recordIndex + 1);
    var monthIdx:int = int(Math.floor(recordIndex / 2));
    ba.writeInt(numDays[monthIdx]);
    ba.writeFloat(MapUtils.atKeyOrAlt(header,
      Conditions.MONTHLY_CLEARNESS_INDEXES,
      DEFAULT_MONTHLY_CLEARNESS_IDXS)[monthIdx]);
    var grndTemps:* = MapUtils.atKeyOrAlt(header,
      GroundTemperatures.GROUND_TEMPERATURES_C,
      DEFAULT_GROUND_TEMPERATURES_C);
    if (grndTemps is Array) {
      ba.writeFloat(C_to_R(grndTemps[monthIdx]));
    } else {
      ba.writeFloat(C_to_R(DEFAULT_GROUND_TEMPERATURES_C[monthIdx]));
    }
    if (recordIndex % 2 == 0) {
      var startDate:uint = 1;
      var endDate:uint = 16;
    } else {
      startDate = 17;
      endDate = numDays[monthIdx];
    }
    /* This is the "solar flag" which is a combination of the word
    size and file type as follows:
    word size | file type | solar flag =
    word-size + (file-type - 1) * 2 - 1
    1                 1                 0
    2                 1                 1
    1                 2                 2
    2                 2                 3
    1                 3                 4
    2                 3                 5

    For now, let's assume we have a DOE2 header, otherwise it's 5
    fs.writeInt(5); */
    ba.writeInt(MapUtils.atKeyOrAlt(
      header, DOE2Specific.SOLAR_FLAG, 5));
    var date:uint = startDate;
    // unpack unit conversion functions
    for (var cnt:int = 0; cnt < 16; cnt++) {
      if (date <= endDate) {
        for (var hour:uint = 1; hour < 25; hour++) {
          var pt:Object = points.getItemAt(ptIdx);
          _get = DataPoint.makeGetter(
            seriesMetaData, pt, header);
          ptIdx++;
          var isol:int = int(W__m2_to_BTU__ft2(
            _get(Solar.GLOBAL_HORIZ_Wh__m2, 0.0)) + 0.5);
          var idn:int = int(W__m2_to_BTU__ft2(
            _get(Solar.BEAM_NORMAL_Wh__m2, 0.0)) + 0.5);
          var iwet:int = int(C_to_F(
            _get(Psych.WET_BULB_TEMP_C)) + 99.5);
          var idry:int = int(C_to_F(
            _get(Psych.DRY_BULB_TEMP_C)) + 99.5);
          var ipres:int = int(
            kPa_to_inHg(_get(Psych.PRESSURE_kPa)) * 10.0 - 149.5);
          var icldam:int = int(
            _get(Clouds.CLOUD_COVER, 0.0));
          var iwndsp:int = int(m__s_to_knots(
            _get(Wind.SPEED_m__s, 0.0)) + 0.5);
          var ihumrt:int = int(
            _get(Psych.HUMIDITY_RATIO) * 10000.0
            + 0.5);
          var idens:int = int(kg__m3_to_lbm__ft3(
            _get(Psych.DENSITY_kg__m3)) * 1000.0
            - 19.5);
          // this equation from DOE-2.1E p VIII.14
          // apparently the DOE2 enthalpy is not on the same
          // basis as present-day ASHRAE calculations.
          // TODO: Track down what mathematically is going on
          // here to make sure we agree with it...
          // see http://gundog.lbl.gov/dirun/2001weath.pdf page VIII.12
          var tdryf:Number = C_to_F(_get(Psych.DRY_BULB_TEMP_C));
          var humrt:Number = _get(Psych.HUMIDITY_RATIO);
          var enthalpy_BTU__lbm:Number =
            0.24 * tdryf + (1061.0 + 0.444 * tdryf) * humrt;
          //trace('humidity ratio: ', humrt);
          //trace('enthalpy_BTU__lbm_A: ', enthalpy_BTU__lbm_A);
          //trace('enthalpy_BTU__lbm_B: ', enthalpy_BTU__lbm_B);
          //var ienth:int = int(enthalpy_BTU__lbm_A);
          var ienth:int = int(enthalpy_BTU__lbm * 2.0 + 60.5);
          //var ienth:int = int(kJ__kg_to_BTU__lbm(
          //    _get(Psych.ENTHALPY_kJ__kg)) * 2.0
          //    + 60.5);
          var isnow:int = int(
            _get(Conditions.SNOW_FLAG, 0));
          var irain:int = int(
            _get(Conditions.RAIN_FLAG, 0));
          var icldty:int = int(
            _get(Conditions.CLOUD_TYPE, 0));
          var iwnddr:int = int(deg_to_compDir(
            _get(Wind.DIRECTION_deg, 0)));
          // IDAT(IP1) = IPRES * 65536 + IWET * 256 + IDRY
          var idat01:int = ipres * 65536 + iwet * 256 + idry;
          var idat02:int = isol * 1048576 + idn * 1024 +
            icldam * 64 + isnow * 32 + irain * 16 + iwnddr;
          var idat03:int = ihumrt*128 + idens;
          var idat04:int = ienth*2048 + icldty * 128 + iwndsp;
          ba.writeInt(idat01);
          ba.writeInt(idat02);
          ba.writeInt(idat03);
          ba.writeInt(idat04);
        }
        date++;
      } else {
        for (var fauxHour:uint=1; fauxHour<25; fauxHour++) {
          ba.writeInt(0);
          ba.writeInt(0);
          ba.writeInt(0);
          ba.writeInt(0);
        }
      }
    }
    // the "FORTRAN 4-byte tail"
    ba.writeInt(6200);
  }
  FileWriter.writeBytesToFile(file, ba);
}
}
}