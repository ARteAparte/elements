/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package weatherfile.exporters {

import flash.filesystem.File;
import flash.filesystem.FileStream;
import flash.utils.ByteArray;
import flash.utils.IDataOutput;

import dse.DataSet;

import org.as3commons.zip.Zip;

import pdt.IPVec;

import utils.FP;
import utils.MapUtils;

import weatherfile.AbstractHasFileFormat;
import weatherfile.ElementsNativeFormat;

public class
ExporterNativeFormat extends AbstractHasFileFormat implements IExporter {
    public function ExporterNativeFormat() {
        super(ElementsNativeFormat.FORMAT);
    }

    public static function
    writePointToIDataOutput(pt:Object, ptKeys:Array, out:IDataOutput):void {
        var getKey:Function = function(key:String):String {
            if (pt[key] is Number) {
                return pt[key].toString();
            }
            return '"' + pt[key].toString() + '"';
        };
        var ptVals:Array = FP.map(getKey, ptKeys);
        var str:String = ptVals.join(", ") + "\n";
        out.writeUTFBytes(str);
    }

    public function write(file:File, ds:DataSet):void {
        var z:Zip = new Zip();
        var pts:IPVec = ds.points;
        var hdr:Object = ds.header;
        var hdrKeys:Array = MapUtils.keys(hdr);
        var hdrBA:ByteArray = new ByteArray();
        var ptsBA:ByteArray = new ByteArray();
        var ptsArray:Array = pts.toArray();
        var ptsKeys:Array;
        if (pts.length > 0) {
            ptsKeys = MapUtils.keys(ptsArray[0]);
            ptsBA.writeUTFBytes(ptsKeys.join(", ") + "\n");
        } else {
            ptsKeys = [];
        }
        for each (var pt:Object in ptsArray) {
            writePointToIDataOutput(pt, ptsKeys, ptsBA);
        }
        var str:String;
        for each (var hdrKey:String in hdrKeys) {
            if (hdr[hdrKey] is Number) {
                str = hdrKey + ", " + hdr[hdrKey].toString() + '\n';
            } else {
                str = hdrKey + ", " + '"' + hdr[hdrKey].toString() + '"\n';
            }
            hdrBA.writeUTFBytes(str);
        }
        z.addFile(ElementsNativeFormat.HEADER_FILE, hdrBA);
        z.addFile(ElementsNativeFormat.TIME_SERIES_FILE, ptsBA);
        var stream:FileStream = FileWriter.getFileStream(file);
        z.serialize(stream);
        stream.close();
        z.close();
    }
}
}