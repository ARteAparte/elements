/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package weatherfile.exporters {
import flash.errors.IOError;
import flash.filesystem.File;
import flash.filesystem.FileMode;
import flash.filesystem.FileStream;
import flash.utils.ByteArray;
import flash.utils.IDataOutput;

import utils.FormatUtils;

public class FileWriter {
  public static function withFileStream(file:File, f:Function):void {
    var fileStream:FileStream = new FileStream();
    // The following timer code is to prevent "Error #3013: File or directory
    // is in use." on Windows. Fix suggested from
    // http://stackoverflow.com/questions/17238601/adobe-air-filestream-error-3013-file-or-directory-is-in-use
    // http://forums.creativecow.net/thread/190/869842
    // http://www.davidmillington.net/news/index.php/2009/09/14/actionscript-3-error-3013-file-or-direct
    /*
    fileStream.addEventListener(Event.COMPLETE,
      function(e:Event):void {
        f(fileStream);
        fileStream.close();
      });
    fileStream.openAsync(file, FileMode.WRITE);
    */
    try {
      fileStream.open(file, FileMode.WRITE);
      f(fileStream);
      fileStream.close();
    } catch (e:IOError) {
      trace('#####################');
      trace("Error: " + e.name + " ID:" + e.errorID);
      trace("Message: " + e.message);
      trace('FileWriter.withFileStream: save not successful, please try again');
      trace('file: ', file.nativePath);
      trace('#####################');
    }
  }

  public static function getFileStream(file:File):FileStream {
    var proceed:Boolean = false;
    var fileStream:FileStream = new FileStream();
    fileStream.open(file, FileMode.WRITE);
    return fileStream;
  }

  public static function
  writeByteNumTimes(fs:IDataOutput, bytes:String, n:int):void {
    if (n <= 0) return;
      fs.writeUTFBytes(bytes);
      writeByteNumTimes(fs, bytes, n - 1);
  }

    public static function
    writeStrNumTimes(fs:IDataOutput, str:String, n:int):void {
        if (n <= 0) return;
        fs.writeUTFBytes(str);
        writeStrNumTimes(fs, str, n - 1);
    }

    public static function writeBytesToFile(file:File, bytes:ByteArray):void {
      var fs:FileStream = new FileStream();
      try {
        fs.open(file, FileMode.WRITE);
        fs.writeBytes(bytes);
        fs.close();
      } catch (e:IOError) {
        trace('#####################');
        trace('Error: ' + e.name + " ID:" + e.errorID);
        trace("Message: " + e.message);
        trace('FileWriter.writeBytesToFile: save not successful, please try again');
        trace('file: ', file.nativePath);
        trace('num bytes to write: ', bytes.length);
        trace('#####################');
      }
    }

    public static function
    writeFormatted(fs:IDataOutput, str:String, len:int, padDir:String):void {
        var formattedString:String =
            FormatUtils.fixStringLength(len, str, ' ', padDir);
        fs.writeUTFBytes(formattedString);
    }
}
}