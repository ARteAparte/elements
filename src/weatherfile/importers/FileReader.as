/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package weatherfile.importers {
import flash.filesystem.File;
import flash.filesystem.FileMode;
import flash.filesystem.FileStream;
import flash.utils.ByteArray;
import flash.utils.Endian;

import org.as3commons.zip.Zip;
import org.as3commons.zip.ZipFile;

public class FileReader {
    public function
    FileReader() {}

    public static function
    readContents(file:File):String {
        var fileStream:FileStream = new FileStream();
        fileStream.open(file, FileMode.READ);
        var fileContents:String =
            fileStream.readUTFBytes(fileStream.bytesAvailable);
        fileStream.close();
        return fileContents;
    }

    public static function
    readLines(file:File):Array {
      // lineEnding must be consistent across platforms.
      // Cannot use File.lineEnding because it changes based on platform
      // must use "\n" even on Windows
      var lineEnding:String = "\n";
      var fileContents:String = readContents(file);
      var lines:Array = fileContents.split(lineEnding);
        return lines;
    }

    public static function
    readBytes(file:File):ByteArray {
        var bytes:ByteArray = new ByteArray();
        bytes.endian = Endian.LITTLE_ENDIAN;
        var fileStream:FileStream = new FileStream();
        fileStream.open(file, FileMode.READ);
        fileStream.readBytes(bytes, 0, fileStream.bytesAvailable);
        fileStream.close();
        return bytes;
    }

    public static function
    getZipFileFrom(zipArchive:File, fileName:String):ZipFile {
        var z:Zip = new Zip();
        try {
            z.loadBytes(readBytes(zipArchive));
        } catch (e:Error) {
            trace('Error loading zip archive');
            trace(e);
        }
        return z.getFileByName(fileName);
    }

    public static function
    getLinesFromFileInZipArchive(zf:ZipFile):Array {
      // note: can't use File.lineEnding -- the line ending convention
      // needs to be consistent across platforms. Let's just use "\n".
      var lineEnding:String = "\n";
      return zf.getContentAsString().split(lineEnding);
    }

    public static function
    linesFromZip(zipArchive:File, fileName:String):Array {
        return getLinesFromFileInZipArchive(
            getZipFileFrom(zipArchive, fileName));
    }
}
}