/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package weatherfile.importers {
import flash.filesystem.File;

import dse.DataSet;
import dse.DataSetBuilder;

import pdt.PersistentVector;

import plugins.Clouds;
import plugins.Conditions;
import plugins.DateAndTime;
import plugins.EPWSpecific;
import plugins.GroundTemperatures;
import plugins.Location;
import plugins.PlugInfo;
import plugins.Psych;
import plugins.Solar;
import plugins.Wind;

import units.PressureUnits;
import units.UnitConversions;

import weatherfile.AbstractHasFileFormat;
import weatherfile.EnergyPlusEPWFormat;

public class
ImporterEnergyPlusEPW extends AbstractHasFileFormat implements IImporter {
    public function ImporterEnergyPlusEPW() {
        super(EnergyPlusEPWFormat.FORMAT);
    }

    private function adjustForCRLF(txt:String):String {
      if (txt.charAt(txt.length - 1) == '\r') {
        return txt.substr(0, txt.length - 1);
      }
      return txt;
    }

    public function
    read(file:File):DataSet {
        var lines:Array = FileReader.readLines(file);
        var hdr:Object = new Object();
        var pts:Array = new Array();
        var length:uint = lines.length - 1;
        var columns:Array = lines[0].split(",");
        hdr[Location.CITY] = columns[1];
        hdr[Location.STATE_PROVINCE_OR_REGION] = columns[2];
        hdr[Location.COUNTRY] = columns[3];
        hdr[Location.DATA_SOURCE] = columns[4];
        hdr[Location.WMO_STATION_NUMBER] = columns[5];
        hdr[Location.LATITUDE_deg] = parseFloat(columns[6]);
        hdr[Location.LONGITUDE_deg] = parseFloat(columns[7]);
        hdr[DateAndTime.TIME_ZONE] = parseFloat(columns[8]);
        hdr[Location.ELEVATION_m] = parseFloat(columns[9]);
        hdr[EPWSpecific.DESIGN_CONDITIONS] = adjustForCRLF(lines[1]);
        hdr[EPWSpecific.TYPICAL_EXTREME_PERIODS] = adjustForCRLF(lines[2]);
        hdr[GroundTemperatures.GROUND_TEMPERATURES_C] =
            adjustForCRLF(lines[3]);
        hdr[EPWSpecific.HOLIDAYS_AND_DAYLIGHT_SAVINGS] =
            adjustForCRLF(lines[4]);
        hdr[EPWSpecific.COMMENTS_01] = adjustForCRLF(lines[5]);
        hdr[EPWSpecific.COMMENTS_02] = adjustForCRLF(lines[6]);
        hdr[EPWSpecific.DATA_PERIODS] = adjustForCRLF(lines[7]);
        // unpack unit conversion functions
        var Pa_to_kPa:Function = UnitConversions.get_(
            PressureUnits.PA, PressureUnits.KPA);
        var eTimeFn:Function;
        for (var index:uint = 8; index < length; index++) {
            columns = lines[index].split(",");
            var pt:Object = new Object();
            if (index==8) {
                // note: the year information is problematic; actual EPW
                // files may reference different years for a given month.
                // therefore, we're just going to take the year from the
                // first instance.
                var year:int = parseFloat(columns[0]);
            }
            var month:int = parseFloat(columns[1]);
            var day:int = parseFloat(columns[2]);
            var hr:int = parseFloat(columns[3]) - 1;
            var min:int = parseFloat(columns[4]);
            if ((min > 59) || (min < 0)) {
                min = 0; // Fixes EPW Load issue for TMY2 based data
            }
            if (index==8) {
                hdr[DateAndTime.REFERENCE_YEAR] = year;
                hdr[DateAndTime.REFERENCE_MONTH] = month;
                hdr[DateAndTime.REFERENCE_DAY] = day;
                hdr[DateAndTime.REFERENCE_HOUR] = hr;
                hdr[DateAndTime.REFERENCE_MINUTE] = min;
                hdr[DateAndTime.REFERENCE_SECOND] = 0;
                eTimeFn = DateAndTime.makeElapsedHoursToReferenceDateFunc(
                    year, month, day, hr, min, 0);
            }
            var hourOfYear:Number = eTimeFn(year, month, day, hr, min, 0);
            pt[DateAndTime.ELAPSED_TIME_hrs] = hourOfYear;
            pt[EPWSpecific.DATA_SOURCE_AND_UNCERTAINTY_FLAGS] = columns[5];
            var Tdb_C:Number = parseFloat(columns[6]);
            pt[Psych.DRY_BULB_TEMP_C] = Tdb_C;
            var Tdew_C:Number = parseFloat(columns[7]);
            // convert from % to decimal fraction
            var RELATIVE_HUMIDITY:Number = parseFloat(columns[8]) / 100.0;
            var p_kPa:Number = Pa_to_kPa(parseFloat(columns[9]));
            pt[Psych.PRESSURE_kPa] = p_kPa;
            if (true) {
                // OK, we're going to auto-derive wet bulbt temperature
                // from pressure, dry-bulb, and dew point
                //trace('getting ready to derive wet bulb');
                /*pt[Psych.WET_BULB_TEMP_C] = Psych.Twb_from_p_Tdb_and_X(
                    Psych.DEW_POINT_TEMP_C, Tdb_C, p_kPa, Tdew_C);*/
                pt[Psych.WET_BULB_TEMP_C] = Psych.Twb_from_p_Tdb_and_X(
                    Psych.RELATIVE_HUMIDITY, Tdb_C, p_kPa, RELATIVE_HUMIDITY);
            }
            // Note: technically, the solar variables are in Wh/m2 but these
            // are all "per hour" so we'll just use average W/m2 so that we
            // can inter-operate better with DOE2 format
            pt[Solar.EXTRATERRESTRIAL_HORIZ_Wh__m2] = parseFloat(columns[10]);
            pt[Solar.EXTRATERRESTRIAL_NORMAL_Wh__m2] = parseFloat(columns[11]);
            pt[Solar.IR_FROM_SKY_Wh__m2] = parseFloat(columns[12]);
            var solarGlobalHoriz_W__m2:Number = parseFloat(columns[13]);
            pt[Solar.BEAM_NORMAL_Wh__m2] = parseFloat(columns[14]);
            pt[Solar.DIFFUSE_HORIZONTAL_Wh__m2] = parseFloat(columns[15]);
            pt[EPWSpecific.GLOBAL_HORIZONTAL_ILLUMINANCE_lux] =
                parseFloat(columns[16]);
            pt[EPWSpecific.BEAM_NORMAL_ILLUMINANCE_lux] =
                parseFloat(columns[17]);
            pt[EPWSpecific.DIFFUSE_HORIZONTAL_ILLUMINANCE_lux] =
                parseFloat(columns[18]);
            pt[EPWSpecific.ZENITH_LUMINANCE_Cd__m2] = parseFloat(columns[19]);
            pt[Wind.DIRECTION_deg] = parseFloat(columns[20]);
            pt[Wind.SPEED_m__s] = parseFloat(columns[21]);
            var CC:Number = parseFloat(columns[22]);
            pt[Clouds.CLOUD_COVER] = CC;
            pt[Conditions.TOTAL_SKY_COVER] = CC;
            pt[Conditions.OPAQUE_SKY_COVER] = parseFloat(columns[23]);
            pt[Conditions.VISIBILITY_km] = parseFloat(columns[24]);
            pt[Conditions.CEILING_HEIGHT_m] = parseFloat(columns[25]);
            pt[Conditions.PRESENT_WEATHER_OBSERVATION] = parseFloat(columns[26]);
            pt[Conditions.PRESENT_WEATHER_CODES] = parseFloat(columns[27]);
            pt[Conditions.PRECIPITABLE_WATER_mm] = parseFloat(columns[28]);
            pt[Conditions.AEROSOL_OPTICAL_DEPTH_thousandths] =
                parseFloat(columns[29]);
            pt[Conditions.SNOW_DEPTH_cm] = parseFloat(columns[30]);
            pt[Conditions.DAYS_SINCE_LAST_SNOWFALL] = parseFloat(columns[31]);
            pt[Conditions.ALBEDO] = parseFloat(columns[32]);
            pt[Conditions.LIQUID_PRECIPITATION_DEPTH_mm] =
                parseFloat(columns[33]);
            pt[Conditions.LIQUID_PRECIPITATION_QUANTITY_hr] =
                parseFloat(columns[34]);
            pts.push(pt);
        }
        var seriesMetaMap:Object = PlugInfo.knownSeriesMetaData();
        var headerMetaMap:Object = PlugInfo.knownHeaderMetaData();
        return (new DataSetBuilder())
          .withHeader(hdr)
          .withPoints(PersistentVector.create(pts))
          .withSeriesMetaData(seriesMetaMap)
          .withHeaderMetaData(headerMetaMap)
          .build();
    }
}
}