/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package plugins {
import dse.MetaData;

import utils.MapUtils;

public class PlugInfo {
    /* TO ADD A NEW PLUGIN, ADD THE CLASS NAME TO THE LIST BELOW */
    public static const KNOWN_PLUGINS:Array = [
        Clouds,
        Conditions,
        DateAndTime,
        DOE2Specific,
        EPWSpecific,
        GroundTemperatures,
        Location,
        Psych,
        Solar,
        Wind];

    public static const DISPLAY_PLUGINS:Array = [Psych, Solar, Wind, Clouds];

    public static function
    listPlugins():String {
        var out:Array = [];
        for each (var item:Object in KNOWN_PLUGINS) {
            out.push(item.PLUGIN_NAME);
        }
        return out.join(", ");
    }

    private static var _SERIES_META_DATA:Object = null;

    public static function
    knownSeriesMetaData():Object {
        if (_SERIES_META_DATA == null) {
            var md:Object = new Object();
            for each (var item:Object in KNOWN_PLUGINS) {
                if (item.hasOwnProperty('SERIES_META_DATA')) {
                    md = MapUtils.merge(md, item.SERIES_META_DATA);
                }
            }
            _SERIES_META_DATA = md;
        }
        return _SERIES_META_DATA;
    }

    public static function seriesMetaForModules(mods:Array):Object {
      var md:Object = new Object();
      for each (var item:Object in mods) {
        if (item.hasOwnProperty('SERIES_META_DATA')) {
          md = MapUtils.merge(md, item.SERIES_META_DATA);
        }
      }
      return md;
    }

    private static var _HEADER_META_DATA:Object = null;

    public static function knownHeaderMetaData():Object {
      if (_HEADER_META_DATA == null) {
        var md:Object = new Object();
        for each (var item:Object in KNOWN_PLUGINS) {
          if (item.hasOwnProperty('HEADER_META_DATA')) {
            md = MapUtils.merge(md, item.HEADER_META_DATA);
          }
        }
        _HEADER_META_DATA = md;
      }
      return _HEADER_META_DATA;
    }

    public static function
    selectSeriesMeta(names:Array):Object {
        var MD:Object = knownSeriesMetaData();
        var out:Object = new Object();
        for each (var name:String in names) {
            if (MD.hasOwnProperty(name)) {
                out[name] = MD[name];
            } else {
                // warning?
            }
        }
        return out;
    }

    private static var _DERIVATION_FUNCS:Object = null;

    public static function
    knownDerivationFuncs():Object {
        if (_DERIVATION_FUNCS == null) {
            var dfs:Object = new Object();
            for each (var item:Object in KNOWN_PLUGINS) {
                if (item.hasOwnProperty('DERIVATION_FUNCS')) {
                    dfs = MapUtils.merge(dfs, item.DERIVATION_FUNCS);
                }
            }
            _DERIVATION_FUNCS = dfs;
        }
        return _DERIVATION_FUNCS;
    }

    private static var _MINS:Object = null;

    public static function
    knownMins():Object {
        if (_MINS == null) {
            var mins:Object = new Object();
            for each (var item:Object in KNOWN_PLUGINS) {
                if (item.hasOwnProperty('MINS')) {
                    mins = MapUtils.merge(mins, item.MINS);
                }
            }
            _MINS = mins;
        }
        return _MINS;
    }

    private static var _MAXS:Object = null;

    public static function
    knownMaxs():Object {
        if (_MAXS == null) {
            var maxs:Object = new Object();
            for each (var item:Object in KNOWN_PLUGINS) {
                if (item.hasOwnProperty('MAXS')) {
                    maxs = MapUtils.merge(maxs, item.MAXS);
                }
            }
            _MAXS = maxs;
        }
        return _MAXS;
    }

    public static function allDisplayableNumericSeries():Array {
      var seriesMeta:Object = seriesMetaForModules(DISPLAY_PLUGINS);
      var attrs:Array = MapUtils.keys(seriesMeta);
      var numericSeries:Array = [];
      for each (var attr:String in attrs) {
        var md:MetaData = seriesMeta[attr] as MetaData;
        if (md.dataType == MetaData.DATA_TYPE__NUMBER) {
          numericSeries.push(attr);
        }
      }
      numericSeries.sort();
      return numericSeries;
    }

    public static function
    testForConformance():void {
        var report:Function = function(idx:int, key:String):void {
            trace('item #', idx, '::', key, 'is missing');
        }
        for (var idx:int=0; idx < KNOWN_PLUGINS.length; idx++) {
            var item:Object = KNOWN_PLUGINS[idx];
            if (!item.hasOwnProperty('PLUGIN_NAME')) {
                report(idx, 'PLUGIN_NAME');
            }
            if (!item.hasOwnProperty('SERIES_META_DATA')) {
                report(idx, 'SERIES_META_DATA');
            }
            if (!item.hasOwnProperty('DERIVATION_FUNCS')) {
                report(idx, 'DERIVATION_FUNCS');
            }
            if (!item.hasOwnProperty('MINS')) {
                report(idx, 'MINS');
            }
            if (!item.hasOwnProperty('MAXS')) {
                report(idx, 'MAXS');
            }
        }
    }
}
}