/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package plugins {
import dse.ConstraintFunctions;
import dse.MetaData;

import units.NoUnits;

public class EPWSpecific {
    public static const PLUGIN_NAME:String = 'epwSpecific';

    // ** OTHER HEADER INFO *
    public static const DESIGN_CONDITIONS:String = 'designConditions';
    public static const TYPICAL_EXTREME_PERIODS:String =
        'typicalExtremePeriods';
    public static const HOLIDAYS_AND_DAYLIGHT_SAVINGS:String =
        'holidaysAndDaylightSavings';
    public static const COMMENTS_01:String = 'comments01';
    public static const COMMENTS_02:String = 'comments02';
    public static const DATA_PERIODS:String = 'dataPeriods';

    // Data Series Tags
    public static const DATA_SOURCE_AND_UNCERTAINTY_FLAGS:String =
        'dataSourceAndUncertaintyFlags';
    // ** ILLUMINANCE *******
    // These *could* be pulled into their own file if we anticipate their
    // use outside of populating EPW files...
    public static const
    GLOBAL_HORIZONTAL_ILLUMINANCE_lux:String = 'globalHorizIllum_lux';
    public static const
    BEAM_NORMAL_ILLUMINANCE_lux:String = 'beamNormIllum_lux';
    public static const
    DIFFUSE_HORIZONTAL_ILLUMINANCE_lux:String = 'diffuseHorizIllum_lux';
    public static const
    ZENITH_LUMINANCE_Cd__m2:String = 'zenithLuminance_Cd__m2';

    public static const CONSTRAINT_FUNCITON:Function =
        ConstraintFunctions.makeAlwaysGoodConstraintFunction();

    public static const SETTING_SETS:Array = [];

    public static const SERIES_META_DATA:Object = new Object();
    SERIES_META_DATA[DATA_SOURCE_AND_UNCERTAINTY_FLAGS] = new MetaData(
        DATA_SOURCE_AND_UNCERTAINTY_FLAGS,
        "Data Source and Uncertainty Flags",
        NoUnits.NONE,
        NoUnits.QUANTITY_OF_MEASURE,
        MetaData.makeValueGetter(DATA_SOURCE_AND_UNCERTAINTY_FLAGS),
        MetaData.DATA_TYPE__STRING,
        [], CONSTRAINT_FUNCITON, {}, false, [NoUnits.NONE]);
    SERIES_META_DATA[GLOBAL_HORIZONTAL_ILLUMINANCE_lux] = new MetaData(
        GLOBAL_HORIZONTAL_ILLUMINANCE_lux,
        "Global Horizontal Illuminance",
        NoUnits.NONE,
        NoUnits.QUANTITY_OF_MEASURE,
        MetaData.makeValueGetter(GLOBAL_HORIZONTAL_ILLUMINANCE_lux),
        MetaData.DATA_TYPE__NUMBER,
        [], CONSTRAINT_FUNCITON, {}, true, [NoUnits.NONE]);
    SERIES_META_DATA[BEAM_NORMAL_ILLUMINANCE_lux] = new MetaData(
        BEAM_NORMAL_ILLUMINANCE_lux,
        "Beam Normal Illuminance",
        NoUnits.NONE,
        NoUnits.QUANTITY_OF_MEASURE,
        MetaData.makeValueGetter(BEAM_NORMAL_ILLUMINANCE_lux),
        MetaData.DATA_TYPE__NUMBER,
        [], CONSTRAINT_FUNCITON, {}, true, [NoUnits.NONE]);
    SERIES_META_DATA[DIFFUSE_HORIZONTAL_ILLUMINANCE_lux] = new MetaData(
        DIFFUSE_HORIZONTAL_ILLUMINANCE_lux,
        "Diffuse Horizontal Illuminance",
        NoUnits.NONE,
        NoUnits.QUANTITY_OF_MEASURE,
        MetaData.makeValueGetter(DIFFUSE_HORIZONTAL_ILLUMINANCE_lux),
        MetaData.DATA_TYPE__NUMBER,
        [], CONSTRAINT_FUNCITON, {}, true, [NoUnits.NONE]);
    SERIES_META_DATA[ZENITH_LUMINANCE_Cd__m2] = new MetaData(
        ZENITH_LUMINANCE_Cd__m2,
        "Zenith Luminance",
        NoUnits.NONE,
        NoUnits.QUANTITY_OF_MEASURE,
        MetaData.makeValueGetter(ZENITH_LUMINANCE_Cd__m2),
        MetaData.DATA_TYPE__NUMBER,
        [], CONSTRAINT_FUNCITON, {}, true, [NoUnits.NONE]);

    public static const HEADER_META_DATA:Object = new Object();
    HEADER_META_DATA[DESIGN_CONDITIONS] = new MetaData(
      DESIGN_CONDITIONS,
      "Design Conditions",
      NoUnits.NONE,
      NoUnits.QUANTITY_OF_MEASURE,
      function(h:Object, pt:Object):String { return h[DESIGN_CONDITIONS]; },
      MetaData.DATA_TYPE__STRING,
      [],
      null,
      null,
      true,
      NoUnits.AVAILABLE_UNITS);
    HEADER_META_DATA[TYPICAL_EXTREME_PERIODS] = new MetaData(
      TYPICAL_EXTREME_PERIODS,
      "Typical Extreme Periods",
      NoUnits.NONE,
      NoUnits.QUANTITY_OF_MEASURE,
      function(h:Object, pt:Object):String {
        return h[TYPICAL_EXTREME_PERIODS];
      },
      MetaData.DATA_TYPE__STRING,
      [],
      null,
      null,
      true,
      NoUnits.AVAILABLE_UNITS);
    HEADER_META_DATA[HOLIDAYS_AND_DAYLIGHT_SAVINGS] = new MetaData(
      HOLIDAYS_AND_DAYLIGHT_SAVINGS,
      "Holidays and Daylight Savings",
      NoUnits.NONE,
      NoUnits.QUANTITY_OF_MEASURE,
      function(h:Object, pt:Object):String {
        return h[HOLIDAYS_AND_DAYLIGHT_SAVINGS];
      },
      MetaData.DATA_TYPE__STRING,
      [],
      null,
      null,
      true,
      NoUnits.AVAILABLE_UNITS);
    HEADER_META_DATA[COMMENTS_01] = new MetaData(
      COMMENTS_01,
      "Comments (Line 1)",
      NoUnits.NONE,
      NoUnits.QUANTITY_OF_MEASURE,
      function(h:Object, pt:Object):String {
        return h[COMMENTS_01];
      },
      MetaData.DATA_TYPE__STRING,
      [],
      null,
      null,
      true,
      NoUnits.AVAILABLE_UNITS);
    HEADER_META_DATA[COMMENTS_02] = new MetaData(
      COMMENTS_02,
      "Comments (Line 2)",
      NoUnits.NONE,
      NoUnits.QUANTITY_OF_MEASURE,
      function(h:Object, pt:Object):String {
        return h[COMMENTS_02];
      },
      MetaData.DATA_TYPE__STRING,
      [],
      null,
      null,
      true,
      NoUnits.AVAILABLE_UNITS);
    HEADER_META_DATA[DATA_PERIODS] = new MetaData(
      DATA_PERIODS,
      "Data Periods",
      NoUnits.NONE,
      NoUnits.QUANTITY_OF_MEASURE,
      function(h:Object, pt:Object):String {
        return h[DATA_PERIODS];
      },
      MetaData.DATA_TYPE__STRING,
      [],
      null,
      null,
      true,
      NoUnits.AVAILABLE_UNITS);

    public static const DERIVATION_FUNCS:Object = {};
    public static const MINS:Object = {};
    public static const MAXS:Object = {};
}
}