/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package plugins {
import mx.collections.ArrayCollection;

import dse.ConstraintFunctions;
import dse.DataPoint;
import dse.DataSet;
import dse.DateTime;
import dse.MetaData;

import pdt.IPVec;

import units.AngleUnits;
import units.IrradiationUnits;
import units.NoUnits;
import units.UnitConversions;

import utils.AdvMath;
import utils.FP;
import utils.FormatUtils;
import utils.MapUtils;
import utils.OptUtils;
import utils.Set;

/*
TODO: Go back through this module and the functions and be sure we're handling
local time to solar time correctly. I believe many of the solar angles are
being inadvertently calculated off of local time! THAT's INCORRECT!

References
=============

State Point: Data Series
* irradiance, global, horizontal surface (W/m2)
* irradiance, beam, normal to surface (W/m2)
* elapsed time since reference date/time (hrs) -- reference only
State Point: HEADER
* reference date/time
* time-zone of local site (hrs wrt UTC)
* local site latitude (degrees)
* local site longitude (degrees) (West -, East +: -180 <= Long <= 180 degrees)
* local site elevation (meters)

See [http://rredc.nrel.gov/solar/glossary/gloss_g.html] for discussion of the
state equation that ties global horizontal, diffuse horizontal, and beam normal
together.

Based on WŁODARCZYK and NOWAK (2009), the Reindl model, the Gueymard model,
the Perez model, the Koronakis model and the Muneer model offered the best
correlation and lowest errors as compared to measured results in Poland.
Since Duffie and Beckman (1991) also recommend the Reindl model, and since
the Reindl model has fewer required calculations and coefficients, we will
use this model as the basis for solar relations.

The solar values on the weather file are average values over the hour. For
interpolation of hourly weather data (i.e., when the specified timestep is
greater than 1), the average value is assumed to be the value at the midpoint
of the hour. The reported values in the output are totals for each reporting
period. So, hourly reported values will not match the original values in the
weather file, but the total solar for a day should agree.
Reference (Ellis, Liesen, Pedersen, 2003)

According to Wilcox and Marrian (2008) and Wilcox (2007), the solar information
is the "mean of all 1-minute sunup values for the 60-minute period ending at
the timestamp". However, in looking at the TMY Tucson data for 2005, it appears
that the data is best fit if assumed that the insolation is integrated from
1/2 hour before the time-stamp to 1/2 after.

REFERENCES
==========
D. WŁODARCZYK, H. NOWAK (2009) "Statistical analysis of solar radiation
models onto inclined planes for climatic conditions of Lower Silesia in
Poland". Archives of Civil and Mechanical Engineering. Vol IX. No. 2.

Duffie and Beckman (1991). "Solar Engineering of Thermal Processes". Second
Edition. John Wiley and Sons. pp 102.

Wilcox, S. et al. (2007). "National Solar Radiation Database
1991–2005 Update: User’s Manual". NREL Technical Report NREL/TP-581-41364.
Available on-line at:
http://www.nrel.gov/docs/fy07osti/41364.pdf

Wilcox, S. and Marion, W. (2008). "Users Manual for TMY3 Data Sets". NREL
Technical Report NREL/TP-581-43156. Available from:
http://www.nrel.gov/docs/fy08osti/43156.pdf

Ellis, P.G., Liesen, R.J. and Pedersen, C.O. 2003. “Energyplus Experimental
Data Validation Work: Development and Validation of the Unvented Trombe Wall
Model and Other Heat Balance Components”, CERL Final Report DACA42-01-D-0004,
Task 3.

Griffith, B., Deru, M., Torcellini, P., Ellis, P. 2005. "Analysis of the Energy
Performance of the Chesapeake Bay Foundation’s Philip Merrill Environmental
Center". page 116, bottom., eqs 4 and 5.

Muneer, T. (2004). "Solar Radiation and Daylight Models". Book.
*/
public class Solar {
    public static const PLUGIN_NAME:String = "solar";
    public static const D2R:Function = UnitConversions.deg_to_rads;
    public static const R2D:Function = UnitConversions.rad_to_degs;
    public static const degPerHour:Number = 15.0;
    public static const degPerMinute:Number = degPerHour * (1.0 / 60.0);
    public static const SOLAR_CONSTANT_W__m2:Number = 1367.0;

    public static const
    STEFAN_BOLTZMANN_CONST_W__m2_K4:Number = 5.67037321e-8;

    public static const
    GLOBAL_HORIZ_Wh__m2:String = 'solarGlobalHoriz_Wh__m2';
    public static const
    BEAM_NORMAL_Wh__m2:String = 'solarBeamNormal_Wh__m2';
    public static const
    DIFFUSE_HORIZONTAL_Wh__m2:String = 'solarDiffuseHoriz_Wh__m2';
    public static const
    EXTRATERRESTRIAL_HORIZ_Wh__m2:String = 'solarExtHoriz_Wh__m2';
    public static const EXTRATERRESTRIAL_NORMAL_Wh__m2:String =
        'solarExtNormal_Wh__m2';
    public static const IR_FROM_SKY_Wh__m2:String =
        'horizInfraredSolarRadFromSky_Wh__m2';
    public static const HOUR_ANGLE_deg:String = 'solarHourAngle_deg';
    public static const ZENITH_ANGLE_deg:String = 'zenithAngle_deg';
    public static const ELEVATION_ANGLE_deg:String = 'elevationAngle_deg';
    public static const AZIMUTH_ANGLE_FROM_NORTH_deg:String =
        'solarAzimuthFromNorth_deg';
    public static const AZIMUTH_ANGLE_FROM_SOUTH_deg:String =
        'solarAzimuthFromSouth_deg';
    public static const DECLINATION_deg:String = 'solarDeclination_deg';
    public static const HOURLY_AVERAGED_COSZ:String =
        'hourlyAveragedCosZenAng';

    public static const STATE_NAMES:Object = Set.make([
        BEAM_NORMAL_Wh__m2, DIFFUSE_HORIZONTAL_Wh__m2,
        DateAndTime.ELAPSED_TIME_hrs]);

    public static const SETTING_SETS:Array = [
        Set.make([
            DateAndTime.ELAPSED_TIME_hrs,
            GLOBAL_HORIZ_Wh__m2,
            BEAM_NORMAL_Wh__m2]),
        Set.make([
            DateAndTime.ELAPSED_TIME_hrs,
            GLOBAL_HORIZ_Wh__m2,
            DIFFUSE_HORIZONTAL_Wh__m2]),
        Set.make([
            DateAndTime.ELAPSED_TIME_hrs,
            BEAM_NORMAL_Wh__m2,
            DIFFUSE_HORIZONTAL_Wh__m2])];

    public static function CONSTRAINT_FUNC(hdr:Object, vals:Object):Number
    {
      // Ig_h = Ib_n * cos(theta) + Id_h
      // Note: for the below calcs, we're relying on the fact
      // that X Wh/m2 over one hour is the same as X W/m2
      var err:Number = 0.0;
      var Id_h:Number = vals[DIFFUSE_HORIZONTAL_Wh__m2];
      var Ib_n:Number = vals[BEAM_NORMAL_Wh__m2];
      var solarScale:Number = 100.0 / SOLAR_CONSTANT_W__m2;
      if (Id_h > SOLAR_CONSTANT_W__m2) {
        err += solarScale * (Id_h - SOLAR_CONSTANT_W__m2);
      }
      else if (Id_h < 0.0) {
        err += solarScale * (0.0 - Id_h);
      }
      if (Ib_n > SOLAR_CONSTANT_W__m2) {
        err += solarScale * (Ib_n - SOLAR_CONSTANT_W__m2);
      }
      else if (Ib_n < 0.0) {
        err += solarScale * (0.0 - Ib_n);
      }
      var dt:DateTime =
        DateAndTime.headerAndValueObjectsToDateTime(hdr, vals);
      var tz:Number = hdr[DateAndTime.TIME_ZONE] as Number;
      var lat_deg:Number = hdr[Location.LATITUDE_deg] as Number;
      var long_deg:Number = hdr[Location.LONGITUDE_deg] as Number;
      var long_360W_deg:Number = Solar.longToLong360W(long_deg);
      var Ig_h:Number = irradianceGlobalHorizontal_W__m2(
        Id_h, Ib_n, dt, tz, lat_deg, long_360W_deg);
      if (Ig_h > SOLAR_CONSTANT_W__m2) {
        err += solarScale * (Ig_h - SOLAR_CONSTANT_W__m2);
      }
      else if (Ig_h < 0.0) {
        err += solarScale * (0.0 - Ig_h);
      }
      return err;
    }

    public static const MAX_GLOBAL_HORIZ_Wh__m2:Number = SOLAR_CONSTANT_W__m2;
    public static const MAX_BEAM_NORMAL_Wh__m2:Number = SOLAR_CONSTANT_W__m2;
    public static const MAX_DIFFUSE_HORIZ_Wh__m2:Number = SOLAR_CONSTANT_W__m2;
    public static const MAXS:Object = new Object();
    MAXS[GLOBAL_HORIZ_Wh__m2] = MAX_GLOBAL_HORIZ_Wh__m2;
    MAXS[BEAM_NORMAL_Wh__m2] = MAX_BEAM_NORMAL_Wh__m2;
    MAXS[DIFFUSE_HORIZONTAL_Wh__m2] = MAX_DIFFUSE_HORIZ_Wh__m2;

    public static const MIN_GLOBAL_HORIZ_Wh__m2:Number = 0.0;
    public static const MIN_BEAM_NORMAL_Wh__m2:Number = 0.0;
    public static const MIN_DIFFUSE_HORIZ_Wh__m2:Number = 0.0;
    public static const MINS:Object = new Object();
    MINS[GLOBAL_HORIZ_Wh__m2] = MIN_GLOBAL_HORIZ_Wh__m2;
    MINS[BEAM_NORMAL_Wh__m2] = MIN_BEAM_NORMAL_Wh__m2;
    MINS[DIFFUSE_HORIZONTAL_Wh__m2] = MIN_DIFFUSE_HORIZ_Wh__m2;

    public static const DEFAULT_BEAM_NORMAL_W__m2:Number = 0.0;
    public static const DEFAULT_DIFFUSE_HORIZ_W__m2:Number = 0.0;
    public static const DEFAULTS:Object = new Object();
    DEFAULTS[BEAM_NORMAL_Wh__m2] = DEFAULT_BEAM_NORMAL_W__m2;
    DEFAULTS[DIFFUSE_HORIZONTAL_Wh__m2] = DEFAULT_DIFFUSE_HORIZ_W__m2;

    public static const DERIVATION_FUNCS:Object = new Object();
    DERIVATION_FUNCS[GLOBAL_HORIZ_Wh__m2] =
      deriveIrradianceGlobalHorizontal_W__m2_alt;
      //deriveX(deriveIrradianceGlobalHorizontal_W__m2);
    DERIVATION_FUNCS[HOUR_ANGLE_deg] = deriveX(deriveSolarHourAngle_deg);
    DERIVATION_FUNCS[AZIMUTH_ANGLE_FROM_NORTH_deg] =
        deriveX(deriveAzimuthAngleFromNorth_deg);
    DERIVATION_FUNCS[AZIMUTH_ANGLE_FROM_SOUTH_deg] =
        deriveX(deriveAzimuthAngleFromSouth_deg);
    DERIVATION_FUNCS[DECLINATION_deg] = deriveX(deriveDeclination_deg);
    DERIVATION_FUNCS[ZENITH_ANGLE_deg] = deriveX(deriveZenithAngle_deg);
    DERIVATION_FUNCS[ELEVATION_ANGLE_deg] = deriveX(deriveElevationAngle_deg);
    DERIVATION_FUNCS[EXTRATERRESTRIAL_HORIZ_Wh__m2] =
        deriveX(deriveIrradianceExtHoriz_W__m2);
    DERIVATION_FUNCS[EXTRATERRESTRIAL_NORMAL_Wh__m2] =
        deriveX(deriveIrradianceExtNormal_W__m2);
    DERIVATION_FUNCS[HOURLY_AVERAGED_COSZ] = deriveX(deriveHourlyAveragedCosZ);
    // Add Sunset/Sunrise, Solar Time, etc.

    public static function
    deriveX(f:Function):Function {
        return function(hdr:Object, vals:Object):Number {
            var allVals:Object = MapUtils.merge(hdr, vals);
            //ObjectUtils.traceObject(allVals, 'allVals');
            return f(allVals);
        };
    }

    public static const SERIES_META_DATA:Object = new Object();
    SERIES_META_DATA[GLOBAL_HORIZ_Wh__m2] = new MetaData(
        GLOBAL_HORIZ_Wh__m2,
        "Global Solar",
        IrradiationUnits.WATT_HOURS_PER_METER2,
        IrradiationUnits.QUANTITY_OF_MEASURE,
        DERIVATION_FUNCS[GLOBAL_HORIZ_Wh__m2],
        MetaData.DATA_TYPE__NUMBER,
        [[DateAndTime.ELAPSED_TIME_hrs, DIFFUSE_HORIZONTAL_Wh__m2],
         [DateAndTime.ELAPSED_TIME_hrs, BEAM_NORMAL_Wh__m2],
         [DateAndTime.ELAPSED_TIME_hrs]],
        CONSTRAINT_FUNC,
        STATE_NAMES,
        true,
        IrradiationUnits.AVAILABLE_UNITS,
        NaN,
        ["Diffuse Solar", "Normal Solar", "Beam Fraction of Total"]);
    SERIES_META_DATA[GLOBAL_HORIZ_Wh__m2].updater = set_Ig_holding_X;
    SERIES_META_DATA[BEAM_NORMAL_Wh__m2] = new MetaData(
        BEAM_NORMAL_Wh__m2,
        "Normal Solar",
        IrradiationUnits.WATT_HOURS_PER_METER2,
        IrradiationUnits.QUANTITY_OF_MEASURE,
        MetaData.makeValueGetterWithDefault(BEAM_NORMAL_Wh__m2, 0.0),
        MetaData.DATA_TYPE__NUMBER,
        [[DateAndTime.ELAPSED_TIME_hrs, DIFFUSE_HORIZONTAL_Wh__m2],
         [DateAndTime.ELAPSED_TIME_hrs, GLOBAL_HORIZ_Wh__m2]],
        CONSTRAINT_FUNC,
        STATE_NAMES,
        true,
        IrradiationUnits.AVAILABLE_UNITS,
        NaN,
        ["Diffuse Solar", "Global Solar"]);
    SERIES_META_DATA[DIFFUSE_HORIZONTAL_Wh__m2] = new MetaData(
        DIFFUSE_HORIZONTAL_Wh__m2,
        "Diffuse Solar",
        IrradiationUnits.WATT_HOURS_PER_METER2,
        IrradiationUnits.QUANTITY_OF_MEASURE,
        MetaData.makeValueGetterWithDefault(DIFFUSE_HORIZONTAL_Wh__m2, 0.0),
        MetaData.DATA_TYPE__NUMBER,
        [[DateAndTime.ELAPSED_TIME_hrs, BEAM_NORMAL_Wh__m2],
         [DateAndTime.ELAPSED_TIME_hrs, GLOBAL_HORIZ_Wh__m2]],
        CONSTRAINT_FUNC,
        STATE_NAMES,
        true,
        IrradiationUnits.AVAILABLE_UNITS,
        NaN,
        ["Normal Solar", "Global Solar"]);
    SERIES_META_DATA[EXTRATERRESTRIAL_HORIZ_Wh__m2] = new MetaData(
        EXTRATERRESTRIAL_HORIZ_Wh__m2,
        "Extraterrestrial Horizontal Solar",
        IrradiationUnits.WATT_HOURS_PER_METER2,
        IrradiationUnits.QUANTITY_OF_MEASURE,
        MetaData.makeValueGetterWithDefault(
          EXTRATERRESTRIAL_HORIZ_Wh__m2, 0.0),
        MetaData.DATA_TYPE__NUMBER,
        [],
        ConstraintFunctions.makeAlwaysGoodConstraintFunction(),
        {},
        false,
        IrradiationUnits.AVAILABLE_UNITS);
    SERIES_META_DATA[EXTRATERRESTRIAL_NORMAL_Wh__m2] = new MetaData(
        EXTRATERRESTRIAL_NORMAL_Wh__m2,
        "Extraterrestrial Normal Solar",
        IrradiationUnits.WATT_HOURS_PER_METER2,
        IrradiationUnits.QUANTITY_OF_MEASURE,
        MetaData.makeValueGetterWithDefault(
          EXTRATERRESTRIAL_NORMAL_Wh__m2, 0.0),
        MetaData.DATA_TYPE__NUMBER,
        [],
        ConstraintFunctions.makeAlwaysGoodConstraintFunction(),
        {},
        false,
        IrradiationUnits.AVAILABLE_UNITS);
    SERIES_META_DATA[IR_FROM_SKY_Wh__m2] = new MetaData(
        IR_FROM_SKY_Wh__m2,
        "Infrared Sky Radiation",
        IrradiationUnits.WATT_HOURS_PER_METER2,
        IrradiationUnits.QUANTITY_OF_MEASURE,
        MetaData.makeValueGetterWithDefault(IR_FROM_SKY_Wh__m2, 0.0),
        MetaData.DATA_TYPE__NUMBER,
        [],
        ConstraintFunctions.makeAlwaysGoodConstraintFunction(),
        {},
        false,
        IrradiationUnits.AVAILABLE_UNITS);
    SERIES_META_DATA[AZIMUTH_ANGLE_FROM_NORTH_deg] = new MetaData(
        AZIMUTH_ANGLE_FROM_NORTH_deg,
        "Solar Azimuth Angle from North",
        AngleUnits.DEGREES,
        AngleUnits.QUANTITY_OF_MEASURE,
        DERIVATION_FUNCS[AZIMUTH_ANGLE_FROM_NORTH_deg],
        MetaData.DATA_TYPE__NUMBER,
        [],
        ConstraintFunctions.makeAlwaysGoodConstraintFunction(),
        {},
        false,
        AngleUnits.AVAILABLE_UNITS);
    SERIES_META_DATA[AZIMUTH_ANGLE_FROM_SOUTH_deg] = new MetaData(
        AZIMUTH_ANGLE_FROM_SOUTH_deg,
        "Solar Azimuth Angle from South",
        AngleUnits.DEGREES,
        AngleUnits.QUANTITY_OF_MEASURE,
        DERIVATION_FUNCS[AZIMUTH_ANGLE_FROM_SOUTH_deg],
        MetaData.DATA_TYPE__NUMBER,
        [],
        ConstraintFunctions.makeAlwaysGoodConstraintFunction(),
        {},
        false,
        AngleUnits.AVAILABLE_UNITS);
    SERIES_META_DATA[HOUR_ANGLE_deg] = new MetaData(
        HOUR_ANGLE_deg,
        "Solar Hour Angle",
        AngleUnits.DEGREES,
        AngleUnits.QUANTITY_OF_MEASURE,
        DERIVATION_FUNCS[HOUR_ANGLE_deg],
        MetaData.DATA_TYPE__NUMBER,
        [],
        ConstraintFunctions.makeAlwaysGoodConstraintFunction(),
        {},
        false,
        AngleUnits.AVAILABLE_UNITS);
    SERIES_META_DATA[ZENITH_ANGLE_deg] = new MetaData(
        ZENITH_ANGLE_deg,
        "Solar Zenith Angle",
        AngleUnits.DEGREES,
        AngleUnits.QUANTITY_OF_MEASURE,
        DERIVATION_FUNCS[ZENITH_ANGLE_deg],
        MetaData.DATA_TYPE__NUMBER,
        [],
        ConstraintFunctions.makeAlwaysGoodConstraintFunction(),
        {},
        false,
        AngleUnits.AVAILABLE_UNITS);
    SERIES_META_DATA[ELEVATION_ANGLE_deg] = new MetaData(
      ELEVATION_ANGLE_deg,
      "Solar Elevation Angle",
      AngleUnits.DEGREES,
      AngleUnits.QUANTITY_OF_MEASURE,
      DERIVATION_FUNCS[ELEVATION_ANGLE_deg],
      MetaData.DATA_TYPE__NUMBER,
      [],
      ConstraintFunctions.makeAlwaysGoodConstraintFunction(),
      {},
      false,
      AngleUnits.AVAILABLE_UNITS);
    SERIES_META_DATA[HOURLY_AVERAGED_COSZ] = new MetaData(
        HOURLY_AVERAGED_COSZ,
        "Cosine of Zenith",
        NoUnits.NONE,
        NoUnits.QUANTITY_OF_MEASURE,
        DERIVATION_FUNCS[HOURLY_AVERAGED_COSZ],
        MetaData.DATA_TYPE__NUMBER,
        [],
        ConstraintFunctions.makeAlwaysGoodConstraintFunction(),
        {},
        false,
        NoUnits.AVAILABLE_UNITS);

    public static function
    makeDataPoint(Idh_W__m2:Number, Ibn_W__m2:Number):Object {
        if (Idh_W__m2 < 0.0) {
            throw new Error("Diffuse horizontal irradiance cannot be < 0.0");
        }
        if (Ibn_W__m2 < 0.0) {
            throw new Error("Beam normal irradiance cannot be < 0.0");
        }
        return MapUtils.makeObject([
            DIFFUSE_HORIZONTAL_Wh__m2, Idh_W__m2,
            BEAM_NORMAL_Wh__m2, Ibn_W__m2]);
    }

    /* timeZoneToStandardMeridian
    - TZ :: Number -- the time zone (hours adjustment to UTC/GMT)
    RETURNS:
    the standard meridian for the given time-zone in degrees */
    public static function
    timeZoneToLocalStandardMeridian_deg(TZ_hrs:Number):Number {
        return TZ_hrs * degPerHour;
    }

    /* standardMeridianToTimeZone
    - sm_deg :: Number -- standard meridian in degrees
    RETURNS:
    time-zone relative to GMT in hours */
    public static function
    localStandardMeridianToTimeZone_hrs(LSM_deg:Number):Number {
        return LSM_deg / degPerHour;
    }

    /* universalTime_hrs
    - dt :: DateTime -- the local date/time for the site
    - TZ_hrs :: Number -- time-zone relative to GMT (hour adjustment to GMT)
    RETURNS:
    - the universal time in hours since start of day */
    public static function
    universalTime_hrs(dt:DateTime, TZ_hrs:Number):Number {
        return dt.hourOfDay + dt.minuteOfHour / 60.0
            + dt.secondOfMinute / 3600.0 - TZ_hrs;
    }

    /* _t (meant for internal use only)
    - dt :: DateTime -- the Julian Date & Local Time for the calculation
    - TZ :: Number -- the time-zone in hours relative to GMT
    RETURNS t */
    public static function
    _t(dt:DateTime, TZ_hrs:Number):Number {
        var y:Number; // IYR1
        var m:Number; // IMT1
        if (dt.month > 2) {
            y = Number(dt.year);
            m = Number(dt.month) - 3.0;
        } else {
            y = Number(dt.year) - 1.0;
            m = Number(dt.month) + 9.0;
        }
        var Day:Number = Number(dt.dayOfMonth);
        var h:Number = Number(dt.hourOfDay);
        var min:Number = Number(dt.minuteOfHour);
        var s:Number = Number(dt.secondOfMinute);
        var UT:Number = universalTime_hrs(dt, TZ_hrs);
        return (
            (UT / 24.0) + Day + AdvMath.integerPart(30.6 * m + 0.5) +
            AdvMath.integerPart(365.25 * (y - 1976.0)) - 8707.5) / 36525.0;
    }

    /* _G_deg (meant for internal use only)
    - t :: Number -- the t calculated by _t
    RETURNS:
    - G in degrees */
    public static function _G_deg(t:Number):Number {
        return AdvMath.scaleToN(357.528 + 35999.05 * t, 360.0);
    }

    /* _C (meant for internal use only)
    - G_deg :: Number -- the G parameter from Yallop
    RETURNS:
    - C */
    public static function
    _C(G_deg:Number):Number {
        return 1.915 * Math.sin(D2R(G_deg)) +
            0.020 * Math.sin(D2R(2.0 * G_deg));
    }

    /* _L_deg (meant for internal use only)
    - t :: Number -- the Yallop t parameter
    - C :: Number -- the Yallop C parameter
    RETURNS:
    - the Yallop L parameter in degrees */
    public static function
    _L_deg(t:Number, C:Number):Number {
        return AdvMath.scaleToN(280.460 + 36000.770 * t + C, 360.0);
    }

    /* _alpha_deg (meant for internal use only)
    - L_deg :: Number -- the Yallop L parameter in degrees
    RETURNS:
    - the alpha paramter in degrees. */
    public static function
    _alpha_deg(L_deg:Number):Number {
        return L_deg - 2.466 * Math.sin(D2R(2.0 * L_deg)) +
            0.053 * Math.sin(D2R(4.0 * L_deg));
    }

    /* _greenwichHourAngle_deg (meant for internal use only)
    - UT_hrs :: Number - the universal time hour of the day (hours)
    - C :: Number - the Yallop C parameter
    - L_deg :: Number - the Yallop L parameter
    RETURNS:
    - the hour angle with respect to Universal Time
      (Greenwich Mean Time or GMT) in the range 0 to 360 degrees. */
    public static function
    _greenwichHourAngle_deg(UT_hrs:Number, C:Number, L_deg:Number,
                            alpha_deg:Number):Number {
        return AdvMath.scaleToN(
            15.0 * UT_hrs - 180.0 - C + L_deg - alpha_deg, 360.0);
    }

    /* _eps_deg (meant for internal use only)
    t :: Number -- the t parameter from Yallop
    RETURNS:
    the "Obliquity of the Ecliptic" in degrees. */
    public static function
    _eps_deg(t:Number):Number {
        return 23.4393 - 0.013 * t;
    }

    /* _declination_deg (meant for internal use only)
    - eps_deg :: Number -- obliquity of the ecliptic in degrees
    - alpha_deg :: Number -- the Yallop alpha parameter in degrees
    RETURNS:
    solar declination angle in degrees */
    public static function
    _declination_deg(eps_deg:Number, alpha_deg:Number):Number {
        return R2D(
            Math.atan(Math.tan(D2R(eps_deg)) * Math.sin(D2R(alpha_deg))));
    }

    /* declination_deg
    "The angular position of the sun at solar noon (i.e., when the sun is on
    the local meridian) with respect to the plane of the equator, north
    positive, -23.45 <= declination <= 23.45" -- Duffie & Beckman p13
    - dt :: DateTime -- the local date/time at the site.
    - TZ_hrs :: Number -- local time-zone w/ respect to universal time (GMT)
    RETURNS:
    solar declination angle in degrees */
    public static function
    declination_deg(dt:DateTime, TZ_hrs:Number):Number {
        var t:Number = _t(dt, TZ_hrs);
        var eps:Number = _eps_deg(t);
        var G:Number = _G_deg(t);
        var C:Number = _C(G);
        var L:Number = _L_deg(t, C);
        var alpha:Number = _alpha_deg(L);
        return _declination_deg(eps, alpha);
    }

    /* _EOT_hrs (meant for internal use only)
    - L :: Number -- the Yallop L parameter
    - C :: Number -- the Yallop C parameter
    - alpha :: Number -- the Yallop alpha parameter
    RETURNS:
    calculate the 'Equation of Time' per Yallop equation in hours */
    public static function
    _EOT_hrs(L:Number, C:Number, alpha:Number):Number {
        return (L - C - alpha) / degPerHour;
    }

    /* _EOT_min (meant for internal use only)
    - L :: Number -- the Yallop L parameter
    - C :: Number -- the Yallop C parameter
    - alpha :: Number -- the Yallop alpha parameter
    RETURNS:
    calculate the 'Equation of Time' per Yallop equation in minutes */
    public static function
    _EOT_min(L:Number, C:Number, alpha:Number):Number {
        return (L - C - alpha) / degPerMinute;
    }

    /* _solarHourAngle_deg (meant for internal use only)
    - gha_deg :: Number -- Greenwich Hour Angle (degrees)
    - longitude_360W_deg :: Number -- site longitude (degrees) (+, 0 <= L < 360)
    RETURNS:
    calculate the solar hour angle for the local site in degrees.
    Note: the returned angle is wrapped to +/- 180.0 */
    public static function
    _solarHourAngle_deg(gha_deg:Number, longitude_360W_deg:Number):Number
    {
      var sha:Number = gha_deg - longitude_360W_deg;
      if (sha > 180.0)
      {
        while (sha > 180)
        {
          sha = sha - 360.0;
        }
      }
      else if (sha < -180)
      {
        while (sha < -180)
        {
          sha = sha + 360.0;
        }
      }
      return sha;
    }

    /* solarHourAngle_deg
    - dt :: DateTime -- the local Date/Time at the location
    - TZ_hrs :: Number -- local time-zone with respect to Universal Time (UTC)
    - long_360W_deg :: Number -- the location's longitude (+ 0 <= L < 360).
    RETURNS:
    calculate the solar hour angle for the local site in degrees. At solar
    noon, the hour angle is zero. */
    public static function
    solarHourAngle_deg(dt:DateTime, TZ_hrs:Number, long_360W_deg:Number):Number {
        var UT:Number = universalTime_hrs(dt, TZ_hrs);
        var t:Number = _t(dt, TZ_hrs);
        var G:Number = _G_deg(t);
        var C:Number = _C(G);
        var L:Number = _L_deg(t, C);
        var alpha:Number = _alpha_deg(L);
        var gha:Number = _greenwichHourAngle_deg(UT, C, L, alpha);
        return _solarHourAngle_deg(gha, long_360W_deg);
    }

    /* _apparentSolarTime
    - sha_deg :: Number -- the solar hour angle in degrees
    RETURNS:
    the apparent solar time in hours */
    public static function
    _apparentSolarTime_hr(sha_deg:Number):Number {
        return AdvMath.scaleToN(12.0 + (sha_deg / degPerHour), 24.0);
    }

    /* EOT_min -- Equation of Time in minutes
    - dt:DateTime :: the date/time for the EOT calculation (assumed to be
                     local Julian date + universal time for the given timezone)
    RETURNS
    The "equation of time" in minutes. */
    public static function
    EOT_min(dt:DateTime, TZ_hrs:Number):Number {
        var t:Number = _t(dt, TZ_hrs);
        var G:Number = _G_deg(t);
        var C:Number = _C(G);
        var L:Number = _L_deg(t, C);
        var alpha:Number = _alpha_deg(L);
        return _EOT_min(L, C, alpha);
    }

    /* EOTsimple_min -- Equation of Time (simple) in minutes
    NOTE: the purpose of this equation is to give a comparison to Yallop's
          rather involved derivation.
    - dayOfYear :: int -- the day of year with Jan 1 being day 1; i.e.
                          1 <= dayOfYear <= 365
    RETURNS:
    Equation of Time calculation in minutes. */
    public static function
    EOTsimple_min(dayOfYear:int):Number {
        if ((dayOfYear > 365) || (dayOfYear < 1)) {
            throw new Error(
                'EOTsimple_min :: 1 <= dayOfYear <= 365 but dayOfYear = '
                + dayOfYear.toString());
        }
        var B_deg:Number = (dayOfYear - 1.0) * 360.0 / 365.0;
        return 2.2918 * (0.0075 + 0.1868 * Math.cos(D2R(B_deg))
            - 3.2077 * Math.sin(D2R(B_deg))
            - 1.4615 * Math.cos(D2R(2.0 * B_deg))
            - 4.089 * Math.sin(D2R(2.0 * B_deg)));
    }

    /* Apparent Solar Time (hrs)
    - LST_hrs :: Number -- local standard time (hour of the day)
    - EOT_min :: Number -- Equation of time result for the location (minutes)
    - long_360W_deg :: Number -- the longitude of the site (degrees)
                                 0 <= L < 360 West is +
    - LSM_deg :: Number -- Local standard meridian (degrees)
    RETURNS:
    the apparent solar time in hours from start of day 0 <= AST < 24 */
    public static function
    apparentSolarTime_hrs(LST_hrs:Number, EOT_min:Number,
                          long_360W_deg:Number, LSM_deg:Number):Number {
        return LST_hrs + EOT_min / 60.0 + (long_360W_deg - LSM_deg) / degPerHour;
    }

    /* solarAltitudeAngle_deg -- Solar Altitude Angle (degrees)
    This is the angle between the horizontal and the sun's rays.
    - lat_deg :: Number -- local site latitude (degrees)
    - decl_deg :: Number -- local solar declination angle (degrees)
    - sha_deg :: Number -- local solar hour angle (degrees)
    RETURNS:
    Solar altitude angle in degrees */
    public static function
    solarAltitude_deg(lat_deg:Number, decl_deg:Number, sha_deg:Number):Number {
        var term1:Number = Math.sin(D2R(lat_deg)) * Math.sin(D2R(decl_deg));
        var term2:Number =
            Math.cos(D2R(lat_deg)) * Math.cos(D2R(decl_deg))
            * Math.cos(D2R(sha_deg + 180.0));
        var term3:Number = term1 - term2;
        return R2D(Math.asin(term3));
    }

    public static function
    normalizeAngle(ang_deg:Number,
                          low:Number=-180.0, high:Number=180.0):Number {
      while (ang_deg > high) {
        ang_deg = ang_deg - 360.0;
      }
      while (ang_deg <= low) {
        ang_deg = ang_deg + 360.0;
      }
      return ang_deg;
    }

    /* solarAzimuthAngleFromNorth_deg
    Muneer, p12 (Prog1-6.FOR)
    "The sun’s position in the sky can be described in terms of two angles:
    SOLALT, the elevation angle above the horizon and SOLAZM, the azimuth
    from north of the sun’s beam projection on the horizontal plane
    (clockwise = positive)."
    - lat_deg :: Number -- latitude (degrees)
    - decl_deg :: Number -- solar declination (degrees)
    - sha_deg :: Number -- solar hour angle (degrees)
    - solAlt_deg :: Number -- solar altitude angle (degrees)
    RETURNS:
    the solar azimuth angle in degrees. */
    public static function
    solarAzimuthAngleFromNorth_deg(lat_deg:Number, decl_deg:Number,
                          sha_deg:Number, solAlt_deg:Number):Number {
        if (solAlt_deg < 0.0) return 0.0;
        var term1:Number = Math.cos(D2R(lat_deg)) * Math.tan(D2R(decl_deg));
        var term2:Number = Math.sin(D2R(lat_deg))
            * Math.cos(D2R(sha_deg + 180.0));
        var term3:Number = term1 + term2;
        var term4:Number = term3 * Math.cos(D2R(decl_deg))
            / Math.cos(D2R(solAlt_deg));
        var solAzm_deg:Number = R2D(Math.acos(term4));
        if (sha_deg > 0.0) {
            solAzm_deg = 360.0 - solAzm_deg;
        }
        return normalizeAngle(solAzm_deg, 0.0, 360.0);
    }

    /* from Duffie & Beckman 1991 p 16 eq 1.6.6a-g
    Sign convention in Duffie & Beckman 2013 p 13 Figure 1.6.1, East is negative
    West is positive. See Duffie & Beckman 2013 eq 1.6.6
    */
    public static function
    solarAzimuthAngleFromSouth_deg(decl_deg:Number, lat_deg:Number,
                                   sha_deg:Number,
                                   zenithAng_deg:Number):Number {
        var SIGN_SHA:Number;
        var tmp1:Number;
        if (lat_deg == 0.0) {
          // Duffie and Beckman warn to avoid calculating at latitude == 0.0
          // so we'll pretend we're just above equator if we get lat_deg == 0.0
            lat_deg = 1e-6;
        }
        if (zenithAng_deg >= 90.0) return 0.0;
        var _sha_deg:Number = normalizeAngle(sha_deg, -180.0, 180.0);
        if (_sha_deg >= 0.0) {
            SIGN_SHA = 1.0;
        } else {
            SIGN_SHA = -1.0;
        }
        var cosOz:Number = Math.cos(D2R(zenithAng_deg));
        var sinOz:Number = Math.sin(D2R(zenithAng_deg));
        var azm_deg:Number = SIGN_SHA *
          Math.abs(
            R2D(
              Math.acos(
                (cosOz * Math.sin(D2R(lat_deg)) - Math.sin(D2R(decl_deg)))
                /
                (sinOz * Math.cos(D2R(lat_deg))))));
        return normalizeAngle(azm_deg, -180.0, 180.0);
    }

    /* solarIncidenceAngle_deg
    "The angle between the beam radiation on a surface and the normal to that
    surface." -- Duffie & Beckman p13
    - azimuth_deg :: Number -- the azimuth angle of the surface in degrees
    - tilt_deg :: Number -- tilt of surface from horizontal
                            (vertical = 90 deg; horizontal = 0 deg)
    - decl_deg :: Number -- solar declination angle in degrees
    - lat_deg :: Number -- latitude in degrees
    - sha_deg :: Number -- solar hour angle in degrees (0 is solar noon)
    RETURNS:
    Solar incidence angle in degrees */
    public static function
    solarIncidenceAngle_deg(azimuth_deg:Number, tilt_deg:Number,
                            decl_deg:Number, lat_deg:Number,
                            sha_deg:Number):Number {
        var gamma_rad:Number = D2R(azimuth_deg - 180);
        var term1:Number = Math.sin(D2R(decl_deg)) * Math.sin(D2R(lat_deg))
            * Math.cos(D2R(tilt_deg));
        var term2:Number = Math.sin(D2R(decl_deg)) * Math.cos(D2R(lat_deg))
            * Math.sin(D2R(tilt_deg)) * Math.cos(gamma_rad);
        var term3:Number = Math.cos(D2R(decl_deg)) * Math.cos(D2R(lat_deg))
            * Math.cos(D2R(tilt_deg)) * Math.cos(D2R(sha_deg));
        var term4:Number = Math.cos(D2R(decl_deg)) * Math.sin(D2R(lat_deg))
            * Math.sin(D2R(tilt_deg)) * Math.cos(gamma_rad)
            * Math.cos(D2R(sha_deg));
        var term5:Number = Math.cos(D2R(decl_deg)) * Math.sin(D2R(tilt_deg))
            * Math.sin(gamma_rad) * Math.sin(D2R(sha_deg));
        return R2D(Math.acos(term1 - term2 + term3 + term4 + term5));
    }

    /* zenithAngle_rad
    - lat_deg :: Number -- the latitude of the site (degrees)
    - decl_deg :: Number -- the declination angle of the site (degrees)
    - sha_deg :: Number -- the solar hour angle (degrees)
    RETURNS:
    zenith angle (angle of incidence for a horizontal surface) in radians */
    public static function
    zenithAngle_rad(lat_deg:Number, decl_deg:Number, sha_deg:Number):Number {
        var term1:Number =
            Math.cos(D2R(lat_deg)) * Math.cos(D2R(decl_deg))
            * Math.cos(D2R(sha_deg));
        var term2:Number = Math.sin(D2R(lat_deg)) * Math.sin(D2R(decl_deg));
        return Math.acos(term1 + term2);
    }

    /* zenithAngle_deg
    - lat_deg :: Number -- the latitude of the site (degrees)
    - decl_deg :: Number -- the declination angle of the site (degrees)
    - sha_deg :: Number -- the solar hour angle (degrees)
    RETURNS:
    zenith angle (angle of incidence for a horizontal surface) in degrees */
    public static function
    zenithAngle_deg(lat_deg:Number, decl_deg:Number, sha_deg:Number):Number {
        return R2D(zenithAngle_rad(lat_deg, decl_deg, sha_deg));
    }

    /* Calculate the solar elevation angle in degrees
    - lat_deg:Real, the site's latitude in degrees
    - decl_deg:Real, the declination angle at the site (degrees)
    - sha_deg:Real, the solar hour angle for the site (degrees)
    RETURNS: Real, the solar elevation angle in radians */
    public static function
    solarElevationAngle_rad(lat_deg:Number, decl_deg:Number, sha_deg:Number):Number {
      var Oz_rad:Number = zenithAngle_rad(lat_deg, decl_deg, sha_deg);
      return (Math.PI / 2.0) - Oz_rad;
    }
    
    /* Calculate the solar elevation angle in radians
    - lat_deg:Real, the site's latitude in degrees
    - decl_deg:Real, the declination angle at the site (degrees)
    - sha_deg:Real, the solar hour angle for the site (degrees)
    RETURNS: Real, the solar elevation angle in degrees */
    public static function
    solarElevationAngle_deg(lat_deg:Number, decl_deg:Number, sha_deg:Number):Number {
      var Oz_deg:Number = zenithAngle_deg(lat_deg, decl_deg, sha_deg);
      return 90.0 - Oz_deg;
    }
    
    /* sunset hour angle (degrees)
    */
    public static function
    sunsetHourAngle_deg(decl_deg:Number, lat_deg:Number):Number {
        return R2D(Math.acos(
            -1.0 * Math.tan(D2R(lat_deg)) * Math.tan(D2R(decl_deg))));
    }

    /* avg_cosz -- average cosine of zenith angle over given time-period
    given by hour angles. sha1_deg must be less than sha2_deg.
    this function uses the definition of cos Oz and the definition of an
    average: avg(cos Oz) = integral(cosOz(w) dw, w1, w2) / integral(dw, w1, w2);
    w = solar hour angle or sha. cos(Oz) = cos(lat)*cos(d)*cos(w) + sin(lat)*sin(d);
    c1 = cos(lat)*cos(d) where lat is latitude and d is declination
    c2 = sin(lat)*sin(d)
    c3 is 1/(w2-w1) where w1 and w2 are expressed in radians
    The average of cos(Oz) is then c3 * (c1 * (sin(w2)-sin(w1)) + c2(w2-w1)
    where again w1 and w2 are in radians. */
    public static function
    avg_cosz(
      sha1_deg:Number,
      sha2_deg:Number,
      dt:DateTime,
      lat_deg:Number,
      decl_deg:Number):Number
    {
        var c1:Number = Math.cos(D2R(lat_deg)) * Math.cos(D2R(decl_deg));
        var c2:Number = Math.sin(D2R(lat_deg)) * Math.sin(D2R(decl_deg));
        var c3:Number = 1.0 / D2R(sha2_deg - sha1_deg);
        return c3 * (
            c1 * (Math.sin(D2R(sha2_deg)) - Math.sin(D2R(sha1_deg))) +
            c2 * D2R(sha2_deg - sha1_deg));
    }

    /* irradinceDiffuseHorizontal_W__m2
    - Igh_W__m2 :: Number -- global irradiance on a horizontal surface (W/m2)
    - Ibn_W__m2 :: Number -- beam irradiance on a surface normal to sun (W/m2)
    - dt :: DateTime -- the local date/time at location
    - TZ_hrs :: Number -- timezone (wrt coordinated universal time or UTC)
    - lat_deg :: Number -- the latitude of location (degrees)
    - long_360W_deg :: Number -- longitude of location (degrees)
                                 West is +: 0 <= L < 360
    RETURNS:
    calculation of diffuse irradiance on horizontal surface in W/m2 */
    public static function
    irradianceDiffuseHorizontal_W__m2(
      Igh_W__m2:Number,
      Ibn_W__m2:Number,
      dt:DateTime,
      TZ_hrs:Number,
      lat_deg:Number,
      long_360W_deg:Number):Number
    {
      var avgCosZ:Number = avgCosZHourCentered(
        dt, TZ_hrs, lat_deg, long_360W_deg);
      // don't allow beam contribution when sun is not up
      if (avgCosZ < 0.0)
      {
        avgCosZ = 0.0;
      }
      var Idh_W__m2:Number = Igh_W__m2 - Ibn_W__m2 * avgCosZ;
      if (Idh_W__m2 < 0.0)
      {
        return 0.0;
      }
      return Idh_W__m2;
    }

    /* irradianceGlobalHorizontal_W__m2
    - Idh_W__m2 :: Number -- the diffuse horizontal average irradiance
                             over the time-period (W/m2)
    - Ibn_W__m2 :: Number -- the average beam irradiance over the time
                             period incident on a surface normal to the sun's
                             rays (W/m2)
    - dt :: Number -- time-zone (wrt coordinated univeral time or UTC)
    - lat_deg :: Number -- the latitude of the site (degrees)
    - long_360W_deg :: Number -- the longitude of the site (degrees)
                                 West is +: 0 <= L < 360
    RETURNS:
    calculates the global horizontal average irradiance over the time-period
    assuming an average cos(Z) (cosine of the zenith angle) holds for the
    period. If the cos(Z) is negative or zero, the global horizontal irradiance
    is equal to the value of the diffuse horizontal component (accounts for
    sunrise times when sky can be bright before sun is up). */
    public static function irradianceGlobalHorizontal_W__m2(
      Idh_W__m2:Number,
      Ibn_W__m2:Number,
      dt:DateTime,
      TZ_hrs:Number,
      lat_deg:Number,
      long_360W_deg:Number):Number
    {
      var avgCosZ:Number = avgCosZHourCentered(
        dt, TZ_hrs, lat_deg, long_360W_deg);
      if (avgCosZ < 0.0)
      {
        return Idh_W__m2;
      }
      return Ibn_W__m2 * avgCosZ + Idh_W__m2;
    }

    /* irradianceExtNormal_W__m2 -- Gon f/ Duffie & Beckman p 10
    - dayOfYear :: Number -- the day of the year
    RETURNS:
    The beam normal extraterrestrial radiation for the given day of year
    (W/m2). (That is, this radiation is measured normal to the plane of
    the sun). */
    public static function
    irradianceExtNormal_W__m2(dayOfYear:Number):Number {
        return Solar.SOLAR_CONSTANT_W__m2 *
            (1.0 + 0.033 * Math.cos(D2R(360.0 * dayOfYear / 365.0)));
    }

    /* irradianceExtHoriz_kWh__m2
    - doy :: Number -- day of year, the number of days elapsed in a given year
                       up to a particular date.
    - lat_deg :: Number -- the latitude of the site (degrees)
    - decl_deg :: Number -- the solar declination (degrees)
    - sha_deg :: Number -- the solar hour angle (degrees)
    RETURNS:
    The radiation received incident on a horizontal surface
    under the absence of any atomosphere (W/m2) */
    public static function
    irradianceExtHoriz_W__m2(doy:Number, lat_deg:Number, decl_deg:Number,
                             sha_deg:Number):Number {
        var Ixn:Number = irradianceExtNormal_W__m2(doy);
        var zenAng_rad:Number = zenithAngle_rad(lat_deg, decl_deg, sha_deg);
        return Math.cos(zenAng_rad) * Ixn;
    }

    public static function
    makePointFromGlobalAndBeam(
      Igh_W__m2:Number,
      Ibn_W__m2:Number,
      hourOfYear:Number,
      year:int,
      hdr:Object):Object
    {
      if ((Igh_W__m2 < 0.0) || (Ibn_W__m2 < 0.0))
      {
        Igh_W__m2 = 0.0;
        Ibn_W__m2 = 0.0;
      }
      var halfSolarConstant_W__m2:Number = 0.5 * SOLAR_CONSTANT_W__m2;
      if (Ibn_W__m2 > halfSolarConstant_W__m2)
      {
        Ibn_W__m2 = halfSolarConstant_W__m2;
      }
      if (Igh_W__m2 > halfSolarConstant_W__m2)
      {
        Igh_W__m2 = halfSolarConstant_W__m2;
      }
      var solarDiffuseHoriz_W__m2:Number =
        Solar.irradianceDiffuseHorizontal_W__m2(
          Igh_W__m2,
          Ibn_W__m2,
          DateTime.hourOfYearToDateTime(hourOfYear, year),
          hdr[DateAndTime.TIME_ZONE],
          hdr[Location.LATITUDE_deg],
          longToLong360W(hdr[Location.LONGITUDE_deg]));
      if (solarDiffuseHoriz_W__m2 < 0.0)
      {
        solarDiffuseHoriz_W__m2 = 0.0;
      }
      return MapUtils.makeObject([
        Solar.DIFFUSE_HORIZONTAL_Wh__m2, solarDiffuseHoriz_W__m2,
        Solar.BEAM_NORMAL_Wh__m2, Ibn_W__m2]);
    }

    public static function
    deriveIrradianceGlobalHorizontal_W__m2(vals:Object):Number {
        var Idh:Number = vals[DIFFUSE_HORIZONTAL_Wh__m2] as Number;
        var Ibn:Number = vals[BEAM_NORMAL_Wh__m2] as Number;
        var dt:DateTime = DateAndTime.headerAndValueObjectsToDateTime(
            vals, vals);
        var tz:Number = vals[DateAndTime.TIME_ZONE] as Number;
        var lat:Number = vals[Location.LATITUDE_deg] as Number;
        var long:Number = vals[Location.LONGITUDE_deg] as Number;
        var long_360W_deg:Number = longToLong360W(long);
        return irradianceGlobalHorizontal_W__m2(
          Idh, Ibn, dt, tz, lat, long_360W_deg);
    }

    // holding:int, 0 means hold diffuse constant Id;
    //              1 means hold beam-normal constant;
    //              2 means hold same fraction of diffuse and beam-normal
    public static function set_Ig_holding_X(
      header:Object,
      point:Object,
      Igh_target:Number,
      holding:int):Object
    {
      var Idh:Number = point[DIFFUSE_HORIZONTAL_Wh__m2] as Number;
      var Ibn:Number = point[BEAM_NORMAL_Wh__m2] as Number;
      var dt:DateTime = DateAndTime.headerAndValueObjectsToDateTime(
        header, point);
      var tz:Number = header[DateAndTime.TIME_ZONE] as Number;
      var lat:Number = header[Location.LATITUDE_deg] as Number;
      var long:Number = header[Location.LONGITUDE_deg] as Number;
      var long_360W_deg:Number = longToLong360W(long);
      var avgCosZ:Number = avgCosZHourCentered(dt, tz, lat, long_360W_deg);
      var newPoint:Object;
      var Ibn_new:Number;
      var Igh_new:Number;
      var Idh_new:Number;
      var scaleFactor:Number;
      var Igh_current:Number;
      if ((avgCosZ <= 0.0) || (Igh_target <= 0.0))
      {
        newPoint = MapUtils.copyObject(point);
        newPoint[BEAM_NORMAL_Wh__m2] = 0.0;
        newPoint[DIFFUSE_HORIZONTAL_Wh__m2] = 0.0;
      } else if (holding == 0) {
        // diffuse gets held, so we set beam.
        Ibn_new = Math.max(
          Math.min(
            (Igh_target - Idh) / avgCosZ,
            MAXS[BEAM_NORMAL_Wh__m2]),
          MINS[BEAM_NORMAL_Wh__m2]);
        Igh_new = Math.max(
          Math.min(
            Ibn_new * avgCosZ + Idh,
            MAXS[GLOBAL_HORIZ_Wh__m2]),
          MINS[GLOBAL_HORIZ_Wh__m2]);
        Ibn_new = Math.max(
          Math.min(
            (Igh_new - Idh) / avgCosZ,
            MAXS[BEAM_NORMAL_Wh__m2]),
          MINS[BEAM_NORMAL_Wh__m2]);
        newPoint = MapUtils.copyObject(point);
        newPoint[BEAM_NORMAL_Wh__m2] = Ibn_new;
      } else if (holding == 1) {
        // beam gets held, so we set diffuse
        Idh_new = Math.max(
          Math.min(
            Igh_target - Ibn * avgCosZ,
            MAXS[DIFFUSE_HORIZONTAL_Wh__m2]),
          MINS[DIFFUSE_HORIZONTAL_Wh__m2]);
        Igh_new = Math.max(
          Math.min(
            Ibn * avgCosZ + Idh_new,
            MAXS[GLOBAL_HORIZ_Wh__m2]),
          MINS[GLOBAL_HORIZ_Wh__m2]);
        Idh_new = Math.max(
          Math.min(
            Igh_new - Ibn * avgCosZ,
            MAXS[DIFFUSE_HORIZONTAL_Wh__m2]),
          MINS[DIFFUSE_HORIZONTAL_Wh__m2]);
        newPoint = MapUtils.copyObject(point);
        newPoint[DIFFUSE_HORIZONTAL_Wh__m2] = Idh_new;
      } else if (holding == 2) {
        // we're going to hold the fraction of beam to total the same
        Igh_current = Ibn * avgCosZ + Idh;
        scaleFactor = Math.max(Igh_target / Igh_current, 0.0);
        Idh_new = Math.max(
          Math.min(Idh * scaleFactor, MAXS[DIFFUSE_HORIZONTAL_Wh__m2]),
          MINS[DIFFUSE_HORIZONTAL_Wh__m2]);
        Ibn_new = Math.max(
          Math.min(Ibn * scaleFactor, MAXS[BEAM_NORMAL_Wh__m2]),
          MINS[BEAM_NORMAL_Wh__m2]);
        Igh_new = Math.max(
          Math.min(
            Ibn_new * avgCosZ + Idh_new,
            MAXS[GLOBAL_HORIZ_Wh__m2]),
          MINS[GLOBAL_HORIZ_Wh__m2]);
        scaleFactor = Math.max(Igh_new / Igh_current, 0.0);
        Idh_new = Math.max(
          Math.min(Idh * scaleFactor, MAXS[DIFFUSE_HORIZONTAL_Wh__m2]),
          MINS[DIFFUSE_HORIZONTAL_Wh__m2]);
        Ibn_new = Math.max(
          Math.min(Ibn * scaleFactor, MAXS[BEAM_NORMAL_Wh__m2]),
          MINS[BEAM_NORMAL_Wh__m2]);
        newPoint = MapUtils.copyObject(point);
        newPoint[BEAM_NORMAL_Wh__m2] = Ibn_new;
        newPoint[DIFFUSE_HORIZONTAL_Wh__m2] = Idh_new;
      } else {
        // throw an error
        throw new Error("Unhandled holding value " + holding.toString());
      }
      return newPoint;
    }

    public static function deriveIrradianceGlobalHorizontal_W__m2_alt(
      header:Object,
      point:Object
      ):Number
    {
      var Idh:Number = point[DIFFUSE_HORIZONTAL_Wh__m2] as Number;
      var Ibn:Number = point[BEAM_NORMAL_Wh__m2] as Number;
      var dt:DateTime = DateAndTime.headerAndValueObjectsToDateTime(
        header, point);
      var tz:Number = header[DateAndTime.TIME_ZONE] as Number;
      var lat:Number = header[Location.LATITUDE_deg] as Number;
      var long:Number = header[Location.LONGITUDE_deg] as Number;
      var long_360W_deg:Number = longToLong360W(long);
      return irradianceGlobalHorizontal_W__m2(
        Idh, Ibn, dt, tz, lat, long_360W_deg);
    }

    public static function
    deriveZenithAngle_deg(vals:Object):Number {
        var lat_deg:Number = vals[Location.LATITUDE_deg] as Number;
        var long_deg:Number = longToLong360W(
          vals[Location.LONGITUDE_deg] as Number);
        var dt:DateTime = DateAndTime.headerAndValueObjectsToDateTime(
            vals, vals);
        var tz_hrs:Number = vals[DateAndTime.TIME_ZONE] as Number;
        var decl_deg:Number = declination_deg(dt, tz_hrs);
        var sha_deg:Number = solarHourAngle_deg(dt, tz_hrs, long_deg);
        return zenithAngle_deg(lat_deg, decl_deg, sha_deg);
    }

    public static function
    deriveElevationAngle_deg(vals:Object):Number {
      var lat_deg:Number = vals[Location.LATITUDE_deg] as Number;
      var long_deg:Number = longToLong360W(
        vals[Location.LONGITUDE_deg] as Number);
      var dt:DateTime = DateAndTime.headerAndValueObjectsToDateTime(
        vals, vals);
      var tz_hrs:Number = vals[DateAndTime.TIME_ZONE] as Number;
      var decl_deg:Number = declination_deg(dt, tz_hrs);
      var sha_deg:Number = solarHourAngle_deg(dt, tz_hrs, long_deg);
      return solarElevationAngle_deg(lat_deg, decl_deg, sha_deg);
    }
    
    public static function
    deriveSolarHourAngle_deg(vals:Object):Number {
        var dt:DateTime = DateAndTime.headerAndValueObjectsToDateTime(
            vals, vals);
        var tz_hrs:Number = vals[DateAndTime.TIME_ZONE] as Number;
        var long_deg:Number = longToLong360W(
          vals[Location.LONGITUDE_deg] as Number);
        return solarHourAngle_deg(dt, tz_hrs, long_deg);
    }

    public static function
    deriveAzimuthAngleFromNorth_deg(vals:Object):Number {
        var lat_deg:Number = vals[Location.LATITUDE_deg] as Number;
        var dt:DateTime = DateAndTime.headerAndValueObjectsToDateTime(
            vals, vals);
        var tz_hrs:Number = vals[DateAndTime.TIME_ZONE] as Number;
        var long_deg:Number = vals[Location.LONGITUDE_deg];
        var long_360W_deg:Number = longToLong360W(long_deg);
        var decl_deg:Number = declination_deg(dt, tz_hrs);
        var sha_deg:Number = solarHourAngle_deg(dt, tz_hrs, long_360W_deg);
        var solAlt_deg:Number = solarAltitude_deg(lat_deg, decl_deg, sha_deg);
        return solarAzimuthAngleFromNorth_deg(
          lat_deg, decl_deg, sha_deg, solAlt_deg);
    }

    public static function
    deriveAzimuthAngleFromSouth_deg(vals:Object):Number {
        var dt:DateTime = DateAndTime.headerAndValueObjectsToDateTime(
            vals, vals);
        var tz_hrs:Number = vals[plugins.DateAndTime.TIME_ZONE] as Number;
        var lat_deg:Number = vals[plugins.Location.LATITUDE_deg] as Number;
        var long_deg:Number = vals[plugins.Location.LONGITUDE_deg] as Number;
        var long_360W_deg:Number = longToLong360W(long_deg);
        var sha_deg:Number = solarHourAngle_deg(dt, tz_hrs, long_360W_deg);
        var decl_deg:Number = declination_deg(dt, tz_hrs);
        var zenith_deg:Number = zenithAngle_deg(lat_deg, decl_deg, sha_deg);
        return solarAzimuthAngleFromSouth_deg(
            decl_deg, lat_deg, sha_deg, zenith_deg);
    }

    public static function
    deriveDeclination_deg(vals:Object):Number {
        var dt:DateTime = DateAndTime.headerAndValueObjectsToDateTime(
            vals, vals);
        var tz_hrs:Number = vals[plugins.DateAndTime.TIME_ZONE] as Number;
        return declination_deg(dt, tz_hrs);
    }

    public static function
    deriveIrradianceExtHoriz_W__m2(vals:Object):Number {
        var dt:DateTime = DateAndTime.headerAndValueObjectsToDateTime(
            vals, vals);
        var dayOfYear:Number = dt.dayOfYear;
        var lat_deg:Number = vals[Location.LATITUDE_deg] as Number;
        var long_deg:Number = vals[Location.LONGITUDE_deg];
        var long_360W_deg:Number = longToLong360W(long_deg);
        var tz_hrs:Number = vals[DateAndTime.TIME_ZONE] as Number;
        var sha_deg:Number = solarHourAngle_deg(dt, tz_hrs, long_360W_deg);
        var decl_deg:Number = declination_deg(dt, tz_hrs);
        return irradianceExtHoriz_W__m2(
            dayOfYear, lat_deg, decl_deg, sha_deg);
    }

    public static function
    deriveIrradianceExtNormal_W__m2(vals:Object):Number {
        var dt:DateTime = DateAndTime.headerAndValueObjectsToDateTime(
            vals, vals);
        var dayOfYear:Number = dt.dayOfYear;
        return irradianceExtNormal_W__m2(dayOfYear);
    }

    // Convert longitude on a West is - East is + and -180 <= L <= 180
    // convention to a "West is positive" and 0 <= L < 360 convention
    public static function longToLong360W(long_deg:Number):Number {
      if (long_deg < 0.0) {
        return -long_deg;
      } else if (long_deg == 0.0) {
        return 0.0;
      }
      return 360.0 - long_deg;
    }

    public static function
    deriveHourlyAveragedCosZ(vals:Object):Number {
        var dt:DateTime = DateAndTime.headerAndValueObjectsToDateTime(
            vals, vals);
        var lat_deg:Number = vals[Location.LATITUDE_deg];
        var long_deg:Number = vals[Location.LONGITUDE_deg];
        // put longitude on "W is positive" with range 0 <= L < 360
        var long360_deg:Number = longToLong360W(long_deg);
        var tz_hrs:Number = vals[DateAndTime.TIME_ZONE];
        return avgCosZHourCentered(dt, tz_hrs, lat_deg, long360_deg);
    }

    // long_360W_deg is longitude on a "West is positive" convention with
    // 0 <= L <= 360.
    public static function avgCosZHourCentered(
      dt:DateTime,
      TZ_hrs:Number,
      lat_deg:Number,
      long_360W_deg:Number):Number
    {
      var hoursPerYear:Number = 8760.0;
      var hoy:Number = DateAndTime.hourOfYearFromDateObject(dt);
      var hoy1:Number = hoy; // hoy - 0.5;
      var hoy1Year:Number = dt.year;
      var decl_deg:Number = declination_deg(dt, TZ_hrs);
      while (hoy1 < 0.0)
      {
        hoy1Year--;
        hoy1 = hoy1 + hoursPerYear;
      }
      while (hoy1 > hoursPerYear)
      {
        hoy1Year++;
        hoy1 = hoy1 - hoursPerYear;
      }
      // due to sunset hour angle flipping signs at midnight, we don't
      // go quite one whole hour forward to do the averaging. Otherwise,
      // causes problems for calculations at 11 PM (23 hours to 0 hours of
      // the next day)
      //var hoy2:Number = hoy1 + 1;
      // NOTE: ideally, the 0.999 should be passed in as a parameter to allow
      // this function to handle averages over time steps that are not 1 hour
      var hoy2:Number = hoy1 + 0.999;
      var hoy2Year:Number = hoy1Year;
      while (hoy2 < 0.0)
      {
        // this should never happen since we adjusted hoy1 and add 0.999
        trace('WARNING! Unanticipated case in Solar.avgCosZHourCentered');
        hoy2Year--;
        hoy2 = hoy2 + hoursPerYear;
      }
      while (hoy2 > hoursPerYear)
      {
        hoy2Year++;
        hoy2 = hoy2 - hoursPerYear;
      }
      var hoy1Mins:Number = (hoy1 - Math.floor(hoy1)) * 60.0;
      var hoy1MinsInt:int = int(Math.floor(hoy1Mins));
      var hoy1Secs:Number = (hoy1Mins - Math.floor(hoy1Mins)) * 60.0;
      var hoy1SecsInt:int = int(Math.floor(hoy1Secs));
      var hoy2Mins:Number = (hoy2 - Math.floor(hoy2)) * 60.0;
      var hoy2MinsInt:int = int(Math.floor(hoy2Mins));
      var hoy2Secs:Number = (hoy2Mins - Math.floor(hoy2Mins)) * 60.0;
      var hoy2SecsInt:int = int(Math.floor(hoy2Secs));
      var sha1_deg:Number = solarHourAngle_deg(
        DateTime.hourOfYearToDateTime(
          Math.floor(hoy1),
          hoy1Year,
          hoy1MinsInt,
          hoy1SecsInt),
        TZ_hrs, long_360W_deg);
      var sha2_deg:Number = solarHourAngle_deg(
        DateTime.hourOfYearToDateTime(
          Math.floor(hoy2),
          hoy2Year,
          hoy2MinsInt,
          hoy2SecsInt),
        TZ_hrs, long_360W_deg);
      //trace('avgCosZHourCentered');
      //trace('sha1:', sha1_deg);
      //trace('sha2:', sha2_deg);
      return avg_cosz(sha1_deg, sha2_deg, dt, lat_deg, decl_deg);
    }

    public static function
    deriveMonthlyAvgGH(dataSet:DataSet):ArrayCollection {
        var points:IPVec = dataSet.points;
        var hdr:Object = dataSet.header;
        var _data:ArrayCollection = new ArrayCollection();
        var current:Object = null;
        var dt_hrs:Number = 0.0;
        // simulating dt(0) - (dt(1) - dt(0))
        var lastElapsedTime_hrs:Number =
            points.nth(0)[DateAndTime.ELAPSED_TIME_hrs] -
            (points.nth(1)[DateAndTime.ELAPSED_TIME_hrs] -
             points.nth(0)[DateAndTime.ELAPSED_TIME_hrs]);
        var elapsedTime_hrs:Number;
        var I:Number = 0.0;
        var d:int = 1;
        var lastDay:int = d;
        /* Need to straighten up the time integration. Currently we
        project forward for average and integrated values. That is, the
        data logged for 0:00 hours is the average or integrated from 0:00
        to 1:00. */
        var Wh__m2_to_MJ__m2:Function = UnitConversions.Wh__m2_to_MJ__m2;
        var smd:Object = dataSet.seriesMetaData;
        var getElapsedTime_hrs:Function =
            (smd[DateAndTime.ELAPSED_TIME_hrs] as MetaData).getter;
        var year:MetaData = smd[DateAndTime.YEAR] as MetaData;
        var month:MetaData = smd[DateAndTime.MONTH] as MetaData;
        var day:MetaData = smd[DateAndTime.DAY] as MetaData;
        var Igh_W__m2:MetaData = smd[Solar.GLOBAL_HORIZ_Wh__m2] as MetaData;
        for (var i:int=0; i<points.count; i++) {
            var point:Object = points.nth(i);
            var m:int = month.getter(hdr, point);
            var y:int = year.getter(hdr, point);
            lastDay = d;
            d = day.getter(hdr, point);
            I = Igh_W__m2.getter(hdr, point);
            elapsedTime_hrs = getElapsedTime_hrs(hdr, point);
            dt_hrs = elapsedTime_hrs - lastElapsedTime_hrs;
            var E_MJ__m2:Number = Wh__m2_to_MJ__m2(I * dt_hrs);
            var labelText:String = FormatUtils.fixStringLength(
                2, m.toString(), '0', FormatUtils.PAD_LEFT) + "/" +
                y.toString();
            if ((current == null) ||
                (current.month != m) || (current.year != y)) {
                if (current != null) {
                    current.Hgh_MJ__m2 = current.Hgh_MJ__m2 / Number(lastDay);
                    _data.addItem(current);
                }
                current = new Object();
                current.month = m;
                current.year = y;
                current.Hgh_MJ__m2 = 0.0;
            }
            current.Hgh_MJ__m2 += E_MJ__m2;
            lastElapsedTime_hrs = elapsedTime_hrs;
        }
        //trace('current', current);
        //MapUtils.traceObject(current, 'current');
        //trace('lastDay', lastDay);
        if (current != null) {
            current.Hgh_MJ__m2 = current.Hgh_MJ__m2 / Number(lastDay);
            _data.addItem(current);
        }
        return _data;
    }

    public static function
    scaleSolarByMonthlyAvgGH(points:Array, hdr:Object, metaDataMap:Object,
                             year:int, month:int,
                             desiredHgh_MJ__m2:Number):Array {
        if (points.length == 0) {
            return [];
        }
        for (var idx:int=0; idx < points.length; idx++) {
            if (idx == (points.length - 1)) {
                if (idx > 0) {
                    point.dt_hrs = points[idx - 1].dt_hrs;
                } else {
                    point.dt_hrs = 1.0;
                }
            } else {
                var point:Object = points[idx];
                var nextPoint:Object = points[idx+1];
                point.dt_hrs =
                    nextPoint[DateAndTime.ELAPSED_TIME_hrs] -
                    point[DateAndTime.ELAPSED_TIME_hrs];
            }
        }
        var getIdotGH_W__m2:Function =
            (metaDataMap[GLOBAL_HORIZ_Wh__m2] as MetaData).getter;
        var getIdotBN_W__m2:Function =
            (metaDataMap[BEAM_NORMAL_Wh__m2] as MetaData).getter;
        var getIdotDH_W__m2:Function =
            (metaDataMap[DIFFUSE_HORIZONTAL_Wh__m2] as MetaData).getter;
        var getYear:Function =
            (metaDataMap[DateAndTime.YEAR] as MetaData).getter;
        var getMonth:Function =
            (metaDataMap[DateAndTime.MONTH] as MetaData).getter;
        var getElapsedTime_hrs:Function =
            (metaDataMap[DateAndTime.ELAPSED_TIME_hrs] as MetaData).getter;
        var isSelected:Function =
            function (item:Object, idx:int, xs:Array):Boolean {
                var y:int = getYear(hdr, item);
                var m:int = getMonth(hdr, item);
                return ((year == y) && (month == m));
        };
        var selectedPoints:Array = points.filter(isSelected);
        var reduceMonthlyAvgDailyInsolation_MJ__m2:Function =
            function (sumAndCount:Object, point:Object):Object {
                var Idot_W__m2:Number = getIdotGH_W__m2(hdr, point);
                // TODO: change hard-coded dt to use a dt embedded into the
                // data point itself.
                var dt_hr:Number = point.dt_hrs;
                var I_Wh__m2:Number = Idot_W__m2 * dt_hr;
                return {
                    sum:sumAndCount.sum + I_Wh__m2,
                    count: sumAndCount.count + dt_hr
                };
        };
        var sumAndCount:Object = FP.reduce(
            reduceMonthlyAvgDailyInsolation_MJ__m2, {sum:0.0, count:0.0},
            selectedPoints);
        var Hgh_MJ__m2:Number =
            UnitConversions.Wh__m2_to_MJ__m2(sumAndCount.sum) /
            UnitConversions.hrs_to_days(sumAndCount.count);
        //trace('actual Hgh_MJ__m2', Hgh_MJ__m2);
        //trace('desired Hgh_MJ__m2', desiredHgh_MJ__m2);
        var scaleFactor:Number = desiredHgh_MJ__m2 / Hgh_MJ__m2;
        //trace('scaleFactor', scaleFactor);
        var MINS:Object = PlugInfo.knownMins();
        var MAXS:Object = PlugInfo.knownMaxs();
        return points.map(function (item:Object, idx:int, xs:Array):Object {
            if (isSelected(item, idx, xs)) {
                var IdotBN_W__m2:Number = getIdotBN_W__m2(hdr, item);
                var settings:Object = new Object();
                settings[BEAM_NORMAL_Wh__m2] = IdotBN_W__m2 * scaleFactor;
                settings[DateAndTime.ELAPSED_TIME_hrs] =
                    getElapsedTime_hrs(hdr, item);
                settings[DIFFUSE_HORIZONTAL_Wh__m2] =
                    getIdotDH_W__m2(hdr, item) * scaleFactor;
                var newPt:Object = DataPoint.update(
                    hdr, item, CONSTRAINT_FUNC, DERIVATION_FUNCS, settings,
                    MINS, MAXS, STATE_NAMES);
                if (CONSTRAINT_FUNC(hdr, newPt) == 0.0) {
                    newPt = MapUtils.merge(item, newPt);
                    return newPt;
                }
                var badPoint:Object = MapUtils.merge(item, newPt);
                var conF:Function = function(alpha:Number):Number {
                    return CONSTRAINT_FUNC(hdr, OptUtils.scaleByX(
                        item, badPoint, alpha));
                };
                var optAlpha:Number = OptUtils.findGreatestGoodBound(conF);
                newPt = OptUtils.scaleByX(item, badPoint, optAlpha);
                return newPt;
            }
            return item;
        });
    }
  // From Griffith et al 2005, bottom of page 116, equations 4 and 5
  // Note: cloudCover must be in the range 0.0 to 1.0 (not "tenths")
  public static function skyIRFromCloudCover_W__m2(Tdb_C:Number,
                                                   Twb_C:Number,
                                                   p_kPa:Number,
                                                   cloudCover:Number):Number {
    var CC:Number = cloudCover;
    var Tdew_C:Number = Psych.dewPointTemp_C(Tdb_C, Twb_C, p_kPa);
    var Tdew_K:Number = UnitConversions.C_to_K(Tdew_C);
    var Tdb_K:Number = UnitConversions.C_to_K(Tdb_C);
    var emis:Number = ((0.787 + 0.764 * Math.log(Tdew_K/273.15)) *
      (1.0 + 0.0224 * CC - 0.0035 * CC * CC + 0.00028 * CC * CC * CC));
    return emis * STEFAN_BOLTZMANN_CONST_W__m2_K4 * Math.pow(Tdb_K, 4);
  }
}
}