/*
Copyright (C) 2014-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package plugins {
import dse.ConstraintFunctions;
import dse.MetaData;
import dse.Normalization;

import units.ScaleUnits;

public class Clouds {
    public static const PLUGIN_NAME:String = 'cloudConditions';
    // CLOUD_COVER is aliased with Conditions.TOTAL_SKY_COVER
    public static const CLOUD_COVER:String = 'cloudCover';

    public static const MINS:Object = new Object();
    MINS[CLOUD_COVER] = 0;

    public static const MAXS:Object = new Object();
    MAXS[CLOUD_COVER] = 10;

    public static const
    SCALE_FACTORS:Object = Normalization.makeScaleFactors(MINS, MAXS);

    public static const
    CONSTRAINT_FUNC:Function =
        ConstraintFunctions.makeRangeConstraintFunction(
            [CLOUD_COVER],
            [CLOUD_COVER],
            MINS, MAXS, SCALE_FACTORS);

    public static const DERIVATION_FUNCS:Object = {};

    // an empty setting set will be interpreted as "No Restrictions" on setting
    // values.
    public static const SETTING_SETS:Array = [];

    public static const SERIES_META_DATA:Object = new Object();
    SERIES_META_DATA[CLOUD_COVER] = new MetaData(
      CLOUD_COVER,
      "Cloud Cover",
      ScaleUnits.TENTHS,
      ScaleUnits.QUANTITY_OF_MEASURE,
      MetaData.makeValueGetter(CLOUD_COVER),
      MetaData.DATA_TYPE__NUMBER,
      [],
      CONSTRAINT_FUNC,
      null,
      true,
      ScaleUnits.AVAILABLE_UNITS);

    public static const HEADER_META_DATA:Object = new Object();
}
}