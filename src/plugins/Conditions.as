/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package plugins {
import dse.ConstraintFunctions;
import dse.MetaData;
import dse.Normalization;

import units.NoUnits;

import utils.MapUtils;

public class Conditions {
    public static const PLUGIN_NAME:String = 'weatherConditions';
    public static const CLOUD_TYPE:String = 'cloudType';
    public static const OPAQUE_SKY_COVER:String = 'opaqueSkyCover';
    // TOTAL_SKY_COVER is aliased with Clouds.CLOUD_COVER
    public static const TOTAL_SKY_COVER:String = 'totalSkyCover';
    public static const SNOW_FLAG:String = 'snowFlag'; // 1 = snow
    public static const RAIN_FLAG:String = 'rainFlag'; // 1 = rain
    public static const MONTHLY_CLEARNESS_INDEXES:String = 'clearnessNums';
    public static const VISIBILITY_km:String = 'visibility_km';
    public static const CEILING_HEIGHT_m:String = 'ceilingHeight_m';
    public static const PRESENT_WEATHER_OBSERVATION:String =
        'presentWeatherObservation';
    public static const PRESENT_WEATHER_CODES:String = 'presentWeatherCodes';
    public static const PRECIPITABLE_WATER_mm:String = 'precipitableWater_mm';
    public static const AEROSOL_OPTICAL_DEPTH_thousandths:String =
        'aerosolOpticalDepth_thousandths';
    public static const SNOW_DEPTH_cm:String = 'snowDepth_cm';
    public static const DAYS_SINCE_LAST_SNOWFALL:String =
        'daysSinceLastSnowfall';
    public static const ALBEDO:String = 'albedo';
    public static const LIQUID_PRECIPITATION_DEPTH_mm:String =
        'liquidPrecipDepth_mm';
    // is LIQUID_PRECIPITATION_QUANTITY_hr -- shouldn't this be duration?
    public static const LIQUID_PRECIPITATION_QUANTITY_hr:String =
        'liquidPrecipQuantity_hr';

    public static const MINS:Object = new Object();
    MINS[CLOUD_TYPE] = 0;
    MINS[OPAQUE_SKY_COVER] = 0;
    MINS[TOTAL_SKY_COVER] = 0;
    MINS[SNOW_FLAG] = 0;
    MINS[RAIN_FLAG] = 0;
    MINS[AEROSOL_OPTICAL_DEPTH_thousandths] = 0;

    public static const MAXS:Object = new Object();
    MAXS[CLOUD_TYPE] = 10;
    MAXS[OPAQUE_SKY_COVER] = 10;
    MAXS[TOTAL_SKY_COVER] = 10;
    MAXS[SNOW_FLAG] = 1;
    MAXS[RAIN_FLAG] = 1;
    MAXS[AEROSOL_OPTICAL_DEPTH_thousandths] = 1;

    public static const
    CONSTRAINT_FUNC:Function =
        ConstraintFunctions.makeRangeConstraintFunction(
            MapUtils.keys(MINS),
            MapUtils.keys(MAXS),
            MINS, MAXS, SCALE_FACTORS);

    public static const
    SCALE_FACTORS:Object = Normalization.makeScaleFactors(MINS, MAXS);

    public static const DERIVATION_FUNCS:Object = {};

    // an empty setting set will be interpreted as "No Restrictions" on setting
    // values.
    public static const SETTING_SETS:Array = [];

    public static const SERIES_META_DATA:Object = new Object();
    SERIES_META_DATA[OPAQUE_SKY_COVER] = new MetaData(
        OPAQUE_SKY_COVER,
        "Opaque Sky Cover",
        NoUnits.NONE,
        NoUnits.QUANTITY_OF_MEASURE,
        MetaData.makeValueGetter(OPAQUE_SKY_COVER),
        MetaData.DATA_TYPE__NUMBER,
        [],
        CONSTRAINT_FUNC,
        {},
        true,
        [NoUnits.NONE]);
    SERIES_META_DATA[SNOW_FLAG] = new MetaData(
        SNOW_FLAG,
        "Snow Flag",
        NoUnits.NONE,
        NoUnits.QUANTITY_OF_MEASURE,
        MetaData.makeValueGetter(SNOW_FLAG),
        MetaData.DATA_TYPE__NUMBER,
        [],
        CONSTRAINT_FUNC,
        {},
        true,
        [NoUnits.NONE]);
    SERIES_META_DATA[RAIN_FLAG] = new MetaData(
        RAIN_FLAG,
        "Rain Flag",
        NoUnits.NONE,
        NoUnits.QUANTITY_OF_MEASURE,
        MetaData.makeValueGetter(RAIN_FLAG),
        MetaData.DATA_TYPE__NUMBER,
        [],
        CONSTRAINT_FUNC,
        {},
        true,
        [NoUnits.NONE]);
    SERIES_META_DATA[CLOUD_TYPE] = new MetaData(
        CLOUD_TYPE,
        "Cloud Type",
        NoUnits.NONE,
        NoUnits.QUANTITY_OF_MEASURE,
        MetaData.makeValueGetter(CLOUD_TYPE),
        MetaData.DATA_TYPE__NUMBER,
        [],
        CONSTRAINT_FUNC,
        {},
        true,
        [NoUnits.NONE]);
    SERIES_META_DATA[TOTAL_SKY_COVER] = new MetaData(
        TOTAL_SKY_COVER,
        "Total Sky Cover",
        NoUnits.NONE,
        NoUnits.QUANTITY_OF_MEASURE,
        MetaData.makeValueGetter(TOTAL_SKY_COVER),
        MetaData.DATA_TYPE__NUMBER,
        [],
        CONSTRAINT_FUNC,
        {},
        true,
        [NoUnits.NONE]);
    SERIES_META_DATA[VISIBILITY_km] = new MetaData(
        VISIBILITY_km,
        "Visibility",
        NoUnits.NONE,
        NoUnits.QUANTITY_OF_MEASURE,
        MetaData.makeValueGetterWithDefault(VISIBILITY_km, 0.0),
        MetaData.DATA_TYPE__NUMBER,
        [],
        CONSTRAINT_FUNC,
        {},
        true,
        [NoUnits.NONE]);
    SERIES_META_DATA[CEILING_HEIGHT_m] = new MetaData(
        CEILING_HEIGHT_m,
        "Ceiling Height",
        NoUnits.NONE,
        NoUnits.QUANTITY_OF_MEASURE,
        MetaData.makeValueGetterWithDefault(CEILING_HEIGHT_m, 0.0),
        MetaData.DATA_TYPE__NUMBER,
        [],
        CONSTRAINT_FUNC,
        {},
        true,
        [NoUnits.NONE]);
    SERIES_META_DATA[PRESENT_WEATHER_OBSERVATION] = new MetaData(
        PRESENT_WEATHER_OBSERVATION,
        "Present Weather Observation",
        NoUnits.NONE,
        NoUnits.QUANTITY_OF_MEASURE,
        MetaData.makeValueGetterWithDefault(
            PRESENT_WEATHER_OBSERVATION, 0.0),
        MetaData.DATA_TYPE__NUMBER,
        [],
        CONSTRAINT_FUNC,
        {},
        true,
        [NoUnits.NONE]);
    SERIES_META_DATA[PRESENT_WEATHER_CODES] = new MetaData(
        PRESENT_WEATHER_CODES,
        "Present Weather Codes",
        NoUnits.NONE,
        NoUnits.QUANTITY_OF_MEASURE,
        MetaData.makeValueGetterWithDefault(
            PRESENT_WEATHER_CODES, 0.0),
        MetaData.DATA_TYPE__NUMBER,
        [],
        CONSTRAINT_FUNC,
        {},
        true,
        [NoUnits.NONE]);
    SERIES_META_DATA[PRECIPITABLE_WATER_mm] = new MetaData(
        PRECIPITABLE_WATER_mm,
        "Precipitable Water",
        NoUnits.NONE,
        NoUnits.QUANTITY_OF_MEASURE,
        MetaData.makeValueGetterWithDefault(
            PRECIPITABLE_WATER_mm, 0.0),
        MetaData.DATA_TYPE__NUMBER,
        [],
        CONSTRAINT_FUNC,
        {},
        true,
        [NoUnits.NONE]);
    SERIES_META_DATA[AEROSOL_OPTICAL_DEPTH_thousandths] = new MetaData(
        AEROSOL_OPTICAL_DEPTH_thousandths,
        "Aerosol Optical Depth",
        NoUnits.NONE,
        NoUnits.QUANTITY_OF_MEASURE,
        MetaData.makeValueGetterWithDefault(
            AEROSOL_OPTICAL_DEPTH_thousandths, 0.0),
        MetaData.DATA_TYPE__NUMBER,
        [],
        CONSTRAINT_FUNC,
        {},
        true,
        [NoUnits.NONE]);
    SERIES_META_DATA[SNOW_DEPTH_cm] = new MetaData(
        SNOW_DEPTH_cm,
        "Snow Depth",
        NoUnits.NONE,
        NoUnits.QUANTITY_OF_MEASURE,
        MetaData.makeValueGetterWithDefault(SNOW_DEPTH_cm, 0.0),
        MetaData.DATA_TYPE__NUMBER,
        [],
        CONSTRAINT_FUNC,
        {},
        true,
        [NoUnits.NONE]);
    SERIES_META_DATA[DAYS_SINCE_LAST_SNOWFALL] = new MetaData(
        DAYS_SINCE_LAST_SNOWFALL,
        "Days Since Last Snowfall",
        NoUnits.NONE,
        NoUnits.QUANTITY_OF_MEASURE,
        MetaData.makeValueGetterWithDefault(DAYS_SINCE_LAST_SNOWFALL, 0.0),
        MetaData.DATA_TYPE__NUMBER,
        [],
        CONSTRAINT_FUNC,
        {},
        true,
        [NoUnits.NONE]);
    SERIES_META_DATA[ALBEDO] = new MetaData(
        ALBEDO,
        "Albedo",
        NoUnits.NONE,
        NoUnits.QUANTITY_OF_MEASURE,
        MetaData.makeValueGetterWithDefault(ALBEDO, 0.0),
        MetaData.DATA_TYPE__NUMBER,
        [],
        CONSTRAINT_FUNC,
        {},
        true,
        [NoUnits.NONE]);
    SERIES_META_DATA[LIQUID_PRECIPITATION_DEPTH_mm] = new MetaData(
        LIQUID_PRECIPITATION_DEPTH_mm,
        "Liquid Precipitation Depth",
        NoUnits.NONE,
        NoUnits.QUANTITY_OF_MEASURE,
        MetaData.makeValueGetterWithDefault(
            LIQUID_PRECIPITATION_DEPTH_mm, 0.0),
        MetaData.DATA_TYPE__NUMBER,
        [],
        CONSTRAINT_FUNC,
        {},
        true,
        [NoUnits.NONE]);
    SERIES_META_DATA[LIQUID_PRECIPITATION_QUANTITY_hr] = new MetaData(
        LIQUID_PRECIPITATION_QUANTITY_hr,
        "Liquid Precipitation Quantity",
        NoUnits.NONE,
        NoUnits.QUANTITY_OF_MEASURE,
        MetaData.makeValueGetterWithDefault(
            LIQUID_PRECIPITATION_QUANTITY_hr, 0.0),
        MetaData.DATA_TYPE__NUMBER,
        [],
        CONSTRAINT_FUNC,
        {},
        true,
        [NoUnits.NONE]);

    public static const HEADER_META_DATA:Object = new Object();
    HEADER_META_DATA[MONTHLY_CLEARNESS_INDEXES] = new MetaData(
      "clearnessNums",
      "Monthly Clearness Indexes",
      NoUnits.NONE,
      NoUnits.QUANTITY_OF_MEASURE,
      MetaData.makeValueGetterWithDefault(MONTHLY_CLEARNESS_INDEXES, ""),
      MetaData.DATA_TYPE__STRING,
      [],
      null,
      {},
      true,
      [NoUnits.NONE]);
}
}