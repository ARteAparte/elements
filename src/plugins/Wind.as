/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package plugins {
import dse.ConstraintFunctions;
import dse.MetaData;
import dse.Normalization;

import units.AngleUnits;
import units.SpeedUnits;

public class Wind {
    public static const PLUGIN_NAME:String = 'wind';
    public static const SPEED_m__s:String = "windSpeed_m__s";
    public static const DIRECTION_deg:String = "windDirection_deg";

    public static const SETTING_SETS:Array = [];

    public static const MINS:Object = new Object();
    MINS[SPEED_m__s] = 0.0;
    MINS[DIRECTION_deg] = 0.0;

    public static const MAXS:Object = new Object();
    MAXS[SPEED_m__s] = 100.0;
    MAXS[DIRECTION_deg] = 360.0;

    public static const SCALE_FACTORS:Object =
        Normalization.makeScaleFactors(MINS, MAXS);

    public static const CONSTRAINT_FUNCTION:Function =
        ConstraintFunctions.makeRangeConstraintFunction(
          [SPEED_m__s, DIRECTION_deg],
          [SPEED_m__s, DIRECTION_deg],
          MINS,
          MAXS,
          SCALE_FACTORS);

    public static const SERIES_META_DATA:Object = new Object();
    SERIES_META_DATA[DIRECTION_deg] = new MetaData(
        DIRECTION_deg,
        'Wind Direction',
        AngleUnits.DEGREES,
        AngleUnits.QUANTITY_OF_MEASURE,
        MetaData.makeValueGetter(DIRECTION_deg),
        MetaData.DATA_TYPE__NUMBER,
        SETTING_SETS,
        CONSTRAINT_FUNCTION,
        null,
        true,
        AngleUnits.AVAILABLE_UNITS);
    SERIES_META_DATA[SPEED_m__s] = new MetaData(
        SPEED_m__s,
        'Wind Speed',
        SpeedUnits.METERS_PER_SECOND,
        SpeedUnits.QUANTITY_OF_MEASURE,
        MetaData.makeValueGetter(SPEED_m__s),
        MetaData.DATA_TYPE__NUMBER,
        SETTING_SETS,
        CONSTRAINT_FUNCTION,
        null,
        true,
        SpeedUnits.AVAILABLE_UNITS);

    public static const DERIVATION_FUNCS:Object = {};
}
}