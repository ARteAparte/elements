/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package plugins {
import dse.DateTime;
import dse.MetaData;

import units.ElapsedTimeUnits;
import units.NoUnits;

import utils.FormatUtils;
import utils.MapUtils;

/* State Point (8 items)
========================
(in Global Header)
- refYear :: int -- year for the reference starting date/time
- refMonth :: int -- month of the reference starting date/time
                     1 = January, 12 = December
- refDayOfMonth :: int -- reference day of month for starting date/time
                          1 - 31
- refHourOfDay :: int -- reference hour of the day for starting date/time
                         0 - 23
- refMinuteOfHour :: int -- reference minute of hour of starting date/time
                            0 - 59
- refSecondOfMinute :: Number -- reference second of minute of starting
                                 date/time
                                 0.0 - 59.999*
- timeZone :: Number -- time-zone of the reference point
(in ReadingSet)
- elapsedTime_hrs :: Number -- elapsed time in hours since reference
                               date/time */
public class DateAndTime {
    public static const PLUGIN_NAME:String = 'dateTime';

    // STATE POINT READINGS
    // in the ReadingSet
    public static const ELAPSED_TIME_hrs:String = 'elapsedTime_hrs';
    // in the Global Header
    public static const REFERENCE_YEAR:String = 'refYear';
    public static const REFERENCE_MONTH:String = 'refMonth';
    public static const REFERENCE_DAY:String = 'refDayOfMonth';
    public static const REFERENCE_HOUR:String = 'refHourOfDay';
    public static const REFERENCE_MINUTE:String = 'refMinOfHour';
    public static const REFERENCE_SECOND:String = 'refSecOfMin';
    public static const TIME_ZONE:String = 'timeZone';

    // DERIVED READINGS
    public static const DAY_OF_THE_YEAR:String = 'dayOfTheYear';

    // Date Parts
    public static const YEAR:String = 'year';
    public static const MONTH:String = 'month';
    public static const DAY:String = 'dayOfMonth';
    public static const HOUR:String = 'hourOfDay';
    public static const MINUTE:String = 'minuteOfHour';
    public static const SECOND:String = 'secondOfMinute';
    // the date/time stamp
    public static const DATE_TIME:String = 'dateTime';
    public static const MONTH_YEAR:String = 'monthYear';

    // Constants
    public static const MONTH_NAMES:Array = [
        "January", "February", "March",
        "April", "May", "June",
        "July", "August", "September",
        "October", "November", "December"];
    public static const JANUARY:int = 1;
    public static const JAN:int = JANUARY;
    public static const FEBRUARY:int = 2;
    public static const FEB:int = FEBRUARY;
    public static const MARCH:int = 3;
    public static const MAR:int = MARCH;
    public static const APRIL:int = 4;
    public static const APR:int = APRIL;
    public static const MAY:int = 5;
    public static const JUNE:int = 6;
    public static const JUN:int = JUNE;
    public static const JULY:int = 7;
    public static const JUL:int = JULY;
    public static const AUGUST:int = 8;
    public static const AUG:int = AUGUST;
    public static const SEPTEMBER:int = 9;
    public static const SEP:int = SEPTEMBER;
    public static const OCTOBER:int = 10;
    public static const OCT:int = OCTOBER;
    public static const NOVEMBER:int = 11;
    public static const NOV:int = NOVEMBER;
    public static const DECEMBER:int = 12;
    public static const DEC:int = DECEMBER;
    public static const DAYS_PER_MONTH:Array =
        [31,28,31,30,31,30,31,31,30,31,30,31];
    public static const CUMULATIVE_DAYS__MONTH:Array =
        [0,31,59,90,120,151,181,212,243,273,304,334,365];
    public static const HOURS_PER_DAY:Number = 24.0;
    public static const HOURS_PER_YEAR:Number = 8760.0;
    public static const SECONDS_PER_HOUR:Number = 3600.0;
    public static const SECONDS_PER_MINUTE:Number = 60.0;
    public static const MINUTES_PER_HOUR:Number = 60.0;

    public static function
    makeDataPoint(elapsedTime_hrs:Number):Object {
        return MapUtils.makeObject([
            ELAPSED_TIME_hrs, elapsedTime_hrs]);
    }

    public static function
    makeHeaderFromDate(timeZone:Number,
                       year:int, month:int, day:int, hour:int, minute:int=0,
                       second:int=0):Object {
        return MapUtils.makeObject([
            TIME_ZONE, timeZone,
            REFERENCE_YEAR, year,
            REFERENCE_MONTH, month,
            REFERENCE_DAY, day,
            REFERENCE_HOUR, hour,
            REFERENCE_MINUTE, minute,
            REFERENCE_SECOND, second]);
    }

    public static function
    constraintFunction(globalHeader:Object, vals:Object):Number {
        // elapsed hours can never be inconsistent. If it goes negative,
        // that just means we're before the reference time.
        return 0.0;
    }

    public static function
    deriveX(f:Function):Function {
        return function(header:Object, vals:Object):* {
                var elapsedTime_hrs:Number = vals[ELAPSED_TIME_hrs] as Number;
                var refYr:int = MapUtils.atKeyOrAlt(
                    header, REFERENCE_YEAR, 2011);
                var refMonth:int = MapUtils.atKeyOrAlt(
                    header, REFERENCE_MONTH, 1);
                var refDay:int = MapUtils.atKeyOrAlt(
                    header, REFERENCE_DAY, 1);
                var refHr:int = MapUtils.atKeyOrAlt(
                    header, REFERENCE_HOUR, 0);
                var refMin:int = MapUtils.atKeyOrAlt(
                    header, REFERENCE_MINUTE, 0);
                var refSec:int = MapUtils.atKeyOrAlt(
                    header, REFERENCE_SECOND, 0.0);
                validateDate(refYr, refMonth, refDay, refHr, refMin, refSec);
                return f(elapsedTime_hrs,
                    refYr, refMonth, refDay, refHr, refMin, refSec);
        };
    }

    public static const DERIVATION_FUNCS:Object = new Object();
    DERIVATION_FUNCS[YEAR] = deriveX(deriveCurrentYear);
    DERIVATION_FUNCS[MONTH] = deriveX(deriveCurrentMonth);
    DERIVATION_FUNCS[DAY] = deriveX(deriveDayOfMonth);
    DERIVATION_FUNCS[HOUR] = deriveX(deriveHourOfDay);
    DERIVATION_FUNCS[MINUTE] = deriveX(deriveMinuteOfHour);
    DERIVATION_FUNCS[SECOND] = deriveX(deriveSecondOfMinute);
    DERIVATION_FUNCS[DATE_TIME] = deriveX(deriveDateTime);
    DERIVATION_FUNCS[DAY_OF_THE_YEAR] = deriveX(deriveDayOfYear);
    DERIVATION_FUNCS[MONTH_YEAR] = deriveX(deriveMonthYear);

    public static const SETTING_SETS:Array = new Array();

    public static const SERIES_META_DATA:Object = new Object();
    SERIES_META_DATA[ELAPSED_TIME_hrs] = new MetaData(
        ELAPSED_TIME_hrs, // name
        "Elapsed Time", // fullName
        ElapsedTimeUnits.HOURS, // baseUnits
        'Elapsed Time', // quantity of measure
        MetaData.makeValueGetter(ELAPSED_TIME_hrs), // getter
        MetaData.DATA_TYPE__NUMBER, // data type
        null, // setting sets
        null, // constraint function
        null,
        false,
        [ElapsedTimeUnits.SECONDS, ElapsedTimeUnits.MINUTES,
         ElapsedTimeUnits.HOURS, ElapsedTimeUnits.DAYS,
         ElapsedTimeUnits.WEEKS]);
    SERIES_META_DATA[DAY_OF_THE_YEAR] = new MetaData(
        DAY_OF_THE_YEAR,
        "Day of the Year",
        ElapsedTimeUnits.DAYS,
        'Elapsed Time',
        DERIVATION_FUNCS[DAY_OF_THE_YEAR],
        MetaData.DATA_TYPE__NUMBER,
        null,
        null,
        null,
        false,
        [NoUnits.NONE]);
    SERIES_META_DATA[YEAR] = new MetaData(
        YEAR, "Current Year", NoUnits.NONE,
        'Instant in Time', DERIVATION_FUNCS[YEAR],
        MetaData.DATA_TYPE__NUMBER, null, null, null, false, [NoUnits.NONE]);
    SERIES_META_DATA[MONTH] = new MetaData(
        MONTH, "Current Month", NoUnits.NONE,
        'Instant in Time', DERIVATION_FUNCS[MONTH],
        MetaData.DATA_TYPE__NUMBER, null, null, null, false, [NoUnits.NONE]);
    SERIES_META_DATA[DAY] = new MetaData(
        DAY, "Day of the Month", NoUnits.NONE,
        'Instant in Time', DERIVATION_FUNCS[DAY],
        MetaData.DATA_TYPE__NUMBER, null, null, null, false, [NoUnits.NONE]);
    SERIES_META_DATA[HOUR] = new MetaData(
        HOUR, "Hour of the Day", NoUnits.NONE,
        'Instant in Time', DERIVATION_FUNCS[HOUR],
        MetaData.DATA_TYPE__NUMBER, null, null, null, false, [NoUnits.NONE]);
    SERIES_META_DATA[MINUTE] = new MetaData(
        MINUTE, "Minute of the Hour", NoUnits.NONE,
        'Instant in Time', DERIVATION_FUNCS[MINUTE],
        MetaData.DATA_TYPE__NUMBER, null, null, null, false, [NoUnits.NONE]);
    SERIES_META_DATA[SECOND] = new MetaData(
        SECOND, "Second of the Minute", NoUnits.NONE,
        'Instant in Time', DERIVATION_FUNCS[SECOND],
        MetaData.DATA_TYPE__NUMBER, null, null, null, false, [NoUnits.NONE]);
    SERIES_META_DATA[DATE_TIME] = new MetaData(
        DATE_TIME, "Date/Time", NoUnits.NONE,
        'Instant in Time', DERIVATION_FUNCS[DATE_TIME],
        MetaData.DATA_TYPE__STRING, null, null, null, false, [NoUnits.NONE]);
    SERIES_META_DATA[MONTH_YEAR] = new MetaData(
        MONTH_YEAR, 'Month/Year', NoUnits.NONE,
        'Instant in Time', DERIVATION_FUNCS[MONTH_YEAR],
        MetaData.DATA_TYPE__STRING, null, null, null, false, [NoUnits.NONE]);

    public static var HEADER_META_DATA:Object = {};
    HEADER_META_DATA[REFERENCE_YEAR] =  new MetaData(
      REFERENCE_YEAR, // name
      'Reference Year', // full name
      NoUnits.NONE, // base units
      'Reference Year', // quantity of measure
      function(h:Object,pt:Object):Number {
        return h[REFERENCE_YEAR];
      }, //getter
      MetaData.DATA_TYPE__NUMBER, // data type
      null, // setting sets
      function(hdr:Object, val:Object):Number{
        if (hdr.hasOwnProperty(REFERENCE_YEAR)) {
          trace('REFERENCE_YEAR constraint function');
          var yr:int = int(hdr[REFERENCE_YEAR]);
          if ((yr > 2100) || (yr < 1900)) {
            return 1.0;
          }
        }
        return 0.0;
      }, // constraint function
      null,
      true,
      [NoUnits.NONE]);
    HEADER_META_DATA[REFERENCE_MONTH] =  new MetaData(
      REFERENCE_MONTH,
      'Reference Month of the Year',
      NoUnits.NONE,
      'Reference Month',
      function(h:Object,pt:Object):Number { return h[REFERENCE_MONTH]; },
      MetaData.DATA_TYPE__NUMBER,
      null,
      function(h:Object, pt:Object):Number{
        trace('REFERENCE_MONTH constraint function');
        if (h.hasOwnProperty(REFERENCE_MONTH) && h.hasOwnProperty(REFERENCE_DAY)) {
          var m:int = int(h[REFERENCE_MONTH]);
          if ((m < 1) || (m > 12)) {
            return 1.0;
          }
          var d:int = int(h[REFERENCE_DAY]);
          var maxDays:int = DAYS_PER_MONTH[m - 1];
          if ((d < 1) || (d > maxDays)) {
            return 1.0;
          }
        }
        return 0.0;
      },
      null,
      true,
      [NoUnits.NONE]);
    HEADER_META_DATA[REFERENCE_DAY] =  new MetaData(
      REFERENCE_DAY,
      'Reference Day of the Month',
      NoUnits.NONE,
      'Reference Day',
      function(h:Object,pt:Object):Number { return h[REFERENCE_DAY]; },
      MetaData.DATA_TYPE__NUMBER,
      null,
      function(h:Object,pt:Object):Number {
        trace('REFERENCE_DAY constraint function');
        MapUtils.traceObject(h, 'h');
        MapUtils.traceObject(pt, 'pt');
        if (h.hasOwnProperty(REFERENCE_MONTH) && h.hasOwnProperty(REFERENCE_DAY)) {
          var m:int = int(h[REFERENCE_MONTH]);
          var d:int = int(h[REFERENCE_DAY]);
          trace('m', m);
          trace('d', d);
          var maxDays:int = DAYS_PER_MONTH[m - 1];
          trace('maxDays', maxDays);
          if ((d > maxDays) || (d < 1)) {
            return 1.0;
          }
        }
        return 0.0;
      },
      null,
      true,
      [NoUnits.NONE]);
    HEADER_META_DATA[REFERENCE_HOUR] =  new MetaData(
      REFERENCE_HOUR,
      'Reference Hour of the Day',
      NoUnits.NONE,
      'Hour of the Day',
      function(h:Object,pt:Object):Number { return h[REFERENCE_HOUR]; },
      MetaData.DATA_TYPE__NUMBER,
      null,
      function(h:Object,pt:Object):Number {
        trace('REFERENCE_HOUR constraint function');
        if (h.hasOwnProperty(REFERENCE_HOUR)) {
          var hr:int = int(h[REFERENCE_HOUR]);
          if ((hr < 0) || (hr > 23)) {
            return 1.0;
          }
        }
        return 0.0;
      },
      null,
      true,
      [NoUnits.NONE]);
    HEADER_META_DATA[REFERENCE_MINUTE] =  new MetaData(
      REFERENCE_MINUTE,
      'Reference Minute of the Hour',
      NoUnits.NONE,
      'Minute of the Hour',
      function(h:Object,pt:Object):Number { return h[REFERENCE_MINUTE]; },
      MetaData.DATA_TYPE__NUMBER,
      null,
      function(h:Object,pt:Object):Number {
        trace('REFERENCE_MINUTE constraint function');
        if (h.hasOwnProperty(REFERENCE_MINUTE)) {
          var min:int = int(h[REFERENCE_MINUTE]);
          if ((min < 0) || (min > 59)) {
            return 1.0;
          }
        }
        return 0.0;
      },
      null,
      true,
      [NoUnits.NONE]);
    HEADER_META_DATA[REFERENCE_SECOND] =  new MetaData(
      REFERENCE_SECOND,
      'Reference Second of the Minute',
      NoUnits.NONE,
      'Second of the Minute',
      function(h:Object,pt:Object):Number { return h[REFERENCE_SECOND]; },
      MetaData.DATA_TYPE__NUMBER,
      null,
      function(h:Object,pt:Object):Number {
        trace('REFERENCE_SECOND constraint function');
        if (h.hasOwnProperty(REFERENCE_SECOND)) {
          var sec:Number = h[REFERENCE_SECOND];
          if ((sec >= 60.0) || (sec < 0.0)) {
            return 1.0;
          }
        }
        return 0.0;
      },
      null,
      true,
      [NoUnits.NONE]);
    HEADER_META_DATA[TIME_ZONE] =  new MetaData(
      TIME_ZONE,
      'Time Zone',
      NoUnits.NONE,
      'Time Zone',
      function(h:Object,pt:Object):Number { return h[TIME_ZONE]; },
      MetaData.DATA_TYPE__NUMBER,
      null,
      function(h:Object,pt:Object):Number {
        trace('TIME_ZONE constraint function');
        if (h.hasOwnProperty(TIME_ZONE)) {
          var tz:Number = h[TIME_ZONE];
          if ((tz <= -24) || (tz >= 24)) {
            return 1.0;
          }
        }
        return 0.0;
      },
      null,
      true,
      [NoUnits.NONE]);

    public static const MINS:Object = {};
    public static const MAXS:Object = {};

    public static function
    validateDate(year:int, month:int, dayOfMonth:int, hourOfDay:int,
                 minuteOfHour:int=0, secondOfMinute:int=0):void {
        var err:String;
        if ((month < 1) || (month > 12)) {
            trace(year, month, dayOfMonth, hourOfDay,
                minuteOfHour, secondOfMinute);
            throw new Error("month must be from 1 to 12 inclusive");
        }
        if ((dayOfMonth < 1) || (dayOfMonth > 31)) {
            trace('error: ', year, month, dayOfMonth, hourOfDay,
                minuteOfHour, secondOfMinute);
            throw new Error("dayOfMonth must be between 1 and 31 inclusive");
        }
        if ((hourOfDay < 0) || (hourOfDay > 23)) {
            trace('error: ', year, month, dayOfMonth, hourOfDay,
                minuteOfHour, secondOfMinute);
            throw new Error(
                "hourOfDay must be between 0 (midnight) " +
                "and 23 (11 PM) inclusive");
        }
        if ((minuteOfHour < 0) || (minuteOfHour > 59)) {
            trace('error: ', year, month, dayOfMonth, hourOfDay,
                minuteOfHour, secondOfMinute);
            throw new Error("minutes must be from 0 to 59 inclusive");
        }
        if ((secondOfMinute < 0) || (secondOfMinute > 59)) {
            trace('error: ', year, month, dayOfMonth, hourOfDay,
                minuteOfHour, secondOfMinute);
            throw new Error("seconds must be from 0 to 59 inclusive");
        }
    }

    public static function
    secondsSinceYearStart(year:int, month:int,
                          dayOfMonth:int, hourOfDay:int,
                          minuteOfHour:int=0,
                          secondOfMinute:int=0):Number {
        validateDate(
            year, month, dayOfMonth, hourOfDay, minuteOfHour, secondOfMinute);
        var dayOfYear:int = dayOfYearFromDate(year, month, dayOfMonth);
        var days:int = dayOfYear - 1;
        return days * 24 * 60 * 60 + hourOfDay * 60 * 60
            + minuteOfHour * 60 + secondOfMinute;
    }

    public static function
    hourOfYearFromDate(year:int, month:int, dayOfMonth:int,
                                hourOfDay:int, minuteOfHour:int=0,
                                secondOfMinute:int=0):Number {
        // we *could* handle leap years... but we won't for now
        validateDate(
            year, month, dayOfMonth, hourOfDay, minuteOfHour, secondOfMinute);
        return secondsSinceYearStart(year, month, dayOfMonth, hourOfDay,
                                     minuteOfHour, secondOfMinute)
            / SECONDS_PER_HOUR;
    }

    public static function
    hourOfYearFromDateObject(date:Object):Number {
        validateDate(date.year, date.month, date.dayOfMonth,
            date.hourOfDay,
            MapUtils.atKeyOrAlt(date, 'minuteOfHour', 0),
            MapUtils.atKeyOrAlt(date, 'secondOfMinute', 0));
        return secondsSinceYearStart(date.year, date.month, date.dayOfMonth,
            date.hourOfDay,
            MapUtils.atKeyOrAlt(date, 'minuteOfHour', 0),
            MapUtils.atKeyOrAlt(date, 'secondOfMinute', 0))
            / SECONDS_PER_HOUR;
    }

    public static function hourOfYearToDate(
      hourOfYear:Number,
      year:int):Object
    {
      var addedYears:int = int(Math.floor(
        hourOfYear / HOURS_PER_YEAR));
      var remainingHours:Number = hourOfYear - addedYears * HOURS_PER_YEAR;
      var dayOfYear:int = int(Math.floor(remainingHours / HOURS_PER_DAY));
      var hourOfDay:int = int(Math.floor(
        remainingHours - dayOfYear * HOURS_PER_DAY));
      var minOfHour:int = int(Math.floor(
        (remainingHours - dayOfYear * HOURS_PER_DAY - hourOfDay)
        * MINUTES_PER_HOUR));
      var secOfMin:int = int(Math.round(
        ((remainingHours - dayOfYear * HOURS_PER_DAY - hourOfDay)
          * MINUTES_PER_HOUR - minOfHour) * SECONDS_PER_MINUTE));
      if (secOfMin == 60) {
        secOfMin = 0;
        minOfHour += 1;
      }
      if (minOfHour == 60) {
        minOfHour = 0;
        hourOfDay += 1;
      }
      if (hourOfDay == 24) {
        hourOfDay = 0;
        dayOfYear += 1;
      }
      if (dayOfYear >= 365) {
        dayOfYear = dayOfYear - 365;
        year += 1;
      }
      for (var monthIdx:int = 1; monthIdx < 13; monthIdx++) {
        if ((dayOfYear >= CUMULATIVE_DAYS__MONTH[monthIdx-1]) &&
          (dayOfYear < CUMULATIVE_DAYS__MONTH[monthIdx])) {
          break;
        }
      }
      return makeDate(
        year + addedYears, monthIdx,
        dayOfYear - CUMULATIVE_DAYS__MONTH[monthIdx-1] + 1,
        hourOfDay, minOfHour, secOfMin);
    }
    
    public static function hourOfYearToDateTime(
      hourOfYear:Number,
      year:int):DateTime
    {
      var addedYears:int = int(Math.floor(
        hourOfYear / HOURS_PER_YEAR));
      var remainingHours:Number = hourOfYear - addedYears * HOURS_PER_YEAR;
      var dayOfYear:int = int(Math.floor(remainingHours / HOURS_PER_DAY));
      var hourOfDay:int = int(Math.floor(
        remainingHours - dayOfYear * HOURS_PER_DAY));
      var minOfHour:int = int(Math.floor(
        (remainingHours - dayOfYear * HOURS_PER_DAY - hourOfDay)
        * MINUTES_PER_HOUR));
      var secOfMin:int = int(Math.round(
        ((remainingHours - dayOfYear * HOURS_PER_DAY - hourOfDay)
          * MINUTES_PER_HOUR - minOfHour) * SECONDS_PER_MINUTE));
      if (secOfMin == 60) {
        secOfMin = 0;
        minOfHour += 1;
      }
      if (minOfHour == 60) {
        minOfHour = 0;
        hourOfDay += 1;
      }
      if (hourOfDay == 24) {
        hourOfDay = 0;
        dayOfYear += 1;
      }
      if (dayOfYear >= 365) {
        dayOfYear = dayOfYear - 365;
        year += 1;
      }
      for (var monthIdx:int = 1; monthIdx < 13; monthIdx++) {
        if ((dayOfYear >= CUMULATIVE_DAYS__MONTH[monthIdx-1]) &&
          (dayOfYear < CUMULATIVE_DAYS__MONTH[monthIdx])) {
          break;
        }
      }
      return new DateTime(
        year + addedYears,
        monthIdx,
        dayOfYear - CUMULATIVE_DAYS__MONTH[monthIdx-1] + 1,
        hourOfDay,
        minOfHour,
        secOfMin);
    }

    public static function
    elapsedHoursBetweenDates(year0:int, month0:int, day0:int, hour0:int,
                             minute0:int, second0:int,
                             year1:int, month1:int, day1:int, hour1:int,
                             minute1:int, second1:int):Number {
        var hrs0:Number = hourOfYearFromDate(
            year0, month0, day0, hour0, minute0, second0);
        var hrs1:Number = hourOfYearFromDate(
            year1, month1, day1, hour1, minute1, second1);
        var years0:Number = year0 + hrs0 / HOURS_PER_YEAR;
        var years1:Number = year1 + hrs1 / HOURS_PER_YEAR;
        var diffYears:Number = years1 - years0;
        return diffYears * HOURS_PER_YEAR;
    }

    public static function
    makeElapsedHoursToReferenceDateFunc(refYear:int, refMonth:int, refDay:int,
                                        refHour:int, refMinute:int,
                                        refSecond:int):Function {
        return function(year:int, month:int, day:int, hour:int,
                        minute:int, second:int):Number {
            return elapsedHoursBetweenDates(refYear, refMonth, refDay, refHour,
                refMinute, refSecond, year, month, day, hour, minute, second);
        };
    }

    public static function
    dayOfYearFromDate(year:int, month:int, dayOfMonth:int):int {
        // no leap years for now
        return CUMULATIVE_DAYS__MONTH[month-1] + dayOfMonth;
    }

    public static function
    dateToCompactString(year:int, month:int, dayOfMonth:int, hourOfDay:int,
                        minuteOfHour:int=0, secondOfMinute:int=0):String {
        var pad02:Function = FormatUtils.pad02;
        var yr:String = year.toString();
        var m:String = pad02(month.toString());
        var d:String = pad02(dayOfMonth.toString());
        var hr:String = pad02(hourOfDay.toString());
        var min:String = pad02(minuteOfHour.toString());
        var sec:String = pad02(secondOfMinute.toString());
        return yr + "/" + m + "/" + d + " @ " + hr + ":" + min + ":" + sec;
    }

    public static function
    parseCompactString(str:String):Object {
        var m:Array = str.match(/\d+/g);
        if (m.length != 6) {
            throw new Error(
                "DateTime :: string does not consist of 6 time components: "
                + str);
        }
        return makeDate(
            int(m[0]), int(m[1]), int(m[2]), int(m[3]), int(m[4]), int(m[5]));
    }

    public static function makeDate(
      year:int,
      month:int,
      dayOfMonth:int,
      hourOfDay:int,
      minuteOfHour:int=0,
      secondOfMinute:int=0):Object
    {
      validateDate(
        year, month, dayOfMonth, hourOfDay, minuteOfHour, secondOfMinute);
      return {
        year:year,
        month:month,
        dayOfMonth:dayOfMonth,
        hourOfDay:hourOfDay,
        minuteOfHour:minuteOfHour,
        secondOfMinute:secondOfMinute};
    }

    public static function
    dateToString(year:int, month:int, dayOfMonth:int, hourOfDay:int,
                 minuteOfHour:int=0, secondOfMinute:int=0):String {
        var pad02:Function = FormatUtils.pad02;
        return MONTH_NAMES[month - 1] + " " +
            pad02(dayOfMonth.toString()) + ", " +
            year.toString() + " @ " +
            pad02(hourOfDay.toString()) +
            ":" + pad02(minuteOfHour.toString()) + ":" +
            pad02(secondOfMinute.toString());
    }

    /* TODO: Investigate if this is robust for subtracting elapsed hours...
    */
    public static function datePlusElapsedHoursToDate(
      elapsedTime_hrs:Number,
      yr:int,
      month:int,
      day:int,
      hr:int,
      min:int=0,
      sec:int=0):Object
    {
      var refHrs:Number = hourOfYearFromDate(yr, month, day, hr, min, sec);
      var totalTime_hrs:Number = refHrs + elapsedTime_hrs;
      var addedYears:int = int(Math.floor(
        totalTime_hrs / HOURS_PER_YEAR));
      var remainingHours:Number = totalTime_hrs % HOURS_PER_YEAR;
      var newDate:Object = hourOfYearToDate(remainingHours, yr);
      return MapUtils.update(newDate,
        [YEAR, newDate[YEAR] + addedYears]);
    }

    public static function datePlusElapsedHoursToDateEfficient(
      elapsedTime_hrs:Number,
      yr:int,
      month:int,
      day:int,
      hr:int,
      min:int=0,
      sec:int=0):DateTime
    {
      var refHrs:Number = hourOfYearFromDate(yr, month, day, hr, min, sec);
      var totalTime_hrs:Number = refHrs + elapsedTime_hrs;
      var addedYears:int = int(Math.floor(
        totalTime_hrs / HOURS_PER_YEAR));
      var remainingHours:Number = totalTime_hrs % HOURS_PER_YEAR;
      var newDate:DateTime = hourOfYearToDateTime(remainingHours, yr);
      if (addedYears == 0) {
        return newDate;
      }
      return new DateTime(
        newDate.year + addedYears,
        newDate.month,
        newDate.dayOfMonth,
        newDate.hourOfDay,
        newDate.minuteOfHour,
        newDate.secondOfMinute);
    }

    /* headerAndValueObjectsToDateTime
    hdr -- a header object such as a DataSet header
        the fields REFERENCE_YEAR must be present and optionally,
                   REFERENCE_MONTH
                   REFERENCE_DAY
                   REFERENCE_HOUR
                   REFERENCE_MINUTE
                   REFERENCE_SECOND
    vals -- a value object such as the values in a data point of a data set
        the field ELAPSED_TIME_hrs must be present
    RETURNS
    A DateTime instance */
    public static function headerAndValueObjectsToDateTime(
      hdr:Object,
      vals:Object):DateTime
    {
      var refYear:int = hdr[REFERENCE_YEAR] as int;
      var refMonth:int = MapUtils.atKeyOrAlt(
        hdr, REFERENCE_MONTH, 1);
      var refDay:int = MapUtils.atKeyOrAlt(
        hdr, REFERENCE_DAY, 1);
      var refHour:int = MapUtils.atKeyOrAlt(
        hdr, REFERENCE_HOUR, 0);
      var refMinute:int = MapUtils.atKeyOrAlt(
        hdr, REFERENCE_MINUTE, 0);
      var refSecond:int = MapUtils.atKeyOrAlt(
        hdr, REFERENCE_SECOND, 0);
      var elapsedTime_hrs:Number = vals[ELAPSED_TIME_hrs] as Number;
      return datePlusElapsedHoursToDateEfficient(
        elapsedTime_hrs,
        refYear,
        refMonth,
        refDay,
        refHour,
        refMinute,
        refSecond);
    }

    public static function
    deriveElapsedTime_sec(elapsedTime_hrs:Number,
                refYr:int, refMonth:int, refDy:int,
                refHr:int, refMin:int, refSc:Number):Number {
        return elapsedTime_hrs * SECONDS_PER_HOUR;
    }

    public static function
    deriveDayOfYear(elapsedTime_hrs:Number,
                refYr:int, refMonth:int, refDay:int,
                refHr:int, refMin:int, refSec:int):Number {
        var dateTime:Object = datePlusElapsedHoursToDate(elapsedTime_hrs,
            refYr, refMonth, refDay, refHr, refMin, refSec);
        return dayOfYearFromDate(
            dateTime[YEAR], dateTime[MONTH], dateTime[DAY]);
    }

    public static function
    deriveCurrentYear(elapsedTime_hrs:Number,
                      refYr:int, refMonth:int, refDay:int,
                      refHr:int, refMin:int, refSec:int):Number {
        var dateTime:Object = datePlusElapsedHoursToDate(elapsedTime_hrs,
            refYr, refMonth, refDay, refHr, refMin, refSec);
        return dateTime[YEAR];
    }

    public static function
    deriveCurrentMonth(elapsedTime_hrs:Number,
                       refYr:int, refMonth:int, refDay:int,
                       refHr:int, refMin:int, refSec:int):Number {
        var dateTime:Object = datePlusElapsedHoursToDate(elapsedTime_hrs,
            refYr, refMonth, refDay, refHr, refMin, refSec);
        return dateTime[MONTH];
    }

    public static function
    deriveDayOfMonth(elapsedTime_hrs:Number,
                     refYr:int, refMonth:int, refDay:int,
                     refHr:int, refMin:int, refSec:int):Number {
        var dateTime:Object = datePlusElapsedHoursToDate(elapsedTime_hrs,
            refYr, refMonth, refDay, refHr, refMin, refSec);
        return dateTime[DAY];
    }

    public static function
    deriveHourOfDay(elapsedTime_hrs:Number,
                    refYr:int, refMonth:int, refDay:int,
                    refHr:int, refMin:int, refSec:int):Number {
        var dateTime:Object = datePlusElapsedHoursToDate(elapsedTime_hrs,
            refYr, refMonth, refDay, refHr, refMin, refSec);
        return dateTime[HOUR];
    }

    public static function
    deriveMinuteOfHour(elapsedTime_hrs:Number,
                       refYr:int, refMonth:int, refDay:int,
                       refHr:int, refMin:int, refSec:int):Number {
        var dateTime:Object = datePlusElapsedHoursToDate(elapsedTime_hrs,
            refYr, refMonth, refDay, refHr, refMin, refSec);
        return dateTime[MINUTE];
    }

    public static function
    deriveSecondOfMinute(elapsedTime_hrs:Number,
                         refYr:int, refMonth:int, refDay:int,
                         refHr:int, refMin:int, refSec:int):Number {
        var dateTime:Object = datePlusElapsedHoursToDate(elapsedTime_hrs,
            refYr, refMonth, refDay, refHr, refMin, refSec);
        return dateTime[SECOND];
    }

    public static function
    deriveDateTime(elapsedTime_hrs:Number,
                   refYr:int, refMonth:int, refDay:int,
                   refHr:int, refMin:int, refSec:int):String {
        var dateTime:Object = datePlusElapsedHoursToDate(elapsedTime_hrs,
            refYr, refMonth, refDay, refHr, refMin, refSec);
        var str:String = DateTime.objectToDateTime(dateTime).compactString();
        return str;
    }

    public static function
    deriveMonthYear(elapsedTime_hrs:Number,
                    refYr:int, refMonth:int, refDay:int,
                    refHr:int, refMin:int, refSec:int):String {
        var dateTime:Object = datePlusElapsedHoursToDate(elapsedTime_hrs,
            refYr, refMonth, refDay, refHr, refMin, refSec);
        var dt:DateTime = DateTime.objectToDateTime(dateTime);
        return MONTH_NAMES[dt.month - 1] + " " + dt.year.toString();
    }
}
}