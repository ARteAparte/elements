/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package scaling {
public class ScalingFactors {
    public static const x1__1e6:Number = 1e-6;
    public static const x1__1e4:Number = 1e-4;
    public static const x1__1e3:Number = 1e-3;
    public static const x1:Number = 1.0;
    public static const x1e1:Number = 1e1;
    public static const PERCENTAGE:Number = 1e2;
    public static const x1e3:Number = 1e3;
    public static const x1e4:Number = 1e4;
    public static const x1e6:Number = 1e6;

    public static function toString(scale:Number):String {
        if (scale == x1__1e6) {
            return "x 1/1e6";
        } else if (scale == x1__1e4) {
            return "x 1/10,000";
        } else if (scale == x1__1e3) {
            return "x 1/1000";
        } else if (scale == x1) {
            return "";
        } else if (scale == x1e1) {
            return "tenths";
        } else if (scale == PERCENTAGE) {
            return "%";
        } else if (scale == x1e3) {
            return "x 1000";
        } else if (scale == x1e4) {
            return "x 10,000";
        } else if (scale == x1e6) {
            return "x 1e6";
        } else {
            return "x " + scale.toString();
        }
    }
}
}