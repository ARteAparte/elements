/*
Copyright (C) 2014-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package utils {
import flash.net.URLRequest;

public class NetUtils {

  public static function openExternalURL(url:String):void {
    var request:URLRequest = new URLRequest(url);
    try {
      flash.net.navigateToURL(request);
    }
      catch(error:Error) {
      // handle error here
      trace("openExternalURL error: " + error);
    }
  }

}
}
