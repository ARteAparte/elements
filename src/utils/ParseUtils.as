/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package utils {
public class ParseUtils {
    /* Split a line by a delimeter, use a regular expression to filter
    elements to keep, and map a function over the remaining token elements
    and return. */
    public static function
    parseToks(line:String, delim:String, re:RegExp, f:Function):Array {
        return line.split(delim).filter(
            function(item:String, idx:int, a:Array):Boolean {
                var matches:Array = item.match(re);
                if (matches == null) {
                    return false;
                }
                return true;
            }).map(function(item:String, idx:int, a:Array):Number {
                return f(item);
            });
    }

    public static function trim(s:String):String {
        //return s.replace(/^([\s|\t|\n]+)?(.*)([\s|\t|\n]+)?$/gm, "$2");
        return s.replace(/^[\s\t]+/gm, "").replace(/[\s\t]+$/gm, "");

    }

    public static function tryToNum(val:String):* {
        var trans:Number = Number(val);
        if (isNaN(trans)) return val;
        return trans;
    }

    public static function toStrOrNum(val:String):* {
        var val_:String = trim(val);
        var len:int = val_.length;
        if ((val_.substr(0, 1) == '"') && (val_.substr(len-1,1) == '"')) {
            return val_.substr(1, len-2);
        }
        return Number(val_);
    }

    // reference: http://www.greywyvern.com/?post=258
    public static function split(str:String, sep:String=","):Array {
        var foo:Array = str.split(sep);
        var newFoo:String;
        var tail:String;
        for (var x:int=foo.length - 1; x >= 0; x--) {
            newFoo = (foo[x] as String).replace(/"\s+$/m, '"');
            if (newFoo.charAt(newFoo.length - 1) == '"') {
                tail = foo[x].replace(/^\s+"/m, '"');
                if ((tail.length > 1) && (tail.charAt(0) == '"')) {
                    foo[x] = (foo[x] as String).replace(
                        /^\s*"|"\s*$/mg, '').replace(/""/g, '"');
                } else if (x > 0) {
                    foo.splice(x - 1, 2, [foo[x - 1], foo[x]].join(sep));
                } else {
                    foo = (foo.shift() as String).split(sep).concat(foo);
                }
            } else {
                (foo[x] as String).replace(/""/g, '"');
            }
        }
        return foo;
    }

    // Number Number -> Number
    // given a number, n, and a minimum bound, return the number if >= minBound,
    // else return minBound.
    public static function minBound(
      n:Number,
      min:Number
    ):Number
    {
      if (n < min)
      {
        return min;
      }
      else
      {
        return n;
      }
    }
    
    // Number Number -> Number
    // given a number, n, and a maxBound, return the number if <= maxBound,
    // else return maxBound
    public static function maxBound(
      n:Number,
      max:Number
    ):Number
    {
      if (n > max)
      {
        return max;
      }
      else
      {
        return n;
      }
    }
    
    // Number Number Number -> Number
    // Given a number, a maximum value, and a minimum value,
    // return the parsed number, constrained to be not lower than minimum
    // and not higher than maximum.
    public static function bound(
      n:Number, min:Number, max:Number
    ):Number
    {
      if (min > max)
      {
        var temp:Number = min;
        min = max;
        max = temp;
      }
      return maxBound(minBound(n, min), max);
    }
}
}