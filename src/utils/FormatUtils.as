/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package utils {
import mx.utils.StringUtil;

public class FormatUtils {
  public static const PAD_RIGHT:String = 'pad to the right';
  public static const PAD_LEFT:String = 'pad to the left';

  public static function fixStringLength(length:int, str:String,
                                         padChar:String,
                                         padDirection:String):String {
    if (str.length == length) {
      return str;
    } else if (str.length > length) {
      return str.substr(0, length);
    }
    if (padDirection == PAD_RIGHT) {
      return padString(length, str, padChar);
    }
    return padString(length, str.split("").reverse().join(""), padChar)
      .split("").reverse().join("");
  }

  public static function padString(length:int, str:String,
                                   padChar:String):String {
    return str + StringUtil.repeat(padChar, length - str.length);
  }

  public static function numberToFormattedString(n:Number,
                                                 precision:int):String {
    var multiplier:Number = Math.pow(10, precision);
    return (Math.round(n * multiplier) / multiplier).toString();
  }

  public static function formatFloat(n:Number, numDecimalPlaces:int):String {
    if (numDecimalPlaces == 0) {
      return Math.round(n).toString() + ".";
    } else {
      var mult:Number = Math.pow(10.0, numDecimalPlaces);
      var num:Number = Math.round(n * mult) / mult;
      var numParts:Array = num.toString().split('.');
      if (numParts.length == 1) {
        numParts.push(StringUtil.repeat('0', numDecimalPlaces));
      }
      if (numParts[1].length < numDecimalPlaces) {
        numParts[1] = numParts[1] + StringUtil.repeat('0',
          numDecimalPlaces - numParts[1].length);
      }
      return numParts.join(".");
    }
  }

  // format a floating point number but drop extra zeros and decimal point
  // if possible
  public static function floatDrop0(n:Number, numDecimalPlaces:int):String {
    if (numDecimalPlaces == 0) {
      return Math.round(n).toString();
    } else {
      var mult:Number = Math.pow(10.0, numDecimalPlaces);
      var num:Number = Math.round(n * mult) / mult;
      var numParts:Array = num.toString().split('.');
      if (numParts.length == 1) {
        return numParts[0];
      }
      var first:String = numParts[0];
      var rest:String = (numParts[1] as String).replace(/0*$/,'');
      if (rest == '') {
        return first;
      } else {
        return first + '.' + rest;
      }
    }
  }

  public static function formatInt(n:Number):String {
    var s:String = formatFloat(n, 0);
    return s.slice(0, s.length-1); // the slice cuts off the last "."
  }

  public static function
    pad02(s:String):String {
    return fixStringLength(2, s, '0', FormatUtils.PAD_LEFT);
  }
}
}