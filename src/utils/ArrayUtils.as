/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package utils {
public class ArrayUtils {
    public static function copyArray(xs:Array):Array {
        var len:int = xs.length;
        var newArray:Array = new Array(len);
        copyIntoArray(xs, 0, newArray, 0, len);
        return newArray;
    }

    public static function
    copyIntoArray(src:Array, srcPos:int,
                  dest:Array, destPos:int, length:int):void {
        var maxSrcCpy:int = src.length - srcPos;
        var maxDestCpy:int = dest.length - destPos;
        var maxCpy:int = Math.min(maxSrcCpy, maxDestCpy, length);
        for (var i:int = 0; i < maxCpy; i++) {
            dest[destPos + i] = src[srcPos + i];
        }
    }

    public static function copyArrayLike(iterable:*):Array {
        var out:Array = [];
        for each (var item:* in iterable) {
            out.push(item);
        }
        return out;
    }

    public static function contains(xs:Array, y:*):Boolean {
        for each (var x:* in xs) {
            if (x == y) {
                return true;
            }
        }
        return false;
    }

    public static function
    sort(xs:Array):Array {
        var ys:Array = xs.sort(function (x:int, y:int):Number {
            var val:Number;
            if (x < y) {
                val = -1;
            } else if (x == y) {
                val = 0;
            } else {
                val = 1;
            }
            return val;
        });
        return ys;
    }

    public static function equals(xs:Array, ys:Array):Boolean {
        if (xs.length != ys.length) return false;
        for (var i:int=0; i<xs.length; i++) {
            if (xs[i] == ys[i]) {
                continue;
            } else {
                return false;
            }
        }
        return true;
    }
    
    // Returns the first index where x matches an element of xs
    // If there is no matching index, return -1.
    public static function firstIndexOf(x:*, xs:Array):int {
      for (var n:int = 0; n<xs.length; n++){
        if (x == xs[n]) {
          return n;
        }
      }
      return -1;
    }
}
}