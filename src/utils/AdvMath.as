/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package utils {
public class AdvMath {
    public static const DX:Number = 1e-6;
    public static const GOLDEN_RATIO_PLUS_ONE:Number = 1.618033989;

    public static function integerPart(x:Number):int {
        var xFloor:Number;
        if (x >= 0.0) {
            xFloor = Math.floor(x);
        } else {
            xFloor = Math.ceil(x);
        }
        return int(xFloor);
    }

    public static function wrapToPlusMinusN(n:Number, N:Number):Number
    {
      if (n > N)
      {
        return scaleToN(n, N);
      }
      if (n < -N)
      {
        return -1 * scaleToN(-n, N);
      }
      return n;
    }
    
    public static function scaleToN(n:Number, N:Number):Number {
        var factor:Number;
        if (n > N) {
            factor = Math.floor(n / N);
            return n - factor * N;
        } else {
            factor = 0.0;
        }
        return n - factor * N;
    }

    public static function derivative(f:Function, x:Number, dx:Number):Number {
        var fx:Number = f(x);
        var fxPlusDx:Number = f(x + dx);
        return (fxPlusDx - fx) / dx;
    }

    /*
    note: the function g(x) must have signature "g :: x:Number -> Number" where g(x) = 0 when x is a fixed point.
    This is the Newton-Raphson method. g(x) must be differentiable at x.
    Note: TODO, extend this function to go into a brent or bisection method if blows up due
    to being non-linear or having a bad derivative. Also, offer the brent/bisection explicitly for
    cases where you know up-front that you don't want to do Newton-Raphson due to derivative being
    expensive or hard to compute or ill conditioned.
    */
    public static function findFixedPoint(g:Function, x0:Number, dx:Number, tol:Number):Number {
        var f:Function = function(x:Number):Number {
            var dgdx:Number = AdvMath.derivative(g, x, dx);
            var gx:Number = g(x);
            return x - gx / dgdx;
        };
        var xlast:Number = x0;
        var x:Number = f(xlast);
        while (Math.abs(x - xlast) > tol) {
            xlast = x;
            x = f(xlast);
        }
        return x;
    }

    public static function signum(x:Number):Number {
      if (x > 0.0) {
        return 1.0;
      } else if (x < 0.0) {
        return -1.0;
      } else {
        return 0.0;
      }
    }
    /* find_good_bound
    f :: Number -> Number -- the bounds function to test
    x :: Number -- initial value for testing f
    dx :: Number -- the step rate
    next_x_fn :: Number * Number -> Number -- determine next x from x and dx
    max_iter :: int -- maximum number of iterations
    dx_tol :: Number -- the smallest value of dx
    RETURN :: Vector.<Number> = [x, f(x)] where x is the good-bound closest
                                           to the starting x
    NOTE: meant to replace findGoodBound */
    public static function find_good_bound(f:Function, x:Number, dx:Number,
                                           next_x_fn:Function, max_iter:int,
                                           dx_tol:Number):Vector.<Number> {
      var dx_sign:Number = signum(dx);
      var orig_x:Number = x;
      var fx:Number = f(x);
      var iter:int = 0;
      var best_solution:Vector.<Number> = Vector.<Number>([x, fx]);
      var next_x:Number;
      var new_dx:Number;
      var orig_next_x:Number;
      var next_xs:Array;
      while (iter != max_iter) {
        if (isNaN(fx)) {
          next_x = next_x_fn(x, dx);
          x = next_x;
          fx = f(next_x);
          iter = iter + 1;
        } else if ((x == orig_x) || (dx < dx_tol)) {
          break;
        } else {
          new_dx = 0.5 * dx;
          orig_next_x = next_x_fn(x, -new_dx);
          next_xs = [orig_next_x, orig_x];
          if (dx_sign == 1.0) {
            if (next_xs[0] < next_xs[1]) {
              next_x = next_xs[0];
            } else {
              next_x = next_xs[1];
            }
          } else {
            if (next_xs[0] > next_xs[1]) {
              next_x = next_xs[0];
            } else {
              next_x = next_xs[1];
            }
          }
          best_solution = Vector.<Number>([x, fx]);
          x = next_x;
          dx = new_dx;
          fx = f(next_x);
          iter = iter + 1;
        }
      }
      return best_solution;
    }

    public static function findGoodBound(f:Function, x:Number, dx:Number, nextXFun:Function,
                                         maxCnt:int, dxTol:Number):Vector.<Number> {
        var xVec:Vector.<Number> = new Vector.<Number>();
        var nextX:Number;
        var fNextX:Number;
        var fx:Number = f(x);
        for (var cnt:int = 0; cnt<maxCnt; cnt++)
        {
            if (isNaN(fx))
            {
                nextX = nextXFun(x, dx);
                fNextX = f(nextX);
                dx = 0.5 * dx;
                if (isNaN(fNextX))
                {
                    x = nextX;
                    fx = fNextX;
                    continue;
                }
                else
                {
                    if (Math.abs(dx) < dxTol)
                    {
                        x = nextX;
                        fx = fNextX;
                        xVec.push(x);
                        break;
                    }
                    continue;
                }
            }
            else
            {
                xVec.push(x);
                break;
            }
        }
        if (xVec.length == 1) xVec.push(fx);
        return xVec;
    }

    /*
    returns a vector of length four that contains a new a, fa, b, and fb such that f(a) and f(b) are not NaN.
    If two such numbers cannot be found, returns an empty vector. Tol is the difference in a or b on iteration
    beyond which we no longer care and is used as the criterion to stop refining the boundary.
    */
    public static function findValidBounds(f:Function, a:Number, b:Number, tol:Number, maxCnt:int):Vector.<Number> {
        var abVec:Vector.<Number> = new Vector.<Number>();
        var fa:Number = f(a);
        var fb:Number = f(b);
        if (!isNaN(fa) && !isNaN(fb) && (fa * fb <= 0.0)) {
            abVec.push(a);
            abVec.push(fa);
            abVec.push(b);
            abVec.push(fb);
            return abVec;
        }
        var step:Number = (b - a) * 0.5;
        var aBound:Vector.<Number> = findGoodBound(f, a, step, function (a:Number, da:Number):Number {
            return a + da;
        }, maxCnt, tol);
        var bBound:Vector.<Number> = findGoodBound(f, b, step, function (b:Number, db:Number):Number {
            return b - db;
        }, maxCnt, tol);
        if ((aBound.length == 2) && (bBound.length == 2)) {
            if (aBound[0] > bBound[0]) {
                abVec.push(bBound[0]);
                abVec.push(bBound[1]);
                abVec.push(aBound[0]);
                abVec.push(aBound[1]);
            } else {
                abVec.push(aBound[0]);
                abVec.push(aBound[1]);
                abVec.push(bBound[0]);
                abVec.push(bBound[1]);
            }
        }
        return abVec;
    }

    public static function inc(x:Number, dx:Number):Number {
      return x + dx;
    }

    public static function dec(x:Number, dx:Number):Number {
      return x - dx;
    }

    /* find_valid_bounds
    - f :: Number -> Number -- the function to test
    - a :: Number -- the lower bound
    - b :: Number -- the upper bound
    - tol :: Number -- tolerance to get close to the bounds
    - max_iter :: int -- maximum number of iterations in exploring for valid
                         bounds
    Explores on a and b to pull bounds in (a increases and b decreases) until
    a and b are valid (i.e., f(a) & f(b) return non-NaN) and then feathers
    around that point to find the largest non-NaN region possible (i.e.,
    pushing the a and b bounds out again).
    RETURNS :: Vector.<Number> which is [a, f(a), b, f(b)]
    NOTE: should eventually replace findValidBounds */
    public static function find_valid_bounds(f:Function, a:Number, b:Number,
                                             tol:Number,
                                             max_iter:int):Vector.<Number> {
      var out:Vector.<Number>;
      var step:Number;
      var fa:Number = f(a);
      var fb:Number = f(b);
      if (!isNaN(fa) && !isNaN(fb) && ((fa * fb) <= 0.0)) {
        out = Vector.<Number>([a, fa, b, fb]);
      } else {
        step = 0.1 * (b - a);
        var a_bound:Vector.<Number> = find_good_bound(f, a, step, inc,
          max_iter, tol);
        var b_bound:Vector.<Number> = find_good_bound(f, b, step, dec,
          max_iter, tol);
        if (a_bound[0] > b_bound[0]) {
          out = Vector.<Number>([b_bound[0], b_bound[1],
                                 a_bound[0], a_bound[1]]);
        } else {
          out = Vector.<Number>([a_bound[0], a_bound[1],
                                 b_bound[0], b_bound[1]]);
        }
      }
      return out;
    }
    /*
    root = safeNewtonRaphson(f, df, a, b, boundsTol, tol, maxCnt)
    The function f must have type "f :: x:Number -> Number" where f(x) = 0
    when x is a fixed point.
    The function df is the derivative of f at x "df :: x:Number -> Number".
    f(x) must be differentiable at x. This function performs over the range
    a to b where a < b. f can be NaN over the range a to b but must have real
    return values somewhere in the region between a and b (inclusive).

    boundsTol is the tolerance for any bounds exploration that needs doing
    tol is the tolerance for conversion of the algorithm to the zero value of f.
    maxCnt -- max iterations on bound finder.
    */
    public static function
    safeNewtonRaphson(f:Function, df:Function, a:Number, b:Number,
                      boundsTol:Number, tol:Number, maxCnt:int):Number {
        if (a > b) throw new Error("a must be less than b");
        if (a == b) return a; // range degenerate; only one possible answer
        var bounds:Vector.<Number> =
          find_valid_bounds(f, a, b, boundsTol, maxCnt);
        if (bounds.length == 0) {
          throw new Error("No valid bounds found -- all NaN");
        }
        a = bounds[0];
        var fa:Number = bounds[1];
        if (Math.abs(fa) < tol) return a;
        b = bounds[2];
        var fb:Number = bounds[3];
        if (Math.abs(fb) < tol) return b;
        if (fa * fb > 0.0) throw new Error("Zero is not bracketed");
        var x:Number = 0.5 * (a + b);
        var fx:Number;
        var dfx:Number;
        var dx:Number;
        for (var i:int=0; i<30; i++) {
            fx = f(x);
            if (Math.abs(fx) < tol) {
                return x;
            }
            if (fa * fx < 0.0) {
                b = x;
            } else {
                a = x;
            }
            dfx = df(x);
            if (dfx == 0.0) {
                dx = b - a;
            } else {
                dx = -fx/dfx;
            }
            x = x + dx;
            if ((b - x) * (x - a) < 0.0) {
                dx = 0.5 * (b - a);
                x = a + dx;
            }
            if (Math.abs(dx) < (tol * Math.max(Math.abs(b), 1.0))) {
                return x;
            }
        }
        trace('max iterations reached...');
        return x;
    }

    public static function makeDerivativeFunction(f:Function, dx:Number):Function {
        return function (x:Number):Number {
            return AdvMath.derivative(f, x, dx);
        };
    }

    public static function makeDerivativeFunctionStencil(f:Function, dx:Number):Function {
      return function (x:Number):Number {
        var a:Number = -f(x + 2*dx);
        var b:Number = 8 * f(x + dx);
        var c:Number = -8 * f(x - dx);
        var d:Number = f(x - 2*dx);
        var numer:Number = a + b + c + d;
        var denom:Number = 12 * dx;
        return numer / denom;
      }
    }

    public static function zipWith(xs:Array, ys:Array, f:Function):Array {
        var z:Array = new Array();
        for (var idx:int=0; idx < Math.min(xs.length, ys.length); idx++) {
            z.push(f(xs[idx], ys[idx]));
        }
        return z;
    }

    public static function vecMul(xs:Array, ys:Array):Array {
        return zipWith(xs, ys, function(x:Number, y:Number):Number { return x * y; });
    }

    public static function vecAdd(xs:Array, ys:Array):Array {
        return zipWith(xs, ys, function(x:Number, y:Number):Number { return x + y; });
    }

    public static function vecSub(xs:Array, ys:Array):Array {
        return zipWith(xs, ys, function(x:Number, y:Number):Number { return x - y; });
    }

    public static function scalarMul(x:Number, xs:Array):Array {
        var ys:Array = new Array();
        for (var idx:int=0; idx < xs.length; idx++) {
            ys.push(x * xs[idx]);
        }
        return ys;
    }

    public static function copyVec(xs:Array):Array {
        var ys:Array = new Array();
        for each (var x:Number in xs) {
            ys.push(x);
        }
        return ys;
    }

    public static function reduce(xs:Array, init:*, f:Function):* {
        var acc:* = init;
        for each (var item:* in xs) {
            acc = f(acc, item);
        }
        return acc;
    }

    public static function sum(xs:Array):Number {
        return reduce(xs, 0.0, function(acc:Number, x:Number):Number { return acc + x; });
    }

    public static function vecDot(xs:Array, ys:Array):Number {
        return sum(vecMul(xs, ys));
    }

    public static function vecMax(xs:Array):Number {
        return reduce(xs, xs[0], function(acc:Number, x:Number):Number {
            if (x > acc) {
                return x;
            }
            return acc;
        });
    }

    public static function idxOfMin(xs:Array):int {
        if (xs.length == 0) return -1;
        var min:Number = xs[0];
        var minIdx:int = 0;
        for (var idx:int=0; idx < xs.length; idx++) {
            if (xs[idx] < min) {
                min = xs[idx];
                minIdx = idx;
            }
        }
        return minIdx;
    }

    public static function idxOfMax(xs:Array):int {
        if (xs.length == 0) return -1;
        var max:Number = xs[0];
        var maxIdx:int = 0;
        for (var idx:int=0; idx < xs.length; idx++) {
            if (xs[idx] > max) {
                max = xs[idx];
                maxIdx = idx;
            }
        }
        return maxIdx;
    }

    public static function bracket(func:Function, xa:Number=0.0, xb:Number=1.0,
                                   grow_limit:int=110.0, maxiter:int=1000):Object {
        const GOLD:Number = 1.618034;
        const VERY_SMALL_NUMBER:Number = 1e-21;
        var fa:Number = func(xa);
        var fb:Number = func(xb);
        if (fa < fb) {
            var dum:Number = xa; xa = xb; xb = dum;
            dum = fa; fa = fb; fb = dum;
        }
        var xc:Number = xb + GOLD * (xb - xa);
        var fc:Number = func(xc);
        var funcalls:int = 3;
        var iter:int = 0;
        while (fc < fb) {
            var tmp1:Number = (xb - xa) * (fb - fc);
            var tmp2:Number = (xb - xc) * (fb - fa);
            var val:Number = tmp2 - tmp1;
            if (Math.abs(val) < VERY_SMALL_NUMBER) {
                var denom:Number = 2.0 * VERY_SMALL_NUMBER;
            } else {
                denom = 2.0 * val;
            }
            var w:Number = xb - ((xb - xc) * tmp2 - (xb - xa) * tmp1) / denom;
            var wlim:Number = xb + grow_limit * (xc - xb);
            if (iter > maxiter) {
                throw new Error('bracket :: too many iterations.');
            }
            iter += 1;
            if ((w - xc) * (xb - w) > 0.0) {
                var fw:Number = func(w);
                funcalls += 1;
                if (fw < fc) {
                    xa = xb; xb = w; fa = fb; fb = fw;
                    return {xa:xa, xb:xb, xc:xc, fa:fa, fb:fb, fc:fc, funcalls:funcalls};
                } else if (fw > fb) {
                    xc = w; fc = fw;
                    return {xa:xa, xb:xb, xc:xc, fa:fa, fb:fb, fc:fc, funcalls:funcalls};
                }
                w = xc + GOLD * (xc - xb);
                fw = func(w);
                funcalls += 1;
            } else if ((w - wlim) * (wlim - xc) >= 0.0) {
                w = wlim;
                fw = func(w);
                funcalls += 1;
            } else if ((w - wlim) * (xc - w) > 0.0) {
                fw = func(w);
                funcalls += 1;
                if (fw < fc) {
                    xb = xc; xc = w; w = xc + GOLD * (xc - xb);
                    fb = fc; fc = fw; fw = func(w);
                    funcalls += 1;
                }
            } else {
                w = xc + GOLD * (xc - xb);
                fw = func(w);
                funcalls += 1;
            }
            xa = xb; xb = xc; xc = w;
            fa = fb; fb = fc; fc = fw;
        }
        return {xa:xa, xb:xb, xc:xc, fa:fa, fb:fb, fc:fc, funcalls:funcalls};
    }

    /*
    ## module goldSearch
    range = bracket(f,xStart,h)
    range.lower:Number
    range.upper:Number

    Finds the brackets (lower,upper) of a minimum point of the user-supplied scalar function f(x). The search
    starts downhill from xStart with a step length dxStep.
    */
    public static function goldenBracket(f:Function, x1:Number, dxStep:Number):Object {
        const MAX_ITER:int = 1000;
        var f1:Number = f(x1);
        var x2:Number = x1 + dxStep;
        var f2:Number = f(x2);
        if (f2 > f1) {
            dxStep = -1.0 * dxStep;
            x2 = x1 + dxStep;
            f2 = f(x2);
            if (f2 > f1) {
                return {
                    success:true,
                    lower:Math.min(x2, x1 - dxStep),
                    upper:Math.max(x2, x1 - dxStep)};
            }
        }
        for (var cnt:int=0; cnt < MAX_ITER; cnt++) {
            dxStep = GOLDEN_RATIO_PLUS_ONE * dxStep;
            var x3:Number = x2 + dxStep;
            var f3:Number = f(x3);
            if (f3 > f2) {
                return {
                    success:true,
                    lower:Math.min(x1, x3),
                    upper:Math.max(x1, x3)};
            }
            x1 = x2;
            x2 = x3;
            f1 = f2;
            f2 = f3;
        }
        // trace('goldenBracket :: unable to bracket minimum');
        return {
            success:false,
            lower:NaN,
            upper:NaN};
    }

    /* brent
    Find the minimum of a function of one variable over a bracketed range.
    parameters:
    * func : Function -- function(x:Number):Number, the function to minimize (i.e., objective function)
    * range : Array -- array of null, length 2, or length 3.
                       For null, the bracket algorithm is run with default bracket arguments to try and find
                       a bound.
                    -- For length 3, brack is [a, b, c]
                       with a,b,c:Number and (a < b < c) and func(b) < func(a),func(c)
                    -- For length 2, brack is [a, c] where a,c:Number and a,c are taken as the starting interval
                       for a downhill bracket search (such as with bracket)
    * tol:Number -- convergence tolerance

    returns:Object
    { xmin:Number -- argument to func that yields the minimum
    , fval:Number -- the value of func(xmin)
    , iter:int -- the number of iterations required
    , funcalls:int -- the number of objective function evaluations
    }
    */
    public static function brent(func:Function, range:Array=null, tol:Number=1.48e-8, maxiter:int=500):Object {
        const GOLDEN:Number = 0.3819660;
        const MIN_TOL:Number = 1.0e-11;
        if (range == null) {
            var brack:Object = bracket(func);
        } else if (range.length == 2) {
            brack = bracket(func, range[0], range[1]);
        } else if (range.length == 3) {
            brack = {
                xa:range[0], xb:range[1], xc:range[2],
                fa:func(range[0]), fb:func(range[1]), fc:func(range[2])};
            if (!((brack.fb < brack.fa) && (brack.fb < brack.fc))) {
                throw new Error("brent :: not a bracketing interval");
            }
            brack['funcalls'] = 3;
        } else {
            throw new Error("brent :: range must be null or array of 2 or 3 values bracketing the minimum");
        }
        var xa:Number = brack.xa;
        var xb:Number = brack.xb;
        var xc:Number = brack.xc;
        var fa:Number = brack.fa;
        var fb:Number = brack.fb;
        var fc:Number = brack.fc;
        var funcalls:int = brack.funcalls;
        var x:Number = xb;
        var w:Number = xb;
        var v:Number = xb;
        var fw:Number = fb;
        var fv:Number = fb;
        var fx:Number = fb;
        if (xa < xc) {
            var a:Number = xa; var b:Number = xc;
        } else {
            a = xc; b = xa;
        }
        var deltax:Number = 0.0;
        var iter:int = 0;
        while (iter < maxiter) {
            var tol1:Number = tol * Math.abs(x) + MIN_TOL;
            var tol2:Number = 2.0 * tol1;
            var xmid:Number = 0.5 * (a + b);
            if (Math.abs(x - xmid) < (tol2 - 0.5 * (b - a))) {
                xmid = x; var fval:Number = fx;
                break;
            }
            if (Math.abs(deltax) <= tol1) {
                if (x >= xmid) {
                    deltax = a - x;
                } else {
                    deltax = b - x;
                }
                var rat:Number = GOLDEN * deltax;
            } else {
                var tmp1:Number = (x - w) * (fx - fv);
                var tmp2:Number = (x - v) * (fx - fw);
                var p:Number = (x - v) * tmp2 - (x - w) * tmp1;
                tmp2 = 2.0 * (tmp2 - tmp1);
                if (tmp2 > 0.0) {
                    p = -p;
                }
                tmp2 = Math.abs(tmp2);
                var dx_temp:Number = deltax;
                deltax = rat;
                if ((p > tmp2 * (a - x)) && (p < tmp2 * (b - x)) &&
                    (Math.abs(p) < Math.abs(0.5 * tmp2 * dx_temp))) {
                    rat = p / tmp2;
                    var u:Number = x + rat;
                    if (((u - a) < tol2) || ((b - u) < tol2)) {
                        if ((xmid - x) >= 0.0) {
                            rat = tol1;
                        } else {
                            rat = -tol1;
                        }
                    }
                } else {
                    if (x >= xmid) {
                        deltax = a - x;
                    } else {
                        deltax = b - x;
                    }
                    rat = GOLDEN * deltax;
                }
            }
            if (Math.abs(rat) < tol1) {
                if (rat >= 0.0) {
                    u = x + tol1;
                } else {
                    u = x - tol1;
                }
            } else {
                u = x + rat;
            }
            var fu:Number = func(u);
            funcalls += 1;
            if (fu > fx) {
                if (u < x) {
                    a = u;
                } else {
                    b = u;
                }
                if ((fu <= fw) || (w == x)) {
                    v = w; w = u; fv = fw; fw = fu;
                } else if ((fu <= fv) || (v == x) || (v == w)) {
                    v = u; fv = fu;
                }
            } else {
                if (u >= x) {
                    a = x;
                } else {
                    b = x;
                }
                v = w; w = x; x = u;
                fv = fw; fw = fx; fx = fu;
            }
            iter += 1;
        }
        return {
            xmin:x,
            fval:fx,
            iter:iter,
            funcalls:funcalls
        };
    }

    /*
    results = search(f, lower, upper, tol=1.0e-6):Object
    results.x:Number
    results.fx:Number

    Golden section method for determining x the user-supplied scalar function f(x). The minimum must be bracketed
    in (lower, upper).
    */
    public static function goldenSearch(f:Function, a:Number, b:Number, tol:Number=1e-6):Object {
        var nIter:Number = -2.078087 * Math.log(tol / Math.abs(b - a));
        // trace('nIter', nIter);
        const R:Number = 0.618033989;
        const C:Number = 1.0 - R;
        var x1:Number = R * a + C * b;
        var x2:Number = C * a + R * b;
        var f1:Number = f(x1);
        var f2:Number = f(x2);
        for (var cnt:int=0; cnt < nIter; cnt++) {
            if (f1 > f2) {
                a = x1;
                x1 = x2;
                f1 = f2;
                x2 = C * a + R * b;
                f2 = f(x2);
            } else {
                b = x2;
                x2 = x1;
                f2 = f1;
                x1 = R * a + C * b;
                f1 = f(x1);
            }
        }
        if (f1 < f2) {
            return {x:x1, fx:f1};
        }
        return {x:x2, fx:f2};
    }

    /* eye
    -- return an 'identity matrix' (1's on the diagonal but 0's elsewhere) for the given dimensions
    -- the identity matrix is square of N x N with N being the dimension (i.e., cells per row or column)
    dims: int -- dimension

    returns
    an array of arrays

    example
    var I:Array = eye(3);
    // I = [[1.0, 0.0, 0.0], [0.0, 1.0, 0.0], [0.0, 0.0, 1.0]];

    */
    public static function eye(dims:int):Array {
        var I:Array = new Array();
        for (var idx1:int=0; idx1 < dims; idx1++) {
            var row:Array = new Array();
            for (var idx2:int=0; idx2 < dims; idx2++) {
                if (idx1 == idx2) {
                    row.push(1.0);
                } else {
                    row.push(0.0);
                }
            }
            I.push(row);
        }
        return I;
    }

    /*
    Powell's algorithm attempts to find the state-point corresponding to the minimum value of a cost-function, F,
    which is a function of a state vector, xs.
    * F :: function(xs:Array):Number -- a function that takes the state point and returns the error of mis-fit
    * xs :: Array -- a state vector of the design variables to optimize
    * step :: Number -- the initial step size for goldenBracket
    * tol :: Number -- a tolerance for convergence (defaults to 1.0e-6)
    */
    public static function powellsMethod(F:Function, x0:Array, step:Number=0.1, tol:Number=1.0e-6):Array {
        var xs:Array = copyVec(x0);
        const MAX_CYCLES:int = 30;
        var make_F_AlongVec:Function = function(v:Array):Function {
            return function(s:Number):Number {
                var nextState:Array = vecAdd(xs, scalarMul(s, v));
                return F(nextState);
            }
        };
        var numDims:int = xs.length;
        var df:Array = new Array();
        var vectors:Array = eye(numDims);
        for (var idx1:int=0; idx1 < numDims; idx1++) {
            df.push(0.0);
        }
        // trace('vectors=', vectors);
        for (var cnt:int=0; cnt < MAX_CYCLES; cnt++) {
            // trace('cnt:',cnt);
            var xsPrev:Array = copyVec(xs);
            var costPrev:Number = F(xs);
            for (var directionCount:int=0; directionCount < numDims; directionCount++) {
                var v:Array = vectors[directionCount];
                var f:Function = make_F_AlongVec(v);
                var range:Object = goldenBracket(f, 0.0, step);
                var optResults:Object = goldenSearch(f, range.lower, range.upper);
                df[directionCount] = costPrev - optResults.fx;
                costPrev = optResults.fx;
                xs = vecAdd(xs, scalarMul(optResults.x, v));
            }
            v = vecSub(xs, xsPrev);
            f = make_F_AlongVec(v);
            range = goldenBracket(f, 0.0, step);
            optResults = goldenSearch(f, range.lower, range.upper);
            xs = vecAdd(xs, scalarMul(optResults.x, v));
            var diff:Array = vecSub(xs, xsPrev);
            if (Math.sqrt(vecDot(diff, diff) / numDims) < tol) {
                return [xs, cnt];
            }
            var maxChangeIndex:int = idxOfMax(df);
            for (directionCount=maxChangeIndex; directionCount < (numDims - 1); directionCount++) {
                vectors[directionCount] = vectors[directionCount + 1];
            }
            vectors[numDims - 1] = v;
        }
        return [xs, cnt];
    }

    public static function vecMin(xs:Array):Number {
        var min:Number = xs[0];
        for each (var item:Number in xs) {
            if (item < min) {
                min = item;
            }
        }
        return min;
    }

    public static function makeRangeFuncForLinsearch(mins:Array, maxs:Array):Function {
        return function(point:Array, vector:Array):Array {
            var maxAlphas:Array = new Array();
            for (var idx:int=0; idx < point.length; idx++) {
                if (vector[idx] == 0.0) {
                    continue;
                }
                // depending on which way vector is facing, either
                //      point[idx] + alpha * vector[idx] = max[idx]
                // or
                //      point[idx] + alpha * vector[idx] = min[idx]
                // if we're facing the "wrong way", then the alpha for min value will be negative
                var alpha1:Number = (maxs[idx] - point[idx]) / vector[idx];
                var alpha2:Number = (mins[idx] - point[idx]) / vector[idx];
                if (alpha1 > 0.0) {
                    maxAlphas.push(alpha1);
                } else if (alpha2 > 0.0) {
                    maxAlphas.push(alpha2);
                }
            }
            var maxAlpha:Number = AdvMath.vecMin(maxAlphas);
            return [0.0, maxAlpha];
        };
    }

    public static function linesearch_powell(func:Function, point:Array, vector:Array, tol:Number=1e-3,
                                             rngFunc:Function=null):Object {
        var funcAlongVec:Function = function(alpha:Number):Number {
            return func(vecAdd(point, scalarMul(alpha, vector)));
        };
        var rng:Array;
        if (rngFunc == null) {
            rng = null;
        } else {
            rng = rngFunc(point, vector);
        }
        var out:Object = brent(funcAlongVec, rng, tol);
        var opt_vector:Array = scalarMul(out.xmin, vector);
        return {
            xmin:vecAdd(point, opt_vector),
            fval:out.fval,
            opt_vector:opt_vector,
            funcalls:out.funcalls
        };
    }

    public static function range(stop:int, start:int=0, step:int=1):Array {
        var out:Array = new Array();
        var val:int = start;
        while (val < stop) {
            out.push(val);
            val += step;
        }
        return out;
    }

    /* fmin_powell
    ==============
    minimize a function of multiple variables using Powell's method (modified).
    Source: SciPy function fmin_powell from version 0.10.1 of SciPy: scipy/optimize/optimize.py
    translation to ActionScript by Michael O'Keefe

    Note: only uses function values, no derivatives

    Parameters
    ----------
    func : Function(x:Array):Number -- objective function to be minimized
    x0 : (Array Number) -- initial guess
    direc : (Array (Array Number)) -- initial direction set
    xtol : Number -- tolerance for change of state
    ftol : tolerance for change in the objective function
    maxiter : int -- the maximum number of iterations allowed
    maxfun : the maximum number of function evaluations allowed
    rngFunc : Function(pt:Array, vec:Array):Array -- a function that takes a point and a vector and returns the
                                                     range for linsearch as an [a, b] tuple

    Returns
    -------
    xopt : (Array Number) -- the state point that minimizes the objective function
    fopt : Number -- the minimum value of the objective function found by the algorithm
    direc : (Array (Array Number)) -- the most recent set of directions used by the algorithm
    iter : int -- the number of iterations used
    funcalls : int -- the number of calls to the objective function

    Notes (from SciPy source):

    <blockquote>
    Uses a modification of Powell's method to find the minimum of
    a function of N variables. Powell's method is a conjugate
    direction method.

    The algorithm has two loops. The outer loop
    merely iterates over the inner loop. The inner loop minimizes
    over each current direction in the direction set. At the end
    of the inner loop, if certain conditions are met, the direction
    that gave the largest decrease is dropped and replaced with
    the difference between the current estiamted x and the estimated
    x from the beginning of the inner-loop.

    The technical conditions for replacing the direction of greatest
    increase amount to checking that

    1. No further gain can be made along the direction of greatest increase
    from that iteration.
    2. The direction of greatest increase accounted for a large sufficient
    fraction of the decrease in the function value from that iteration of
    the inner loop.

    References
    ----------
    Powell M.J.D. (1964) An efficient method for finding the minimum of a
    function of several variables without calculating derivatives,
    Computer Journal, 7 (2):155-162.

    Press W., Teukolsky S.A., Vetterling W.T., and Flannery B.P.:
    Numerical Recipes (any edition), Cambridge University Press
    </blockquote>
    */
    public static function fmin_powell(func:Function, x0:Array, direc:Array=null, xtol:Number=1.0e-4,
                                       ftol:Number=1.0e-4, maxiter:int=-1,
                                       maxfun:int=-1, rngFunc:Function=null):Object {
        const SMALL_NUMBER:Number = 1.0e-20;
        var x:Array = copyVec(x0);
        var N:int = x.length;
        if (maxiter <= 0) {
            maxiter = N * 1000;
        }
        if (maxfun <= 0) {
            maxfun = N * 4000;
        }
        if (direc == null) {
            direc = eye(N);
        }
        var funcalls:int = 0;
        var fval:Number = func(x);
        funcalls += 1;
        var x1:Array = copyVec(x);
        var iter:int = 0;
        var ilist:Array = range(N);
        while (true) {
            var fx:Number = fval;
            var bigind:int = 0;
            var delta:Number = 0.0;
            for each (var idx:int in ilist) {
                var direc1:Array = direc[idx];
                var fx2:Number = fval;
                var linesearch_results:Object = linesearch_powell(func, x, direc1, xtol * 100, rngFunc);
                fval = linesearch_results.fval;
                x = linesearch_results.xmin;
                direc1 = linesearch_results.opt_vector;
                funcalls += linesearch_results.funcalls;
                if ((fx2 - fval) > delta) {
                    delta = fx2 - fval;
                    bigind = idx;
                }
            }
            iter += 1;
            if (2.0 * (fx - fval) <= ftol * (Math.abs(fx) + Math.abs(fval) + SMALL_NUMBER)) {
                break;
            }
            if (funcalls >= maxfun) {
                break;
            }
            if (iter >= maxiter) {
                break;
            }
            direc1 = vecSub(x, x1);
            var x2:Array = vecSub(scalarMul(2.0, x), x1);
            x1 = copyVec(x);
            fx2 = func(x2);
            funcalls += 1;
            if (fx > fx2) {
                var t:Number = 2.0 * (fx + fx2 - 2.0 * fval);
                var temp:Number = fx - fval - delta;
                t *= temp * temp;
                temp = fx - fx2;
                t -= delta * temp * temp;
                if (t < 0.0) {
                    linesearch_results = linesearch_powell(func, x, direc1, xtol * 100, rngFunc);
                    fval = linesearch_results.fval;
                    x = linesearch_results.xmin;
                    direc1 = linesearch_results.opt_vector;
                    funcalls += linesearch_results.funcalls;
                    direc[bigind] = direc[direc.length - 1];
                    direc[direc.length - 1] = direc1;
                }
            }
        }
        return makeOptimizerResults(x, fval, direc, iter, funcalls);
    }

    public static function makeOptimizerResults(xmin:Array, fval:Number, direc:Array, iter:int,
                                                funcalls:int):Object {
        return {
            xmin:xmin,
            fval:fval,
            direc:direc,
            iter:iter,
            funcalls:funcalls
        };
    }

    public static function xmin(obj:Object):Array {
        return obj.xmin;
    }

    public static function fval(obj:Object):Number {
        return obj.fval;
    }

    public static function direc(obj:Object):Array {
        return obj.direc;
    }

    public static function iter(obj:Object):int {
        return obj.iter;
    }

    public static function funcalls(obj:Object):int {
        return obj.funcalls;
    }

    public static function map(f:Function, xs:Array):Array {
        var out:Array = new Array();
        for each (var item:* in xs) {
            out.push(f(item));
        }
        return out;
    }

    public static function linsearch():Object {

        return makeOptimizerResults([0.0, 0.0], 0.0, [[1.0, 0.0], [0.0, 1.0]], 1, 100);
    }

    public static function makeFullFactorialMatrix(numDesignVariables:int, numLevels:int):Array {
        var dD:Number = 1.0 / (1 + numLevels);
        var points:Array = new Array();
        var levels:Array = new Array();
        for (var idx:int = 0; idx < numDesignVariables; idx++) {
            points.push(new Array());
            levels.push(0);
        }
        var notDone:Boolean = true;
        while (notDone) {
            var carry:Boolean = true;
            for (var dvIdx:int=0; dvIdx < numDesignVariables; dvIdx++) {
                points[dvIdx].push((levels[dvIdx] + 1) * dD);
                // trace('cnt:', dvIdx, 'row:', points[dvIdx]);
                if (carry) {
                    levels[dvIdx] += 1;
                    if ((dvIdx == (levels.length - 1)) && (levels[dvIdx] == numLevels)) {
                        notDone = false;
                    }
                    if (levels[dvIdx] == numLevels) {
                        levels[dvIdx] = 0;
                        carry = true;
                    } else {
                        carry = false;
                    }
                }
            }
            // trace('-----------');
        }
        return points;
    }
}
}