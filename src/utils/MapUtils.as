/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package utils {
public class MapUtils {
    public static function copyObject(objToCopy:Object):Object {
        var newObj:Object = new Object();
        for (var attr:String in objToCopy) {
            newObj[attr] = objToCopy[attr];
        }
        return newObj;
        // return ObjectUtil.copy(objToCopy);
    }

    public static function atKeyOrAlt(obj:Object, prop:String, def:*):* {
        if (obj.hasOwnProperty(prop)) {
            return obj[prop];
        }
        return def;
    }

    public static function traceObject(obj:Object, name:String):void {
        trace(objectToString(obj, name));
    }

    public static function objectToString(obj:Object, name:String):String {
        var str:String = "";
        var attrs:Array = keys(obj);
        attrs.sort();
        for each (var attr:String in attrs) {
          try {
            str += name + '.' + attr + ' = ' + obj[attr].toString() + '\n';
          } catch (e:Error) {
            str += name + '.' + attr + ' = <unable to call toString()>\n';
          }
        }
        return str;
    }

    public static function stringRepresentation(obj:Object):String {
        var items:Array = new Array();
        var attrs:Array = keys(obj);
        attrs.sort();
        for each (var attr:String in attrs) {
            items.push(attr + ": " + obj[attr].toString());
        }
        return "{" + items.join(", ") + "}";
    }

    public static function intercalStr(sep:String, obj:Object):String {
        var vals:Array = [];
        var attrs:Array = keys(obj);
        attrs.sort();
        for each (var attr:String in attrs) {
            vals.push(attr + ":" + obj[attr].toString());
        }
        return vals.join(sep);
    }

    public static function numAttributes(obj:Object):int {
        var cnt:int = 0;
        for (var attr:String in obj) {
            cnt++;
        }
        return cnt;
    }

    public static function keys(obj:Object):Array {
        var attrs:Array = new Array();
        for (var attr:String in obj) {
            attrs.push(attr);
        }
        attrs.sort();
        return attrs;
    }

    public static function makeObject(keyVals:Array):Object {
        var obj:Object = new Object();
        for (var idx:int=0; idx < (keyVals.length - 1); idx+=2) {
            obj[keyVals[idx]] = keyVals[idx+1];
        }
        return obj;
    }

    public static function makeFromKeysAndVals(keys:Array, vals:Array):Object {
        var obj:Object = new Object();
        for (var idx:int=0; idx < Math.min(keys.length, vals.length); idx++) {
            obj[keys[idx]] = vals[idx];
        }
        return obj;
    }

    public static function update(obj:Object, keyVals:Array):Object {
        var newObj:Object = copyObject(obj);
        for (var idx:int=0; idx < (keyVals.length - 1); idx += 2) {
            newObj[keyVals[idx]] = keyVals[idx+1];
        }
        return newObj;
    }

    public static function toKeyValArray(obj:Object):Array {
        var xs:Array = new Array();
        var attrs:Array = keys(obj);
        for each (var attr:String in attrs) {
            xs.push(attr);
            xs.push(obj[attr]);
        }
        return xs;
    }

    public static function
    setIfNotSet(obj:Object, attr:String, val:*):Object {
        if (obj.hasOwnProperty(attr)) {
            return obj;
        }
        return update(obj, [attr, val]);
    }

    public static function
    setManyIfNotSet(obj:Object, keyVals:Array):Object {
        var newObj:Object = copyObject(obj);
        for (var idx:int=0; idx<keyVals.length; idx+=2) {
            if (!newObj.hasOwnProperty(keyVals[idx])) {
                newObj[keyVals[idx]] = keyVals[idx+1];
            }
        }
        return newObj;
    }

    // Merge two objects -- if both have the same key, objB "wins"
    public static function
    merge(objA:Object, objB:Object):Object {
        var objC:Object = copyObject(objA);
        for each (var attr:String in keys(objB)) {
            objC[attr] = objB[attr];
        }
        return objC;
    }

    public static function
    mergeWith(a:Object, b:Object, f:Function):Object {
        var c:Object = copyObject(a);
        for each (var attr:String in keys(b)) {
            if (c.hasOwnProperty(attr)) {
                c[attr] = f(a[attr], b[attr]);
            } else {
                c[attr] = b[attr];
            }
        }
        return c;
    }

    /* Merges an array of several objects into one; the later the object is
    // in the array list, the higher precidence it has if there is a clash for
    // attribute names. */
    public static function
    mergeAll(objs:Array):Object {
        var merged:Object = new Object();
        for each (var obj:Object in objs) {
            merged = merge(merged, obj);
        }
        return merged;
    }

    public static function
    ensureFieldsExist(obj:Object, fields:Array):void {
        for each (var field:String in fields) {
            if (!obj.hasOwnProperty(field)) {
                throw new Error(
                    'required field, ' + field + ' does not exist');
            }
        }
    }

    public static function
    onlyTheseKeys(obj:Object, keys:Array):Object {
        var newObj:Object = new Object();
        for each (var key:String in keys) {
            if (obj.hasOwnProperty(key)) {
                newObj[key] = obj[key];
            }
        }
        return newObj;
    }

    public static function
    updateWithoutAddingKeys(objA:Object, objB:Object):Object {
        var out:Object = MapUtils.copyObject(objA);
        for each (var key:String in keys(objA)) {
            if (objB.hasOwnProperty(key)) {
                out[key] = objB[key];
            }
        }
        return out;
    }

    public static function
    mapOver(obj:Object, f:Function):Object {
        var newObj:Object = new Object();
        for each (var key:String in keys(obj)) {
            newObj[key] = f(obj[key]);
        }
        return newObj;
    }

    public static function
    getIn(obj:Object, tags:Array):* {
        var out:* = copyObject(obj);
        for each (var tag:String in tags) {
            if (out.hasOwnProperty(tag)) {
                out = out[tag];
            } else {
                trace('tried to read: out[' + tags.join("][") + "]");
                trace('got as far as ' + tag);
                MapUtils.traceObject(out, 'out[...]');
                MapUtils.traceObject(obj, 'obj');
                throw new Error("MapUtils.getIn :: Attempt to read " +
                    "non-existing tags ");
            }
        }
        return out;
    }

    public static function
    assocIn(obj:Object, tags:Array, value:*):Object {
        var tags_:Array = ArrayUtils.copyArray(tags);
        var out:Object = copyObject(obj);
        var parents:Array = new Array();
        var tmp:Object = out;
        if (tags.length == 0) {
            return value;
        } else if (tags.length == 1) {
            out[tags[0]] = value;
            return out;
        }
        for each (var tag:String in tags) {
            parents.push(copyObject(tmp));
            tmp = tmp[tag];
        }
        var lastParent:Object = parents.pop();
        var lastTag:String = tags_.pop();
        lastParent[lastTag] = value;
        for each (tag in tags_.reverse()) {
            tmp = parents.pop();
            tmp[tag] = lastParent;
            lastParent = tmp;
        }
        return lastParent;
    }
}
}