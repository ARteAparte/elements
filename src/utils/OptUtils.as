/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package utils {
public class OptUtils {
    /* finds a zero value of fCon between lower and upper bounds
    that's as large as possible (i.e., as close to target point). We assume
    that this function runs over the variable alpha such that 0 <= alpha <= 1
    and fCon(0) is 0.0 (valid) and fCon(1) is non-valid. We attempt
    to make alpha as large as possible (i.e., as close to alpha=1 as possible)
    while staying valid

    lb -- lower bound; starts at 0.0
    ub -- upper bound; starts at 1.0
    fCon -- returns the error on alpha where 0 <= alpha <= 1

    fCon :: alpha -> error
    where alpha :: Number and error :: Number

    if fCon returns any number > 0, the function is invalid at that point.
    An fCon value of 0.0 means the function is valid. */
    public static function
    findGreatestGoodBound(fCon:Function,
                 lb:Number=0.0, ub:Number=1.0,
                 dxTol:Number=1e-4, TOL:Number=1e-4, MAX_ITER:int=16):Number {
        var dx:Number = (lb + ub) / 2.0;
        var iterNum:int = 0;
        var x:Number = 0.5;
        var xMin:Number = 0.0;
        var con:Number;
        var direction:Number;
        while ((dx > dxTol) && (iterNum < MAX_ITER)) {
            con = fCon(x);
            if (con == 0.0) {
                xMin = x;
                direction = 1.0;
            } else {
                direction = -1.0;
            }
            dx = dx / 2.0;
            x = x + direction * dx;
            iterNum += 1;
            //trace('x:', iterNum, '::', x);
        }
        return xMin;
    }

    public static const R:Number = 0.61803399;
    public static const C:Number = 1.0 - R;

    public static function
    binarySearchBounds(fCon:Function, lb:Number=0.0, ub:Number=1.0,
                       dxTol:Number=1e-4, maxIter:int=32):Array {
        if (ub < lb) {
            var tmp:Number = lb;
            lb = ub;
            ub = tmp;
        }
        var dx:Number = ub - lb;
        var iter:int = 0;
        var ubNext:Number = ub;
        var lbNext:Number = lb;
        var fUb:Number = fCon(ubNext);
        var fLb:Number = fCon(lbNext);
        if ((fUb == 0.0) && (fLb == 0.0)) {
            return [lbNext, ubNext];
        }
        var lbGood:Number = NaN;
        var ubGood:Number = NaN;
        while ((iter < maxIter) && (dx > dxTol)) {
            dx = dx / 2.0;
            if (fUb > 0.0) {
                ubNext = Math.max(ub - dx, lbNext);
            } else if (ubNext < ub) {
                ubGood = ubNext;
                ubNext = ubNext + dx;
            }
            if (fLb > 0.0) {
                lbNext = Math.min(lb + dx, ubNext);
            } else if (lbNext > lb) {
                lbGood = lbNext;
                lbNext = lb - dx;
            }
            fUb = fCon(ubNext);
            fLb = fCon(lbNext);
            iter++;
        }
        return [lbGood, ubGood];
    }

    /* binarySearch
    Here we will do a bounded search to find x* such that fOpt(x*) is
    minimized and fCon(x*) is 0.0. We know apriori that fOpt(.) = 0.0 is the
    global minimum but it may not be reachable while staying within
    constraints.

    fOpt :: x:Number -> Number -- a function of one variable, x, to minimize.
    fCon :: x:Number -> Number -- constraint function of one variable, x.
                                  0.0 means valid; > 0.0 indicates that
                                  constraints are violated.
    lb :: Number -- the lower bound for bisection
    ub :: Number -- the upper bound for bisection

    returns Number, the x*
    */
    public static function
    binarySearch(f:Function, lb:Number=0.0, ub:Number=1.0,
                 fOptTol:Number=1e-9, maxIter:int=16,
                 minStep:Number=1e-9):Number {
        if (lb > ub) {
            var tmp:Number = lb;
            lb = ub;
            ub = tmp;
        }
        var fMin:Number = NaN;
        var xMin:Number = NaN;
        var step:Number = (ub - lb) / 2.0;
        var xUpper:Number = ub;
        var fUpper:Number = f(xUpper);
        var xMid:Number = step + lb;
        var fMid:Number = f(xMid);
        var xLower:Number = lb;
        var fLower:Number = f(xLower);
        var numIters:int = 0;
        while ((numIters < maxIter) && (step > minStep)) {
            step = step / 2.0;
            if ((fLower <= fUpper) && (fLower < fMid)) {
                xMin = xLower;
                fMin = fLower;
                xUpper = xMid;
                fUpper = fMid;
                xMid = step + xLower;
                fMid = f(xMid);
                //trace('<<', xLower, '/', xMid, '/', xUpper, '[', xMin, ']');
            } else if ((fUpper < fLower) && (fUpper < fMid)) {
                xMin = xUpper;
                fMin = fUpper;
                xLower = xMid;
                fLower = fMid;
                xMid = xUpper - step;
                fMid = f(xMid);
                //trace('>>', xLower, '/', xMid, '/', xUpper, '[', xMin, ']');
            } else if ((fMid <= fLower) && (fMid <= fUpper)) {
                xMin = xMid;
                fMin = fMid;
                xLower = xLower + step;
                fLower = f(xLower);
                xUpper = xUpper - step;
                fUpper = f(xUpper);
                //trace('><', xLower, '/', xMid, '/', xUpper, '[', xMin, ']');
            } else {
                throw new Error('unanticipated case');
            }
            // trace('fmin = ', fMin, 'iter = ', numIters);
            if (fMin <= fOptTol) {
                return xMin;
            }
            numIters++;
        }
        return xMin;
    }

    public static function
    scaleByX(pointA:Object, pointB:Object, x:Number):Object {
        var pointC:Object = new Object();
        for each (var attr:String in MapUtils.keys(pointA)) {
            pointC[attr] = pointA[attr] + x * (pointB[attr] - pointA[attr]);
        }
        return pointC;
    }

    public static function
    makeBoundFindingFunc(pointA:Object, pointB:Object,
                         header:Object, conFunc:Function):Function {
        return function(x:Number):Number {
            var pt:Object = scaleByX(pointA, pointB, x);
            return conFunc(header, pt);
        };
    }

    public static function
    normalize0to1(val:Number, min:Number, max:Number):Number {
        var nval:Number = (val - min) / (max - min);
        if (nval < 0.0) nval = 0.0;
        if (nval > 1.0) nval = 1.0;
        return nval;
    }

    public static function
    denormalize(nval:Number, min:Number, max:Number):Number {
        if (nval > 1.0) {
            nval = 1.0;
        }
        if (nval < 0.0) {
            nval = 0.0;
        }
        return (max - min) * nval + min;
    }

    public static function
    makeErrFunc(values:Object, settings:Object, mins:Object, maxs:Object,
                header:Object, conFunc:Function, derivFuncs:Object,
                stateVariables:Array, targetMetrics:Array):Function {
        return function(xs:Array):Number {
            var guessPt:Object = MapUtils.copyObject(values);
            for (var idx:int=0; idx < stateVariables.length; idx++) {
                var stateVariableName:String = stateVariables[idx];
                var val:Number = denormalize(
                    xs[idx], mins[stateVariableName], maxs[stateVariableName]);
                /*
                trace('setting', stateVariableName,
                    'from', xs[idx], 'to', val);
                ObjectUtils.traceObject(mins, 'mins');
                ObjectUtils.traceObject(maxs, 'maxs');
                */
                guessPt[stateVariableName] = val;
            }
            var con:Number = conFunc(header, guessPt);
            if (false) {
                if (con > 0.0) {
                    var g:Function = makeBoundFindingFunc(
                        values, guessPt, header, conFunc);
                    var scale:Number = findGreatestGoodBound(g);
                    guessPt = scaleByX(values, guessPt, scale);
                }
            }
            var errors:Array = new Array();
            //trace('targetMetrics = ', targetMetrics);
            for each (var targetMetric:String in targetMetrics) {
                var targetMin:Number = mins[targetMetric];
                var targetMax:Number = maxs[targetMetric];
                var scaleFactor:Number = 100.0 / (targetMax - targetMin);
                var targetMetricVal:Number = derivFuncs[targetMetric](
                    header, guessPt);
                //trace(targetMetric,'=',targetMetricVal);
                errors.push(Math.abs(
                    settings[targetMetric] - targetMetricVal) * scaleFactor);
            }
            /*
            trace('error:', AdvMath.sum(errors) / Number(targetMetrics.length));
            */
            var numMetrics:Number = Number(targetMetrics.length);
            var err:Number = con * 100 + AdvMath.sum(errors) / numMetrics;
            if (isNaN(err)) {
                return 1e6;
            }
            return err;
        };
    }

    public static function
    bruteForceSearch(values:Object, settings:Object, numLevels:int,
                     header:Object, conFunc:Function, derFuncs:Object,
                     mins:Object, maxs:Object):Object {
        var attributes:Function = MapUtils.keys;
        var targetSettings:Array = attributes(
            Set.difference(settings, values));
        var freeValues:Array = attributes(
            Set.difference(values, settings));
        var numDesignVars:int = freeValues.length;
        var newValues:Object = MapUtils.updateWithoutAddingKeys(
            values, settings);
        var errFunc:Function = makeErrFunc(newValues, settings, mins, maxs,
            header, conFunc, derFuncs, freeValues, targetSettings);
        var runs:Array = AdvMath.makeFullFactorialMatrix(
            numDesignVars, numLevels);
        var errs:Array = new Array();
        var optStates:Array;
        var minError:Number;
        var error:Number;
        for (var run:int=0; run<Math.pow(numLevels, numDesignVars); run++) {
            var f:Function = function(theSettings:Array):Number {
                return theSettings[run];
            };
            var xs:Array = FP.map(f, runs);
            error = errFunc(xs);
            if (run == 0) {
                optStates = xs;
                minError = error;
            }
            if (error < minError) {
                optStates = xs;
                minError = error;
            }
        }
        //trace('minError', minError);
        var newPt:Object = MapUtils.copyObject(values);
        for (var idx:int=0; idx < freeValues.length; idx++) {
            var freeValue:String = freeValues[idx];
            newPt[freeValue] = denormalize(
                optStates[idx], mins[freeValue], maxs[freeValue]);
            //trace(freeValue, '::', optStates[idx], '->', newPt[freeValue]);
        }
        return newPt;
    }

    public static function
    findRoot(initPt:Object, objective:Object, freeVar:String, depndtVar:String,
             depndtVarFunc:Function, conFunc:Function, header:Object,
             freeVarMin:Number, freeVarMax:Number):Object {
        var newPt:Object = MapUtils.copyObject(initPt);
        var f:Function = function(x:Number):Number {
            var guessPt:Object = MapUtils.copyObject(initPt);
            guessPt[freeVar] = x;
            //var valErr:Number = conFunc(header, guessPt);
            //if (valErr > 0.0) {
            //    var g:Function = makeBoundFindingFunc(
            //        initPt, guessPt, header, conFunc);
            //    var scale:Number = OptUtils.findGreatestGoodBound(g);
            //    guessPt = scaleByX(initPt, guessPt, scale);
            //}
            var guessValue:Number = depndtVarFunc(header, guessPt);
            return guessValue - objective[depndtVar];
        };
        //var df:Function = AdvMath.makeDerivativeFunction(f, 1e-6);
        var df:Function = AdvMath.makeDerivativeFunctionStencil(f, AdvMath.DX);
        var boundsTol:Number = (freeVarMax - freeVarMin) / 1000.0;
        var maxCount:int = 400;
        var okFlag:Boolean = true;
        //trace('starting safeNewtonRaphson');
        try {
            var xOpt:Number = AdvMath.safeNewtonRaphson(
                f, df, freeVarMin, freeVarMax, boundsTol, AdvMath.DX, maxCount);
            if (isNaN(xOpt)) {
              okFlag = false;
            }
        } catch (e:Error) {
          okFlag = false;
          //trace('Error! OptUtils.findRoot -- switching to brute force solve');
            // OK, newton raphson crashed we'll try a brute force solution:
        }
        if (!okFlag) {
          var numPts:int = 1001;
          var errs:Array = [];
          var range:Number = freeVarMax - freeVarMin;
          var dD:Number = range / (numPts - 1.0);
          for (var ptIdx:int = 0; ptIdx < numPts; ptIdx++) {
            errs.push(Math.abs(f(freeVarMin + ptIdx * dD)));
          }
          var idxForMinErr:int = AdvMath.idxOfMin(errs);
          xOpt = freeVarMin + idxForMinErr * dD;
        }
        newPt[freeVar] = xOpt;
        /*if (true) {
          trace('START OptUtils.findRoot');
          MapUtils.traceObject(initPt, 'initPt');
          MapUtils.traceObject(objective, 'objective');
          trace('freeVar: ', freeVar);
          trace('depndtVar: ', depndtVar);
          trace('freeVarMin: ', freeVarMin);
          trace('freeVarMax: ', freeVarMax);
          trace('boundsTol: ', boundsTol);
          trace('xOpt: ', xOpt);
          trace('END OptUtils.findRoot');
        }*/
        // if the newPt found does not meet constraints, adjust it
        // so that it will
        var valErr:Number = conFunc(header, newPt);
        if (valErr > 0.0) {
            var g:Function = makeBoundFindingFunc(
                initPt, newPt, header, conFunc);
            var scale:Number = OptUtils.findGreatestGoodBound(g);
            newPt = scaleByX(initPt, newPt, scale);
        }
        return newPt;
    }

    public static function
    makeDirectReturn(xMin:Array, fMin:Array, iter:int):Object {
        return {
            xMin:xMin,
            fMin:fMin,
            iter:iter
        };
    }

    /* DIRECT -- DIvide RECTangles
    An algorithm to globally minimize multivariate (non-linear) functions of
    (non-linear) constraints of the form:

    minimize f(x) with respect to x such that:
        x_L <=   x  <= x_U
        b_L <= A x  <= b_U
        c_L <= c(x) <= c_U
        x(i) integer, for i in I

    Results = direct(p_f, p_c, x_L, x_U, A, b_L, b_U, ...
                     c_L, c_U, I, GLOBAL, PriLevel);

    arguments:
    p_f :: Function -- the function to minimize
        :: (x:Array):Number
    p_c :: Function -- the non-linear constraints
        :: (x:Array):Array
        p_c must return an array of non-linear constraints, one for
            each design variable in x_L/x_U (which will correspond to the
            bounds in c_L and c_U); If there are no constraints, set p_c
            to null.
    x_L :: Array -- the lower bounds of x
    x_U :: Array -- the upper bounds of x
        NOTE: x_L must be always lower than corresponding x_U
    A   :: Matrix -- the linear constraint matrix.
        :: Array of Array of Number
        NOTE if there are no linear constraints, set A to null.
    b_L :: Array -- the lower bounds for linear constraints
    b_U :: Array -- the upper bounds for linear constraints
    c_L :: Array -- the lower bounds for non-linear constraints
    c_U :: Array -- the upper bounds for non-linear constraints
        NOTE: b_L, b_U, c_L, and c_U must be the same length as
              x_L and x_U and correspond to constraints (linear or
              non-linear respectively) on x[i]. If x[i] has no
              linear constraint, just have that row of A be zeros and
              have the b_L and b_U work accordingly. If a given x[i]
              doesn't have a non-linear constraint, just have p_c return
              0.0 for that x[i] and set c_L and c_U accordingly
        NOTE: if there are no linear constraints, set b_L and b_U to null.
              if there are no non-linear constraints, set c_L and c_U to
               null.
    GLOBAL :: Object -- global settings:
        - MaxEval :: int -- the maximum number of evaluations
        - MaxIter :: int -- the maximum number of iterations
        - epsilon :: Number -- global/local search weight parameter
        NOTE: the algorithm runs until BOTH of MaxEval and MaxIter have been
              violated.
    PriLev :: int -- printing level:
        - 0 -- warnings only
        - 1 -- each iteration info, warnings

    results:
    x_k :: Array -- the optimal result
    f_k :: Number -- function value at optimal

    References
    ----------
    Jones, D.R., Perttunen, C.D., and Stuckman, B.E. (1993). "Lipschitzian
        Optimization Without the Lipschitz Constant". Journal of Optimization
        Theory and Application. Vol. 79. No. 1. October 1993.

    gclSolve.m by Kenneth Holmstrom
        HKH MatrisAnalys AB, hkh@acm.org, April 8, 1999
        available from ADVISOR project:
        http://www.bigladdersoftware.com/advisor

    gclSolve.m modifications by John Whitehead
        Optimal Design Laboratory
        University of Michigan
        March 4, 2001

    Direct.m by Dan Finkel (definkel@unity.ncsu.edu)
        created 1/27/2003; last updated 6/21/2004
        downloaded from:
        http://www4.ncsu.edu/~ctk/Finkel_Direct/

    this version authored by
        Michael O'Keefe
        Big Ladder Software,
        May 28, 2012
        (C) 2012, Michael O'Keefe, Big Ladder Software
        All Rights Reserved
    */
    public static function
    direct(p_f:Function, p_c:Function, x_L:Array, x_U:Array,
           A:Array=null, b_L:Array=null, b_U:Array=null,
           c_L:Array=null, c_U:Array=null,
           GLOBAL:Object=null, PriLev:int=1):Object {
        var cnt:int;
        if ((p_f == null) || (x_L == null) || (x_U == null)) {
            throw new Error('p_f, x_L, and x_U must be defined');
        }
        if (x_L.length != x_U.length) {
            throw new Error('x_L and x_U must be of same length');
        }
        var n:int = x_L.length; // problem dimension (# design variables)
        for (cnt=0; cnt < n; cnt++) {
            if (x_L[cnt] > x_U[cnt]) {
                throw new Error('x_L must always be lower than x_U');
            }
        }
        var getit:Function = MapUtils.atKeyOrAlt;
        if (GLOBAL==null) {
            GLOBAL = new Object();
        }
        // maximum function evaluations
        var MaxEval:int = getit(GLOBAL, 'MaxEval', 500);
        // global/local weight parameter.
        var epsilon:Number = getit(GLOBAL, 'epsilon', 1.0e-4);
        // maximum number of iterations
        var MaxIter:int = getit(GLOBAL, 'MaxIter', 20);
        var cTol:Number = 1e-6; // constraint feasibility tolerance
        var nFunc:int = 0; // function evaluation counter

        /* STEP 1: INITIALIZATION */
        var PrevIter:int = 0;
        // C is matrix with all rectangle centerpoints
        // the first point is the centerpoint of the unit hypercube
        // (i.e., the center of the entire design space)
        var C:Array = new Array();
        asetall(C, 0.5);
        // Split[i][j] is the number of times rectangle j has been
        // split along dimension i. Split[i][j] is set to NaN if
        // rectangle j has sidelength 0 for integer i
        // If all variables are integers, it is possible for a rectangle
        // to be reduced to a single point in which case it should be
        // ignored in the rectangle selection procedure.
        var Split:Array = new Array();
        asetall(Split, 0.0);
        // Transform C to original search space
        var x:Array = aAdd(x_L, aMul(C, aSub(x_U, x_L)));
        var f:Number = p_f(x);
        nFunc++;
        // F[i] is the ith function value
        var F:Array = [f];
        // T[i] is the number of times rectangle i has been trisected
        var T:Array = [0];
        // D is a vector with distances from centerpoint to the verticies
        var D:Array = [Math.sqrt(n)/2.0];
        var lincon:Boolean = A != null;
        var nonlincon:Boolean = p_c != null;
        var con:Boolean = lincon || nonlincon;
        var Ax:Array;
        if (lincon) {
            Ax = matTimesVec(A, x);
        } else {
            Ax = [];
        }
        var cx:Array;
        if (nonlincon) {
            cx = p_c(x);
        } else {
            cx = [];
        }
        // G is an array of the constraint values at each point
        var G:Array = [];
        var m:int;
        var feasible:Boolean;
        if (con) {
            feasible = true;
            // we're going to put this in the g(x) <= 0.0 form
            var gx_minus_g_U:Array = new Array();
            if (lincon) {
                for (cnt=0; cnt < n; cnt++) {
                    gx_minus_g_U.push(-Ax[cnt] + b_L[cnt]);
                    if (Ax[cnt] < b_L[cnt]) {
                        feasible = false;
                    }
                }
                for (cnt=0; cnt < n; cnt++) {
                    gx_minus_g_U.push(Ax[cnt] - b_U[cnt]);
                    if (Ax[cnt] > b_U[cnt]) {
                        feasible = false;
                        break;
                    }
                }
            }
            if (nonlincon) {
                for (cnt=0; cnt < n; cnt++) {
                    gx_minus_g_U.push(-cx[cnt] + c_L[cnt]);
                    if (cx[cnt] < c_L[cnt]) {
                        feasible = false;
                    }
                }
                for (cnt=0; cnt < n; cnt++) {
                    gx_minus_g_U.push(cx[cnt] - c_U[cnt]);
                    if (cx[cnt] > c_U[cnt]) {
                        feasible = false;
                    }
                }
            }
            m = gx_minus_g_U.length;
            G.push(gx_minus_g_U);
        } else {
            G = [];
            m = 0;
            feasible = true;
        }
        var f_min:Number;
        var f_min_list:Array = new Array();
        if (feasible) {
            f_min = f;
        } else {
            f_min = NaN;
        }
        f_min_list.push(f_min);
        var s_0:Array;
        var s:Array;
        // Set s[j] = 0 for j=1,2,3,....,m
        if (con) {
            s_0 = [0]; // Used as s[0]
            // s[j] is the sum of observed rates of change for constraint j
            s = agen(0.0, m);
        } else {
            s_0 = [];
            s = [];
        }
        // t[i] is the # of times rectangle has been split on dimension i
        var t:Array = agen(0.0, n);
        // If max number of function evaluations or iterations has been
        // reached, set BreakFlag = true, update s_0, s, and then break
        // main loop.
        var BreakFlag:Boolean = false;
        var Iter:int = 0;
        while ((nFunc < MaxEval) && ((Iter + 1) <= MaxIter)) {
            Iter += 1;
            /* STEP 2: SELECT RECTANGLES */
            // compute the c[j] values using the current values of s_0 and
            // s(j), j=1,2,...,m
            var c:Array;
            var cGD:Array;
            var makeMax:Function = function(max:Number):Function {
                return function(x:Number):Number {
                    if (x > max) {
                        return x;
                    }
                    return max;
                };
            };
            if (con) {
                c = aDiv(s_0, FP.map(makeMax(1e-30), s));
                cGD = aDiv(aMul(c, FP.map(makeMax(0.0), G)), D);
            } else {
                cGD = agen(0.0, F.length);
            }
            // the set of rectangles selected for trisection
            var S:Array = [];

            // If a feasible rectangle has not been found, select the
            // rectangle that minimizes the rate of change required to
            // bring the weighted constraint violations to zero.
            if (!feasible) {

            }
        }


        return null;
    }

    public static function
    aSum(xs:Array):Number {
        var acc:Number = 0.0;
        for each (var x:Number in xs) {
            acc += x;
        }
        return acc;
    }

    public static function
    aDiv(xs:Array, ys:Array):Array {
        var ans:Array = new Array();
        for (var idx:int=0; idx<xs.length; idx++) {
            ans.push(xs[idx] / ys[idx]);
        }
        return ans;
    }

    public static function
    agen(seed:Number, n:int):Array {
        var ans:Array = new Array();
        for (var idx:int=0; idx < n; idx++) {
            ans.push(n);
        }
        return ans;
    }

    public static function
    dot(xs:Array, ys:Array):Number {
        var ans:Number = 0.0;
        for (var idx:int=0; idx<xs.length; idx++) {
            ans += xs[idx] * ys[idx];
        }
        return ans;
    }

    public static function
    matTimesVec(mat:Array, vec:Array):Array {
        var ans:Array = new Array();
        for (var row:int=0; row<mat.length; row++) {
            ans.push(dot(mat[row], vec));
        }
        return ans;
    }

    public static function
    aAdd(xs:Array, ys:Array):Array {
        if (xs.lenght != ys.length) {
            throw new Error('attempt to add arrays of differing lengths');
        }
        var zs:Array = new Array();
        for (var idx:int=0; idx<xs.length; idx++) {
            zs.push(xs[idx] + ys[idx]);
        }
        return zs;
    }

    public static function
    aSub(xs:Array, ys:Array):Array {
        if (xs.length != ys.length) {
            throw new Error('attempt to subtract arrays of different lengths');
        }
        var zs:Array = new Array();
        for (var idx:int=0; idx<xs.length; idx++) {
            zs.push(xs[idx] - ys[idx]);
        }
        return zs;
    }

    public static function
    aMul(xs:Array, ys:Array):Array {
        if (xs.length != ys.length) {
            throw new Error('attempt to multiply arrays of different lengths.');
        }
        var zs:Array = new Array();
        for (var idx:int=0; idx<xs.length; idx++) {
            zs.push(xs[idx] * ys[idx]);
        }
        return zs;
    }

    public static function
    aMulEach(xs:Array, y:Number):Array {
        var zs:Array = new Array();
        for each (var x:Number in xs) {
            zs.push(x * y);
        }
        return zs;
    }

    public static function
    asetall(xs:Array, v:*):void {
        for (var idx:int=0; idx<xs.length; idx++) {
            xs[idx] = v;
        }
    }

    public static function
    direct_finkel(Problem:Object, om_lower:Array, om_upper:Array,
           opts:Object):Object {
        var lengths:Array = [];
        var c:Array = [];
        var fc:Array = [];
        var con:Array = [];
        var szes:Array = [];
        var feas_flags:Array = [];
        var fcncounter:int = 0;
        var perror:int = 0;
        var itctr:int = 1;
        var done:Boolean = false;
        var n:int = om_lower.length;
        // get options
        var getit:Function = MapUtils.atKeyOrAlt;
        // maximum iterations
        var maxits:int = getit(opts, 'maxits', 20);
        // maximum number of function evaluations
        var maxevals:int = getit(opts, 'maxevals', 10);
        // maximum side divisions
        var maxdeep:int = getit(opts, 'maxdeep', 100);
        // terminate if within tolerance of f_opt
        var testflag:Boolean = getit(opts, 'testflag', false);
        // minimum value of the function
        var globalmin:Number = getit(opts, 'globalmin', 0.0);
        // global/local weight parameter
        var ep:Number = getit(opts, 'ep', 1.0e-4);
        // allowable relative error if f_reach set
        var tol:Number = getit(opts, 'tol', 0.01);
        // print iteration stats
        var showits:Boolean = getit(opts, 'showits', false);
        // flag for using implicit constraints
        var impcons:Boolean = getit(opts, 'impcons', false);
        // perturbation for implicit constraints
        var pert:Number = getit(opts, 'pert', 1e-6);
        var theglobalmin:Number = globalmin;
        return null;
    }
}
}