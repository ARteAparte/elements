/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package observer {

public class SubscriberManager implements IPublisher {
  private var _subscribers:Array;

  public function SubscriberManager():void {
    _subscribers = new Array();
  }
  public function registerSubscriberFunction(func:Function):void {
    _subscribers.push(func);
  }
  public function removeSubscriberFunction(func:Function):void {
    var newSubscribers:Array = [];
    for each (var subscriber:Function in _subscribers) {
      if ((func === subscriber)||(func == subscriber)) {
        continue;
      } else {
        newSubscribers.push(subscriber);
      }
    }
    this._subscribers = newSubscribers;
  }
  public function notifySubscribers(topic:String):void {
    for each (var f:Function in _subscribers) {
      f(topic);
    }
  }
}
}