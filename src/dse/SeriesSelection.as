/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package dse {
import mx.collections.IList;
import mx.utils.ObjectUtil;

import spark.components.gridClasses.CellPosition;
import spark.components.gridClasses.GridColumn;

import pdt.IPVec;
import pdt.PersistentVector;

import utils.ArrayUtils;

public class SeriesSelection {
    public var dataPoints:IPVec;
    public var selectedSeries:String;
    public var startIndex:int;
    public var numberOfValues:int;
    public var getter:Function;
    public var header:Object;

    public function SeriesSelection(points:IPVec,
                                    seriesName:String,
                                    startIndex:int,
                                    numberOfValues:int) {
        if (startIndex < 0) {
            throw new Error(
                "start index must be greater than or equal to zero");
        }
        if ((startIndex + numberOfValues) > points.length) {
            throw new  Error(
                "cannot select more items than available in the model");
        }
        this.dataPoints = points;
        this.selectedSeries = seriesName;
        this.startIndex = startIndex;
        this.numberOfValues = numberOfValues;
    }

    // the last index where a selection can be found
    public function get endIndex():int {
        return this.startIndex + (this.numberOfValues - 1);
    }

    public function get selectedRows():Array {
        var rows:Array = new Array();
        for (var idx:int = startIndex;
            idx < startIndex + numberOfValues; idx++) {
            rows.push(idx);
        }
        return rows;
    }

    public function
    selectedData(pts:IPVec, hdr:Object, getter:Function):Array {
        var ps:Array = pts.toArray();
        var xs:Array = new Array(this.numberOfValues);
        for (var idx:int=startIndex; idx<=(this.endIndex); idx++) {
            var pt:Object = ps[idx];
            xs[idx] = getter(hdr, pt);
        }
        return xs;
    }

    public static function
    proposedSelectionDiffers(current:IPVec,
                             proposed:IPVec):Boolean {
        if(proposed.length != current.length) {
            return true;
        } else {
            for (var i:int = 0; i < current.length; i++) {
                if (ObjectUtil.compare(current.nth(i), proposed.nth(i)) !=0) {
                    return true;
                }
            }
        }
        return false;
    }

    public static function onlyOneSeriesSelected(selections:IPVec):Boolean {
      //trace('SeriesSelection.onlyOneSeriesSelected');
      if (selections.length == 0) return false;
      var dataSeries:String = "";
      for (var i:int=0; i<selections.length; i++) {
        var selection:SeriesSelection =
          selections.nth(i) as SeriesSelection;
        //trace('selection on ', selection.selectedSeries);
        if (dataSeries == "") {
          dataSeries = selection.selectedSeries;
        }
        if (dataSeries != selection.selectedSeries) {
          //trace('returning false');
          return false;
        }
      }
      //trace('returning true');
      return true;
    }

    public function
    isSelectedIndex(idx:int):Boolean {
        if (idx < this.startIndex) return false;
        if (idx > this.endIndex) return false;
        return true;
    }

    public function
    mapOver(f:Function, pts:IPVec, hdr:Object, getter:Function):Array {
        var data:Array = selectedData(pts, hdr, getter);
        var out:Array = new Array(data.length);
        var i:int=0;
        for each (var datum:Number in data) {
            out[i] = f(datum);
            i++;
        }
        return out;
    }

    public function
    reduceOver(f:Function, seed:*, pts:IPVec, hdr:Object, getter:Function):* {
        var data:Array = selectedData(pts, hdr, getter);
        var out:* = seed;
        for each (var datum:* in data) {
            out = f(out, datum);
        }
        return out;
    }

    public function
    seedlessReduceOver(f:Function, pts:IPVec, hdr:Object, getter:Function):* {
        var data:Array = selectedData(pts, hdr, getter);
        var out:Number;
        var first:Boolean = true;
        for each (var datum:* in data) {
            if (first) {
                out = datum;
                first = false;
            } else {
                out = f(out, datum);
            }
        }
        return out;
    }

    public function average(pts:IPVec, hdr:Object, getter:Function):Number {
        if (this.numberOfValues == 0) return NaN;
        var f:Function = function (acc:Object, x:Number):Object {
            return {sum: acc.sum + x, num: acc.num + 1.0};
        };
        var out:Object =
            reduceOver(f, {sum:0.0, num:0.0}, pts, hdr, getter) as Object;
        if (out.num == 0.0) return NaN;
        return out.sum / out.num;
    }

    public function sum(pts:IPVec, hdr:Object, getter:Function):Number {
      if (this.numberOfValues == 0) return 0.0;
      var f:Function = function (acc:Object, x:Number):Object {
        return {sum: acc.sum + x};
      };
      var out:Object = reduceOver(f, {sum:0.0}, pts, hdr, getter) as Object;
      return out.sum;
    }

    public function minimum(pts:IPVec, hdr:Object, getter:Function):Number {
        if (this.numberOfValues == 0) return NaN;
        var f:Function = function (acc:Number, x:Number):Number {
            if (x < acc) return x;
            return acc;
        };
        return seedlessReduceOver(f, pts, hdr, getter);
    }

    public function maximum(pts:IPVec, hdr:Object, getter:Function):Number {
        if (this.numberOfValues == 0) return NaN;
        var f:Function = function (acc:Number, x:Number):Number {
            if (x > acc) return x;
            return acc;
        };
        return seedlessReduceOver(f, pts, hdr, getter);
    }

    public function selectionFor(seriesName:String):SeriesSelection {
        return new SeriesSelection(
            this.dataPoints, seriesName, this.startIndex, this.numberOfValues);
    }

    public function
    toCellPositions(columns:IList):Vector.<CellPosition> {
      var colIdx:int = -1;
      var out:Vector.<CellPosition> = Vector.<CellPosition>([]);
      for (colIdx = 0; colIdx<columns.length; colIdx++) {
        var col:GridColumn = columns.getItemAt(colIdx) as GridColumn;
        if (col.dataField == this.selectedSeries) break;
      }
      if (colIdx == -1) return out;
      for each (var rowIdx:int in this.selectedRows) {
        out.push(new CellPosition(rowIdx, colIdx));
      }
      return out;
    }

    public function increaseSelection():SeriesSelection {
      return new SeriesSelection(
        this.dataPoints,
        this.selectedSeries,
        this.startIndex,
        this.numberOfValues + 1);
    }

    public static function simplifySelections(ss:IPVec):IPVec {
      var selsBySeries:Object = {};
      var newSS:Array = [];
      for (var selIdx:int=0; selIdx<ss.length; selIdx++) {
        var sel:SeriesSelection = ss.getItemAt(selIdx) as SeriesSelection;
        if (selsBySeries.hasOwnProperty(sel.selectedSeries)) {
          selsBySeries[sel.selectedSeries].push(sel);
        } else {
          selsBySeries[sel.selectedSeries] = [sel];
        }
      }
      for (var name:String in selsBySeries) {
        newSS = newSS.concat(
          consolidateSelectionsOnSameSeries(selsBySeries[name]));
      }
      return PersistentVector.create(newSS);
    }

    // all of ss must have same value for selectedSeries attribute
    public static function consolidateSelectionsOnSameSeries(ss:Array):Array {
      if (ss.length == 0) return ss;
      var newSS:Array = [];
      var mergedIdxs:Array = [];
      for (var pivotIdx:int=0; pivotIdx<ss.length; pivotIdx++) {
        if (ArrayUtils.contains(mergedIdxs, pivotIdx)) continue;
        var pivot:SeriesSelection = ss[pivotIdx] as SeriesSelection;
        for (var cmpIdx:int=pivotIdx+1; cmpIdx<ss.length; cmpIdx++) {
          if (ArrayUtils.contains(mergedIdxs, cmpIdx)) continue;
          var cmp:SeriesSelection = ss[cmpIdx] as SeriesSelection;
          if (pivot.startIndex == (cmp.endIndex + 1)) {
            pivot = new SeriesSelection(
              pivot.dataPoints, pivot.selectedSeries, cmp.startIndex,
              cmp.numberOfValues + pivot.numberOfValues);
            mergedIdxs.push(cmpIdx);
          } else if (cmp.startIndex == (pivot.endIndex + 1)) {
            pivot = new SeriesSelection(
              cmp.dataPoints, cmp.selectedSeries, pivot.startIndex,
              pivot.numberOfValues + cmp.numberOfValues);
            mergedIdxs.push(cmpIdx);
          }
        }
        newSS.push(pivot);
      }
      return newSS;
    }
}
}