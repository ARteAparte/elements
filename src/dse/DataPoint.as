/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package dse {
import elements.DocState;

import pdt.IPVec;

import plugins.PlugInfo;

import utils.MapUtils;
import utils.OptUtils;
import utils.Set;

public class DataPoint {
  public static const ERROR_STATE_NAMES_MUST_CONTAINT_GTE_ONE_VALUE:String =
    'ERROR! State names must contain at least 1 value';

  public static function valid(
    hdr:Object,
    vals:Object,
    cnstr:Function):Boolean
  {
    //trace('DataPoint.valid -- begin');
    if (Math.abs(cnstr(hdr, vals)) < 1e-3) {
      return true;
    }
    return false;
  }
  public static function settingError(
    header:Object,
    values:Object,
    derivationFuncs:Object,
    settings:Object,
    scaleFactors:Object):Number
  {
    //trace('DataPoint.settingError -- begin');
    var err:Number = 0.0;
    var val:Number;
    var scaleFactor:Number;
    var df:Function;
    for each (var settingName:String in MapUtils.keys(settings)) {
      if (values.hasOwnProperty(settingName)) {
        val = values[settingName];
      } else if (derivationFuncs.hasOwnProperty(settingName)) {
        df = derivationFuncs[settingName] as Function;
        val = df(header, values);
      } else {
        throw new Error('Unknown setting, ' + settingName);
      }
      if (scaleFactors.hasOwnProperty(settingName)) {
        scaleFactor = scaleFactors[settingName];
      } else {
        scaleFactor = 1.0;
      }
      err += scaleFactor * Math.abs(settings[settingName] - val);
    }
    return err;
  }
  // update
  // - header :: Object -- the DataSet header.
  //             Map String (Number|String)
  // - allValues :: Object -- collection of state-point values
  //                Map SeriesName Number
  // - constrFunc :: Function -- the constraint function:
  //   constrFunc :: header of Object x values of Object -> Number
  // - derivationFuncs :: Object
  // Map SeriesName DerivationFunc
  // DerivationFunc :: header of Object x values of Object -> Number
  // derivation function returns the derived value of given series name
  // - settings :: Object -- Map SeriesName Number, settings is the desired
  // values for a new data point
  // - mins :: Object -- Map SeriesName MinValue, the minimums for the given
  // series values.
  // - maxs :: Object -- Map SeriesName MaxValue, the maximums for the given
  // series values.
  // - stateNames :: Object -- object being used as a mathematical set to
  // hold the names of the relevant state values in allValues
  // RETURNS:
  // returns the updated values for this data point
  // Notes:
  // targetSettings are the settings that are not values
  // freeValues are the values we can change
  public static function update(
    header:Object,
    allValues:Object,
    constrFunc:Function,
    derivationFuncs:Object,
    settings:Object,
    mins:Object,
    maxs:Object,
    stateNames:Object):Object
  {
    //trace('DataPoint.update -- begin');
    checkStateNames(stateNames);
    var keys:Function = MapUtils.keys;
    var states:Object = MapUtils.onlyTheseKeys(
      allValues, keys(stateNames));
    if (!valid(header, states, constrFunc)) {
      throw new Error('Cannot update values');
    }
    var settingNames:Array = keys(settings);
    var numSettings:int = settingNames.length;
    var numSettingsThatAreStates:int = Set.numberOfMembers(
      settingNames, states);
    var numSettingsNotStates:int = numSettings - numSettingsThatAreStates;
    // the purpose of the next section of code is to update the values
    var updatedValues:Object;
    var updatedValuesTmp:Object;
    if (numSettingsNotStates == 0) {
      updatedValues = settings;
    } else {
      updatedValues = _updateMultipleTargetSettings(header, allValues,
        constrFunc, settings, derivationFuncs, mins, maxs, stateNames);
    }
    /*if (true) {
      trace('START DataPoint.update');
      MapUtils.traceObject(header, 'header');
      MapUtils.traceObject(allValues, 'allValues');
      MapUtils.traceObject(stateNames, 'stateNames');
      MapUtils.traceObject(settings, 'settings');
      MapUtils.traceObject(states, 'states');
      MapUtils.traceObject(updatedValues, 'updatedValues');
      trace('numSettings =', numSettings);
      trace('numSettingsThatAreStates =', numSettingsThatAreStates);
      trace('numSettingsNotStates =', numSettingsNotStates);
      trace('END DataPoint.update');
    }*/
    return _updateValuesWithinConstraints(
      header, allValues, constrFunc, updatedValues);
  }
  public static function checkStateNames(stateNames:Object):void
  {
    //trace('DataPoint.checkStateNames -- begin');
    if ((stateNames == null) ||
      (MapUtils.keys(stateNames).length == 0)) {
      throw new Error(ERROR_STATE_NAMES_MUST_CONTAINT_GTE_ONE_VALUE);
    }
  }
  // header - the header for this data point
  // allValues - the current values of the data point
  // cf:Function - constraint function
  // (header:Object, point:Object) -> error:Number
  // where header and point are objects and error is a number >= 0.0
  // settings - the desired settings to adjust
  // dfs:Object - a map of derivation functions with key being the metric name
  // and value a function (header:Object, point:Object) -> metricValue:Number
  // mins:Object - the minimums for relevant metrics
  // maxs:Object - the maximums for relevant metrics
  // stateNames - set of stateNames
  public static function _updateMultipleTargetSettings(
    header:Object,
    allValues:Object,
    cf:Function,
    settings:Object,
    dfs:Object,
    mins:Object,
    maxs:Object,
    stateNames:Object):Object
  {
    //trace('DataPoint._updateMultipleTargetSettings -- begin');
    var values:Object = MapUtils.onlyTheseKeys(
      allValues, MapUtils.keys(stateNames));
    var targetSettings:Array = MapUtils.keys(
      Set.difference(settings, values));
    var freeValues:Array = MapUtils.keys(
      Set.difference(values, settings));
    var wrappedConFunc:Function = function(pt:Object):Number {
      return cf(header, pt);
    };
    var updatedValues:Object;
    if ((targetSettings.length==1) && (freeValues.length==1)) {
      // TODO: targetSettings.length==1 overconstrains this; we only
      // need to ensure freeValues.length==1; we can deal with fitting
      // multiple targetSettings if we use an error function to wrap
      // the "setting error" in.
      updatedValues = _updateForOneFreeValue(freeValues[0],
        targetSettings, header, values, settings, dfs, cf, mins, maxs);
    } else {
      updatedValues = _updateForMultipleFreeValues(freeValues, header,
        values, settings, cf, dfs, mins, maxs);
    }
    /*if (true) {
      trace('START DataPoint._updateMultipleTargetSettings');
      MapUtils.traceObject(values, 'values');
      trace('targetSettings =', targetSettings);
      trace('freeValues =', freeValues);
      MapUtils.traceObject(updatedValues, 'updatedValues');
      trace('END DataPoint._updateMultipleTargetSettings');
    }*/
    return updatedValues;
  }
  // We have two or more "free" reading set values now.
  // I'd like to use multivariate optimization but not ready
  // yet SO... we'll resort to a brute force solution.
  public static function _updateForMultipleFreeValues(
    freeValues:Array,
    header:Object,
    values:Object,
    settings:Object,
    cf:Function,
    dfs:Object,
    mins:Object,
    maxs:Object):Object
  {
    //trace('DataPoint._updateForMultipleFreeValues -- begin');
    var numLevels:int;
    if (freeValues.length == 2) {
      numLevels = 100;
    } else {
      numLevels = 30;
    }
    /*if (true) {
      trace('START DataPoint._updateForMultipleFreeValues');
      trace('FINISH DataPoint._updateForMultipleFreeValues');
    }*/
    return OptUtils.bruteForceSearch(
      values, settings, numLevels, header,
      cf, dfs, mins, maxs);
  }
  // OK, only one of the settings is not in the reading set
  // and only one state variable is 'free'
  // the set of states user want's held constant overlaps with
  // 2 of our internal states. i.e., if A is the set of user
  // metrics to set the Psychrometric state with and V is the
  // set of our internal reading set values then
  // "count (A ∩ V) = 2" so one metric does not overlap with
  // our internal set of State Points and ∴ 1 dimensional root
  // finding needed.
  //
  // Objective: find the "zero" in the error function for our
  // target metric. In other words, vary the "free" internal
  // state metric until the difference between the derived
  // value for the target metric and the desired setting for
  // that target metric are zero.
  //
  // the target metric is "A \ V" (i.e., "set difference" or
  // the member of A not in V). The internal state metric to
  // treat as a variable is "V \ A" (i.e., member of V not
  // in A).
  public static function _updateForOneFreeValue(
    freeValue:String,
    targetSettings:Array,
    header:Object,
    values:Object,
    settings:Object,
    dfs:Object,
    cf:Function,
    mins:Object,
    maxs:Object):Object
  {
    //trace('DataPoint._updateForOneFreeValue -- begin');
    var dpndtSetting:String = targetSettings[0];
    var f:Function = dfs[dpndtSetting];
    var wrappedDepFunc:Function = function(pt:Object):Number {
      return f(header, pt);
    };
    var updatedValuesTmp:Object = MapUtils.updateWithoutAddingKeys(
      values, settings);
    var updatedValues:Object = OptUtils.findRoot(
      updatedValuesTmp, settings, freeValue, dpndtSetting,
      dfs[dpndtSetting], cf,
      header, mins[freeValue], maxs[freeValue]);
    //var origUpdatedValues:Object = MapUtils.copyObject(updatedValues);
    //var origUpdatedValuesTmp:Object = MapUtils.copyObject(updatedValuesTmp);
    //var hitConstraints:Boolean = false;
    if (cf(header, updatedValues) > 0.0) {
      //hitConstraints = true;
      updatedValuesTmp =
        _updateValuesWithinConstraints(
          header, values, cf, settings);
      updatedValues = OptUtils.findRoot(
        updatedValuesTmp, settings, freeValue, dpndtSetting,
        dfs[dpndtSetting], cf,
        header, mins[freeValue], maxs[freeValue]);
    }
    /*if (true) {
      trace('START DataPoint._updateForOneFreeValue');
      MapUtils.traceObject(values, 'values');
      MapUtils.traceObject(settings, 'settings');
      MapUtils.traceObject(mins, 'mins');
      MapUtils.traceObject(maxs, 'maxs');
      MapUtils.traceObject(updatedValuesTmp, 'updatedValuesTmp');
      MapUtils.traceObject(updatedValues, 'updatedValues');
      MapUtils.traceObject(origUpdatedValues, 'origUpdatedValues');
      MapUtils.traceObject(origUpdatedValuesTmp, 'origUpdatedValuesTmp');
      trace('hit constraints?', hitConstraints);
      trace('END DataPoint._updateForOneFreeValue');
    }*/
    return updatedValues;
  }
  // Update values attribute while staying valid.
  // This method creates a new values object, V2, by copying the existing
  // values object, V1. The attributes of V2 that are also in settings are then
  // explicitly set to the settings values (ignoring other settings for now).
  // The method then checks if V2 is valid. If it is, V2 is returned. If not,
  // we use the OptUtils.findGreatestGoodBound function to scale from a good
  // known point (V1) to the objective (V2), getting as close to V2 as possible.
  //
  // NOTE: This is an internal method (note the leading underscore) but we
  // expose it for testing purposes. Also, note: this algorithm is sometimes
  // called prior to optimization and, as such, may lead to incorrect results.
  // For example, if we have a value set V1 with params a=a1, b=b1, and c=c1
  // and have settings to change a from some a1 to a2, the point a2,b1,c1 may
  // be invalid. HOWEVER, if b and/or c are free (presumably a is not since
  // we're setting it here) in a subsequent optimization, it may be possible
  // to meet some new valid state a2, b*, c* where b* and/or c* are the
  // result of an optimization.
  public static function _updateValuesWithinConstraints(
    header:Object,
    values:Object,
    constrFunc:Function,
    settings:Object):Object
  {
    //trace('DataPoint._updateValuesWithinConstraints -- begin');
    var updatedValues:Object = MapUtils.updateWithoutAddingKeys(
      values, settings);
    var err:Number = constrFunc(header, updatedValues);
    if (err > 0.0) {
      var fCon0:Function = function(x:Number):Number {
        var guessVals:Object =
          OptUtils.scaleByX(values, updatedValues, x);
        return constrFunc(header, guessVals);
      };
      var xOpt:Number = OptUtils.findGreatestGoodBound(fCon0);
      updatedValues = OptUtils.scaleByX(values, updatedValues, xOpt);
    }
    return updatedValues;
  }
  public static function makeGetter(
    seriesMetaMap:Object,
    dataPoint:Object,
    dataSetHeader:Object):Function
  {
    return function(seriesName:String, dflt:*=null):* {
      try {
        if (seriesMetaMap.hasOwnProperty(seriesName)) {
          var metaData:MetaData = seriesMetaMap[seriesName] as MetaData;
          if (metaData.getter != null) {
            var getter:Function = metaData.getter;
          } else {
            if (dflt != null) return dflt;
          }
        } else {
          if (dflt != null) return dflt;
        }
      } catch (e:Error) {
        trace('attempt to find seriesName, ' + seriesName + ' failed');
        if (dflt != null) return dflt;
      }
      try {
        var result:* = getter(dataSetHeader, dataPoint);
        if (result == undefined) {
          if (dflt != null) return dflt;
        }
      } catch (e:Error) {
        trace('---');
        trace('error trying to run getter for ' + seriesName);
        trace('error:', e);
        MapUtils.traceObject(dataSetHeader, 'dataSetHeader');
        MapUtils.traceObject(dataPoint, 'dataPoint');
        trace('--- END OF ERROR MESSAGE');
        if (dflt != null) return dflt;
      }
      return result;
    }
  }
  // _makeSettings helper function for updateWithSettingsAndDocState
  // dataField -- the dataField
  // dataValue
  // mdMap  series meta-data map
  // dp -- data point
  // settingSet the mathematical set of settings that should be tracked for this
  // update
  // hdr a map of header values
  public static function _makeSettings(
    dataField:String,
    dataValue:Number,
    mdMap:Object,
    dp:Object,
    settingSet:Array,
    hdr:Object):Object
  {
    //trace('DataPoint._makeSettings -- begin');
    var settings:Object = new Object();
    for each (var attr:String in settingSet) {
      var getter:Function = (mdMap[attr] as MetaData).getter;
      settings[attr] = getter(hdr, dp);
    }
    settings[dataField] = dataValue;
    return settings;
  }
  public static function updateMultiplePointsWithSettingsAndDocState(
    dataField:String,
    dataValues:Array,
    ds:DocState,
    pointIndices:Array,
    settingSetIdx:int):DocState
  {
    //trace('DataPoint.updateMultiplePointsWithSettingsAndDocState -- start');
    var dataSet:DataSet = ds.dataSet;
    var smd:Object = dataSet.seriesMetaData;
    var md:MetaData = dataSet.metaDataFor(dataField);
    var hdr:Object = dataSet.header;
    var dfs:Object = PlugInfo.knownDerivationFuncs();
    var mins:Object = PlugInfo.knownMins();
    var maxs:Object = PlugInfo.knownMaxs();
    var newPoints:IPVec = dataSet.points;
    var settings:Object;
    for (var idx:int = 0; idx < dataValues.length; idx++)
    {
      var pointIndex:int = pointIndices[idx];
      var dataValue:Number = dataValues[idx];
      var currentPoint:Object = newPoints.getItemAt(pointIndex);
      if (md.canUpdate) {
        newPoints = newPoints.assocN(
          pointIndex,
          md.update(hdr, currentPoint, dataValue, settingSetIdx));
      } else {
        if (settingSetIdx >= md.settingSets.length) {
          // assume independent reading
          settings = new Object();
          settings[dataField] = dataValue;
        } else {
          settings = _makeSettings(
            dataField,
            dataValue,
            smd,
            currentPoint,
            md.settingSetN(settingSetIdx),
            hdr);
        }
        var currentValue:Number = md.getter(hdr, currentPoint);
        if (currentValue != dataValue) {
          newPoints = newPoints.assocN(
            pointIndex,
            update(
              hdr,
              currentPoint,
              md.constraintFunc,
              dfs,
              settings,
              mins,
              maxs,
              md.stateNames));
        }
      }
    }
    var newDataSet:DataSet = DataSetBuilder.fromDataSet(dataSet)
      .withPoints(newPoints).build();
    //trace('DataPoint.updateMultiplePoints ... -- newDataSet computed');
    return ds.settingDataSetTo(newDataSet);
  }
  public static function updatePointWithSettingsAndDocState(
    dataField:String,
    dataValue:Number,
    ds:DocState,
    pointIndex:int,
    settingSetIdx:int):Object
  {
    //trace('DataPoint.updatePointWithSettingsAndDocState -- begin');
    //trace('DataPoint.updatePoint... :: dataValue', dataValue);
    //trace('DataPoint.updatePoint... :: pointIndex', pointIndex);
    var dataSet:DataSet = ds.dataSet;
    var smd:Object = dataSet.seriesMetaData;
    var md:MetaData = dataSet.metaDataFor(dataField);
    var hdr:Object = dataSet.header;
    var currentPoint:Object = dataSet.pointN(pointIndex);
    var currentValue:Number = md.getter(hdr, currentPoint);
    if (currentValue == dataValue) {
      return currentPoint;
    }
    var dfs:Object = PlugInfo.knownDerivationFuncs();
    var mins:Object = PlugInfo.knownMins();
    var maxs:Object = PlugInfo.knownMaxs();
    if (settingSetIdx >= md.settingSets.length) {
      // assume independent reading
      settings = new Object();
      settings[dataField] = dataValue;
    } else {
      var settingSet:Array = md.settingSetN(settingSetIdx);
      var settings:Object = _makeSettings(dataField, dataValue,
        smd, currentPoint, settingSet, hdr);
    }
    /*if (false) {
      trace('updateWithSettingsAndDocState');
      trace('meta data for', md.name);
      trace('settingSetIdx =', settingSetIdx);
      trace('current value =', currentValue);
      trace('data value =', dataValue);
      MapUtils.traceObject(hdr, 'hdr');
      MapUtils.traceObject(currentPoint, 'currentPoint');
      //MapUtils.traceObject(dfs, 'dfs');
      MapUtils.traceObject(settings, 'settings');
      MapUtils.traceObject(mins, 'mins');
      MapUtils.traceObject(maxs, 'maxs');
      MapUtils.traceObject(md.stateNames, 'md.stateNames');
    }*/
    return update(hdr, currentPoint, md.constraintFunc, dfs, settings,
      mins, maxs, md.stateNames);
  }
  // updateDocStateWithSettingsAndDocState --
  // an update built around making a single change to a single field and
  // holding other propreties constant per the settingSet values.
  //
  // TODO:
  // * change settingSet to settingSetIdx
  // * REUSE update!
  // - if update isn't doing the right thing, fix update!
  // * this function should be about 20 lines long!
  // * change name to 'updateWithDocStateAndSettingSetIndex
  public static function updateDocStateWithSettingsAndDocState(
    dataField:String,
    dataValue:Number,
    ds:DocState,
    pointIndex:int,
    settingSetIdx:int):DocState
  {
    //trace('DataPoint.updateDocStateWithSettingsAndDocState -- begin');
    var newPoint:Object = updatePointWithSettingsAndDocState(
      dataField, dataValue, ds, pointIndex, settingSetIdx);
    var dataSet:DataSet = ds.dataSet;
    var pts:IPVec = dataSet.points;
    var newPts:IPVec = pts.assocN(pointIndex, newPoint);
    var newDataSet:DataSet = DataSetBuilder.fromDataSet(dataSet)
      .withPoints(newPts).build();
    return ds.settingDataSetTo(newDataSet);
  }
}
}
