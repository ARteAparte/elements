/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package dse {
import mx.collections.ArrayCollection;
import mx.collections.IList;

import pdt.IPVec;

import utils.MapUtils;

public class DataSet {
  public static const ERROR_UNKNOWN_META_DATA_KEY:String =
    'Error! Unknown meta-data key';
  private var _header:Object;
  private var _points:IPVec;
  private var _seriesMetas:Object;
  private var _headerMetas:Object;

  public function DataSet(dsb:DataSetBuilder) {
    _header = dsb.header;
    _points = dsb.points;
    _seriesMetas = dsb.seriesMetaData;
    _headerMetas = dsb.headerMetaData;
  }
  public function get header():Object {
    return _header as Object;
  }
  public function settingHeaderTo(h:Object):DataSet {
    return DataSetBuilder.fromDataSet(this).withHeader(h).build();
  }
  public function get points():IPVec {
    return _points;
  }
  public function get pointsAsArray():Array {
    return _points.toArray();
  }
  public function get pointsAsIList():IList {
    return new ArrayCollection(_points.toArray()) as IList;
  }
  public function pointN(n:int):Object {
    return this.points.getItemAt(n);
  }
  public function settingPointsTo(ps:IPVec):DataSet {
    return DataSetBuilder.fromDataSet(this).withPoints(ps).build();
  }
  public function settingNthPointTo(n:int, pt:Object):DataSet {
    return DataSetBuilder.fromDataSet(this)
      .withPoints(_points.assocN(n, pt))
      .build();
  }
  public function get seriesMetaData():Object{
    return _seriesMetas;
  }
  public function settingSeriesMetaDataTo(smd:Object):DataSet{
    return DataSetBuilder.fromDataSet(this).withSeriesMetaData(smd).build();
  }
  public function get headerMetaData():Object{
    return _headerMetas;
  }
  public function settingHeaderMetaDataTo(hmd:Object):DataSet{
    return DataSetBuilder.fromDataSet(this).withHeaderMetaData(hmd).build();
  }
  public function get seriesMetricNames():Array {
    return MapUtils.keys(this.seriesMetaData);
  }
  public function get numericMetricNames():Array {
    var smd:Object = this.seriesMetaData;
    var attrs:Array = MapUtils.keys(smd);
    var names:Array = [];
    for each (var attr:String in attrs) {
      var md:MetaData = smd[attr] as MetaData;
      if (md.dataType == MetaData.DATA_TYPE__NUMBER) {
        names.push(md.name);
      }
    }
    return names;
  }
  public function get numericMetricFullNames():Array {
    var smd:Object = this.seriesMetaData;
    var attrs:Array = MapUtils.keys(smd);
    var names:Array = [];
    for each (var attr:String in attrs) {
      var md:MetaData = smd[attr] as MetaData;
      if (md.dataType == MetaData.DATA_TYPE__NUMBER) {
        names.push(md.fullName);
      }
    }
    return names;
  }
  public function seriesValue(idx:int, name:String):Number {
    var md:MetaData = metaDataFor(name);
    var hdr:Object = this.header;
    var pt:Object = this.pointN(idx);
    return md.getter(hdr, pt) as Number;
  }
  public function metaDataFor(name:String):MetaData {
    var smd:Object = this.seriesMetaData;
    if (smd.hasOwnProperty(name)) {
      return smd[name] as MetaData;
    }
    var msg:String = 'Warning! request for meta data ' + name +
      ' returned null';
    trace(msg);
    throw new Error(ERROR_UNKNOWN_META_DATA_KEY);
  }
  /* settingSetMap
  returns an object (map) with keys by metric name and values of
  arrays of arrays of String (i.e., array of array of metric names).*/
  public function get settingSetMap():Object {
    var smd:Object = this.seriesMetaData;
    var attrs:Array = this.seriesMetricNames;
    var map:Object = {};
    for each (var attr:String in attrs) {
      map[attr] = (smd[attr] as MetaData).settingSets;
    }
    return map;
  }
  /* settingSetStringMap
  returns an object (map) with keys by metric name and values of
  arrays of setting set strings. */
  public function get settingSetStringMap():Object {
    var ssm:Object = this.settingSetMap;
    var mdMap:Object = this.seriesMetaData;
    var sssm:Object = {};
    for each (var attr:String in MapUtils.keys(ssm)) {
      var md:MetaData = mdMap[attr] as MetaData;
      if (md.settingSetTags == null) {
        var ssStrs:Array = [];
        for each (var settingSet:Array in (ssm[attr] as Array)) {
          var tmpArray:Array = [];
          for each (var item:String in settingSet) {
            tmpArray.push((mdMap[item] as MetaData).fullName);
          }
          ssStrs.push(tmpArray.join(', '));
        }
        sssm[attr] = ssStrs;
      } else {
        sssm[attr] = md.settingSetTags;
      }
    }
    return sssm;
  }
  public function headerValueFor(name:String):String {
    if (this.header.hasOwnProperty(name)){
      return this.header[name] as String;
    }
    return "";
  }
}
}