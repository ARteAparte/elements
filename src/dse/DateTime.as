/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package dse {
import plugins.DateAndTime;

import utils.FormatUtils;
import utils.MapUtils;

public class
DateTime {
    public static const DATE_TIME:String = 'dateTime';
    public static const MONTH_NAMES:Array = [
        "January", "February", "March",
        "April", "May", "June",
        "July", "August", "September",
        "October", "November", "December"];
    public static const JANUARY:int = 1;
    public static const JAN:int = JANUARY;
    public static const FEBRUARY:int = 2;
    public static const FEB:int = FEBRUARY;
    public static const MARCH:int = 3;
    public static const MAR:int = MARCH;
    public static const APRIL:int = 4;
    public static const APR:int = APRIL;
    public static const MAY:int = 5;
    public static const JUNE:int = 6;
    public static const JUN:int = JUNE;
    public static const JULY:int = 7;
    public static const JUL:int = JULY;
    public static const AUGUST:int = 8;
    public static const AUG:int = AUGUST;
    public static const SEPTEMBER:int = 9;
    public static const SEP:int = SEPTEMBER;
    public static const OCTOBER:int = 10;
    public static const OCT:int = OCTOBER;
    public static const NOVEMBER:int = 11;
    public static const NOV:int = NOVEMBER;
    public static const DECEMBER:int = 12;
    public static const DEC:int = DECEMBER;
    public static const DAYS_PER_MONTH:Array =
        [31,28,31,30,31,30,31,31,30,31,30,31];
    public static const CUMULATIVE_DAYS__MONTH:Array =
        [0,31,59,90,120,151,181,212,243,273,304,334,365];
    public static const HOURS_PER_DAY:int = 24;

    public var year:int;
    public var month:int;
    public var dayOfMonth:int;
    public var hourOfDay:int;
    public var minuteOfHour:int;
    public var secondOfMinute:int;

    public function DateTime(
      year:int,
      month:int,
      dayOfMonth:int,
      hourOfDay:int,
      minuteOfHour:int=0,
      secondOfMinute:int=0)
    {
      if ((month < 1) || (month > 12)) {
        throw new Error("month must be from 1 to 12 inclusive");
      }
      if ((dayOfMonth < 1) || (dayOfMonth > 31)) {
        throw new Error("dayOfMonth must be between 1 and 31 inclusive");
      }
      if ((hourOfDay < 0) || (hourOfDay > 23)) {
        throw new Error(
          "hourOfDay must be between 0 (midnight) " +
          "and 23 (11 PM) inclusive");
      }
      if ((minuteOfHour < 0) || (minuteOfHour > 59)) {
        throw new Error("minutes must be from 0 to 59 inclusive");
      }
      if ((secondOfMinute < 0) || (secondOfMinute > 59)) {
        throw new Error("seconds must be from 0 to 59 inclusive");
      }
      this.year = year;
      this.month = month;
      this.dayOfMonth = dayOfMonth;
      this.hourOfDay = hourOfDay;
      this.minuteOfHour = minuteOfHour;
      this.secondOfMinute = secondOfMinute;
    }

    public function get
    dayOfYear():int {
        return CUMULATIVE_DAYS__MONTH[this.month - 1] + this.dayOfMonth;
    }

    public function
    compactString():String {
        var yr:String = this.year.toString();
        var m:String = pad02(this.month.toString());
        var d:String = pad02(this.dayOfMonth.toString());
        var hr:String = pad02(this.hourOfDay.toString());
        var min:String = pad02(this.minuteOfHour.toString());
        var sec:String = pad02(this.secondOfMinute.toString());
        return yr + "/" + m + "/" + d + " @ " + hr + ":" + min + ":" + sec;
    }

    private static function
    pad02(s:String):String {
        return FormatUtils.fixStringLength(2, s, '0', FormatUtils.PAD_LEFT);
    }

    public static function
    parseCompactString(str:String):DateTime {
        var m:Array = str.match(/\d+/g);
        if (m.length != 5) {
            throw new Error(
                "DateTime :: string does not consist of 5 time components: "
                + str);
        }
        return new DateTime(
            Number(m[0]), Number(m[1]), Number(m[2]), Number(m[3]),
            Number(m[4]));
    }

    public function
    toString():String {
        return MONTH_NAMES[this.month - 1] + " " +
            pad02(this.dayOfMonth.toString()) + ", " +
            this.year.toString() + " @ " + pad02(this.hourOfDay.toString()) +
            ":" + pad02(this.minuteOfHour.toString()) + ":" +
            pad02(this.secondOfMinute.toString());
    }

    public function
    secondsSinceStartOfGivenYear(refYear:int):int {
        var days:int = (this.year - refYear) * 365 + (this.dayOfYear - 1);
        var seconds:int =
            days * 24 * 60 * 60 + this.hourOfDay * 60 * 60
            + this.minuteOfHour * 60 + this.secondOfMinute;
        return seconds;
    }

    // assumes not a leap-year
    public static function
    hourOfYearToDateTime(hourOfYear:int, year:int,
                         minuteOfHour:int=0, secondOfMinute:int=0):DateTime {
        var dayOfYear:int = int(Math.floor(hourOfYear / HOURS_PER_DAY));
        var hourOfDay:int = hourOfYear % HOURS_PER_DAY;
        if (hourOfYear >= 8760) {
            throw new Error('hour of year is greater than 365 days of time');
        }
        for (var monthIdx:int = 1; monthIdx < 13; monthIdx++) {
            if ((dayOfYear >= CUMULATIVE_DAYS__MONTH[monthIdx-1]) &&
                (dayOfYear < CUMULATIVE_DAYS__MONTH[monthIdx])) {
                break;
            }
        }
        return new DateTime(
            year, monthIdx, dayOfYear - CUMULATIVE_DAYS__MONTH[monthIdx-1] + 1,
            hourOfDay, minuteOfHour, secondOfMinute);
    }

    public function copy():DateTime {
        return new DateTime(
            this.year, this.month, this.dayOfMonth, this.hourOfDay,
            this.minuteOfHour, this.secondOfMinute);
    }

    public static function
    objectToDateTime(dt:Object):DateTime {
        var year:int = dt[DateAndTime.YEAR] as int;
        var month:int = MapUtils.atKeyOrAlt(dt, DateAndTime.MONTH, 1);
        var day:int = MapUtils.atKeyOrAlt(dt, DateAndTime.DAY, 1);
        var hour:int = MapUtils.atKeyOrAlt(dt, DateAndTime.HOUR, 0);
        var minute:int = MapUtils.atKeyOrAlt(dt, DateAndTime.MINUTE, 0);
        var second:int = MapUtils.atKeyOrAlt(dt, DateAndTime.SECOND, 0);
        return new DateTime(year, month, day, hour, minute, second);
    }
}
}