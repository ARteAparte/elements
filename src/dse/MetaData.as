/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package dse {

import elements.DataDisplayOptions;

import units.NoUnits;

import utils.MapUtils;

public class MetaData {
  public static const DATA_TYPE__STRING:String = 'stringType';
  public static const DATA_TYPE__NUMBER:String = 'numberType';

  public var name:String;
  public var fullName:String;
  public var baseUnits:String;
  public var quantityOfMeasure:String;
  /* The getter function is a function with signature:
  header of Object x values of Object -> *
  The return value of a getter depends on the dataType of the datum:
  it could be a number or string. Values are the statepoint
  data for the current point. Header is the dataset header. */
  public var getter:Function;
  // dataType = one of {DATA_TYPE__STRING | DATA_TYPE__NUMBER}
  public var dataType:String;
  /* A setting set is a Set of metrics that need to have defined values
  in order for an update operation to work. The settingSets array is an
  array of array of string with the strings being metric name keys. The
  out array is the possible setting sets that can be used when updating this
  metric. An empty list for a settingSet implies that this data is independent
  and has no relation to other metrics and, as such, can be freely updated
  without worrying about other metrics. Note that an independent metric
  may still have a constraint function, however! Also, an independent
  metric may still have derived readings that reference it. Note:
  all setting sets include the metric in question implicitly so you
  don't need to specify it in the setting set... It would make no sense
  to select a metric to update/edit and not have that be involved in the
  update */
  public var settingSets:Array;
  /* This is human readable text for the setting sets. This allows for
  different human representation of setting set names in pulldowns, etc.
  */
  public var settingSetTags:Array;
  /* the constraint function for this data. The funciton must have
  signature:
  header of Object x values of Object -> Number
  where if Number == 0.0
  then data is valid
  else data is NOT valid */
  public var constraintFunc:Function;
  /* The stateNames field carries an Object that is used as a mathematical
  set. The keys of the object are the only useful data, the values
  are arbitrary and meaningless. The state names give the names of
  other data that this data series is in relation with. For example,
  moist air density would list dry-bulb-temperature, wet bulb temperature,
  and pressure as it's state point since it is in a relation with these three
  variables via psychrometric physics. Similarly, dry bulb temperature would
  also list dry bulb temperature, wet bulb temperature, and pressure because
  those are the statepoints for ALL psychrometric variables. */
  public var stateNames:Object;
  /* Whether or not this value can be updated */
  // TODO: move to DataDisplayOptions?
  public var editable:Boolean;
  public var availableUnits:Array;
  /* The default scaling value for this metric */
  public var defaultScale:Number;
  // updater is a function(header:Object, point:Object, newValue:Number, settingSetIdx:int):Object
  public var updater:Function;

  public function
  MetaData(
      name:String,
      fullName:String=null,
      baseUnits:String=null,
      quantityOfMeasure:String=null,
      getter:Function=null,
      dataType:String=DATA_TYPE__NUMBER,
      settingSets:Array=null,
      constraintFunc:Function=null,
      stateNames:Object=null,
      editable:Boolean=false,
      availableUnits:Array=null,
      defaultScale:Number=NaN,
      settingSetTags:Array=null,
      updater:Function=null)
  {
    this.name = name;
    if (fullName == null) {
      this.fullName = name;
    } else {
      this.fullName = fullName;
    }
    if (baseUnits == null) {
      this.baseUnits = NoUnits.NONE;
    } else {
      this.baseUnits = baseUnits;
    }
    if (quantityOfMeasure == null) {
      this.quantityOfMeasure = NoUnits.QUANTITY_OF_MEASURE;
    } else {
      this.quantityOfMeasure = quantityOfMeasure;
    }
    if (getter == null) {
      this.getter = makeValueGetter(name);
    } else {
      this.getter = getter;
    }
    this.dataType = dataType;
    if (settingSets == null) {
      this.settingSets = [];
    } else {
      this.settingSets = settingSets;
    }
    if (settingSetTags == null) {
      this.settingSetTags = null;
    } else {
      this.settingSetTags = settingSetTags;
    }
    if (constraintFunc == null) {
      this.constraintFunc = function(hdr:Object, vals:Object):Number
      {
        return 0.0;
      };
    } else {
      this.constraintFunc = constraintFunc;
    }
    if ((stateNames == null) ||
      (MapUtils.keys(stateNames).length == 0)) {
      // state names cannot be empty. Assume we include the current
      // metric as a state.
      this.stateNames = {};
      this.stateNames[name] = true;
    } else {
      this.stateNames = stateNames;
    }
    this.editable = editable;
    if (availableUnits == null) {
      this.availableUnits = [];
    }
    if (isNaN(defaultScale)) {
      this.defaultScale = DataDisplayOptions.DEFAULT_SCALE;
    } else {
      this.defaultScale = defaultScale;
    }
    this.updater = updater;
  }
  // returns the nth settingSet as an array (of string)
  public function settingSetN(n:int):Array {
    return this.settingSets[n] as Array;
  }
  public function
  toString():String {
    return this.fullName;
  }

  public static function
  makeValueGetter(seriesName:String):Function {
    return function(header:Object, values:Object):* {
      return values[seriesName];
    };
  }

  public static function
  makeValueGetterWithDefault(seriesName:String, dflt:*):Function {
    return function(header:Object, values:Object):* {
      if (values.hasOwnProperty(seriesName)) {
        return values[seriesName];
      }
      return dflt;
    };
  }

  public function get canUpdate():Boolean
  {
    return (updater != null);
  }

  // Call to a custom update function to short circuit the normal update path
  public function update(
    header:Object,
    point:Object,
    newValue:Number,
    settingSetIdx:int):Object
  {
    if (newValue == point[this.name]) {
      return point;
    }
    return updater(header, point, newValue, settingSetIdx);
  }
}
}