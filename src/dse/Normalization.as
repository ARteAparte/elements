/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package dse
{
import utils.MapUtils;
import utils.Set;

public class Normalization
{
    public static function
    makeScaleFactors(mins:Object, maxs:Object):Object
    {
        var attrs:Array = MapUtils.keys(Set.union(mins, maxs));
        var scaleFactors:Object = new Object();
        for each (var attr:String in attrs)
        {
            scaleFactors[attr] = 100.0 / (maxs[attr] - mins[attr]);
        }
        return scaleFactors;
    }
}
}