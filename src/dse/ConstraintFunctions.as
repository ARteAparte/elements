/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package dse {
public class ConstraintFunctions {
  public static function makeRangeConstraintFunction(
    minKeys:Array,
    maxKeys:Array,
    mins:Object,
    maxs:Object,
    scaleFactors:Object):Function
  {
    return function(hdr:Object, vals:Object):Number {
      var key:String;
      var scale:Number;
      var err:Number = 0.0;
      for each (key in minKeys) {
        if (!vals.hasOwnProperty(key)) continue;
        if (vals[key] < mins[key]) {
          if (scaleFactors.hasOwnProperty(key)) {
            scale = scaleFactors[key];
          } else {
            scale = 1.0;
          }
          err += scale * Math.abs(mins[key] - vals[key]);
        }
      }
      for each (key in maxKeys) {
        if (!vals.hasOwnProperty(key)) continue;
        if (vals[key] > maxs[key]) {
          if (scaleFactors.hasOwnProperty(key)) {
            scale = scaleFactors[key];
          } else {
            scale = 1.0;
          }
          err += scale * Math.abs(vals[key] - maxs[key]);
        }
      }
      return err;
    }
  }
  public static function makeAlwaysGoodConstraintFunction():Function {
    return function(hdr:Object, vals:Object):Number {
      return 0.0;
    }
  }
}
}