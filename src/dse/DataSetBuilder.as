/*
Copyright (C) 2013-2015 Big Ladder Software LLC.

This file is part of Elements.

Elements is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

Elements is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Elements.  If not, see <http://www.gnu.org/licenses/>.
*/
package dse {
  import pdt.IPVec;
  import pdt.PersistentVector;

public class DataSetBuilder {
  internal var header:Object;
  internal var points:IPVec;
  internal var seriesMetaData:Object;
  internal var headerMetaData:Object;
  public function DataSetBuilder() {
    this.header = {};
    this.points = PersistentVector.EMPTY;
    this.seriesMetaData = {};
    this.headerMetaData = {};
  }
  public function withHeader(hdr:Object):DataSetBuilder {
    this.header = hdr;
    return this;
  }
  public function withPoints(pts:IPVec):DataSetBuilder {
    this.points = pts;
    return this;
  }
  public function withSeriesMetaData(smd:Object):DataSetBuilder {
    this.seriesMetaData = smd;
    return this;
  }
  public function withHeaderMetaData(hmd:Object):DataSetBuilder {
    this.headerMetaData = hmd;
    return this;
  }
  public function build():DataSet {
    return new DataSet(this);
  }
  public static function fromDataSet(ds:DataSet):DataSetBuilder {
    return (new DataSetBuilder())
      .withHeader(ds.header)
      .withPoints(ds.points)
      .withSeriesMetaData(ds.seriesMetaData)
      .withHeaderMetaData(ds.headerMetaData);
  }
}
}