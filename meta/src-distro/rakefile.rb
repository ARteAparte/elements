require 'rexml/document'

PATH = File.expand_path(File.join("../../src"), File.dirname(__FILE__))
THIS_DIR = File.expand_path(File.dirname(__FILE__))

def get_elements_version
  path = File.join(PATH, "Elements-app.xml")
  raw = File.open(path)
  doc = REXML::Document.new(raw)
  doc.elements["application/versionNumber"].to_a[0]
end

def zip_src
  version = get_elements_version
  sh("pushd #{PATH} &&" +
     "cd .. &&" +
     "zip -r -X elements-#{version}.zip src &&" +
     "mv elements-#{version}.zip #{THIS_DIR} &&" +
     " popd")
end

desc "Zip Elements "
task :default do |t, args|
  zip_src
end
