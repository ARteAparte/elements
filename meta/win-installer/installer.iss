; Created by P. G. Ellis, 5 Oct 2013

#ifndef proper_name
  #define proper_name "Elements"
#endif

#ifndef version_num
  #define version_num "1.0.1"  ; won't accept any non-numeric characters, e.g, "r65"
#endif

#ifndef file_name
  #define file_name "elements"
#endif

#ifndef file_suffix
  #define file_suffix "1.0.1"
#endif

[Setup]
AppName={#proper_name} {#version_num}
AppVerName={#proper_name} {#version_num}
AppVersion={#version_num}
AppPublisher=Big Ladder Software LLC
AppPublisherURL=http://bigladdersoftware.com/
AppSupportURL=http://bigladdersoftware.com/
AppUpdatesURL=http://bigladdersoftware.com/
DefaultDirName={pf}\Elements
DefaultGroupName=Elements {#version_num}
AppendDefaultDirName=no
UsePreviousAppDir=no
DirExistsWarning=no
DisableDirPage=no
Compression=lzma
SolidCompression=yes
SourceDir=files
OutputDir=..
LicenseFile=LICENSE.txt
Uninstallable=yes
UninstallFilesDir={app}
OutputBaseFilename={#file_name}-{#file_suffix}
Encryption=no
Password=

[Messages]
WelcomeLabel2=This will install [name/ver] on your computer.
;SelectDirDesc=Please locate the application directory for your [name] installation.
SelectDirLabel3=Setup will install [name] into the following directory.
ReadyLabel1=Setup is now ready to begin installing [name] on your computer.
PreparingDesc=Setup is preparing to install [name] on your computer.
InstallingLabel=Please wait while Setup installs [name] on your computer.
FinishedLabelNoIcons=Setup has finished installing [name] on your computer.

[Dirs]
;Name: "{app}\textpad-energyplus"; Flags: deleteafterinstall

[Files]
Source: "*"; DestDir: "{app}"; Flags: ignoreversion recursesubdirs createallsubdirs

[Icons]
Name: "{group}\Elements"; Filename: "{app}\Elements.exe"; Comment: "Run Elements {#version_num}"
Name: "{group}\User Guide"; Filename: "{app}\htmldocs\user-guide\index.html"; Comment: "Help/User Guide"; IconFilename: "{app}\elements.ico"
Name: "{group}\Online Resources"; Filename: "http://bigladdersoftware.com/projects/elements/docs/"; IconFilename: "{app}\bigladder.ico"
Name: "{group}\Website"; Filename: "http://bigladdersoftware.com/projects/elements/"; IconFilename: "{app}\bigladder.ico"
Name: "{group}\Uninstall"; Filename: "{uninstallexe}"; Comment: "Uninstall Elements {#version_num}"

[Run]
Filename: "{app}\Elements.exe"; Description: "{cm:LaunchProgram,Elements}"; Flags: postinstall skipifsilent waituntilidle

