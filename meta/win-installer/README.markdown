# Windows Installer Instructions

1. Create a release build using Flash Builder:

Find the command under "Project > Export Release Build"

Set the build location to the "meta/win-installer/build" directory and select "signed application with captive runtime" for the application type.

Base filename should be "Elements"

2. Remember that you may need to adjust the dialogue window size to see the window pane (a Bug in Flash Builder).

3. Copy the contents of the folder "meta/win-installer/files" into the "meta/win-installer/build/Elements" folder.

4. Rename "meta/win-installer/build/Elements" to "meta/win-installer/build/files"

5. copy "meta/win-installer/installer.iss" to "meta/win-installer/build"

6. Click on the installer.iss script -- you should have the "Inno Setup Compiler" installed; if not, install it first here:

http://www.jrsoftware.org/isdl.php

I am using isetup-5.5.5.exe released 2014-07-09

5. Within installer.iss, update the variables version_num and file_suffix as needed to match the actual version of this Elements release.

6. From within Inno Setup, select "Build > Compile"

7. You should now have a Windows installer executable that you can use to test installing Elements on your system and also give to others
