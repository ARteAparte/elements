Welcome to Elements!

To install, drag Elements.app over the Applications folder shortcut. This will
move Elements.app to your /Applications folder. You can then use Spotlight or
Launchpad to start Elements, OR simply browse to the /Applications folder via
Finder and double click the Elements.app.

Note: in recent versions of Mac OS, you may need to right-click on Elements.app
and select open the first time you launch the program to give proper
permissions to use the program.
